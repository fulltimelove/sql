/*
Navicat MySQL Data Transfer

Source Server         : recycle线上
Source Server Version : 50505
Source Host           : 118.89.118.166:14369
Source Database       : newscms

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-08-03 09:14:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nms_404
-- ----------------------------
DROP TABLE IF EXISTS `nms_404`;
CREATE TABLE `nms_404` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `refer` varchar(255) NOT NULL,
  `robot` varchar(20) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='404日志';

-- ----------------------------
-- Records of nms_404
-- ----------------------------

-- ----------------------------
-- Table structure for nms_ad
-- ----------------------------
DROP TABLE IF EXISTS `nms_ad`;
CREATE TABLE `nms_ad` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `typeid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `currency` varchar(20) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `stat` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `note` text NOT NULL,
  `code` text NOT NULL,
  `text_name` varchar(100) NOT NULL DEFAULT '',
  `text_url` varchar(255) NOT NULL DEFAULT '',
  `text_title` varchar(100) NOT NULL DEFAULT '',
  `text_style` varchar(50) NOT NULL DEFAULT '',
  `image_src` varchar(255) NOT NULL DEFAULT '',
  `image_url` varchar(255) NOT NULL DEFAULT '',
  `image_alt` varchar(100) NOT NULL DEFAULT '',
  `flash_src` varchar(255) NOT NULL DEFAULT '',
  `flash_url` varchar(255) NOT NULL DEFAULT '',
  `flash_loop` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `key_moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `key_catid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `key_word` varchar(100) NOT NULL DEFAULT '',
  `key_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='广告';

-- ----------------------------
-- Records of nms_ad
-- ----------------------------
INSERT INTO `nms_ad` VALUES ('1', '网站首页图片轮播1', '14', '5', '0', '0', '', 'http://www.destoon.com/', '', '0', 'fulltimelink', '1588062475', 'fulltimelink', '1588062475', '1262275200', '1577894399', '0', '', '', '', '', '', '', 'file/image/player_1.jpg', 'http://www.destoon.com/', '', '', '', '1', '0', '0', '', '0', '0', '3');
INSERT INTO `nms_ad` VALUES ('2', '网站首页图片轮播2', '14', '5', '0', '0', '', 'http://www.destoon.com/', '', '0', 'fulltimelink', '1588062475', 'fulltimelink', '1588062475', '1262275200', '1577894399', '0', '', '', '', '', '', '', 'file/image/player_2.jpg', 'http://www.destoon.com/', '', '', '', '1', '0', '0', '', '0', '0', '3');
INSERT INTO `nms_ad` VALUES ('3', '首页旗帜A1', '21', '3', '0', '0', '', 'http://www.destoon.com/', '', '0', 'fulltimelink', '1588062475', 'fulltimelink', '1588062475', '1262275200', '1577894399', '0', '', '', '', '', '', '', 'file/image/a1.jpg', '', '', '', '', '1', '0', '0', '', '0', '0', '3');
INSERT INTO `nms_ad` VALUES ('4', '首页旗帜A2', '22', '3', '0', '0', '', 'http://www.destoon.com/', '', '0', 'fulltimelink', '1588062475', 'fulltimelink', '1588062475', '1262275200', '1577894399', '0', '', '', '', '', '', '', 'file/image/a2.jpg', '', '', '', '', '1', '0', '0', '', '0', '0', '3');
INSERT INTO `nms_ad` VALUES ('5', '首页旗帜A3', '23', '3', '0', '0', '', 'http://www.destoon.com/', '', '0', 'fulltimelink', '1588062475', 'fulltimelink', '1588062475', '1262275200', '1577894399', '0', '', '', '', '', '', '', 'file/image/a3.jpg', '', '', '', '', '1', '0', '0', '', '0', '0', '3');
INSERT INTO `nms_ad` VALUES ('6', '首页旗帜A4', '24', '3', '0', '0', '', 'http://www.destoon.com/', '', '0', 'fulltimelink', '1588062475', 'fulltimelink', '1588062475', '1262275200', '1577894399', '0', '', '', '', '', '', '', 'file/image/a4.jpg', '', '', '', '', '1', '0', '0', '', '0', '0', '3');
INSERT INTO `nms_ad` VALUES ('7', '首页旗帜A5', '25', '3', '0', '0', '', 'http://www.destoon.com/', '', '0', 'fulltimelink', '1588062475', 'fulltimelink', '1588062475', '1262275200', '1577894399', '0', '', '', '', '', '', '', 'file/image/a5.jpg', '', '', '', '', '1', '0', '0', '', '0', '0', '3');

-- ----------------------------
-- Table structure for nms_ad_place
-- ----------------------------
DROP TABLE IF EXISTS `nms_ad_place`;
CREATE TABLE `nms_ad_place` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `typeid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `open` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `code` text NOT NULL,
  `width` smallint(5) unsigned NOT NULL DEFAULT '0',
  `height` smallint(5) unsigned NOT NULL DEFAULT '0',
  `price` float unsigned NOT NULL DEFAULT '0',
  `ads` smallint(4) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `template` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='广告位';

-- ----------------------------
-- Records of nms_ad_place
-- ----------------------------
INSERT INTO `nms_ad_place` VALUES ('1', '5', '6', '1', '供应排名', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('2', '6', '6', '1', '求购排名', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('3', '16', '6', '1', '商城排名', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('4', '4', '6', '1', '公司排名', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('14', '0', '5', '1', '首页图片轮播', '', '', '', '', '660', '300', '0', '2', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('15', '5', '7', '1', '供应赞助商链接', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('17', '4', '7', '1', '公司赞助商链接', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('18', '0', '7', '1', '求购赞助商链接', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('19', '8', '7', '1', '展会赞助商链接', '', '', '', '', '0', '0', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('21', '0', '3', '1', '首页旗帜A1', '', '', '', '', '116', '212', '0', '1', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('22', '0', '3', '1', '首页旗帜A2', '', '', '', '', '116', '212', '0', '1', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('23', '0', '3', '1', '首页旗帜A3', '', '', '', '', '116', '212', '0', '1', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('24', '0', '3', '1', '首页旗帜A4', '', '', '', '', '116', '212', '0', '1', '0', '1588062475', 'fulltimelink', '1588062475', '');
INSERT INTO `nms_ad_place` VALUES ('25', '0', '3', '1', '首页旗帜A5', '', '', '', '', '116', '212', '0', '1', '0', '1588062475', 'fulltimelink', '1588062475', '');

-- ----------------------------
-- Table structure for nms_address
-- ----------------------------
DROP TABLE IF EXISTS `nms_address`;
CREATE TABLE `nms_address` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `truename` varchar(30) NOT NULL DEFAULT '',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `address` varchar(255) NOT NULL DEFAULT '',
  `postcode` varchar(10) NOT NULL DEFAULT '',
  `telephone` varchar(30) NOT NULL DEFAULT '',
  `mobile` varchar(30) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='收货地址';

-- ----------------------------
-- Records of nms_address
-- ----------------------------

-- ----------------------------
-- Table structure for nms_admin
-- ----------------------------
DROP TABLE IF EXISTS `nms_admin`;
CREATE TABLE `nms_admin` (
  `adminid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `title` varchar(30) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `moduleid` smallint(6) NOT NULL DEFAULT '0',
  `file` varchar(20) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `catid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`adminid`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='管理员';

-- ----------------------------
-- Records of nms_admin
-- ----------------------------
INSERT INTO `nms_admin` VALUES ('1', '1', '0', '生成首页', '?action=html', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('2', '1', '0', '更新缓存', '?action=cache', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('3', '1', '0', '网站设置', '?file=setting', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('4', '1', '0', '模块管理', '?file=module', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('5', '1', '0', '数据维护', '?file=database', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('6', '1', '0', '模板管理', '?file=template', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('7', '1', '0', '会员管理', '?moduleid=2', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('8', '1', '0', '单页管理', '?moduleid=3&file=webpage', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('9', '1', '0', '排名推广', '?moduleid=3&file=spread', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('10', '1', '0', '广告管理', '?moduleid=3&file=ad', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('13', '2', '0', '', '', '', '23', '', '', '');
INSERT INTO `nms_admin` VALUES ('12', '2', '0', '大象招聘管理', '?moduleid=23', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('14', '3', '0', '', '', '', '23', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('15', '3', '0', '大象招聘管理', '?moduleid=23', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('16', '4', '0', '', '', '', '23', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('17', '4', '0', '大象招聘管理', '?moduleid=23', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('18', '5', '0', '', '', '', '24', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('34', '5', '0', '', '', '', '1', 'category', '', '');
INSERT INTO `nms_admin` VALUES ('20', '5', '0', '', '', '', '25', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('21', '5', '1', '用户管理', '?moduleid=25', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('22', '5', '0', '', '', '', '26', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('23', '5', '2', '订单管理', '?moduleid=26', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('24', '5', '0', '', '', '', '27', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('25', '5', '3', '焦点图管理', '?moduleid=27', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('26', '5', '0', '', '', '', '28', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('27', '5', '4', '使用指南管理', '?moduleid=28', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('28', '5', '0', '', '', '', '29', 'index', '', '');
INSERT INTO `nms_admin` VALUES ('29', '5', '5', '回收价格管理', '?moduleid=29', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('30', '5', '0', '', '', '', '29', 'setting', '', '');
INSERT INTO `nms_admin` VALUES ('32', '5', '0', '', '', '', '26', 'setting', '', '');
INSERT INTO `nms_admin` VALUES ('33', '5', '6', '订单设置', '?moduleid=26&file=setting', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('35', '5', '0', '求购分类', '?mid=24&file=category', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('36', '5', '0', '', '', '', '1', 'patch', '', '');
INSERT INTO `nms_admin` VALUES ('37', '5', '7', '文件维护', '?file=patch', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('38', '5', '0', '', '', '', '1', 'doctor', '', '');
INSERT INTO `nms_admin` VALUES ('39', '5', '8', '系统体检', '?file=doctor', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('40', '5', '0', '', '', '', '1', 'database', '', '');
INSERT INTO `nms_admin` VALUES ('41', '5', '9', '数据维护', '?file=database', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('42', '5', '0', '', '', '', '1', 'upload', '', '');
INSERT INTO `nms_admin` VALUES ('43', '5', '10', '上传记录', '?file=upload', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('44', '5', '0', '', '', '', '1', 'keyword', '', '');
INSERT INTO `nms_admin` VALUES ('45', '5', '11', '搜索记录', '?file=keyword', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('46', '5', '0', '', '', '', '1', 'log', '', '');
INSERT INTO `nms_admin` VALUES ('47', '5', '12', '后台日志', '?file=log', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('48', '5', '0', '', '', '', '1', '404', '', '');
INSERT INTO `nms_admin` VALUES ('49', '5', '13', '404日志', '?file=404', '', '0', '', '', '');
INSERT INTO `nms_admin` VALUES ('50', '5', '0', '求购管理', '?moduleid=24', '', '0', '', '', '');

-- ----------------------------
-- Table structure for nms_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `nms_admin_log`;
CREATE TABLE `nms_admin_log` (
  `logid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qstring` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `logtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`logid`)
) ENGINE=MyISAM AUTO_INCREMENT=3421 DEFAULT CHARSET=utf8 COMMENT='管理日志';

-- ----------------------------
-- Records of nms_admin_log
-- ----------------------------
INSERT INTO `nms_admin_log` VALUES ('2952', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595905159');
INSERT INTO `nms_admin_log` VALUES ('2951', 'rand=55&moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905155');
INSERT INTO `nms_admin_log` VALUES ('2950', 'moduleid=26&file=index&action=delete&itemid=95', 'fulltimelink', '61.163.87.166', '1595905155');
INSERT INTO `nms_admin_log` VALUES ('2949', 'rand=26&moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905153');
INSERT INTO `nms_admin_log` VALUES ('2948', 'moduleid=26&file=index&action=delete&itemid=94', 'fulltimelink', '61.163.87.166', '1595905153');
INSERT INTO `nms_admin_log` VALUES ('2947', 'rand=39&moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905151');
INSERT INTO `nms_admin_log` VALUES ('2946', 'moduleid=26&file=index&action=delete&itemid=65', 'fulltimelink', '61.163.87.166', '1595905151');
INSERT INTO `nms_admin_log` VALUES ('2945', 'rand=26&moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905149');
INSERT INTO `nms_admin_log` VALUES ('2944', 'moduleid=26&file=index&action=delete&itemid=59', 'fulltimelink', '61.163.87.166', '1595905149');
INSERT INTO `nms_admin_log` VALUES ('2943', 'rand=50&moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905146');
INSERT INTO `nms_admin_log` VALUES ('2942', 'moduleid=26&file=index&action=delete&itemid=56', 'fulltimelink', '61.163.87.166', '1595905146');
INSERT INTO `nms_admin_log` VALUES ('2941', 'rand=95&moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905144');
INSERT INTO `nms_admin_log` VALUES ('2940', 'moduleid=26&file=index&action=delete&itemid=55', 'fulltimelink', '61.163.87.166', '1595905144');
INSERT INTO `nms_admin_log` VALUES ('2939', 'rand=29&moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905140');
INSERT INTO `nms_admin_log` VALUES ('2938', 'moduleid=26&file=index&action=delete&itemid=54', 'fulltimelink', '61.163.87.166', '1595905140');
INSERT INTO `nms_admin_log` VALUES ('2936', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595905122');
INSERT INTO `nms_admin_log` VALUES ('2937', 'moduleid=26&action=&fields=1&orderkey=%E8%87%AA%E7%94%B1%E9%A3%9E&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595905133');
INSERT INTO `nms_admin_log` VALUES ('2935', 'moduleid=25&sum=34&page=2', 'fulltimelink', '61.163.87.166', '1595905115');
INSERT INTO `nms_admin_log` VALUES ('2933', 'moduleid=25&sum=34', 'fulltimelink', '61.163.87.166', '1595899597');
INSERT INTO `nms_admin_log` VALUES ('2934', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595905109');
INSERT INTO `nms_admin_log` VALUES ('2932', 'moduleid=25&sum=34&page=2', 'fulltimelink', '61.163.87.166', '1595899593');
INSERT INTO `nms_admin_log` VALUES ('2929', 'moduleid=25&sum=34&page=2', 'fulltimelink', '61.163.87.166', '1595899492');
INSERT INTO `nms_admin_log` VALUES ('2930', 'moduleid=25&sum=34', 'fulltimelink', '61.163.87.166', '1595899500');
INSERT INTO `nms_admin_log` VALUES ('2931', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595899590');
INSERT INTO `nms_admin_log` VALUES ('2928', 'moduleid=25&sum=34', 'fulltimelink', '61.163.87.166', '1595899468');
INSERT INTO `nms_admin_log` VALUES ('3157', 'file=category&mid=30&action=letter&catname=%E7%AC%94%E8%AE%B0%E6%9C%AC%E7%94%B5%E8%84%91%E5%B9%B3%E6%9D%BF%E7%94%B5%E8%84%91%E6%89%8B%E6%9C%BA%E6%98%BE%E7%A4%BA%E5%B1%8F', 'fulltimelink', '61.163.87.166', '1596191874');
INSERT INTO `nms_admin_log` VALUES ('3156', 'file=category&mid=30&action=letter&catname=%E7%AC%94%E8%AE%B0%E6%9C%AC%E7%94%B5%E8%84%91%E5%B9%B3%E6%9D%BF%E7%94%B5%E8%84%91%E6%89%8B%E6%9C%BA', 'fulltimelink', '61.163.87.166', '1596191870');
INSERT INTO `nms_admin_log` VALUES ('3155', 'file=category&mid=30&action=letter&catname=%E7%AC%94%E8%AE%B0%E6%9C%AC%E7%94%B5%E8%84%91%E5%B9%B3%E6%9D%BF%E7%94%B5%E8%84%91', 'fulltimelink', '61.163.87.166', '1596191866');
INSERT INTO `nms_admin_log` VALUES ('3154', 'file=category&mid=30&action=letter&catname=%E7%AC%94%E8%AE%B0%E6%9C%AC%E7%94%B5%E8%84%91', 'fulltimelink', '61.163.87.166', '1596191860');
INSERT INTO `nms_admin_log` VALUES ('3153', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191852');
INSERT INTO `nms_admin_log` VALUES ('3152', 'mid=30&file=category&action=add&parentid=30', 'fulltimelink', '61.163.87.166', '1596191799');
INSERT INTO `nms_admin_log` VALUES ('3151', 'rand=43&file=category&mid=30&parentid=29', 'fulltimelink', '61.163.87.166', '1596191796');
INSERT INTO `nms_admin_log` VALUES ('3150', 'file=category&mid=30&action=letter&catname=%E9%85%92%E7%93%B6%E9%86%8B%E7%93%B6%E7%83%9F%E7%81%B0%E7%BC%B8%E7%8E%BB%E7%92%83%E8%8A%B1%E7%93%B6', 'fulltimelink', '61.163.87.166', '1596191790');
INSERT INTO `nms_admin_log` VALUES ('3148', 'file=category&mid=30&action=letter&catname=%E9%85%92%E7%93%B6', 'fulltimelink', '61.163.87.166', '1596191774');
INSERT INTO `nms_admin_log` VALUES ('3149', 'file=category&mid=30&action=letter&catname=%E9%85%92%E7%93%B6%E9%86%8B%E7%93%B6%E7%83%9F%E7%81%B0%E7%BC%B8', 'fulltimelink', '61.163.87.166', '1596191783');
INSERT INTO `nms_admin_log` VALUES ('3147', 'mid=30&file=category&action=add&parentid=29', 'fulltimelink', '61.163.87.166', '1596191762');
INSERT INTO `nms_admin_log` VALUES ('3146', 'rand=40&file=category&mid=30&parentid=28', 'fulltimelink', '61.163.87.166', '1596191757');
INSERT INTO `nms_admin_log` VALUES ('3145', 'file=category&mid=30&action=letter&catname=%E9%87%91%E5%B1%9E%E6%98%93%E6%8B%89%E7%BD%90%E9%87%91%E5%B1%9E%E9%94%85%E8%9E%BA%E4%B8%9D%E5%A5%B6%E7%B2%89%E7%BD%90', 'fulltimelink', '61.163.87.166', '1596191753');
INSERT INTO `nms_admin_log` VALUES ('3144', 'file=category&mid=30&action=letter&catname=%E9%87%91%E5%B1%9E%E6%98%93%E6%8B%89%E7%BD%90%E9%87%91%E5%B1%9E%E9%94%85%E8%9E%BA%E4%B8%9D', 'fulltimelink', '61.163.87.166', '1596191746');
INSERT INTO `nms_admin_log` VALUES ('3143', 'file=category&mid=30&action=letter&catname=%E9%87%91%E5%B1%9E%E6%98%93%E6%8B%89%E7%BD%90%E9%87%91%E5%B1%9E%E9%94%85', 'fulltimelink', '61.163.87.166', '1596191740');
INSERT INTO `nms_admin_log` VALUES ('3142', 'file=category&mid=30&action=letter&catname=%E9%87%91%E5%B1%9E%E6%98%93%E6%8B%89%E7%BD%90', 'fulltimelink', '61.163.87.166', '1596191731');
INSERT INTO `nms_admin_log` VALUES ('3141', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191723');
INSERT INTO `nms_admin_log` VALUES ('3140', 'mid=30&file=category&action=add&parentid=28', 'fulltimelink', '61.163.87.166', '1596191719');
INSERT INTO `nms_admin_log` VALUES ('3139', 'rand=65&file=category&mid=30&parentid=27', 'fulltimelink', '61.163.87.166', '1596191712');
INSERT INTO `nms_admin_log` VALUES ('3138', 'file=category&mid=30&action=letter&catname=%E7%BA%B8%E7%AE%B1%E5%BF%AB%E9%80%92%E5%8C%85%E8%A3%85%E6%B3%A1%E6%B2%AB%E7%AE%B1%E7%A4%BC%E7%9B%92%E5%88%A9%E4%B9%90%E5%8C%85%E8%A3%85', 'fulltimelink', '61.163.87.166', '1596191686');
INSERT INTO `nms_admin_log` VALUES ('3137', 'file=category&mid=30&action=letter&catname=%E7%BA%B8%E7%AE%B1%E5%BF%AB%E9%80%92%E5%8C%85%E8%A3%85%E6%B3%A1%E6%B2%AB%E7%AE%B1%E7%A4%BC%E7%9B%92', 'fulltimelink', '61.163.87.166', '1596191665');
INSERT INTO `nms_admin_log` VALUES ('3135', 'rand=44&file=category&mid=30&parentid=26', 'fulltimelink', '61.163.87.166', '1596191628');
INSERT INTO `nms_admin_log` VALUES ('3136', 'mid=30&file=category&action=add&parentid=27', 'fulltimelink', '61.163.87.166', '1596191633');
INSERT INTO `nms_admin_log` VALUES ('3134', 'file=category&mid=30&action=letter&catname=%E5%90%84%E7%B1%BB%E8%A1%A3%E7%89%A9%E6%A3%89%E8%A2%AB%E7%9A%AE%E5%8C%85%E5%90%84%E7%B1%BB%E9%9E%8B', 'fulltimelink', '61.163.87.166', '1596191623');
INSERT INTO `nms_admin_log` VALUES ('3133', 'file=category&mid=30&action=letter&catname=%E5%90%84%E7%B1%BB%E8%A1%A3%E7%89%A9%E6%A3%89%E8%A2%AB%E7%9A%AE%E5%8C%85', 'fulltimelink', '61.163.87.166', '1596191614');
INSERT INTO `nms_admin_log` VALUES ('3131', 'file=category&mid=30&action=letter&catname=%E5%90%84%E7%B1%BB%E8%A1%A3%E7%89%A9', 'fulltimelink', '61.163.87.166', '1596191606');
INSERT INTO `nms_admin_log` VALUES ('3132', 'file=category&mid=30&action=letter&catname=%E5%90%84%E7%B1%BB%E8%A1%A3%E7%89%A9%E6%A3%89%E8%A2%AB', 'fulltimelink', '61.163.87.166', '1596191611');
INSERT INTO `nms_admin_log` VALUES ('3130', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191595');
INSERT INTO `nms_admin_log` VALUES ('3129', 'mid=30&file=category&action=add&parentid=26', 'fulltimelink', '61.163.87.166', '1596191593');
INSERT INTO `nms_admin_log` VALUES ('3128', 'rand=74&file=category&mid=30&parentid=25', 'fulltimelink', '61.163.87.166', '1596191582');
INSERT INTO `nms_admin_log` VALUES ('2926', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595899429');
INSERT INTO `nms_admin_log` VALUES ('2927', 'moduleid=25&sum=34&page=2', 'fulltimelink', '61.163.87.166', '1595899465');
INSERT INTO `nms_admin_log` VALUES ('2925', 'rand=52&moduleid=27', 'fulltimelink', '61.163.87.166', '1595899325');
INSERT INTO `nms_admin_log` VALUES ('2924', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595899306');
INSERT INTO `nms_admin_log` VALUES ('2923', 'rand=93&moduleid=27', 'fulltimelink', '61.163.87.166', '1595899229');
INSERT INTO `nms_admin_log` VALUES ('2922', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595899195');
INSERT INTO `nms_admin_log` VALUES ('2921', 'rand=40&moduleid=27', 'fulltimelink', '61.163.87.166', '1595899193');
INSERT INTO `nms_admin_log` VALUES ('2920', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595899146');
INSERT INTO `nms_admin_log` VALUES ('2919', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595899026');
INSERT INTO `nms_admin_log` VALUES ('2918', 'file=category&mid=27', 'fulltimelink', '61.163.87.166', '1595899022');
INSERT INTO `nms_admin_log` VALUES ('2917', 'rand=18&moduleid=27', 'fulltimelink', '61.163.87.166', '1595898770');
INSERT INTO `nms_admin_log` VALUES ('2916', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595898752');
INSERT INTO `nms_admin_log` VALUES ('2915', 'rand=84&moduleid=27', 'fulltimelink', '61.163.87.166', '1595898751');
INSERT INTO `nms_admin_log` VALUES ('2914', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595898725');
INSERT INTO `nms_admin_log` VALUES ('2913', 'rand=56&moduleid=27', 'fulltimelink', '61.163.87.166', '1595898541');
INSERT INTO `nms_admin_log` VALUES ('2912', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595898515');
INSERT INTO `nms_admin_log` VALUES ('2911', 'rand=37&moduleid=27', 'fulltimelink', '61.163.87.166', '1595898500');
INSERT INTO `nms_admin_log` VALUES ('2886', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595562197');
INSERT INTO `nms_admin_log` VALUES ('2885', 'moduleid=26&file=index&action=edit&itemid=98', 'fulltimelink', '61.163.87.166', '1595561654');
INSERT INTO `nms_admin_log` VALUES ('2883', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595558790');
INSERT INTO `nms_admin_log` VALUES ('2884', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595561354');
INSERT INTO `nms_admin_log` VALUES ('2882', 'file=fields&tb=nms_article_26&widget=1', 'fulltimelink', '61.163.87.166', '1595558785');
INSERT INTO `nms_admin_log` VALUES ('2881', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595558784');
INSERT INTO `nms_admin_log` VALUES ('2880', 'rand=34&moduleid=1&file=fields&tb=article_26', 'fulltimelink', '61.163.87.166', '1595558756');
INSERT INTO `nms_admin_log` VALUES ('2874', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595477034');
INSERT INTO `nms_admin_log` VALUES ('2873', 'moduleid=26&file=index&action=edit&itemid=95', 'fulltimelink', '61.163.87.166', '1595476925');
INSERT INTO `nms_admin_log` VALUES ('2872', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595476919');
INSERT INTO `nms_admin_log` VALUES ('2870', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595476860');
INSERT INTO `nms_admin_log` VALUES ('2871', 'moduleid=26&file=index&action=edit&itemid=96', 'fulltimelink', '61.163.87.166', '1595476871');
INSERT INTO `nms_admin_log` VALUES ('2869', 'moduleid=26&file=index&action=edit&itemid=96', 'fulltimelink', '61.163.87.166', '1595476851');
INSERT INTO `nms_admin_log` VALUES ('2868', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595476234');
INSERT INTO `nms_admin_log` VALUES ('2867', 'rand=22&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595475924');
INSERT INTO `nms_admin_log` VALUES ('2866', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595475830');
INSERT INTO `nms_admin_log` VALUES ('2865', 'rand=77&moduleid=1&file=fields&tb=article_26', 'fulltimelink', '61.163.87.166', '1595475708');
INSERT INTO `nms_admin_log` VALUES ('2864', 'rand=45&moduleid=1&file=fields&tb=article_26', 'fulltimelink', '61.163.87.166', '1595475690');
INSERT INTO `nms_admin_log` VALUES ('2863', 'file=fields&tb=article_26&action=add', 'fulltimelink', '61.163.87.166', '1595475639');
INSERT INTO `nms_admin_log` VALUES ('2862', 'file=fields&tb=nms_article_26&widget=1', 'fulltimelink', '61.163.87.166', '1595475635');
INSERT INTO `nms_admin_log` VALUES ('2861', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595475631');
INSERT INTO `nms_admin_log` VALUES ('2860', 'rand=72&moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1595474988');
INSERT INTO `nms_admin_log` VALUES ('2858', 'moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1595474970');
INSERT INTO `nms_admin_log` VALUES ('2859', 'moduleid=26&file=index&action=edit&itemid=65', 'fulltimelink', '61.163.87.166', '1595474972');
INSERT INTO `nms_admin_log` VALUES ('2857', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595474964');
INSERT INTO `nms_admin_log` VALUES ('2856', 'moduleid=26&file=index&action=edit&itemid=65', 'fulltimelink', '61.163.87.166', '1595474955');
INSERT INTO `nms_admin_log` VALUES ('2855', 'moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1595474948');
INSERT INTO `nms_admin_log` VALUES ('2854', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595474929');
INSERT INTO `nms_admin_log` VALUES ('2853', 'moduleid=26&file=index&action=edit&itemid=59', 'fulltimelink', '61.163.87.166', '1595474896');
INSERT INTO `nms_admin_log` VALUES ('2850', 'moduleid=26&file=index&action=delete&itemid=92', 'fulltimelink', '61.163.87.166', '1595474846');
INSERT INTO `nms_admin_log` VALUES ('2851', 'rand=67&moduleid=26', 'fulltimelink', '61.163.87.166', '1595474846');
INSERT INTO `nms_admin_log` VALUES ('2852', 'rand=67&moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1595474874');
INSERT INTO `nms_admin_log` VALUES ('3197', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81', 'fulltimelink', '61.163.87.166', '1596192122');
INSERT INTO `nms_admin_log` VALUES ('3196', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0', 'fulltimelink', '61.163.87.166', '1596192118');
INSERT INTO `nms_admin_log` VALUES ('3195', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81', 'fulltimelink', '61.163.87.166', '1596192115');
INSERT INTO `nms_admin_log` VALUES ('3194', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596192104');
INSERT INTO `nms_admin_log` VALUES ('3193', 'mid=30&file=category&action=add&parentid=85', 'fulltimelink', '61.163.87.166', '1596192102');
INSERT INTO `nms_admin_log` VALUES ('3192', 'rand=26&file=category&mid=30&parentid=22', 'fulltimelink', '61.163.87.166', '1596192099');
INSERT INTO `nms_admin_log` VALUES ('3191', 'file=category&mid=30&action=letter&catname=%E6%9C%89%E5%AE%B3%E5%9E%83%E5%9C%BE%EF%BC%88%E6%97%A0%E5%81%BF%E5%9B%9E%E6%94%B6%EF%BC%89', 'fulltimelink', '61.163.87.166', '1596192099');
INSERT INTO `nms_admin_log` VALUES ('3190', 'file=category&mid=30&action=letter&catname=%E6%9C%89%E5%AE%B3%E5%9E%83%E5%9C%BE%EF%BC%88%EF%BC%89', 'fulltimelink', '61.163.87.166', '1596192093');
INSERT INTO `nms_admin_log` VALUES ('3189', 'mid=30&file=category&action=add&parentid=22', 'fulltimelink', '61.163.87.166', '1596192070');
INSERT INTO `nms_admin_log` VALUES ('3188', 'rand=16&file=category&mid=30&parentid=79', 'fulltimelink', '61.163.87.166', '1596192063');
INSERT INTO `nms_admin_log` VALUES ('3187', 'file=category&mid=30&action=letter&catname=%E9%AA%A8%E9%AA%BC%E5%86%85%E8%84%8F%E7%93%9C%E6%9E%9C%E6%9E%9C%E7%9A%AE%E8%8F%9C%E6%A2%97%E8%8F%9C%E5%8F%B6%E5%89%A9%E9%A5%AD%E5%89%A9%E8%8F%9C%E6%A0%91%E6%9E%9D%E8%90%BD%E5%8F%B6', 'fulltimelink', '61.163.87.166', '1596192059');
INSERT INTO `nms_admin_log` VALUES ('3186', 'file=category&mid=30&action=letter&catname=%E9%AA%A8%E9%AA%BC%E5%86%85%E8%84%8F%E7%93%9C%E6%9E%9C%E6%9E%9C%E7%9A%AE%E8%8F%9C%E6%A2%97%E8%8F%9C%E5%8F%B6%E5%89%A9%E9%A5%AD%E5%89%A9%E8%8F%9C', 'fulltimelink', '61.163.87.166', '1596192049');
INSERT INTO `nms_admin_log` VALUES ('3185', 'file=category&mid=30&action=letter&catname=%E9%AA%A8%E9%AA%BC%E5%86%85%E8%84%8F%E7%93%9C%E6%9E%9C%E6%9E%9C%E7%9A%AE%E8%8F%9C%E6%A2%97%E8%8F%9C%E5%8F%B6', 'fulltimelink', '61.163.87.166', '1596192041');
INSERT INTO `nms_admin_log` VALUES ('3184', 'file=category&mid=30&action=letter&catname=%E9%AA%A8%E9%AA%BC%E5%86%85%E8%84%8F%E7%93%9C%E6%9E%9C%E6%9E%9C%E7%9A%AE', 'fulltimelink', '61.163.87.166', '1596192032');
INSERT INTO `nms_admin_log` VALUES ('3183', 'file=category&mid=30&action=letter&catname=%E9%AA%A8%E9%AA%BC%E5%86%85%E8%84%8F', 'fulltimelink', '61.163.87.166', '1596192025');
INSERT INTO `nms_admin_log` VALUES ('3182', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596192019');
INSERT INTO `nms_admin_log` VALUES ('3181', 'mid=30&file=category&action=add&parentid=79', 'fulltimelink', '61.163.87.166', '1596192018');
INSERT INTO `nms_admin_log` VALUES ('3180', 'rand=11&file=category&mid=30&parentid=21', 'fulltimelink', '61.163.87.166', '1596192011');
INSERT INTO `nms_admin_log` VALUES ('3179', 'file=category&mid=30&action=letter&catname=%E5%8E%A8%E4%BD%99%E5%9E%83%E5%9C%BE', 'fulltimelink', '61.163.87.166', '1596192011');
INSERT INTO `nms_admin_log` VALUES ('3178', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191983');
INSERT INTO `nms_admin_log` VALUES ('3177', 'mid=30&file=category&action=add&parentid=21', 'fulltimelink', '61.163.87.166', '1596191982');
INSERT INTO `nms_admin_log` VALUES ('3176', 'rand=42&file=category&mid=30&parentid=33', 'fulltimelink', '61.163.87.166', '1596191976');
INSERT INTO `nms_admin_log` VALUES ('3175', 'file=category&mid=30&action=letter&catname=%E7%94%B5%E5%8A%A8%E8%BD%A6%E5%84%BF%E7%AB%A5%E8%BD%A6%E8%87%AA%E8%A1%8C%E8%BD%A6%E6%91%A9%E6%89%98%E8%BD%A6', 'fulltimelink', '61.163.87.166', '1596191975');
INSERT INTO `nms_admin_log` VALUES ('3127', 'file=category&mid=30&action=letter&catname=%E6%8A%A5%E7%BA%B8%E5%AE%A3%E4%BC%A0%E5%8D%95%E6%9D%82%E5%BF%97%E4%B9%A6%E7%B1%8D', 'fulltimelink', '61.163.87.166', '1596191577');
INSERT INTO `nms_admin_log` VALUES ('3124', 'rand=47&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596191541');
INSERT INTO `nms_admin_log` VALUES ('3125', 'mid=30&file=category&action=add&parentid=25', 'fulltimelink', '61.163.87.166', '1596191546');
INSERT INTO `nms_admin_log` VALUES ('3126', 'file=category&mid=30&action=letter&catname=%E6%8A%A5%E7%BA%B8%E5%AE%A3%E4%BC%A0%E5%8D%95', 'fulltimelink', '61.163.87.166', '1596191555');
INSERT INTO `nms_admin_log` VALUES ('3174', 'file=category&mid=30&action=letter&catname=%E7%94%B5%E5%8A%A8%E8%BD%A6%E5%84%BF%E7%AB%A5%E8%BD%A6%E8%87%AA%E8%A1%8C%E8%BD%A6', 'fulltimelink', '61.163.87.166', '1596191970');
INSERT INTO `nms_admin_log` VALUES ('3173', 'file=category&mid=30&action=letter&catname=%E7%94%B5%E5%8A%A8%E8%BD%A6%E5%84%BF%E7%AB%A5%E8%BD%A6', 'fulltimelink', '61.163.87.166', '1596191962');
INSERT INTO `nms_admin_log` VALUES ('3172', 'file=category&mid=30&action=letter&catname=%E7%94%B5%E5%8A%A8%E8%BD%A6', 'fulltimelink', '61.163.87.166', '1596191958');
INSERT INTO `nms_admin_log` VALUES ('2346', 'mid=24&file=category', 'recycle', '61.163.87.166', '1593921631');
INSERT INTO `nms_admin_log` VALUES ('2345', 'file=mymenu', 'recycle', '61.163.87.166', '1593921627');
INSERT INTO `nms_admin_log` VALUES ('2343', 'file=admin&action=right&userid=5&widget=1', 'fulltimelink', '61.163.87.166', '1593921572');
INSERT INTO `nms_admin_log` VALUES ('2344', 'rand=96&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593921611');
INSERT INTO `nms_admin_log` VALUES ('2342', 'file=admin', 'fulltimelink', '61.163.87.166', '1593921556');
INSERT INTO `nms_admin_log` VALUES ('2083', 'moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1593830239');
INSERT INTO `nms_admin_log` VALUES ('2082', 'moduleid=3&file=link', 'fulltimelink', '61.163.87.166', '1593830232');
INSERT INTO `nms_admin_log` VALUES ('2080', 'moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1593830230');
INSERT INTO `nms_admin_log` VALUES ('2081', 'moduleid=3&file=comment', 'fulltimelink', '61.163.87.166', '1593830231');
INSERT INTO `nms_admin_log` VALUES ('2079', 'moduleid=3&file=announce', 'fulltimelink', '61.163.87.166', '1593830229');
INSERT INTO `nms_admin_log` VALUES ('2078', 'moduleid=3&file=ad', 'fulltimelink', '61.163.87.166', '1593830229');
INSERT INTO `nms_admin_log` VALUES ('2076', 'moduleid=3&file=ad', 'fulltimelink', '61.163.87.166', '1593830228');
INSERT INTO `nms_admin_log` VALUES ('2077', 'moduleid=3&file=spread', 'fulltimelink', '61.163.87.166', '1593830229');
INSERT INTO `nms_admin_log` VALUES ('2075', 'moduleid=3&file=announce', 'fulltimelink', '61.163.87.166', '1593830226');
INSERT INTO `nms_admin_log` VALUES ('2074', 'moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1593830225');
INSERT INTO `nms_admin_log` VALUES ('2073', 'moduleid=3&file=link', 'fulltimelink', '61.163.87.166', '1593830225');
INSERT INTO `nms_admin_log` VALUES ('2060', 'moduleid=26&action=successorder', 'recycle', '61.163.87.166', '1593830175');
INSERT INTO `nms_admin_log` VALUES ('2061', 'moduleid=26&action=alreadyorder', 'recycle', '61.163.87.166', '1593830175');
INSERT INTO `nms_admin_log` VALUES ('2062', 'moduleid=26&action=waitorder', 'recycle', '61.163.87.166', '1593830176');
INSERT INTO `nms_admin_log` VALUES ('2063', 'moduleid=26&file=setting', 'recycle', '61.163.87.166', '1593830177');
INSERT INTO `nms_admin_log` VALUES ('2064', 'moduleid=29', 'recycle', '61.163.87.166', '1593830183');
INSERT INTO `nms_admin_log` VALUES ('2065', 'moduleid=28', 'recycle', '61.163.87.166', '1593830185');
INSERT INTO `nms_admin_log` VALUES ('2066', 'moduleid=27', 'recycle', '61.163.87.166', '1593830190');
INSERT INTO `nms_admin_log` VALUES ('2067', 'moduleid=26', 'recycle', '61.163.87.166', '1593830191');
INSERT INTO `nms_admin_log` VALUES ('2068', 'moduleid=25', 'recycle', '61.163.87.166', '1593830193');
INSERT INTO `nms_admin_log` VALUES ('2069', 'mid=24&file=category', 'recycle', '61.163.87.166', '1593830194');
INSERT INTO `nms_admin_log` VALUES ('2070', 'moduleid=3&file=ad', 'fulltimelink', '61.163.87.166', '1593830221');
INSERT INTO `nms_admin_log` VALUES ('2071', 'moduleid=3&file=link', 'fulltimelink', '61.163.87.166', '1593830224');
INSERT INTO `nms_admin_log` VALUES ('2072', 'moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1593830224');
INSERT INTO `nms_admin_log` VALUES ('3348', 'file=category&action=edit&mid=30&catid=77', 'fulltimelink', '61.163.87.166', '1596335790');
INSERT INTO `nms_admin_log` VALUES ('3346', 'file=category&action=edit&mid=30&catid=76', 'fulltimelink', '61.163.87.166', '1596335774');
INSERT INTO `nms_admin_log` VALUES ('3347', 'rand=48&file=category&mid=30&parentid=33', 'fulltimelink', '61.163.87.166', '1596335787');
INSERT INTO `nms_admin_log` VALUES ('3345', 'rand=66&file=category&mid=30&parentid=33', 'fulltimelink', '61.163.87.166', '1596335769');
INSERT INTO `nms_admin_log` VALUES ('3344', 'file=category&action=edit&mid=30&catid=75', 'fulltimelink', '61.163.87.166', '1596335749');
INSERT INTO `nms_admin_log` VALUES ('3342', 'rand=25&file=category&mid=30&parentid=32', 'fulltimelink', '61.163.87.166', '1596335741');
INSERT INTO `nms_admin_log` VALUES ('3343', 'mid=30&file=category&parentid=33', 'fulltimelink', '61.163.87.166', '1596335747');
INSERT INTO `nms_admin_log` VALUES ('3340', 'rand=47&file=category&mid=30&parentid=32', 'fulltimelink', '61.163.87.166', '1596335725');
INSERT INTO `nms_admin_log` VALUES ('3341', 'file=category&action=edit&mid=30&catid=74', 'fulltimelink', '61.163.87.166', '1596335727');
INSERT INTO `nms_admin_log` VALUES ('3337', 'file=category&action=edit&mid=30&catid=72', 'fulltimelink', '61.163.87.166', '1596335697');
INSERT INTO `nms_admin_log` VALUES ('3339', 'file=category&action=edit&mid=30&catid=73', 'fulltimelink', '61.163.87.166', '1596335713');
INSERT INTO `nms_admin_log` VALUES ('3338', 'rand=62&file=category&mid=30&parentid=32', 'fulltimelink', '61.163.87.166', '1596335710');
INSERT INTO `nms_admin_log` VALUES ('3335', 'file=category&action=edit&mid=30&catid=71', 'fulltimelink', '61.163.87.166', '1596335678');
INSERT INTO `nms_admin_log` VALUES ('3336', 'rand=47&file=category&mid=30&parentid=32', 'fulltimelink', '61.163.87.166', '1596335694');
INSERT INTO `nms_admin_log` VALUES ('3333', 'rand=17&file=category&mid=30&parentid=31', 'fulltimelink', '61.163.87.166', '1596335628');
INSERT INTO `nms_admin_log` VALUES ('3334', 'mid=30&file=category&parentid=32', 'fulltimelink', '61.163.87.166', '1596335633');
INSERT INTO `nms_admin_log` VALUES ('3332', 'file=category&action=edit&mid=30&catid=70', 'fulltimelink', '61.163.87.166', '1596335619');
INSERT INTO `nms_admin_log` VALUES ('3331', 'rand=76&file=category&mid=30&parentid=31', 'fulltimelink', '61.163.87.166', '1596335616');
INSERT INTO `nms_admin_log` VALUES ('3330', 'file=category&action=edit&mid=30&catid=69', 'fulltimelink', '61.163.87.166', '1596335603');
INSERT INTO `nms_admin_log` VALUES ('3329', 'rand=20&file=category&mid=30&parentid=31', 'fulltimelink', '61.163.87.166', '1596335599');
INSERT INTO `nms_admin_log` VALUES ('3327', 'rand=71&file=category&mid=30&parentid=31', 'fulltimelink', '61.163.87.166', '1596335585');
INSERT INTO `nms_admin_log` VALUES ('3328', 'file=category&action=edit&mid=30&catid=68', 'fulltimelink', '61.163.87.166', '1596335589');
INSERT INTO `nms_admin_log` VALUES ('3326', 'file=category&action=edit&mid=30&catid=67', 'fulltimelink', '61.163.87.166', '1596335572');
INSERT INTO `nms_admin_log` VALUES ('3325', 'mid=30&file=category&parentid=31', 'fulltimelink', '61.163.87.166', '1596335569');
INSERT INTO `nms_admin_log` VALUES ('3323', 'file=category&action=edit&mid=30&catid=66', 'fulltimelink', '61.163.87.166', '1596335554');
INSERT INTO `nms_admin_log` VALUES ('3322', 'rand=56&file=category&mid=30&parentid=30', 'fulltimelink', '61.163.87.166', '1596335551');
INSERT INTO `nms_admin_log` VALUES ('3321', 'file=category&action=edit&mid=30&catid=65', 'fulltimelink', '61.163.87.166', '1596335541');
INSERT INTO `nms_admin_log` VALUES ('3319', 'file=category&action=edit&mid=30&catid=64', 'fulltimelink', '61.163.87.166', '1596335519');
INSERT INTO `nms_admin_log` VALUES ('3320', 'rand=35&file=category&mid=30&parentid=30', 'fulltimelink', '61.163.87.166', '1596335538');
INSERT INTO `nms_admin_log` VALUES ('3317', 'file=category&action=edit&mid=30&catid=63', 'fulltimelink', '61.163.87.166', '1596335493');
INSERT INTO `nms_admin_log` VALUES ('3318', 'rand=20&file=category&mid=30&parentid=30', 'fulltimelink', '61.163.87.166', '1596335514');
INSERT INTO `nms_admin_log` VALUES ('3123', 'file=category&mid=30&action=letter&catname=%E5%A1%91%E6%96%99%E8%A2%8B%E9%A5%AE%E6%96%99%E7%93%B6%E5%A1%91%E6%96%99%E7%8E%A9%E5%85%B7%E6%89%8B%E6%9C%BA%E5%A3%B3%E5%A1%91%E6%96%99%E5%87%B3%E5%A1%91%E6%96%99%E8%84%B8%E7%9B%86%E5%A1%91%E6%96%99%E6%9D%AF%E4%B', 'fulltimelink', '61.163.87.166', '1596191536');
INSERT INTO `nms_admin_log` VALUES ('2058', 'moduleid=26', 'recycle', '61.163.87.166', '1593830169');
INSERT INTO `nms_admin_log` VALUES ('2059', 'moduleid=26&action=revieworder', 'recycle', '61.163.87.166', '1593830174');
INSERT INTO `nms_admin_log` VALUES ('2056', 'moduleid=29', 'recycle', '61.163.87.166', '1593830166');
INSERT INTO `nms_admin_log` VALUES ('2057', 'moduleid=28', 'recycle', '61.163.87.166', '1593830167');
INSERT INTO `nms_admin_log` VALUES ('2054', 'moduleid=26', 'recycle', '61.163.87.166', '1593830141');
INSERT INTO `nms_admin_log` VALUES ('2055', 'moduleid=26&file=setting', 'recycle', '61.163.87.166', '1593830165');
INSERT INTO `nms_admin_log` VALUES ('2053', 'moduleid=25', 'recycle', '61.163.87.166', '1593830140');
INSERT INTO `nms_admin_log` VALUES ('2052', 'mid=24&file=category', 'recycle', '61.163.87.166', '1593830139');
INSERT INTO `nms_admin_log` VALUES ('2051', 'file=mymenu', 'recycle', '61.163.87.166', '1593830138');
INSERT INTO `nms_admin_log` VALUES ('2050', 'file=fields&tb=nms_article_25&widget=1', 'fulltimelink', '61.163.87.166', '1593829226');
INSERT INTO `nms_admin_log` VALUES ('2049', 'moduleid=25&file=setting', 'fulltimelink', '61.163.87.166', '1593829226');
INSERT INTO `nms_admin_log` VALUES ('3171', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191953');
INSERT INTO `nms_admin_log` VALUES ('3169', 'rand=36&file=category&mid=30&parentid=32', 'fulltimelink', '61.163.87.166', '1596191949');
INSERT INTO `nms_admin_log` VALUES ('3170', 'mid=30&file=category&action=add&parentid=33', 'fulltimelink', '61.163.87.166', '1596191952');
INSERT INTO `nms_admin_log` VALUES ('3168', 'file=category&mid=30&action=letter&catname=%E5%86%B0%E7%AE%B1%E7%A9%BA%E8%B0%83%E6%B4%97%E8%A1%A3%E6%9C%BA%E7%94%B5%E8%A7%86%E6%9C%BA', 'fulltimelink', '61.163.87.166', '1596191947');
INSERT INTO `nms_admin_log` VALUES ('3167', 'file=category&mid=30&action=letter&catname=%E5%86%B0%E7%AE%B1%E7%A9%BA%E8%B0%83%E6%B4%97%E8%A1%A3%E6%9C%BA', 'fulltimelink', '61.163.87.166', '1596191942');
INSERT INTO `nms_admin_log` VALUES ('3166', 'file=category&mid=30&action=letter&catname=%E5%86%B0%E7%AE%B1%E7%A9%BA%E8%B0%83', 'fulltimelink', '61.163.87.166', '1596191927');
INSERT INTO `nms_admin_log` VALUES ('3164', 'mid=30&file=category&action=add&parentid=32', 'fulltimelink', '61.163.87.166', '1596191918');
INSERT INTO `nms_admin_log` VALUES ('3165', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191919');
INSERT INTO `nms_admin_log` VALUES ('3163', 'rand=72&file=category&mid=30&parentid=31', 'fulltimelink', '61.163.87.166', '1596191913');
INSERT INTO `nms_admin_log` VALUES ('3162', 'file=category&mid=30&action=letter&catname=%E7%94%B5%E9%A3%8E%E6%89%87%E6%8F%92%E7%BA%BF%E6%9D%BF%E7%94%B5%E7%A3%81%E7%82%89%E6%94%B6%E9%9F%B3%E6%9C%BA', 'fulltimelink', '61.163.87.166', '1596191913');
INSERT INTO `nms_admin_log` VALUES ('3161', 'file=category&mid=30&action=letter&catname=%E7%94%B5%E9%A3%8E%E6%89%87%E6%8F%92%E7%BA%BF%E6%9D%BF%E7%94%B5%E7%A3%81%E7%82%89', 'fulltimelink', '61.163.87.166', '1596191906');
INSERT INTO `nms_admin_log` VALUES ('3160', 'file=category&mid=30&action=letter&catname=%E7%94%B5%E9%A3%8E%E6%89%87%E6%8F%92%E7%BA%BF%E6%9D%BF', 'fulltimelink', '61.163.87.166', '1596191893');
INSERT INTO `nms_admin_log` VALUES ('3159', 'mid=30&file=category&action=add&parentid=31', 'fulltimelink', '61.163.87.166', '1596191882');
INSERT INTO `nms_admin_log` VALUES ('3158', 'rand=54&file=category&mid=30&parentid=30', 'fulltimelink', '61.163.87.166', '1596191877');
INSERT INTO `nms_admin_log` VALUES ('2977', 'rand=38&moduleid=27', 'fulltimelink', '61.163.87.166', '1596008898');
INSERT INTO `nms_admin_log` VALUES ('2976', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1596008889');
INSERT INTO `nms_admin_log` VALUES ('2975', 'rand=83&moduleid=27', 'fulltimelink', '61.163.87.166', '1596008887');
INSERT INTO `nms_admin_log` VALUES ('2974', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1596008867');
INSERT INTO `nms_admin_log` VALUES ('2973', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1596008865');
INSERT INTO `nms_admin_log` VALUES ('2971', 'moduleid=27&file=setting', 'fulltimelink', '61.163.87.166', '1596008853');
INSERT INTO `nms_admin_log` VALUES ('2972', 'rand=11&moduleid=27&file=setting&tab=0', 'fulltimelink', '61.163.87.166', '1596008863');
INSERT INTO `nms_admin_log` VALUES ('2970', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1596008350');
INSERT INTO `nms_admin_log` VALUES ('2969', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1596008346');
INSERT INTO `nms_admin_log` VALUES ('2968', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1596008344');
INSERT INTO `nms_admin_log` VALUES ('2967', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595931016');
INSERT INTO `nms_admin_log` VALUES ('2965', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595930979');
INSERT INTO `nms_admin_log` VALUES ('2966', 'rand=42&moduleid=27', 'fulltimelink', '61.163.87.166', '1595931000');
INSERT INTO `nms_admin_log` VALUES ('2645', 'file=fetch', 'fulltimelink', '61.163.87.166', '1595127089');
INSERT INTO `nms_admin_log` VALUES ('2646', 'moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1595127096');
INSERT INTO `nms_admin_log` VALUES ('2647', 'moduleid=3&file=webpage&action=edit&itemid=5&item=1', 'fulltimelink', '61.163.87.166', '1595127100');
INSERT INTO `nms_admin_log` VALUES ('2648', 'rand=93&moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1595127390');
INSERT INTO `nms_admin_log` VALUES ('2649', 'moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1595127463');
INSERT INTO `nms_admin_log` VALUES ('2650', 'moduleid=26&file=index&action=edit&itemid=62', 'recycle', '61.163.87.166', '1595138010');
INSERT INTO `nms_admin_log` VALUES ('2651', 'rand=71&moduleid=26&sum=49&page=2', 'recycle', '61.163.87.166', '1595138031');
INSERT INTO `nms_admin_log` VALUES ('2652', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595138703');
INSERT INTO `nms_admin_log` VALUES ('2653', 'moduleid=26&sum=50&page=2', 'fulltimelink', '61.163.87.166', '1595138724');
INSERT INTO `nms_admin_log` VALUES ('2654', 'moduleid=26&file=index&action=edit&itemid=65', 'fulltimelink', '61.163.87.166', '1595138728');
INSERT INTO `nms_admin_log` VALUES ('2655', 'rand=52&moduleid=26&sum=50&page=2', 'fulltimelink', '61.163.87.166', '1595138758');
INSERT INTO `nms_admin_log` VALUES ('2656', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595138760');
INSERT INTO `nms_admin_log` VALUES ('2657', 'moduleid=26&file=index&action=edit&itemid=65', 'recycle', '61.163.87.166', '1595138765');
INSERT INTO `nms_admin_log` VALUES ('2658', 'rand=52&moduleid=26&sum=49&page=2', 'recycle', '61.163.87.166', '1595138772');
INSERT INTO `nms_admin_log` VALUES ('2659', 'moduleid=26&file=index&action=delete&itemid=62', 'recycle', '61.163.87.166', '1595138818');
INSERT INTO `nms_admin_log` VALUES ('2660', 'rand=18&moduleid=26&sum=49&page=2', 'recycle', '61.163.87.166', '1595138818');
INSERT INTO `nms_admin_log` VALUES ('2661', 'moduleid=26', 'recycle', '61.163.87.166', '1595139306');
INSERT INTO `nms_admin_log` VALUES ('2662', 'moduleid=26&file=index&action=delete&itemid=86', 'recycle', '61.163.87.166', '1595139313');
INSERT INTO `nms_admin_log` VALUES ('2663', 'rand=49&moduleid=26', 'recycle', '61.163.87.166', '1595139313');
INSERT INTO `nms_admin_log` VALUES ('2664', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595139762');
INSERT INTO `nms_admin_log` VALUES ('2665', 'moduleid=26', 'recycle', '61.163.87.166', '1595139773');
INSERT INTO `nms_admin_log` VALUES ('2666', 'moduleid=26&file=index&action=delete&itemid=87', 'recycle', '61.163.87.166', '1595139782');
INSERT INTO `nms_admin_log` VALUES ('2667', 'rand=84&moduleid=26', 'recycle', '61.163.87.166', '1595139782');
INSERT INTO `nms_admin_log` VALUES ('2668', 'moduleid=26&file=index&action=edit&itemid=85', 'recycle', '61.163.87.166', '1595139792');
INSERT INTO `nms_admin_log` VALUES ('2669', 'rand=66&moduleid=26', 'recycle', '61.163.87.166', '1595139797');
INSERT INTO `nms_admin_log` VALUES ('2670', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595139877');
INSERT INTO `nms_admin_log` VALUES ('2671', 'moduleid=26&file=index&action=edit&itemid=85', 'fulltimelink', '61.163.87.166', '1595139880');
INSERT INTO `nms_admin_log` VALUES ('2672', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595141480');
INSERT INTO `nms_admin_log` VALUES ('2673', 'moduleid=26&action=&fields=0&orderkey=202007051629500008&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595141504');
INSERT INTO `nms_admin_log` VALUES ('2674', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1595141512');
INSERT INTO `nms_admin_log` VALUES ('2675', 'moduleid=26&action=&fields=0&orderkey=%092020070516292500008&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595141531');
INSERT INTO `nms_admin_log` VALUES ('2676', 'moduleid=26&action=&fields=0&orderkey=2020070516292500008&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595141536');
INSERT INTO `nms_admin_log` VALUES ('2964', 'rand=13&moduleid=27', 'fulltimelink', '61.163.87.166', '1595930977');
INSERT INTO `nms_admin_log` VALUES ('2963', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595930930');
INSERT INTO `nms_admin_log` VALUES ('2962', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595930928');
INSERT INTO `nms_admin_log` VALUES ('2960', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595905732');
INSERT INTO `nms_admin_log` VALUES ('2961', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1595930919');
INSERT INTO `nms_admin_log` VALUES ('2959', 'rand=27&moduleid=25', 'fulltimelink', '61.163.87.166', '1595905600');
INSERT INTO `nms_admin_log` VALUES ('2958', 'moduleid=25&file=index&action=delete&itemid=50', 'fulltimelink', '61.163.87.166', '1595905600');
INSERT INTO `nms_admin_log` VALUES ('2957', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595905595');
INSERT INTO `nms_admin_log` VALUES ('2955', 'moduleid=25&file=index&action=delete&itemid=9', 'fulltimelink', '61.163.87.166', '1595905173');
INSERT INTO `nms_admin_log` VALUES ('2956', 'rand=94&moduleid=25&sum=34&page=2', 'fulltimelink', '61.163.87.166', '1595905173');
INSERT INTO `nms_admin_log` VALUES ('2954', 'moduleid=25&sum=34&page=2', 'fulltimelink', '61.163.87.166', '1595905166');
INSERT INTO `nms_admin_log` VALUES ('2834', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595148093');
INSERT INTO `nms_admin_log` VALUES ('2953', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595905161');
INSERT INTO `nms_admin_log` VALUES ('2833', 'rand=40&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595148092');
INSERT INTO `nms_admin_log` VALUES ('2832', 'moduleid=25&file=index&action=edit&itemid=34', 'fulltimelink', '61.163.87.166', '1595148089');
INSERT INTO `nms_admin_log` VALUES ('2831', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595148081');
INSERT INTO `nms_admin_log` VALUES ('2830', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595148078');
INSERT INTO `nms_admin_log` VALUES ('2829', 'rand=42&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595148076');
INSERT INTO `nms_admin_log` VALUES ('2828', 'moduleid=25&file=index&action=edit&itemid=34', 'fulltimelink', '61.163.87.166', '1595148067');
INSERT INTO `nms_admin_log` VALUES ('2827', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595148065');
INSERT INTO `nms_admin_log` VALUES ('2826', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147962');
INSERT INTO `nms_admin_log` VALUES ('2825', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147961');
INSERT INTO `nms_admin_log` VALUES ('2824', 'moduleid=25&action=reject', 'fulltimelink', '61.163.87.166', '1595147959');
INSERT INTO `nms_admin_log` VALUES ('2822', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147954');
INSERT INTO `nms_admin_log` VALUES ('2823', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147956');
INSERT INTO `nms_admin_log` VALUES ('2821', 'rand=91&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147953');
INSERT INTO `nms_admin_log` VALUES ('2820', 'moduleid=25&file=index&action=delete&itemid=33', 'fulltimelink', '61.163.87.166', '1595147953');
INSERT INTO `nms_admin_log` VALUES ('2819', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147948');
INSERT INTO `nms_admin_log` VALUES ('2818', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147924');
INSERT INTO `nms_admin_log` VALUES ('2817', 'rand=52&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147923');
INSERT INTO `nms_admin_log` VALUES ('2816', 'moduleid=25&file=index&action=delete&itemid=32', 'fulltimelink', '61.163.87.166', '1595147923');
INSERT INTO `nms_admin_log` VALUES ('2815', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147919');
INSERT INTO `nms_admin_log` VALUES ('2814', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147918');
INSERT INTO `nms_admin_log` VALUES ('2811', 'moduleid=25&file=index&action=edit&itemid=32', 'fulltimelink', '61.163.87.166', '1595147907');
INSERT INTO `nms_admin_log` VALUES ('2812', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147911');
INSERT INTO `nms_admin_log` VALUES ('2813', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147913');
INSERT INTO `nms_admin_log` VALUES ('2808', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147898');
INSERT INTO `nms_admin_log` VALUES ('2809', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147901');
INSERT INTO `nms_admin_log` VALUES ('2810', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147902');
INSERT INTO `nms_admin_log` VALUES ('2806', 'rand=31&moduleid=25', 'fulltimelink', '61.163.87.166', '1595147868');
INSERT INTO `nms_admin_log` VALUES ('2807', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147897');
INSERT INTO `nms_admin_log` VALUES ('2804', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147860');
INSERT INTO `nms_admin_log` VALUES ('2805', 'moduleid=25&file=index&action=delete&itemid=31', 'fulltimelink', '61.163.87.166', '1595147868');
INSERT INTO `nms_admin_log` VALUES ('2803', 'rand=82&moduleid=25', 'fulltimelink', '61.163.87.166', '1595147513');
INSERT INTO `nms_admin_log` VALUES ('2802', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147505');
INSERT INTO `nms_admin_log` VALUES ('2801', 'rand=85&moduleid=25', 'fulltimelink', '61.163.87.166', '1595147494');
INSERT INTO `nms_admin_log` VALUES ('2799', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147446');
INSERT INTO `nms_admin_log` VALUES ('2800', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147487');
INSERT INTO `nms_admin_log` VALUES ('2798', 'rand=59&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147445');
INSERT INTO `nms_admin_log` VALUES ('2797', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147442');
INSERT INTO `nms_admin_log` VALUES ('2795', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147430');
INSERT INTO `nms_admin_log` VALUES ('2796', 'rand=28&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147437');
INSERT INTO `nms_admin_log` VALUES ('2794', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147427');
INSERT INTO `nms_admin_log` VALUES ('2793', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147425');
INSERT INTO `nms_admin_log` VALUES ('2792', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147416');
INSERT INTO `nms_admin_log` VALUES ('2790', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147385');
INSERT INTO `nms_admin_log` VALUES ('2791', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147411');
INSERT INTO `nms_admin_log` VALUES ('2789', 'rand=13&moduleid=25', 'fulltimelink', '61.163.87.166', '1595147382');
INSERT INTO `nms_admin_log` VALUES ('2788', 'moduleid=25&file=index&action=delete&itemid=29', 'fulltimelink', '61.163.87.166', '1595147382');
INSERT INTO `nms_admin_log` VALUES ('2787', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147377');
INSERT INTO `nms_admin_log` VALUES ('2786', 'moduleid=25&file=index&action=edit&itemid=29', 'fulltimelink', '61.163.87.166', '1595147373');
INSERT INTO `nms_admin_log` VALUES ('2784', 'moduleid=25&sum=24&page=2', 'fulltimelink', '61.163.87.166', '1595147337');
INSERT INTO `nms_admin_log` VALUES ('2785', 'moduleid=25&sum=24', 'fulltimelink', '61.163.87.166', '1595147341');
INSERT INTO `nms_admin_log` VALUES ('2783', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147325');
INSERT INTO `nms_admin_log` VALUES ('2782', 'moduleid=25&file=index&action=edit&itemid=29', 'fulltimelink', '61.163.87.166', '1595147320');
INSERT INTO `nms_admin_log` VALUES ('2781', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147295');
INSERT INTO `nms_admin_log` VALUES ('2780', 'moduleid=25&file=index&action=edit&itemid=29', 'fulltimelink', '61.163.87.166', '1595147288');
INSERT INTO `nms_admin_log` VALUES ('2778', 'moduleid=25&sum=24', 'fulltimelink', '61.163.87.166', '1595147280');
INSERT INTO `nms_admin_log` VALUES ('2779', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147282');
INSERT INTO `nms_admin_log` VALUES ('2776', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147268');
INSERT INTO `nms_admin_log` VALUES ('2777', 'moduleid=25&sum=24&page=2', 'fulltimelink', '61.163.87.166', '1595147278');
INSERT INTO `nms_admin_log` VALUES ('2775', 'rand=63&moduleid=25', 'fulltimelink', '61.163.87.166', '1595147263');
INSERT INTO `nms_admin_log` VALUES ('2774', 'moduleid=25&file=index&action=edit&itemid=30', 'fulltimelink', '61.163.87.166', '1595147240');
INSERT INTO `nms_admin_log` VALUES ('2773', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147238');
INSERT INTO `nms_admin_log` VALUES ('2772', 'moduleid=25&file=index&action=edit&itemid=30', 'fulltimelink', '61.163.87.166', '1595147233');
INSERT INTO `nms_admin_log` VALUES ('2771', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147232');
INSERT INTO `nms_admin_log` VALUES ('2770', 'moduleid=25&file=index&action=edit&itemid=30', 'fulltimelink', '61.163.87.166', '1595147222');
INSERT INTO `nms_admin_log` VALUES ('2769', 'rand=98&moduleid=25', 'fulltimelink', '61.163.87.166', '1595147220');
INSERT INTO `nms_admin_log` VALUES ('2767', 'rand=92&moduleid=25', 'fulltimelink', '61.163.87.166', '1595147186');
INSERT INTO `nms_admin_log` VALUES ('2768', 'moduleid=25&file=index&action=edit&itemid=29', 'fulltimelink', '61.163.87.166', '1595147212');
INSERT INTO `nms_admin_log` VALUES ('2765', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147177');
INSERT INTO `nms_admin_log` VALUES ('2766', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147182');
INSERT INTO `nms_admin_log` VALUES ('2764', 'moduleid=25&file=index&action=edit&itemid=16', 'fulltimelink', '61.163.87.166', '1595147096');
INSERT INTO `nms_admin_log` VALUES ('2763', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595147094');
INSERT INTO `nms_admin_log` VALUES ('2761', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147089');
INSERT INTO `nms_admin_log` VALUES ('2762', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147093');
INSERT INTO `nms_admin_log` VALUES ('2760', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147053');
INSERT INTO `nms_admin_log` VALUES ('2759', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147044');
INSERT INTO `nms_admin_log` VALUES ('2758', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147040');
INSERT INTO `nms_admin_log` VALUES ('2757', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595147031');
INSERT INTO `nms_admin_log` VALUES ('2755', 'rand=24&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595146981');
INSERT INTO `nms_admin_log` VALUES ('2756', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595147026');
INSERT INTO `nms_admin_log` VALUES ('2754', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595146977');
INSERT INTO `nms_admin_log` VALUES ('2753', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595146969');
INSERT INTO `nms_admin_log` VALUES ('2752', 'moduleid=25&file=index&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595146968');
INSERT INTO `nms_admin_log` VALUES ('2750', 'rand=47&moduleid=25', 'fulltimelink', '61.163.87.166', '1595146858');
INSERT INTO `nms_admin_log` VALUES ('2751', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1595146966');
INSERT INTO `nms_admin_log` VALUES ('2748', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595146851');
INSERT INTO `nms_admin_log` VALUES ('2749', 'moduleid=25&file=index&action=delete&itemid=17', 'fulltimelink', '61.163.87.166', '1595146858');
INSERT INTO `nms_admin_log` VALUES ('2747', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1595146848');
INSERT INTO `nms_admin_log` VALUES ('2746', 'rand=29&moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E8%87%AA%E7%94%B1%E9%A3%9E&exports=0', 'fulltimelink', '61.163.87.166', '1595146846');
INSERT INTO `nms_admin_log` VALUES ('2745', 'moduleid=26&file=index&action=delete&itemid=58', 'fulltimelink', '61.163.87.166', '1595146846');
INSERT INTO `nms_admin_log` VALUES ('2744', 'rand=57&moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E8%87%AA%E7%94%B1%E9%A3%9E&exports=0', 'fulltimelink', '61.163.87.166', '1595146844');
INSERT INTO `nms_admin_log` VALUES ('2743', 'moduleid=26&file=index&action=delete&itemid=60', 'fulltimelink', '61.163.87.166', '1595146844');
INSERT INTO `nms_admin_log` VALUES ('2741', 'rand=86&moduleid=26', 'fulltimelink', '61.163.87.166', '1595146836');
INSERT INTO `nms_admin_log` VALUES ('2742', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E8%87%AA%E7%94%B1%E9%A3%9E&exports=0', 'fulltimelink', '61.163.87.166', '1595146841');
INSERT INTO `nms_admin_log` VALUES ('2740', 'moduleid=26&file=index&action=delete&itemid=89', 'fulltimelink', '61.163.87.166', '1595146836');
INSERT INTO `nms_admin_log` VALUES ('2739', 'rand=64&moduleid=26', 'fulltimelink', '61.163.87.166', '1595146833');
INSERT INTO `nms_admin_log` VALUES ('2738', 'moduleid=26&file=index&action=delete&itemid=90', 'fulltimelink', '61.163.87.166', '1595146833');
INSERT INTO `nms_admin_log` VALUES ('2736', 'moduleid=25&sum=25', 'fulltimelink', '61.163.87.166', '1595146803');
INSERT INTO `nms_admin_log` VALUES ('2737', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595146813');
INSERT INTO `nms_admin_log` VALUES ('2735', 'moduleid=25&sum=25&page=2', 'fulltimelink', '61.163.87.166', '1595146801');
INSERT INTO `nms_admin_log` VALUES ('2734', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595146707');
INSERT INTO `nms_admin_log` VALUES ('2733', 'moduleid=25&file=index&action=edit&itemid=20', 'fulltimelink', '61.163.87.166', '1595146703');
INSERT INTO `nms_admin_log` VALUES ('2732', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595146700');
INSERT INTO `nms_admin_log` VALUES ('2731', 'file=fields&tb=nms_article_25&widget=1', 'fulltimelink', '61.163.87.166', '1595146407');
INSERT INTO `nms_admin_log` VALUES ('2730', 'moduleid=25&file=setting', 'fulltimelink', '61.163.87.166', '1595146406');
INSERT INTO `nms_admin_log` VALUES ('2728', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595146387');
INSERT INTO `nms_admin_log` VALUES ('2729', 'moduleid=25&file=index&action=edit&itemid=22', 'fulltimelink', '61.163.87.166', '1595146393');
INSERT INTO `nms_admin_log` VALUES ('2726', 'file=fields&tb=nms_article_25&widget=1', 'fulltimelink', '61.163.87.166', '1595146331');
INSERT INTO `nms_admin_log` VALUES ('2727', 'rand=88&moduleid=25', 'recycle', '61.163.87.166', '1595146369');
INSERT INTO `nms_admin_log` VALUES ('2725', 'moduleid=25&file=setting', 'fulltimelink', '61.163.87.166', '1595146329');
INSERT INTO `nms_admin_log` VALUES ('2723', 'moduleid=25', 'recycle', '61.163.87.166', '1595146275');
INSERT INTO `nms_admin_log` VALUES ('2724', 'moduleid=25&file=index&action=edit&itemid=11', 'recycle', '61.163.87.166', '1595146279');
INSERT INTO `nms_admin_log` VALUES ('2722', 'file=category&mid=25', 'fulltimelink', '61.163.87.166', '1595146233');
INSERT INTO `nms_admin_log` VALUES ('2721', 'moduleid=25&file=index&action=edit&itemid=11', 'fulltimelink', '61.163.87.166', '1595146115');
INSERT INTO `nms_admin_log` VALUES ('2720', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595146111');
INSERT INTO `nms_admin_log` VALUES ('2719', 'moduleid=25&file=setting', 'fulltimelink', '61.163.87.166', '1595146109');
INSERT INTO `nms_admin_log` VALUES ('2717', 'moduleid=25', 'recycle', '61.163.87.166', '1595145602');
INSERT INTO `nms_admin_log` VALUES ('2718', 'moduleid=25&file=index&action=edit&itemid=11', 'recycle', '61.163.87.166', '1595145674');
INSERT INTO `nms_admin_log` VALUES ('2716', 'moduleid=25&action=reject', 'recycle', '61.163.87.166', '1595145600');
INSERT INTO `nms_admin_log` VALUES ('2714', 'rand=67&moduleid=25', 'recycle', '61.163.87.166', '1595145493');
INSERT INTO `nms_admin_log` VALUES ('2715', 'moduleid=25&action=check', 'recycle', '61.163.87.166', '1595145598');
INSERT INTO `nms_admin_log` VALUES ('2712', 'rand=23&moduleid=25', 'recycle', '61.163.87.166', '1595145470');
INSERT INTO `nms_admin_log` VALUES ('2713', 'moduleid=25&file=index&action=edit&itemid=11', 'recycle', '61.163.87.166', '1595145474');
INSERT INTO `nms_admin_log` VALUES ('2711', 'moduleid=25&file=index&action=edit&itemid=11', 'recycle', '61.163.87.166', '1595145465');
INSERT INTO `nms_admin_log` VALUES ('2710', 'moduleid=25', 'recycle', '61.163.87.166', '1595145453');
INSERT INTO `nms_admin_log` VALUES ('2709', 'moduleid=1&file=fields&tb=article_25&action=edit&itemid=31', 'fulltimelink', '61.163.87.166', '1595144683');
INSERT INTO `nms_admin_log` VALUES ('2708', 'file=fields&tb=nms_article_25&widget=1', 'fulltimelink', '61.163.87.166', '1595144676');
INSERT INTO `nms_admin_log` VALUES ('2707', 'moduleid=25&file=setting', 'fulltimelink', '61.163.87.166', '1595144675');
INSERT INTO `nms_admin_log` VALUES ('2705', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1595144569');
INSERT INTO `nms_admin_log` VALUES ('2706', 'moduleid=25&file=index&action=edit&itemid=29', 'fulltimelink', '61.163.87.166', '1595144573');
INSERT INTO `nms_admin_log` VALUES ('2703', 'rand=69&moduleid=26', 'fulltimelink', '61.163.87.166', '1595143309');
INSERT INTO `nms_admin_log` VALUES ('2704', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595143373');
INSERT INTO `nms_admin_log` VALUES ('2702', 'moduleid=26&file=index&action=edit&itemid=85', 'fulltimelink', '61.163.87.166', '1595143306');
INSERT INTO `nms_admin_log` VALUES ('2701', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595143286');
INSERT INTO `nms_admin_log` VALUES ('2700', 'rand=90&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595143240');
INSERT INTO `nms_admin_log` VALUES ('2699', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142924');
INSERT INTO `nms_admin_log` VALUES ('2697', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142573');
INSERT INTO `nms_admin_log` VALUES ('2698', 'rand=45&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142577');
INSERT INTO `nms_admin_log` VALUES ('2696', 'rand=35&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142570');
INSERT INTO `nms_admin_log` VALUES ('2695', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142459');
INSERT INTO `nms_admin_log` VALUES ('2694', 'rand=50&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142457');
INSERT INTO `nms_admin_log` VALUES ('2693', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142299');
INSERT INTO `nms_admin_log` VALUES ('2692', 'rand=43&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142296');
INSERT INTO `nms_admin_log` VALUES ('2691', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142280');
INSERT INTO `nms_admin_log` VALUES ('2689', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142251');
INSERT INTO `nms_admin_log` VALUES ('2690', 'rand=13&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142279');
INSERT INTO `nms_admin_log` VALUES ('2688', 'rand=95&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142249');
INSERT INTO `nms_admin_log` VALUES ('2687', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142182');
INSERT INTO `nms_admin_log` VALUES ('2686', 'rand=68&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142177');
INSERT INTO `nms_admin_log` VALUES ('2685', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595142062');
INSERT INTO `nms_admin_log` VALUES ('2684', 'rand=54&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595142058');
INSERT INTO `nms_admin_log` VALUES ('2683', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595141980');
INSERT INTO `nms_admin_log` VALUES ('2682', 'rand=29&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595141976');
INSERT INTO `nms_admin_log` VALUES ('2681', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595141966');
INSERT INTO `nms_admin_log` VALUES ('2680', 'rand=18&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595141963');
INSERT INTO `nms_admin_log` VALUES ('2679', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595141546');
INSERT INTO `nms_admin_log` VALUES ('2678', 'rand=22&moduleid=26&action=&fields=0&orderkey=2020070516292500008&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1595141545');
INSERT INTO `nms_admin_log` VALUES ('2677', 'moduleid=26&file=index&action=edit&itemid=82', 'fulltimelink', '61.163.87.166', '1595141539');
INSERT INTO `nms_admin_log` VALUES ('2644', 'moduleid=24&file=index&action=edit&itemid=3', 'fulltimelink', '61.163.87.166', '1595126380');
INSERT INTO `nms_admin_log` VALUES ('2643', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1595126368');
INSERT INTO `nms_admin_log` VALUES ('2641', 'moduleid=24&file=html', 'fulltimelink', '61.163.87.166', '1595126365');
INSERT INTO `nms_admin_log` VALUES ('2642', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1595126365');
INSERT INTO `nms_admin_log` VALUES ('2640', 'moduleid=24&action=check', 'fulltimelink', '61.163.87.166', '1595126364');
INSERT INTO `nms_admin_log` VALUES ('2639', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1595126362');
INSERT INTO `nms_admin_log` VALUES ('2849', 'rand=51&moduleid=26', 'fulltimelink', '61.163.87.166', '1595474785');
INSERT INTO `nms_admin_log` VALUES ('2848', 'moduleid=26&file=index&action=edit&itemid=92', 'fulltimelink', '61.163.87.166', '1595474768');
INSERT INTO `nms_admin_log` VALUES ('2847', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595474765');
INSERT INTO `nms_admin_log` VALUES ('2846', 'rand=64&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1595474734');
INSERT INTO `nms_admin_log` VALUES ('2845', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595474728');
INSERT INTO `nms_admin_log` VALUES ('2844', 'rand=93&moduleid=26', 'fulltimelink', '61.163.87.166', '1595474696');
INSERT INTO `nms_admin_log` VALUES ('2843', 'moduleid=26&file=index&action=edit&itemid=92', 'fulltimelink', '61.163.87.166', '1595474691');
INSERT INTO `nms_admin_log` VALUES ('2842', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595474687');
INSERT INTO `nms_admin_log` VALUES ('2841', 'rand=31&moduleid=26', 'fulltimelink', '61.163.87.166', '1595471770');
INSERT INTO `nms_admin_log` VALUES ('2840', 'moduleid=26&file=index&action=edit&itemid=91', 'fulltimelink', '61.163.87.166', '1595471762');
INSERT INTO `nms_admin_log` VALUES ('3122', 'file=category&mid=30&action=letter&catname=%E5%A1%91%E6%96%99%E8%A2%8B%E9%A5%AE%E6%96%99%E7%93%B6%E5%A1%91%E6%96%99%E7%8E%A9%E5%85%B7%E6%89%8B%E6%9C%BA%E5%A3%B3%E5%A1%91%E6%96%99%E5%87%B3%E5%A1%91%E6%96%99%E8%84%B8%E7%9B%86%E5%A1%91%E6%96%99%E6%9D%AF', 'fulltimelink', '61.163.87.166', '1596191522');
INSERT INTO `nms_admin_log` VALUES ('3121', 'file=category&mid=30&action=letter&catname=%E5%A1%91%E6%96%99%E8%A2%8B%E9%A5%AE%E6%96%99%E7%93%B6%E5%A1%91%E6%96%99%E7%8E%A9%E5%85%B7%E6%89%8B%E6%9C%BA%E5%A3%B3%E5%A1%91%E6%96%99%E5%87%B3%E5%A1%91%E6%96%99%E8%84%B8%E7%9B%86', 'fulltimelink', '61.163.87.166', '1596191518');
INSERT INTO `nms_admin_log` VALUES ('3120', 'file=category&mid=30&action=letter&catname=%E5%A1%91%E6%96%99%E8%A2%8B%E9%A5%AE%E6%96%99%E7%93%B6%E5%A1%91%E6%96%99%E7%8E%A9%E5%85%B7%E6%89%8B%E6%9C%BA%E5%A3%B3%E5%A1%91%E6%96%99%E5%87%B3', 'fulltimelink', '61.163.87.166', '1596191510');
INSERT INTO `nms_admin_log` VALUES ('3119', 'file=category&mid=30&action=letter&catname=%E5%A1%91%E6%96%99%E8%A2%8B%E9%A5%AE%E6%96%99%E7%93%B6%E5%A1%91%E6%96%99%E7%8E%A9%E5%85%B7%E6%89%8B%E6%9C%BA%E5%A3%B3', 'fulltimelink', '61.163.87.166', '1596191489');
INSERT INTO `nms_admin_log` VALUES ('3118', 'mid=30&file=category&action=add&parentid=24', 'fulltimelink', '61.163.87.166', '1596191466');
INSERT INTO `nms_admin_log` VALUES ('3117', 'rand=60&file=category&mid=30&parentid=20', 'fulltimelink', '61.163.87.166', '1596191448');
INSERT INTO `nms_admin_log` VALUES ('3116', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E7%B1%BB%E6%97%A7%E8%A1%A3%E7%89%A9%E7%BA%BA%E7%BB%87%E5%93%81%E7%B1%BB%EF%BC%88%E5%B9%B2%E5%87%80%EF%BC%89%E5%BA%9F%E6%97%A7%E5%8C%85%E8%A3%85%E7%B', 'fulltimelink', '61.163.87.166', '1596191448');
INSERT INTO `nms_admin_log` VALUES ('3115', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E7%B1%BB%E6%97%A7%E8%A1%A3%E7%89%A9%E7%BA%BA%E7%BB%87%E5%93%81%E7%B1%BB%EF%BC%88%E5%B9%B2%E5%87%80%EF%BC%89%E5%BA%9F%E6%97%A7%E5%8C%85%E8%A3%85%E7%B', 'fulltimelink', '61.163.87.166', '1596191416');
INSERT INTO `nms_admin_log` VALUES ('3114', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E7%B1%BB%E6%97%A7%E8%A1%A3%E7%89%A9%E7%BA%BA%E7%BB%87%E5%93%81%E7%B1%BB%EF%BC%88%E5%B9%B2%E5%87%80%EF%BC%89%E5%BA%9F%E6%97%A7%E5%8C%85%E8%A3%85%E7%B', 'fulltimelink', '61.163.87.166', '1596191405');
INSERT INTO `nms_admin_log` VALUES ('3113', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E7%B1%BB%E6%97%A7%E8%A1%A3%E7%89%A9%E7%BA%BA%E7%BB%87%E5%93%81%E7%B1%BB%EF%BC%88%E5%B9%B2%E5%87%80%EF%BC%89', 'fulltimelink', '61.163.87.166', '1596191233');
INSERT INTO `nms_admin_log` VALUES ('3112', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E7%B1%BB%E6%97%A7%E8%A1%A3%E7%89%A9%E7%BA%BA%E7%BB%87%E5%93%81%E7%B1%BB%EF%BC%88%EF%BC%89', 'fulltimelink', '61.163.87.166', '1596191229');
INSERT INTO `nms_admin_log` VALUES ('3111', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E7%B1%BB%E6%97%A7%E8%A1%A3%E7%89%A9%E7%BA%BA%E7%BB%87%E7%B1%BB', 'fulltimelink', '61.163.87.166', '1596191221');
INSERT INTO `nms_admin_log` VALUES ('3110', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E7%B1%BB', 'fulltimelink', '61.163.87.166', '1596191209');
INSERT INTO `nms_admin_log` VALUES ('3109', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB%E5%BA%9F%E6%97%A7%E7%BA%B8%E5%93%81%E7%B1%BB', 'fulltimelink', '61.163.87.166', '1596191203');
INSERT INTO `nms_admin_log` VALUES ('3108', 'file=category&mid=30&action=letter&catname=%E5%BA%9F%E5%A1%91%E6%96%99%E7%B1%BB', 'fulltimelink', '61.163.87.166', '1596191192');
INSERT INTO `nms_admin_log` VALUES ('3107', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191178');
INSERT INTO `nms_admin_log` VALUES ('3106', 'mid=30&file=category&action=add&parentid=20', 'fulltimelink', '61.163.87.166', '1596191176');
INSERT INTO `nms_admin_log` VALUES ('3105', 'rand=78&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596191172');
INSERT INTO `nms_admin_log` VALUES ('3104', 'file=category&mid=30&action=letter&catname=%E5%85%B6%E4%BB%96%E5%9E%83%E5%9C%BE', 'fulltimelink', '61.163.87.166', '1596191149');
INSERT INTO `nms_admin_log` VALUES ('3103', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191143');
INSERT INTO `nms_admin_log` VALUES ('3102', 'file=category&action=add&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596191142');
INSERT INTO `nms_admin_log` VALUES ('3101', 'rand=48&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596191140');
INSERT INTO `nms_admin_log` VALUES ('3100', 'file=category&mid=30&action=letter&catname=%E6%9C%89%E5%AE%B3%E5%9E%83%E5%9C%BE', 'fulltimelink', '61.163.87.166', '1596191109');
INSERT INTO `nms_admin_log` VALUES ('3099', 'file=category&action=add&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596191102');
INSERT INTO `nms_admin_log` VALUES ('3098', 'rand=95&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596191101');
INSERT INTO `nms_admin_log` VALUES ('3097', 'file=category&mid=30&action=letter&catname=%E5%8E%A8%E4%BD%99%E5%9E%83%E5%9C%BE', 'fulltimelink', '61.163.87.166', '1596191081');
INSERT INTO `nms_admin_log` VALUES ('3096', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596191075');
INSERT INTO `nms_admin_log` VALUES ('3095', 'file=category&action=add&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596191073');
INSERT INTO `nms_admin_log` VALUES ('3094', 'rand=25&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596191071');
INSERT INTO `nms_admin_log` VALUES ('3093', 'file=category&mid=30&action=letter&catname=%E5%8F%AF%E5%9B%9E%E6%94%B6%E7%89%A9', 'fulltimelink', '61.163.87.166', '1596190983');
INSERT INTO `nms_admin_log` VALUES ('3092', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596190971');
INSERT INTO `nms_admin_log` VALUES ('3090', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596190968');
INSERT INTO `nms_admin_log` VALUES ('3091', 'file=category&mid=30&action=add&parentid=0', 'fulltimelink', '61.163.87.166', '1596190969');
INSERT INTO `nms_admin_log` VALUES ('3089', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596190939');
INSERT INTO `nms_admin_log` VALUES ('3088', 'file=category&mid=30&action=add&parentid=0', 'fulltimelink', '61.163.87.166', '1596190937');
INSERT INTO `nms_admin_log` VALUES ('3087', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596190936');
INSERT INTO `nms_admin_log` VALUES ('3086', 'file=module', 'fulltimelink', '61.163.87.166', '1596190932');
INSERT INTO `nms_admin_log` VALUES ('3084', 'file=category&mid=30&action=add&parentid=0', 'fulltimelink', '61.163.87.166', '1596190919');
INSERT INTO `nms_admin_log` VALUES ('3085', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596190925');
INSERT INTO `nms_admin_log` VALUES ('3083', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596190918');
INSERT INTO `nms_admin_log` VALUES ('3081', 'file=module&action=add', 'fulltimelink', '61.163.87.166', '1596190886');
INSERT INTO `nms_admin_log` VALUES ('3082', 'rand=69&update=1&file=module', 'fulltimelink', '61.163.87.166', '1596190911');
INSERT INTO `nms_admin_log` VALUES ('3080', 'file=module', 'fulltimelink', '61.163.87.166', '1596187971');
INSERT INTO `nms_admin_log` VALUES ('3079', 'file=category&action=edit&mid=24&catid=6', 'fulltimelink', '61.163.87.166', '1596182586');
INSERT INTO `nms_admin_log` VALUES ('3078', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1596182584');
INSERT INTO `nms_admin_log` VALUES ('3077', 'moduleid=29', 'fulltimelink', '61.163.87.166', '1596180076');
INSERT INTO `nms_admin_log` VALUES ('3076', 'moduleid=28', 'fulltimelink', '61.163.87.166', '1596180068');
INSERT INTO `nms_admin_log` VALUES ('3075', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1596180065');
INSERT INTO `nms_admin_log` VALUES ('3072', 'moduleid=24&file=index&action=edit&itemid=3', 'fulltimelink', '61.163.87.166', '1596180044');
INSERT INTO `nms_admin_log` VALUES ('3074', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596180057');
INSERT INTO `nms_admin_log` VALUES ('3073', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1596180050');
INSERT INTO `nms_admin_log` VALUES ('3071', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1596180032');
INSERT INTO `nms_admin_log` VALUES ('3068', 'moduleid=29', 'fulltimelink', '61.163.87.166', '1596180006');
INSERT INTO `nms_admin_log` VALUES ('3070', 'moduleid=24&action=check', 'fulltimelink', '61.163.87.166', '1596180031');
INSERT INTO `nms_admin_log` VALUES ('3069', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1596180021');
INSERT INTO `nms_admin_log` VALUES ('3067', 'moduleid=28', 'fulltimelink', '61.163.87.166', '1596180003');
INSERT INTO `nms_admin_log` VALUES ('3064', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596179990');
INSERT INTO `nms_admin_log` VALUES ('3065', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1596179996');
INSERT INTO `nms_admin_log` VALUES ('3066', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1596180000');
INSERT INTO `nms_admin_log` VALUES ('3062', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1596179979');
INSERT INTO `nms_admin_log` VALUES ('3063', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1596179985');
INSERT INTO `nms_admin_log` VALUES ('3061', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1596179977');
INSERT INTO `nms_admin_log` VALUES ('3060', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1596179972');
INSERT INTO `nms_admin_log` VALUES ('3059', 'rand=62&moduleid=26', 'fulltimelink', '61.163.87.166', '1596173835');
INSERT INTO `nms_admin_log` VALUES ('3058', 'moduleid=26&file=index&action=edit&itemid=108', 'fulltimelink', '61.163.87.166', '1596173832');
INSERT INTO `nms_admin_log` VALUES ('3057', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1596173830');
INSERT INTO `nms_admin_log` VALUES ('3056', 'moduleid=25&file=index&action=edit&itemid=14', 'fulltimelink', '61.163.87.166', '1596173781');
INSERT INTO `nms_admin_log` VALUES ('3054', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596173771');
INSERT INTO `nms_admin_log` VALUES ('3055', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596173774');
INSERT INTO `nms_admin_log` VALUES ('3052', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596173681');
INSERT INTO `nms_admin_log` VALUES ('3053', 'moduleid=25&file=index&action=edit&itemid=34', 'fulltimelink', '61.163.87.166', '1596173685');
INSERT INTO `nms_admin_log` VALUES ('3051', 'rand=39&moduleid=26', 'fulltimelink', '61.163.87.166', '1596172616');
INSERT INTO `nms_admin_log` VALUES ('3050', 'moduleid=26&file=index&action=edit&itemid=107', 'fulltimelink', '61.163.87.166', '1596172611');
INSERT INTO `nms_admin_log` VALUES ('3049', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1596172428');
INSERT INTO `nms_admin_log` VALUES ('3048', 'rand=52&moduleid=26', 'fulltimelink', '61.163.87.166', '1596172426');
INSERT INTO `nms_admin_log` VALUES ('3046', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1596172165');
INSERT INTO `nms_admin_log` VALUES ('3047', 'moduleid=26&file=index&action=edit&itemid=106', 'fulltimelink', '61.163.87.166', '1596172421');
INSERT INTO `nms_admin_log` VALUES ('3045', 'rand=15&moduleid=26', 'fulltimelink', '61.163.87.166', '1596172155');
INSERT INTO `nms_admin_log` VALUES ('3044', 'moduleid=26&file=index&action=edit&itemid=104', 'fulltimelink', '61.163.87.166', '1596172153');
INSERT INTO `nms_admin_log` VALUES ('3043', 'rand=51&moduleid=26', 'fulltimelink', '61.163.87.166', '1596172150');
INSERT INTO `nms_admin_log` VALUES ('3042', 'moduleid=26&file=index&action=edit&itemid=105', 'fulltimelink', '61.163.87.166', '1596172147');
INSERT INTO `nms_admin_log` VALUES ('3041', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1596172144');
INSERT INTO `nms_admin_log` VALUES ('3040', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596171883');
INSERT INTO `nms_admin_log` VALUES ('3039', 'moduleid=25&file=index&action=edit&itemid=44', 'fulltimelink', '61.163.87.166', '1596168479');
INSERT INTO `nms_admin_log` VALUES ('3038', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596168475');
INSERT INTO `nms_admin_log` VALUES ('3037', 'moduleid=25&file=index&action=edit&itemid=51', 'fulltimelink', '61.163.87.166', '1596168171');
INSERT INTO `nms_admin_log` VALUES ('3036', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596168160');
INSERT INTO `nms_admin_log` VALUES ('3035', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1596165188');
INSERT INTO `nms_admin_log` VALUES ('3033', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596164496');
INSERT INTO `nms_admin_log` VALUES ('3034', 'moduleid=25&file=index&action=edit&itemid=34', 'fulltimelink', '61.163.87.166', '1596164500');
INSERT INTO `nms_admin_log` VALUES ('3032', 'rand=60&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1596164396');
INSERT INTO `nms_admin_log` VALUES ('3030', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1596164393');
INSERT INTO `nms_admin_log` VALUES ('3031', 'moduleid=25&file=index&action=delete&itemid=7', 'fulltimelink', '61.163.87.166', '1596164396');
INSERT INTO `nms_admin_log` VALUES ('3029', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596164389');
INSERT INTO `nms_admin_log` VALUES ('3028', 'rand=33&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164376');
INSERT INTO `nms_admin_log` VALUES ('3026', 'rand=83&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164372');
INSERT INTO `nms_admin_log` VALUES ('3027', 'moduleid=26&file=index&action=delete&itemid=15', 'fulltimelink', '61.163.87.166', '1596164376');
INSERT INTO `nms_admin_log` VALUES ('3025', 'moduleid=26&file=index&action=delete&itemid=14', 'fulltimelink', '61.163.87.166', '1596164372');
INSERT INTO `nms_admin_log` VALUES ('3024', 'rand=70&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164370');
INSERT INTO `nms_admin_log` VALUES ('3023', 'moduleid=26&file=index&action=delete&itemid=13', 'fulltimelink', '61.163.87.166', '1596164370');
INSERT INTO `nms_admin_log` VALUES ('2910', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595898440');
INSERT INTO `nms_admin_log` VALUES ('2909', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898435');
INSERT INTO `nms_admin_log` VALUES ('2908', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595898418');
INSERT INTO `nms_admin_log` VALUES ('2905', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898360');
INSERT INTO `nms_admin_log` VALUES ('2906', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595898363');
INSERT INTO `nms_admin_log` VALUES ('2907', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898414');
INSERT INTO `nms_admin_log` VALUES ('2904', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595898343');
INSERT INTO `nms_admin_log` VALUES ('2903', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898341');
INSERT INTO `nms_admin_log` VALUES ('2902', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595898335');
INSERT INTO `nms_admin_log` VALUES ('2901', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898333');
INSERT INTO `nms_admin_log` VALUES ('2900', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595898318');
INSERT INTO `nms_admin_log` VALUES ('2899', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595898305');
INSERT INTO `nms_admin_log` VALUES ('2898', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898304');
INSERT INTO `nms_admin_log` VALUES ('2896', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898296');
INSERT INTO `nms_admin_log` VALUES ('2897', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595898297');
INSERT INTO `nms_admin_log` VALUES ('2895', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898224');
INSERT INTO `nms_admin_log` VALUES ('2894', 'moduleid=27&file=index&action=edit&itemid=1', 'fulltimelink', '61.163.87.166', '1595898218');
INSERT INTO `nms_admin_log` VALUES ('2892', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595898190');
INSERT INTO `nms_admin_log` VALUES ('2893', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898216');
INSERT INTO `nms_admin_log` VALUES ('2839', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595471758');
INSERT INTO `nms_admin_log` VALUES ('2838', 'moduleid=26&file=index&action=edit&itemid=91', 'fulltimelink', '61.163.87.166', '1595471752');
INSERT INTO `nms_admin_log` VALUES ('2837', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1595471750');
INSERT INTO `nms_admin_log` VALUES ('2836', 'file=fields&tb=nms_article_25&widget=1', 'fulltimelink', '61.163.87.166', '1595470931');
INSERT INTO `nms_admin_log` VALUES ('2835', 'moduleid=25&file=setting', 'fulltimelink', '61.163.87.166', '1595470930');
INSERT INTO `nms_admin_log` VALUES ('2879', 'rand=91&moduleid=1&file=fields&tb=article_26', 'fulltimelink', '61.163.87.166', '1595558751');
INSERT INTO `nms_admin_log` VALUES ('2878', 'file=fields&tb=article_26&action=add', 'fulltimelink', '61.163.87.166', '1595558720');
INSERT INTO `nms_admin_log` VALUES ('2877', 'file=fields&tb=nms_article_26&widget=1', 'fulltimelink', '61.163.87.166', '1595558697');
INSERT INTO `nms_admin_log` VALUES ('2876', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1595558695');
INSERT INTO `nms_admin_log` VALUES ('2875', 'moduleid=3&file=ad', 'fulltimelink', '61.163.87.166', '1595558690');
INSERT INTO `nms_admin_log` VALUES ('2891', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595898171');
INSERT INTO `nms_admin_log` VALUES ('2890', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595897513');
INSERT INTO `nms_admin_log` VALUES ('2889', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595897511');
INSERT INTO `nms_admin_log` VALUES ('2888', 'moduleid=27&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1595897484');
INSERT INTO `nms_admin_log` VALUES ('2887', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1595897477');
INSERT INTO `nms_admin_log` VALUES ('3022', 'rand=96&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164367');
INSERT INTO `nms_admin_log` VALUES ('3021', 'moduleid=26&file=index&action=delete&itemid=12', 'fulltimelink', '61.163.87.166', '1596164367');
INSERT INTO `nms_admin_log` VALUES ('3020', 'rand=86&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164365');
INSERT INTO `nms_admin_log` VALUES ('3019', 'moduleid=26&file=index&action=delete&itemid=11', 'fulltimelink', '61.163.87.166', '1596164365');
INSERT INTO `nms_admin_log` VALUES ('3018', 'rand=71&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164362');
INSERT INTO `nms_admin_log` VALUES ('3017', 'moduleid=26&file=index&action=delete&itemid=10', 'fulltimelink', '61.163.87.166', '1596164362');
INSERT INTO `nms_admin_log` VALUES ('3016', 'rand=76&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164360');
INSERT INTO `nms_admin_log` VALUES ('3015', 'moduleid=26&file=index&action=delete&itemid=9', 'fulltimelink', '61.163.87.166', '1596164360');
INSERT INTO `nms_admin_log` VALUES ('3014', 'rand=80&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164348');
INSERT INTO `nms_admin_log` VALUES ('3013', 'moduleid=26&file=index&action=delete&itemid=8', 'fulltimelink', '61.163.87.166', '1596164348');
INSERT INTO `nms_admin_log` VALUES ('3012', 'rand=71&moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164345');
INSERT INTO `nms_admin_log` VALUES ('3011', 'moduleid=26&file=index&action=delete&itemid=7', 'fulltimelink', '61.163.87.166', '1596164345');
INSERT INTO `nms_admin_log` VALUES ('3010', 'moduleid=26&sum=53&page=3', 'fulltimelink', '61.163.87.166', '1596164255');
INSERT INTO `nms_admin_log` VALUES ('3009', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1596164252');
INSERT INTO `nms_admin_log` VALUES ('3008', 'moduleid=25&file=index&action=edit&itemid=7', 'fulltimelink', '61.163.87.166', '1596164203');
INSERT INTO `nms_admin_log` VALUES ('3007', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1596164200');
INSERT INTO `nms_admin_log` VALUES ('3006', 'moduleid=25&sum=35', 'fulltimelink', '61.163.87.166', '1596164144');
INSERT INTO `nms_admin_log` VALUES ('3005', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596164141');
INSERT INTO `nms_admin_log` VALUES ('3004', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596164025');
INSERT INTO `nms_admin_log` VALUES ('3003', 'file=category&mid=25', 'fulltimelink', '61.163.87.166', '1596164023');
INSERT INTO `nms_admin_log` VALUES ('3002', 'moduleid=25&file=index&action=edit&itemid=24', 'fulltimelink', '61.163.87.166', '1596164007');
INSERT INTO `nms_admin_log` VALUES ('3001', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596164002');
INSERT INTO `nms_admin_log` VALUES ('3000', 'moduleid=25&file=index&action=edit&itemid=44', 'fulltimelink', '61.163.87.166', '1596163944');
INSERT INTO `nms_admin_log` VALUES ('2999', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596163901');
INSERT INTO `nms_admin_log` VALUES ('2998', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1596163823');
INSERT INTO `nms_admin_log` VALUES ('2997', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596163813');
INSERT INTO `nms_admin_log` VALUES ('2996', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596163804');
INSERT INTO `nms_admin_log` VALUES ('2995', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1596163518');
INSERT INTO `nms_admin_log` VALUES ('2994', 'moduleid=25&file=index&action=edit&itemid=7', 'fulltimelink', '61.163.87.166', '1596163504');
INSERT INTO `nms_admin_log` VALUES ('2993', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1596163495');
INSERT INTO `nms_admin_log` VALUES ('2992', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596163493');
INSERT INTO `nms_admin_log` VALUES ('2991', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596163490');
INSERT INTO `nms_admin_log` VALUES ('2990', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596162493');
INSERT INTO `nms_admin_log` VALUES ('2989', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596162491');
INSERT INTO `nms_admin_log` VALUES ('2988', 'moduleid=25&file=index&action=edit&itemid=34', 'fulltimelink', '61.163.87.166', '1596160848');
INSERT INTO `nms_admin_log` VALUES ('2987', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596160845');
INSERT INTO `nms_admin_log` VALUES ('2986', 'moduleid=25&file=index&action=edit&itemid=8', 'fulltimelink', '61.163.87.166', '1596160637');
INSERT INTO `nms_admin_log` VALUES ('2985', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596160634');
INSERT INTO `nms_admin_log` VALUES ('2984', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596160632');
INSERT INTO `nms_admin_log` VALUES ('2983', 'moduleid=25&file=index&action=edit&itemid=8', 'fulltimelink', '61.163.87.166', '1596160619');
INSERT INTO `nms_admin_log` VALUES ('2982', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596160611');
INSERT INTO `nms_admin_log` VALUES ('2981', 'moduleid=25&sum=35', 'fulltimelink', '61.163.87.166', '1596160592');
INSERT INTO `nms_admin_log` VALUES ('2980', 'moduleid=25&sum=35&page=2', 'fulltimelink', '61.163.87.166', '1596160578');
INSERT INTO `nms_admin_log` VALUES ('2979', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1596160561');
INSERT INTO `nms_admin_log` VALUES ('2978', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1596160557');
INSERT INTO `nms_admin_log` VALUES ('3324', 'rand=24&file=category&mid=30&parentid=30', 'fulltimelink', '61.163.87.166', '1596335562');
INSERT INTO `nms_admin_log` VALUES ('3316', 'mid=30&file=category&parentid=30', 'fulltimelink', '61.163.87.166', '1596335490');
INSERT INTO `nms_admin_log` VALUES ('3315', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596335481');
INSERT INTO `nms_admin_log` VALUES ('3314', 'file=category&action=edit&mid=30&catid=61', 'fulltimelink', '61.163.87.166', '1596335476');
INSERT INTO `nms_admin_log` VALUES ('3313', 'rand=18&file=category&mid=30&parentid=29', 'fulltimelink', '61.163.87.166', '1596335472');
INSERT INTO `nms_admin_log` VALUES ('3312', 'file=category&action=edit&mid=30&catid=62', 'fulltimelink', '61.163.87.166', '1596335455');
INSERT INTO `nms_admin_log` VALUES ('3311', 'rand=70&file=category&mid=30&parentid=29', 'fulltimelink', '61.163.87.166', '1596335452');
INSERT INTO `nms_admin_log` VALUES ('3310', 'file=category&action=edit&mid=30&catid=61', 'fulltimelink', '61.163.87.166', '1596335441');
INSERT INTO `nms_admin_log` VALUES ('3309', 'rand=60&file=category&mid=30&parentid=29', 'fulltimelink', '61.163.87.166', '1596335438');
INSERT INTO `nms_admin_log` VALUES ('3308', 'file=category&action=edit&mid=30&catid=60', 'fulltimelink', '61.163.87.166', '1596335422');
INSERT INTO `nms_admin_log` VALUES ('3307', 'rand=53&file=category&mid=30&parentid=29', 'fulltimelink', '61.163.87.166', '1596335417');
INSERT INTO `nms_admin_log` VALUES ('3306', 'file=category&action=edit&mid=30&catid=59', 'fulltimelink', '61.163.87.166', '1596335293');
INSERT INTO `nms_admin_log` VALUES ('3305', 'mid=30&file=category&parentid=29', 'fulltimelink', '61.163.87.166', '1596335289');
INSERT INTO `nms_admin_log` VALUES ('3304', 'rand=18&file=category&mid=30&parentid=28', 'fulltimelink', '61.163.87.166', '1596335283');
INSERT INTO `nms_admin_log` VALUES ('3303', 'file=category&action=edit&mid=30&catid=58', 'fulltimelink', '61.163.87.166', '1596335273');
INSERT INTO `nms_admin_log` VALUES ('3302', 'rand=38&file=category&mid=30&parentid=28', 'fulltimelink', '61.163.87.166', '1596335270');
INSERT INTO `nms_admin_log` VALUES ('3301', 'file=category&action=edit&mid=30&catid=57', 'fulltimelink', '61.163.87.166', '1596335259');
INSERT INTO `nms_admin_log` VALUES ('3300', 'rand=52&file=category&mid=30&parentid=28', 'fulltimelink', '61.163.87.166', '1596335257');
INSERT INTO `nms_admin_log` VALUES ('3299', 'file=category&action=edit&mid=30&catid=56', 'fulltimelink', '61.163.87.166', '1596335246');
INSERT INTO `nms_admin_log` VALUES ('3298', 'rand=24&file=category&mid=30&parentid=28', 'fulltimelink', '61.163.87.166', '1596335242');
INSERT INTO `nms_admin_log` VALUES ('3297', 'file=category&action=edit&mid=30&catid=55', 'fulltimelink', '61.163.87.166', '1596335160');
INSERT INTO `nms_admin_log` VALUES ('3296', 'mid=30&file=category&parentid=28', 'fulltimelink', '61.163.87.166', '1596335156');
INSERT INTO `nms_admin_log` VALUES ('3295', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596335149');
INSERT INTO `nms_admin_log` VALUES ('3294', 'rand=97&file=category&mid=30&parentid=27', 'fulltimelink', '61.163.87.166', '1596335142');
INSERT INTO `nms_admin_log` VALUES ('3293', 'file=category&action=edit&mid=30&catid=54', 'fulltimelink', '61.163.87.166', '1596335114');
INSERT INTO `nms_admin_log` VALUES ('3292', 'rand=13&file=category&mid=30&parentid=27', 'fulltimelink', '61.163.87.166', '1596335111');
INSERT INTO `nms_admin_log` VALUES ('3291', 'file=category&action=edit&mid=30&catid=53', 'fulltimelink', '61.163.87.166', '1596335096');
INSERT INTO `nms_admin_log` VALUES ('3290', 'rand=63&file=category&mid=30&parentid=27', 'fulltimelink', '61.163.87.166', '1596335093');
INSERT INTO `nms_admin_log` VALUES ('3289', 'file=category&action=edit&mid=30&catid=52', 'fulltimelink', '61.163.87.166', '1596335077');
INSERT INTO `nms_admin_log` VALUES ('3288', 'mid=30&file=category&parentid=27', 'fulltimelink', '61.163.87.166', '1596335074');
INSERT INTO `nms_admin_log` VALUES ('3287', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596335058');
INSERT INTO `nms_admin_log` VALUES ('3286', 'file=category&action=edit&mid=30&catid=52', 'fulltimelink', '61.163.87.166', '1596335049');
INSERT INTO `nms_admin_log` VALUES ('3285', 'rand=67&file=category&mid=30&parentid=27', 'fulltimelink', '61.163.87.166', '1596335043');
INSERT INTO `nms_admin_log` VALUES ('3284', 'file=category&action=edit&mid=30&catid=51', 'fulltimelink', '61.163.87.166', '1596335019');
INSERT INTO `nms_admin_log` VALUES ('3283', 'rand=61&file=category&mid=30&parentid=27', 'fulltimelink', '61.163.87.166', '1596335015');
INSERT INTO `nms_admin_log` VALUES ('3282', 'file=category&action=edit&mid=30&catid=50', 'fulltimelink', '61.163.87.166', '1596334986');
INSERT INTO `nms_admin_log` VALUES ('3281', 'mid=30&file=category&parentid=27', 'fulltimelink', '61.163.87.166', '1596334975');
INSERT INTO `nms_admin_log` VALUES ('3280', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596334965');
INSERT INTO `nms_admin_log` VALUES ('3279', 'rand=51&file=category&mid=30&parentid=26', 'fulltimelink', '61.163.87.166', '1596334957');
INSERT INTO `nms_admin_log` VALUES ('3278', 'file=category&action=edit&mid=30&catid=49', 'fulltimelink', '61.163.87.166', '1596334947');
INSERT INTO `nms_admin_log` VALUES ('3277', 'rand=57&file=category&mid=30&parentid=26', 'fulltimelink', '61.163.87.166', '1596334944');
INSERT INTO `nms_admin_log` VALUES ('3276', 'file=category&action=edit&mid=30&catid=48', 'fulltimelink', '61.163.87.166', '1596334931');
INSERT INTO `nms_admin_log` VALUES ('3275', 'rand=80&file=category&mid=30&parentid=26', 'fulltimelink', '61.163.87.166', '1596334927');
INSERT INTO `nms_admin_log` VALUES ('3274', 'file=category&action=edit&mid=30&catid=47', 'fulltimelink', '61.163.87.166', '1596334910');
INSERT INTO `nms_admin_log` VALUES ('3273', 'rand=56&file=category&mid=30&parentid=26', 'fulltimelink', '61.163.87.166', '1596334906');
INSERT INTO `nms_admin_log` VALUES ('3272', 'file=category&action=edit&mid=30&catid=46', 'fulltimelink', '61.163.87.166', '1596334875');
INSERT INTO `nms_admin_log` VALUES ('3271', 'mid=30&file=category&parentid=26', 'fulltimelink', '61.163.87.166', '1596334871');
INSERT INTO `nms_admin_log` VALUES ('3270', 'rand=73&file=category&mid=30&parentid=25', 'fulltimelink', '61.163.87.166', '1596334865');
INSERT INTO `nms_admin_log` VALUES ('3269', 'file=category&action=edit&mid=30&catid=45', 'fulltimelink', '61.163.87.166', '1596334851');
INSERT INTO `nms_admin_log` VALUES ('3268', 'rand=34&file=category&mid=30&parentid=25', 'fulltimelink', '61.163.87.166', '1596334849');
INSERT INTO `nms_admin_log` VALUES ('3267', 'file=category&action=edit&mid=30&catid=44', 'fulltimelink', '61.163.87.166', '1596334836');
INSERT INTO `nms_admin_log` VALUES ('3266', 'rand=70&file=category&mid=30&parentid=25', 'fulltimelink', '61.163.87.166', '1596334833');
INSERT INTO `nms_admin_log` VALUES ('3265', 'file=category&action=edit&mid=30&catid=43', 'fulltimelink', '61.163.87.166', '1596334820');
INSERT INTO `nms_admin_log` VALUES ('3264', 'rand=74&file=category&mid=30&parentid=25', 'fulltimelink', '61.163.87.166', '1596334817');
INSERT INTO `nms_admin_log` VALUES ('3263', 'file=category&action=edit&mid=30&catid=42', 'fulltimelink', '61.163.87.166', '1596334792');
INSERT INTO `nms_admin_log` VALUES ('3262', 'mid=30&file=category&parentid=25', 'fulltimelink', '61.163.87.166', '1596334788');
INSERT INTO `nms_admin_log` VALUES ('3261', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596334514');
INSERT INTO `nms_admin_log` VALUES ('3260', 'file=category&action=edit&mid=30&catid=42', 'fulltimelink', '61.163.87.166', '1596334323');
INSERT INTO `nms_admin_log` VALUES ('3259', 'mid=30&file=category&parentid=25', 'fulltimelink', '61.163.87.166', '1596334309');
INSERT INTO `nms_admin_log` VALUES ('3258', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596333905');
INSERT INTO `nms_admin_log` VALUES ('3257', 'file=category&action=edit&mid=30&catid=41', 'fulltimelink', '61.163.87.166', '1596333891');
INSERT INTO `nms_admin_log` VALUES ('3256', 'rand=47&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333880');
INSERT INTO `nms_admin_log` VALUES ('3255', 'file=category&action=edit&mid=30&catid=41', 'fulltimelink', '61.163.87.166', '1596333866');
INSERT INTO `nms_admin_log` VALUES ('3254', 'rand=83&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333863');
INSERT INTO `nms_admin_log` VALUES ('3253', 'file=category&action=edit&mid=30&catid=40', 'fulltimelink', '61.163.87.166', '1596333850');
INSERT INTO `nms_admin_log` VALUES ('3252', 'rand=36&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333847');
INSERT INTO `nms_admin_log` VALUES ('3251', 'file=category&action=edit&mid=30&catid=39', 'fulltimelink', '61.163.87.166', '1596333837');
INSERT INTO `nms_admin_log` VALUES ('3250', 'rand=71&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333833');
INSERT INTO `nms_admin_log` VALUES ('3249', 'file=category&action=edit&mid=30&catid=38', 'fulltimelink', '61.163.87.166', '1596333822');
INSERT INTO `nms_admin_log` VALUES ('3248', 'rand=93&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333819');
INSERT INTO `nms_admin_log` VALUES ('3247', 'file=category&action=edit&mid=30&catid=37', 'fulltimelink', '61.163.87.166', '1596333809');
INSERT INTO `nms_admin_log` VALUES ('3246', 'rand=73&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333804');
INSERT INTO `nms_admin_log` VALUES ('3245', 'file=category&action=edit&mid=30&catid=36', 'fulltimelink', '61.163.87.166', '1596333793');
INSERT INTO `nms_admin_log` VALUES ('3244', 'rand=65&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333789');
INSERT INTO `nms_admin_log` VALUES ('3243', 'file=category&action=edit&mid=30&catid=35', 'fulltimelink', '61.163.87.166', '1596333776');
INSERT INTO `nms_admin_log` VALUES ('3242', 'rand=31&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596333771');
INSERT INTO `nms_admin_log` VALUES ('3241', 'file=category&action=edit&mid=30&catid=34', 'fulltimelink', '61.163.87.166', '1596333752');
INSERT INTO `nms_admin_log` VALUES ('3240', 'mid=30&file=category&parentid=24', 'fulltimelink', '61.163.87.166', '1596333749');
INSERT INTO `nms_admin_log` VALUES ('3239', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596333740');
INSERT INTO `nms_admin_log` VALUES ('3238', 'file=category&action=edit&mid=30&catid=34', 'fulltimelink', '61.163.87.166', '1596333349');
INSERT INTO `nms_admin_log` VALUES ('3237', 'mid=30&file=category&parentid=24', 'fulltimelink', '61.163.87.166', '1596333343');
INSERT INTO `nms_admin_log` VALUES ('3236', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596333334');
INSERT INTO `nms_admin_log` VALUES ('3235', 'rand=10&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596333183');
INSERT INTO `nms_admin_log` VALUES ('3234', 'file=category&action=edit&mid=30&catid=23', 'fulltimelink', '61.163.87.166', '1596333172');
INSERT INTO `nms_admin_log` VALUES ('3233', 'rand=42&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596333169');
INSERT INTO `nms_admin_log` VALUES ('3232', 'file=category&action=edit&mid=30&catid=22', 'fulltimelink', '61.163.87.166', '1596333157');
INSERT INTO `nms_admin_log` VALUES ('3231', 'rand=62&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596333144');
INSERT INTO `nms_admin_log` VALUES ('3230', 'file=category&action=edit&mid=30&catid=21', 'fulltimelink', '61.163.87.166', '1596333114');
INSERT INTO `nms_admin_log` VALUES ('3229', 'rand=84&file=category&mid=30&parentid=0', 'fulltimelink', '61.163.87.166', '1596333105');
INSERT INTO `nms_admin_log` VALUES ('3228', 'file=category&action=edit&mid=30&catid=20', 'fulltimelink', '61.163.87.166', '1596333049');
INSERT INTO `nms_admin_log` VALUES ('3227', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596333046');
INSERT INTO `nms_admin_log` VALUES ('3226', 'file=category&action=edit&mid=30&catid=20', 'fulltimelink', '61.163.87.166', '1596333030');
INSERT INTO `nms_admin_log` VALUES ('3225', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596333028');
INSERT INTO `nms_admin_log` VALUES ('3224', 'rand=32&moduleid=30&file=setting&tab=0', 'fulltimelink', '61.163.87.166', '1596333025');
INSERT INTO `nms_admin_log` VALUES ('3223', 'moduleid=30&file=setting', 'fulltimelink', '61.163.87.166', '1596333016');
INSERT INTO `nms_admin_log` VALUES ('3222', 'file=category&action=edit&mid=30&catid=20', 'fulltimelink', '61.163.87.166', '1596332926');
INSERT INTO `nms_admin_log` VALUES ('2084', 'moduleid=3&file=webpage&action=edit&itemid=5&item=1', 'fulltimelink', '61.163.87.166', '1593830245');
INSERT INTO `nms_admin_log` VALUES ('2085', 'moduleid=3&file=webpage', 'fulltimelink', '61.163.87.166', '1593830268');
INSERT INTO `nms_admin_log` VALUES ('2086', 'file=database', 'fulltimelink', '61.163.87.166', '1593830273');
INSERT INTO `nms_admin_log` VALUES ('2087', 'file=tag&action=dict&table=nms_admin&note=%E7%AE%A1%E7%90%86%E5%91%98&widget=1', 'fulltimelink', '61.163.87.166', '1593830276');
INSERT INTO `nms_admin_log` VALUES ('2088', 'file=patch', 'fulltimelink', '61.163.87.166', '1593830279');
INSERT INTO `nms_admin_log` VALUES ('2089', 'file=template', 'fulltimelink', '61.163.87.166', '1593830281');
INSERT INTO `nms_admin_log` VALUES ('2090', 'file=cron', 'fulltimelink', '61.163.87.166', '1593830282');
INSERT INTO `nms_admin_log` VALUES ('2091', 'file=upload', 'fulltimelink', '61.163.87.166', '1593830285');
INSERT INTO `nms_admin_log` VALUES ('2092', 'file=keyword', 'fulltimelink', '61.163.87.166', '1593830287');
INSERT INTO `nms_admin_log` VALUES ('2093', 'file=404', 'fulltimelink', '61.163.87.166', '1593830289');
INSERT INTO `nms_admin_log` VALUES ('2094', 'file=cate', 'fulltimelink', '61.163.87.166', '1593830299');
INSERT INTO `nms_admin_log` VALUES ('2095', 'file=count', 'fulltimelink', '61.163.87.166', '1593830301');
INSERT INTO `nms_admin_log` VALUES ('2096', 'file=count&action=js', 'fulltimelink', '61.163.87.166', '1593830301');
INSERT INTO `nms_admin_log` VALUES ('2097', 'file=database', 'fulltimelink', '61.163.87.166', '1593830303');
INSERT INTO `nms_admin_log` VALUES ('2098', 'file=patch', 'fulltimelink', '61.163.87.166', '1593830304');
INSERT INTO `nms_admin_log` VALUES ('2099', 'file=template', 'fulltimelink', '61.163.87.166', '1593830305');
INSERT INTO `nms_admin_log` VALUES ('2100', 'file=cron', 'fulltimelink', '61.163.87.166', '1593830306');
INSERT INTO `nms_admin_log` VALUES ('2101', 'file=upload', 'fulltimelink', '61.163.87.166', '1593830307');
INSERT INTO `nms_admin_log` VALUES ('2102', 'file=keyword', 'fulltimelink', '61.163.87.166', '1593830308');
INSERT INTO `nms_admin_log` VALUES ('2103', 'file=404', 'fulltimelink', '61.163.87.166', '1593830310');
INSERT INTO `nms_admin_log` VALUES ('2104', 'file=doctor', 'fulltimelink', '61.163.87.166', '1593830313');
INSERT INTO `nms_admin_log` VALUES ('2105', 'moduleid=25', 'recycle', '61.163.87.166', '1593830347');
INSERT INTO `nms_admin_log` VALUES ('2106', 'moduleid=26', 'recycle', '61.163.87.166', '1593830348');
INSERT INTO `nms_admin_log` VALUES ('2107', 'moduleid=26&action=revieworder', 'recycle', '61.163.87.166', '1593830351');
INSERT INTO `nms_admin_log` VALUES ('2108', 'moduleid=26', 'recycle', '61.163.87.166', '1593830352');
INSERT INTO `nms_admin_log` VALUES ('2109', 'moduleid=27', 'recycle', '61.163.87.166', '1593830357');
INSERT INTO `nms_admin_log` VALUES ('2110', 'moduleid=28', 'recycle', '61.163.87.166', '1593830360');
INSERT INTO `nms_admin_log` VALUES ('2111', 'moduleid=29', 'recycle', '61.163.87.166', '1593830361');
INSERT INTO `nms_admin_log` VALUES ('2112', 'moduleid=26&file=setting', 'recycle', '61.163.87.166', '1593830363');
INSERT INTO `nms_admin_log` VALUES ('2113', 'file=admin', 'fulltimelink', '61.163.87.166', '1593830413');
INSERT INTO `nms_admin_log` VALUES ('2114', 'file=admin&action=right&userid=5&widget=1', 'fulltimelink', '61.163.87.166', '1593830417');
INSERT INTO `nms_admin_log` VALUES ('2115', 'rand=29&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593830440');
INSERT INTO `nms_admin_log` VALUES ('2116', 'rand=75&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593830451');
INSERT INTO `nms_admin_log` VALUES ('2117', 'rand=35&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593830493');
INSERT INTO `nms_admin_log` VALUES ('2118', 'rand=48&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593830503');
INSERT INTO `nms_admin_log` VALUES ('2119', 'rand=87&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593830511');
INSERT INTO `nms_admin_log` VALUES ('2120', 'rand=47&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593830518');
INSERT INTO `nms_admin_log` VALUES ('2121', 'rand=31&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593830529');
INSERT INTO `nms_admin_log` VALUES ('2122', 'file=404', 'recycle', '61.163.87.166', '1593830538');
INSERT INTO `nms_admin_log` VALUES ('2123', 'moduleid=25', 'recycle', '61.163.87.166', '1593830543');
INSERT INTO `nms_admin_log` VALUES ('2124', 'file=mymenu', 'recycle', '61.163.87.166', '1593830560');
INSERT INTO `nms_admin_log` VALUES ('2125', 'mid=24&file=category', 'recycle', '61.163.87.166', '1593830578');
INSERT INTO `nms_admin_log` VALUES ('2126', 'file=search&widget=1', 'recycle', '61.163.87.166', '1593830640');
INSERT INTO `nms_admin_log` VALUES ('2127', 'file=cloud&action=doc&mfa=destoon-category-&widget=1', 'recycle', '61.163.87.166', '1593830647');
INSERT INTO `nms_admin_log` VALUES ('2128', 'file=admin', 'fulltimelink', '61.163.87.166', '1593831366');
INSERT INTO `nms_admin_log` VALUES ('2129', 'file=setting', 'fulltimelink', '61.163.87.166', '1593833717');
INSERT INTO `nms_admin_log` VALUES ('2130', 'moduleid=3&file=ad', 'fulltimelink', '61.163.87.166', '1593833837');
INSERT INTO `nms_admin_log` VALUES ('2131', 'moduleid=25&action=add', 'fulltimelink', '61.163.87.166', '1593846128');
INSERT INTO `nms_admin_log` VALUES ('2132', 'file=module', 'fulltimelink', '61.163.87.166', '1593846707');
INSERT INTO `nms_admin_log` VALUES ('2133', 'file=fields&tb=nms_article_25&widget=1', 'fulltimelink', '61.163.87.166', '1593846716');
INSERT INTO `nms_admin_log` VALUES ('2134', 'file=module', 'fulltimelink', '61.163.87.166', '1593847544');
INSERT INTO `nms_admin_log` VALUES ('2135', 'file=fields&tb=nms_article_25&widget=1', 'fulltimelink', '61.163.87.166', '1593847546');
INSERT INTO `nms_admin_log` VALUES ('2136', 'moduleid=1&file=fields&tb=article_25&action=edit&itemid=36', 'fulltimelink', '61.163.87.166', '1593847549');
INSERT INTO `nms_admin_log` VALUES ('2137', 'moduleid=25', 'recycle', '115.60.19.9', '1593850056');
INSERT INTO `nms_admin_log` VALUES ('2138', 'moduleid=25&action=check', 'recycle', '115.60.19.9', '1593850063');
INSERT INTO `nms_admin_log` VALUES ('2139', 'moduleid=25&file=index&action=edit&itemid=14', 'recycle', '115.60.19.9', '1593850068');
INSERT INTO `nms_admin_log` VALUES ('2140', 'file=module', 'fulltimelink', '61.163.87.166', '1593850079');
INSERT INTO `nms_admin_log` VALUES ('2141', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593851379');
INSERT INTO `nms_admin_log` VALUES ('2142', 'moduleid=25&file=index&action=edit&itemid=13', 'fulltimelink', '61.163.87.166', '1593851392');
INSERT INTO `nms_admin_log` VALUES ('2143', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593851405');
INSERT INTO `nms_admin_log` VALUES ('2144', 'moduleid=25&file=index&action=edit&itemid=13', 'fulltimelink', '61.163.87.166', '1593851414');
INSERT INTO `nms_admin_log` VALUES ('2145', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593851424');
INSERT INTO `nms_admin_log` VALUES ('2146', 'moduleid=25&file=index&action=edit&itemid=5', 'fulltimelink', '61.163.87.166', '1593851430');
INSERT INTO `nms_admin_log` VALUES ('2147', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593851434');
INSERT INTO `nms_admin_log` VALUES ('2148', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593851438');
INSERT INTO `nms_admin_log` VALUES ('2149', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593851442');
INSERT INTO `nms_admin_log` VALUES ('2150', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593851443');
INSERT INTO `nms_admin_log` VALUES ('2151', 'moduleid=25&file=index&action=edit&itemid=15', 'fulltimelink', '61.163.87.166', '1593851448');
INSERT INTO `nms_admin_log` VALUES ('2152', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593851469');
INSERT INTO `nms_admin_log` VALUES ('2153', 'moduleid=25&file=index&action=edit&itemid=14', 'fulltimelink', '61.163.87.166', '1593851479');
INSERT INTO `nms_admin_log` VALUES ('2154', 'moduleid=25', 'recycle', '115.60.19.9', '1593851479');
INSERT INTO `nms_admin_log` VALUES ('2155', 'moduleid=26', 'recycle', '115.60.19.9', '1593851487');
INSERT INTO `nms_admin_log` VALUES ('2156', 'moduleid=26&file=index&action=delete&itemid=40', 'recycle', '115.60.19.9', '1593851504');
INSERT INTO `nms_admin_log` VALUES ('2157', 'rand=50&moduleid=26', 'recycle', '115.60.19.9', '1593851504');
INSERT INTO `nms_admin_log` VALUES ('2158', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593851608');
INSERT INTO `nms_admin_log` VALUES ('2159', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593851630');
INSERT INTO `nms_admin_log` VALUES ('2160', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593851632');
INSERT INTO `nms_admin_log` VALUES ('2161', 'moduleid=25&file=index&action=edit&itemid=15', 'fulltimelink', '61.163.87.166', '1593851646');
INSERT INTO `nms_admin_log` VALUES ('2162', 'rand=54&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593851650');
INSERT INTO `nms_admin_log` VALUES ('2163', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593851652');
INSERT INTO `nms_admin_log` VALUES ('2164', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593851885');
INSERT INTO `nms_admin_log` VALUES ('2165', 'moduleid=25&action=add', 'fulltimelink', '61.163.87.166', '1593851886');
INSERT INTO `nms_admin_log` VALUES ('2166', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593852020');
INSERT INTO `nms_admin_log` VALUES ('2167', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593852025');
INSERT INTO `nms_admin_log` VALUES ('2168', 'moduleid=25&file=index&action=edit&itemid=17', 'fulltimelink', '61.163.87.166', '1593852029');
INSERT INTO `nms_admin_log` VALUES ('2169', 'moduleid=26', 'recycle', '115.60.19.9', '1593852176');
INSERT INTO `nms_admin_log` VALUES ('2170', 'moduleid=26&file=index&action=delete&itemid=41', 'recycle', '115.60.19.9', '1593852181');
INSERT INTO `nms_admin_log` VALUES ('2171', 'rand=93&moduleid=26', 'recycle', '115.60.19.9', '1593852181');
INSERT INTO `nms_admin_log` VALUES ('2172', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593852207');
INSERT INTO `nms_admin_log` VALUES ('2173', 'moduleid=25&file=index&action=edit&itemid=9', 'fulltimelink', '61.163.87.166', '1593852215');
INSERT INTO `nms_admin_log` VALUES ('2174', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593852225');
INSERT INTO `nms_admin_log` VALUES ('2175', 'moduleid=26', 'recycle', '115.60.19.9', '1593852307');
INSERT INTO `nms_admin_log` VALUES ('2176', 'moduleid=26&file=index&action=delete&itemid=42', 'recycle', '115.60.19.9', '1593852310');
INSERT INTO `nms_admin_log` VALUES ('2177', 'rand=23&moduleid=26', 'recycle', '115.60.19.9', '1593852310');
INSERT INTO `nms_admin_log` VALUES ('2178', 'moduleid=26', 'recycle', '115.60.19.9', '1593852487');
INSERT INTO `nms_admin_log` VALUES ('2179', 'moduleid=26&file=index&action=delete&itemid=43', 'recycle', '115.60.19.9', '1593852492');
INSERT INTO `nms_admin_log` VALUES ('2180', 'rand=41&moduleid=26', 'recycle', '115.60.19.9', '1593852492');
INSERT INTO `nms_admin_log` VALUES ('2181', 'moduleid=26', 'recycle', '115.60.19.9', '1593852498');
INSERT INTO `nms_admin_log` VALUES ('2182', 'moduleid=26&file=index&action=delete&itemid=44', 'recycle', '115.60.19.9', '1593852587');
INSERT INTO `nms_admin_log` VALUES ('2183', 'rand=59&moduleid=26', 'recycle', '115.60.19.9', '1593852587');
INSERT INTO `nms_admin_log` VALUES ('2184', 'moduleid=26', 'recycle', '115.60.19.9', '1593853365');
INSERT INTO `nms_admin_log` VALUES ('2185', 'moduleid=26&file=index&action=delete&itemid=45', 'recycle', '115.60.19.9', '1593853371');
INSERT INTO `nms_admin_log` VALUES ('2186', 'rand=50&moduleid=26', 'recycle', '115.60.19.9', '1593853371');
INSERT INTO `nms_admin_log` VALUES ('2187', 'moduleid=26', 'recycle', '115.60.19.9', '1593853640');
INSERT INTO `nms_admin_log` VALUES ('2188', 'moduleid=26&file=index&action=delete&itemid=46', 'recycle', '115.60.19.9', '1593853644');
INSERT INTO `nms_admin_log` VALUES ('2189', 'rand=22&moduleid=26', 'recycle', '115.60.19.9', '1593853644');
INSERT INTO `nms_admin_log` VALUES ('2190', 'moduleid=26', 'recycle', '115.60.19.9', '1593853724');
INSERT INTO `nms_admin_log` VALUES ('2191', 'moduleid=26&file=index&action=delete&itemid=47', 'recycle', '115.60.19.9', '1593853727');
INSERT INTO `nms_admin_log` VALUES ('2192', 'rand=62&moduleid=26', 'recycle', '115.60.19.9', '1593853728');
INSERT INTO `nms_admin_log` VALUES ('2193', 'moduleid=26', 'recycle', '115.60.19.9', '1593853937');
INSERT INTO `nms_admin_log` VALUES ('2194', 'moduleid=26&file=index&action=delete&itemid=48', 'recycle', '115.60.19.9', '1593853943');
INSERT INTO `nms_admin_log` VALUES ('2195', 'rand=86&moduleid=26', 'recycle', '115.60.19.9', '1593853943');
INSERT INTO `nms_admin_log` VALUES ('2196', 'moduleid=25', 'recycle', '115.60.19.9', '1593854124');
INSERT INTO `nms_admin_log` VALUES ('2197', 'moduleid=25&file=index&action=edit&itemid=6', 'recycle', '115.60.19.9', '1593854127');
INSERT INTO `nms_admin_log` VALUES ('2198', 'rand=23&moduleid=25', 'recycle', '115.60.19.9', '1593854134');
INSERT INTO `nms_admin_log` VALUES ('2199', 'moduleid=25&action=check', 'recycle', '115.60.19.9', '1593854140');
INSERT INTO `nms_admin_log` VALUES ('2200', 'moduleid=25&file=index&action=edit&itemid=11', 'recycle', '115.60.19.9', '1593854142');
INSERT INTO `nms_admin_log` VALUES ('2201', 'rand=86&moduleid=25&action=check', 'recycle', '115.60.19.9', '1593854145');
INSERT INTO `nms_admin_log` VALUES ('2202', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593854690');
INSERT INTO `nms_admin_log` VALUES ('2203', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593854727');
INSERT INTO `nms_admin_log` VALUES ('2204', 'moduleid=25&file=index&action=edit&itemid=14', 'fulltimelink', '61.163.87.166', '1593854735');
INSERT INTO `nms_admin_log` VALUES ('2205', 'rand=66&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593854750');
INSERT INTO `nms_admin_log` VALUES ('2206', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593854752');
INSERT INTO `nms_admin_log` VALUES ('2207', 'moduleid=25&file=index&action=edit&itemid=7', 'fulltimelink', '61.163.87.166', '1593854765');
INSERT INTO `nms_admin_log` VALUES ('2208', 'rand=15&moduleid=25', 'fulltimelink', '61.163.87.166', '1593854777');
INSERT INTO `nms_admin_log` VALUES ('2209', 'moduleid=26', 'recycle', '115.60.19.9', '1593854944');
INSERT INTO `nms_admin_log` VALUES ('2210', 'moduleid=26&file=index&action=delete&itemid=51', 'recycle', '115.60.19.9', '1593854955');
INSERT INTO `nms_admin_log` VALUES ('2211', 'rand=56&moduleid=26', 'recycle', '115.60.19.9', '1593854955');
INSERT INTO `nms_admin_log` VALUES ('2212', 'moduleid=25', 'recycle', '115.60.19.9', '1593855052');
INSERT INTO `nms_admin_log` VALUES ('2213', 'moduleid=26', 'recycle', '115.60.19.9', '1593855053');
INSERT INTO `nms_admin_log` VALUES ('2214', 'moduleid=26&file=index&action=delete&itemid=50', 'recycle', '115.60.19.9', '1593855056');
INSERT INTO `nms_admin_log` VALUES ('2215', 'rand=44&moduleid=26', 'recycle', '115.60.19.9', '1593855056');
INSERT INTO `nms_admin_log` VALUES ('2216', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593855251');
INSERT INTO `nms_admin_log` VALUES ('2217', 'moduleid=25&file=index&action=edit&itemid=14', 'fulltimelink', '61.163.87.166', '1593855256');
INSERT INTO `nms_admin_log` VALUES ('2218', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593855261');
INSERT INTO `nms_admin_log` VALUES ('2219', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593855360');
INSERT INTO `nms_admin_log` VALUES ('2220', 'moduleid=26&file=index&action=edit&itemid=52', 'fulltimelink', '61.163.87.166', '1593855433');
INSERT INTO `nms_admin_log` VALUES ('2221', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593855436');
INSERT INTO `nms_admin_log` VALUES ('2222', 'moduleid=26&sum=21&page=2', 'fulltimelink', '61.163.87.166', '1593855493');
INSERT INTO `nms_admin_log` VALUES ('2223', 'moduleid=26&sum=21', 'fulltimelink', '61.163.87.166', '1593855495');
INSERT INTO `nms_admin_log` VALUES ('2224', 'moduleid=26', 'recycle', '115.60.19.9', '1593855530');
INSERT INTO `nms_admin_log` VALUES ('2225', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593855553');
INSERT INTO `nms_admin_log` VALUES ('2226', 'moduleid=26&file=index&action=edit&itemid=27', 'fulltimelink', '61.163.87.166', '1593855568');
INSERT INTO `nms_admin_log` VALUES ('2227', 'rand=21&moduleid=26', 'fulltimelink', '61.163.87.166', '1593855591');
INSERT INTO `nms_admin_log` VALUES ('2228', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593855598');
INSERT INTO `nms_admin_log` VALUES ('2229', 'moduleid=26&file=index&action=edit&itemid=27', 'fulltimelink', '61.163.87.166', '1593855617');
INSERT INTO `nms_admin_log` VALUES ('2230', 'rand=31&moduleid=26', 'fulltimelink', '61.163.87.166', '1593855626');
INSERT INTO `nms_admin_log` VALUES ('2231', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593855684');
INSERT INTO `nms_admin_log` VALUES ('2232', 'moduleid=26&action=&fields=1&orderkey=SUN&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&itemid=0', 'fulltimelink', '61.163.87.166', '1593855708');
INSERT INTO `nms_admin_log` VALUES ('2233', 'moduleid=26&file=index&action=delete&itemid=22', 'fulltimelink', '61.163.87.166', '1593855733');
INSERT INTO `nms_admin_log` VALUES ('2234', 'rand=56&moduleid=26&action=&fields=1&orderkey=SUN&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&itemid=0', 'fulltimelink', '61.163.87.166', '1593855733');
INSERT INTO `nms_admin_log` VALUES ('2235', 'moduleid=26&file=index&action=delete&itemid=23', 'fulltimelink', '61.163.87.166', '1593855735');
INSERT INTO `nms_admin_log` VALUES ('2236', 'rand=48&moduleid=26&action=&fields=1&orderkey=SUN&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&itemid=0', 'fulltimelink', '61.163.87.166', '1593855735');
INSERT INTO `nms_admin_log` VALUES ('2237', 'moduleid=26&file=index&action=delete&itemid=27', 'fulltimelink', '61.163.87.166', '1593855737');
INSERT INTO `nms_admin_log` VALUES ('2238', 'rand=13&moduleid=26&action=&fields=1&orderkey=SUN&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&itemid=0', 'fulltimelink', '61.163.87.166', '1593855737');
INSERT INTO `nms_admin_log` VALUES ('2239', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593855738');
INSERT INTO `nms_admin_log` VALUES ('2240', 'moduleid=26&file=index&action=edit&itemid=53', 'fulltimelink', '61.163.87.166', '1593856219');
INSERT INTO `nms_admin_log` VALUES ('2241', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593856226');
INSERT INTO `nms_admin_log` VALUES ('2242', 'moduleid=26&file=index&action=edit&itemid=54', 'fulltimelink', '61.163.87.166', '1593856650');
INSERT INTO `nms_admin_log` VALUES ('2243', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593856667');
INSERT INTO `nms_admin_log` VALUES ('2244', 'moduleid=26&file=index&action=edit&itemid=54', 'fulltimelink', '61.163.87.166', '1593856734');
INSERT INTO `nms_admin_log` VALUES ('2245', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593856736');
INSERT INTO `nms_admin_log` VALUES ('2246', 'moduleid=26&file=index&action=edit&itemid=52', 'recycle', '115.60.19.9', '1593857250');
INSERT INTO `nms_admin_log` VALUES ('2247', 'rand=81&moduleid=26', 'recycle', '115.60.19.9', '1593857256');
INSERT INTO `nms_admin_log` VALUES ('2248', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1593857438');
INSERT INTO `nms_admin_log` VALUES ('2249', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593857794');
INSERT INTO `nms_admin_log` VALUES ('2250', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593857798');
INSERT INTO `nms_admin_log` VALUES ('2251', 'moduleid=25&file=index&action=edit&itemid=4', 'fulltimelink', '61.163.87.166', '1593857811');
INSERT INTO `nms_admin_log` VALUES ('2252', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593857815');
INSERT INTO `nms_admin_log` VALUES ('2253', 'moduleid=25&file=index&action=delete&itemid=4', 'fulltimelink', '61.163.87.166', '1593857822');
INSERT INTO `nms_admin_log` VALUES ('2254', 'rand=53&moduleid=25', 'fulltimelink', '61.163.87.166', '1593857822');
INSERT INTO `nms_admin_log` VALUES ('2255', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593857833');
INSERT INTO `nms_admin_log` VALUES ('2256', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593859107');
INSERT INTO `nms_admin_log` VALUES ('2257', 'moduleid=25&action=reject', 'fulltimelink', '61.163.87.166', '1593859107');
INSERT INTO `nms_admin_log` VALUES ('2258', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593859108');
INSERT INTO `nms_admin_log` VALUES ('2259', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593859108');
INSERT INTO `nms_admin_log` VALUES ('2260', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859110');
INSERT INTO `nms_admin_log` VALUES ('2261', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859111');
INSERT INTO `nms_admin_log` VALUES ('2262', 'file=category&mid=26', 'fulltimelink', '61.163.87.166', '1593859112');
INSERT INTO `nms_admin_log` VALUES ('2263', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859113');
INSERT INTO `nms_admin_log` VALUES ('2264', 'moduleid=26&file=html', 'fulltimelink', '61.163.87.166', '1593859114');
INSERT INTO `nms_admin_log` VALUES ('2265', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1593859115');
INSERT INTO `nms_admin_log` VALUES ('2266', 'moduleid=26&file=html', 'fulltimelink', '61.163.87.166', '1593859115');
INSERT INTO `nms_admin_log` VALUES ('2267', 'file=category&mid=26', 'fulltimelink', '61.163.87.166', '1593859116');
INSERT INTO `nms_admin_log` VALUES ('2268', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859116');
INSERT INTO `nms_admin_log` VALUES ('2269', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859119');
INSERT INTO `nms_admin_log` VALUES ('2270', 'moduleid=25&action=add', 'fulltimelink', '61.163.87.166', '1593859121');
INSERT INTO `nms_admin_log` VALUES ('2271', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593859122');
INSERT INTO `nms_admin_log` VALUES ('2272', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859140');
INSERT INTO `nms_admin_log` VALUES ('2273', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859158');
INSERT INTO `nms_admin_log` VALUES ('2274', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859159');
INSERT INTO `nms_admin_log` VALUES ('2275', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859159');
INSERT INTO `nms_admin_log` VALUES ('2276', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859159');
INSERT INTO `nms_admin_log` VALUES ('2277', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859159');
INSERT INTO `nms_admin_log` VALUES ('2278', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859159');
INSERT INTO `nms_admin_log` VALUES ('2279', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859239');
INSERT INTO `nms_admin_log` VALUES ('2280', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859268');
INSERT INTO `nms_admin_log` VALUES ('2281', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859268');
INSERT INTO `nms_admin_log` VALUES ('2282', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859269');
INSERT INTO `nms_admin_log` VALUES ('2283', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859269');
INSERT INTO `nms_admin_log` VALUES ('2284', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859269');
INSERT INTO `nms_admin_log` VALUES ('2285', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859269');
INSERT INTO `nms_admin_log` VALUES ('2286', 'moduleid=27&action=add', 'fulltimelink', '61.163.87.166', '1593859272');
INSERT INTO `nms_admin_log` VALUES ('2287', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1593859273');
INSERT INTO `nms_admin_log` VALUES ('2288', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859276');
INSERT INTO `nms_admin_log` VALUES ('2289', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859446');
INSERT INTO `nms_admin_log` VALUES ('2290', 'file=category&mid=26', 'fulltimelink', '61.163.87.166', '1593859471');
INSERT INTO `nms_admin_log` VALUES ('2291', 'moduleid=26&file=html', 'fulltimelink', '61.163.87.166', '1593859472');
INSERT INTO `nms_admin_log` VALUES ('2292', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1593859473');
INSERT INTO `nms_admin_log` VALUES ('2293', 'moduleid=26&file=html', 'fulltimelink', '61.163.87.166', '1593859473');
INSERT INTO `nms_admin_log` VALUES ('2294', 'file=category&mid=26', 'fulltimelink', '61.163.87.166', '1593859474');
INSERT INTO `nms_admin_log` VALUES ('2295', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859474');
INSERT INTO `nms_admin_log` VALUES ('2296', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859620');
INSERT INTO `nms_admin_log` VALUES ('2297', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&itemid=0&exports=1', 'fulltimelink', '61.163.87.166', '1593859628');
INSERT INTO `nms_admin_log` VALUES ('2298', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593859631');
INSERT INTO `nms_admin_log` VALUES ('2299', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&itemid=0&exports=1', 'fulltimelink', '61.163.87.166', '1593859694');
INSERT INTO `nms_admin_log` VALUES ('2300', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593863847');
INSERT INTO `nms_admin_log` VALUES ('2301', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593863856');
INSERT INTO `nms_admin_log` VALUES ('2302', 'moduleid=25&action=add', 'fulltimelink', '61.163.87.166', '1593863857');
INSERT INTO `nms_admin_log` VALUES ('2303', 'moduleid=27', 'fulltimelink', '61.163.87.166', '1593863869');
INSERT INTO `nms_admin_log` VALUES ('2304', 'moduleid=27&action=add', 'fulltimelink', '61.163.87.166', '1593863870');
INSERT INTO `nms_admin_log` VALUES ('2305', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593863877');
INSERT INTO `nms_admin_log` VALUES ('2306', 'moduleid=25&action=add', 'fulltimelink', '61.163.87.166', '1593863880');
INSERT INTO `nms_admin_log` VALUES ('2307', 'moduleid=24&action=check', 'fulltimelink', '61.163.87.166', '1593863910');
INSERT INTO `nms_admin_log` VALUES ('2308', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1593863911');
INSERT INTO `nms_admin_log` VALUES ('2309', 'moduleid=24&action=add', 'fulltimelink', '61.163.87.166', '1593863911');
INSERT INTO `nms_admin_log` VALUES ('2310', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593865992');
INSERT INTO `nms_admin_log` VALUES ('2311', 'file=category&mid=26', 'fulltimelink', '61.163.87.166', '1593866901');
INSERT INTO `nms_admin_log` VALUES ('2312', 'moduleid=26&file=html', 'fulltimelink', '61.163.87.166', '1593866903');
INSERT INTO `nms_admin_log` VALUES ('2313', 'file=category&mid=26', 'fulltimelink', '61.163.87.166', '1593866903');
INSERT INTO `nms_admin_log` VALUES ('2314', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593866904');
INSERT INTO `nms_admin_log` VALUES ('2315', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&itemid=0&exports=1', 'fulltimelink', '61.163.87.166', '1593869574');
INSERT INTO `nms_admin_log` VALUES ('2316', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593869670');
INSERT INTO `nms_admin_log` VALUES ('2317', 'moduleid=26&file=index&action=edit&itemid=56', 'fulltimelink', '61.163.87.166', '1593869741');
INSERT INTO `nms_admin_log` VALUES ('2318', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593869973');
INSERT INTO `nms_admin_log` VALUES ('2319', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593869977');
INSERT INTO `nms_admin_log` VALUES ('2320', 'moduleid=25&file=index&action=edit&itemid=15', 'fulltimelink', '61.163.87.166', '1593869979');
INSERT INTO `nms_admin_log` VALUES ('2321', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593872271');
INSERT INTO `nms_admin_log` VALUES ('2322', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593872522');
INSERT INTO `nms_admin_log` VALUES ('2323', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593872536');
INSERT INTO `nms_admin_log` VALUES ('2324', 'moduleid=25&file=index&action=edit&itemid=17', 'fulltimelink', '61.163.87.166', '1593872541');
INSERT INTO `nms_admin_log` VALUES ('2325', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593872552');
INSERT INTO `nms_admin_log` VALUES ('2326', 'moduleid=25&file=index&action=edit&itemid=15', 'fulltimelink', '61.163.87.166', '1593872555');
INSERT INTO `nms_admin_log` VALUES ('2327', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593872559');
INSERT INTO `nms_admin_log` VALUES ('2328', 'moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593872561');
INSERT INTO `nms_admin_log` VALUES ('2329', 'moduleid=25&file=index&action=edit&itemid=17', 'fulltimelink', '61.163.87.166', '1593872566');
INSERT INTO `nms_admin_log` VALUES ('2330', 'rand=11&moduleid=25&action=check', 'fulltimelink', '61.163.87.166', '1593872665');
INSERT INTO `nms_admin_log` VALUES ('2331', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593875078');
INSERT INTO `nms_admin_log` VALUES ('2332', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1593875169');
INSERT INTO `nms_admin_log` VALUES ('2333', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593875404');
INSERT INTO `nms_admin_log` VALUES ('2334', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1593875514');
INSERT INTO `nms_admin_log` VALUES ('2335', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593875523');
INSERT INTO `nms_admin_log` VALUES ('2336', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1593875628');
INSERT INTO `nms_admin_log` VALUES ('2337', 'rand=19&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1593875654');
INSERT INTO `nms_admin_log` VALUES ('2338', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1593875663');
INSERT INTO `nms_admin_log` VALUES ('2339', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593875938');
INSERT INTO `nms_admin_log` VALUES ('2340', 'moduleid=25&file=index&action=edit&itemid=15', 'fulltimelink', '61.163.87.166', '1593875961');
INSERT INTO `nms_admin_log` VALUES ('2341', 'moduleid=25', 'fulltimelink', '61.163.87.166', '1593875990');
INSERT INTO `nms_admin_log` VALUES ('2347', 'moduleid=25', 'recycle', '61.163.87.166', '1593921633');
INSERT INTO `nms_admin_log` VALUES ('2348', 'moduleid=26', 'recycle', '61.163.87.166', '1593921635');
INSERT INTO `nms_admin_log` VALUES ('2349', 'moduleid=26&action=revieworder', 'recycle', '61.163.87.166', '1593921642');
INSERT INTO `nms_admin_log` VALUES ('2350', 'moduleid=26&action=successorder', 'recycle', '61.163.87.166', '1593921643');
INSERT INTO `nms_admin_log` VALUES ('2351', 'moduleid=26&action=alreadyorder', 'recycle', '61.163.87.166', '1593921644');
INSERT INTO `nms_admin_log` VALUES ('2352', 'moduleid=26&action=waitorder', 'recycle', '61.163.87.166', '1593921645');
INSERT INTO `nms_admin_log` VALUES ('2353', 'moduleid=26&action=cancelorder', 'recycle', '61.163.87.166', '1593921645');
INSERT INTO `nms_admin_log` VALUES ('2354', 'moduleid=26', 'recycle', '61.163.87.166', '1593921647');
INSERT INTO `nms_admin_log` VALUES ('2355', 'moduleid=26&action=revieworder', 'recycle', '61.163.87.166', '1593921659');
INSERT INTO `nms_admin_log` VALUES ('2356', 'moduleid=26', 'recycle', '61.163.87.166', '1593921660');
INSERT INTO `nms_admin_log` VALUES ('2357', 'moduleid=27', 'recycle', '61.163.87.166', '1593921666');
INSERT INTO `nms_admin_log` VALUES ('2358', 'moduleid=28', 'recycle', '61.163.87.166', '1593921667');
INSERT INTO `nms_admin_log` VALUES ('2359', 'moduleid=29', 'recycle', '61.163.87.166', '1593921669');
INSERT INTO `nms_admin_log` VALUES ('2360', 'moduleid=26&file=setting', 'recycle', '61.163.87.166', '1593921670');
INSERT INTO `nms_admin_log` VALUES ('2361', 'file=patch', 'recycle', '61.163.87.166', '1593921671');
INSERT INTO `nms_admin_log` VALUES ('2362', 'file=scan', 'recycle', '61.163.87.166', '1593921677');
INSERT INTO `nms_admin_log` VALUES ('2363', 'file=md5', 'recycle', '61.163.87.166', '1593921681');
INSERT INTO `nms_admin_log` VALUES ('2364', 'file=doctor', 'recycle', '61.163.87.166', '1593921687');
INSERT INTO `nms_admin_log` VALUES ('2365', 'file=database', 'recycle', '61.163.87.166', '1593921702');
INSERT INTO `nms_admin_log` VALUES ('2366', 'file=database&action=replace', 'recycle', '61.163.87.166', '1593921708');
INSERT INTO `nms_admin_log` VALUES ('2367', 'file=database&action=execute', 'recycle', '61.163.87.166', '1593921708');
INSERT INTO `nms_admin_log` VALUES ('2368', 'file=database&action=process', 'recycle', '61.163.87.166', '1593921712');
INSERT INTO `nms_admin_log` VALUES ('2369', 'file=database&action=move', 'recycle', '61.163.87.166', '1593921714');
INSERT INTO `nms_admin_log` VALUES ('2370', 'file=data', 'recycle', '61.163.87.166', '1593921715');
INSERT INTO `nms_admin_log` VALUES ('2371', 'file=upload', 'recycle', '61.163.87.166', '1593921718');
INSERT INTO `nms_admin_log` VALUES ('2372', 'file=upload&id=1&widget=1', 'recycle', '61.163.87.166', '1593921726');
INSERT INTO `nms_admin_log` VALUES ('2373', 'file=upload&id=9&widget=1', 'recycle', '61.163.87.166', '1593921755');
INSERT INTO `nms_admin_log` VALUES ('2374', 'file=upload&id=8&widget=1', 'recycle', '61.163.87.166', '1593921761');
INSERT INTO `nms_admin_log` VALUES ('2375', 'file=keyword', 'recycle', '61.163.87.166', '1593921768');
INSERT INTO `nms_admin_log` VALUES ('2376', 'file=keyword&status=2', 'recycle', '61.163.87.166', '1593921773');
INSERT INTO `nms_admin_log` VALUES ('2377', 'file=keyword', 'recycle', '61.163.87.166', '1593921775');
INSERT INTO `nms_admin_log` VALUES ('2378', 'file=404', 'recycle', '61.163.87.166', '1593921794');
INSERT INTO `nms_admin_log` VALUES ('2379', 'file=mymenu', 'recycle', '171.11.7.19', '1593926227');
INSERT INTO `nms_admin_log` VALUES ('2380', 'mid=24&file=category', 'recycle', '171.11.7.19', '1593926230');
INSERT INTO `nms_admin_log` VALUES ('2381', 'moduleid=25', 'recycle', '171.11.7.19', '1593926231');
INSERT INTO `nms_admin_log` VALUES ('2382', 'mid=24&file=category', 'recycle', '171.11.7.19', '1593926232');
INSERT INTO `nms_admin_log` VALUES ('2383', 'moduleid=25', 'recycle', '171.11.7.19', '1593926237');
INSERT INTO `nms_admin_log` VALUES ('2384', 'moduleid=26', 'recycle', '171.11.7.19', '1593926238');
INSERT INTO `nms_admin_log` VALUES ('2385', 'moduleid=27', 'recycle', '171.11.7.19', '1593926239');
INSERT INTO `nms_admin_log` VALUES ('2386', 'moduleid=28', 'recycle', '171.11.7.19', '1593926241');
INSERT INTO `nms_admin_log` VALUES ('2387', 'moduleid=29', 'recycle', '171.11.7.19', '1593926243');
INSERT INTO `nms_admin_log` VALUES ('2388', 'moduleid=29&file=index&action=edit&itemid=1', 'recycle', '171.11.7.19', '1593926248');
INSERT INTO `nms_admin_log` VALUES ('2389', 'moduleid=29', 'recycle', '171.11.7.19', '1593926261');
INSERT INTO `nms_admin_log` VALUES ('2390', 'moduleid=29&file=index&action=edit&itemid=1', 'recycle', '171.11.7.19', '1593926262');
INSERT INTO `nms_admin_log` VALUES ('2391', 'moduleid=26', 'recycle', '171.11.7.19', '1593929401');
INSERT INTO `nms_admin_log` VALUES ('2392', 'moduleid=26&file=index&action=edit&itemid=62', 'recycle', '171.11.7.19', '1593929406');
INSERT INTO `nms_admin_log` VALUES ('2393', 'rand=87&moduleid=26', 'recycle', '171.11.7.19', '1593929415');
INSERT INTO `nms_admin_log` VALUES ('2394', 'moduleid=25', 'recycle', '171.11.7.19', '1593929917');
INSERT INTO `nms_admin_log` VALUES ('2395', 'moduleid=25&action=check', 'recycle', '171.11.7.19', '1593929922');
INSERT INTO `nms_admin_log` VALUES ('2396', 'moduleid=25&action=reject', 'recycle', '171.11.7.19', '1593929925');
INSERT INTO `nms_admin_log` VALUES ('2397', 'moduleid=25&action=check', 'recycle', '171.11.7.19', '1593929926');
INSERT INTO `nms_admin_log` VALUES ('2398', 'moduleid=29', 'recycle', '171.11.7.19', '1593931635');
INSERT INTO `nms_admin_log` VALUES ('2399', 'mid=24&file=category', 'recycle', '171.11.7.19', '1593931635');
INSERT INTO `nms_admin_log` VALUES ('2400', 'file=mymenu', 'recycle', '171.11.7.19', '1593931637');
INSERT INTO `nms_admin_log` VALUES ('2401', 'moduleid=25', 'recycle', '171.11.7.19', '1593931638');
INSERT INTO `nms_admin_log` VALUES ('2402', 'moduleid=26', 'recycle', '171.11.7.19', '1593931638');
INSERT INTO `nms_admin_log` VALUES ('2403', 'moduleid=27', 'recycle', '171.11.7.19', '1593931640');
INSERT INTO `nms_admin_log` VALUES ('2404', 'moduleid=28', 'recycle', '171.11.7.19', '1593931641');
INSERT INTO `nms_admin_log` VALUES ('2405', 'moduleid=29', 'recycle', '171.11.7.19', '1593931642');
INSERT INTO `nms_admin_log` VALUES ('2406', 'moduleid=26&file=setting', 'recycle', '171.11.7.19', '1593931642');
INSERT INTO `nms_admin_log` VALUES ('2407', 'file=patch', 'recycle', '171.11.7.19', '1593931644');
INSERT INTO `nms_admin_log` VALUES ('2408', 'moduleid=26&file=setting', 'recycle', '171.11.7.19', '1593931657');
INSERT INTO `nms_admin_log` VALUES ('2409', 'moduleid=29', 'recycle', '171.11.7.19', '1593931658');
INSERT INTO `nms_admin_log` VALUES ('2410', 'moduleid=28', 'recycle', '171.11.7.19', '1593931659');
INSERT INTO `nms_admin_log` VALUES ('2411', 'moduleid=27', 'recycle', '171.11.7.19', '1593931659');
INSERT INTO `nms_admin_log` VALUES ('2412', 'moduleid=26', 'recycle', '171.11.7.19', '1593931660');
INSERT INTO `nms_admin_log` VALUES ('2413', 'moduleid=25', 'recycle', '171.11.7.19', '1593931660');
INSERT INTO `nms_admin_log` VALUES ('2414', 'mid=24&file=category', 'recycle', '171.11.7.19', '1593931661');
INSERT INTO `nms_admin_log` VALUES ('2415', 'file=category&action=copy&mid=24', 'recycle', '171.11.7.19', '1593931663');
INSERT INTO `nms_admin_log` VALUES ('2416', 'file=category&action=caches&mid=24', 'recycle', '171.11.7.19', '1593931664');
INSERT INTO `nms_admin_log` VALUES ('2417', 'file=category&mid=24&action=count', 'recycle', '171.11.7.19', '1593931665');
INSERT INTO `nms_admin_log` VALUES ('2418', 'file=category&mid=24&action=count&sid=6&fid=12&tid=11&num=50', 'recycle', '171.11.7.19', '1593931666');
INSERT INTO `nms_admin_log` VALUES ('2419', 'file=category&mid=24&action=url', 'recycle', '171.11.7.19', '1593931667');
INSERT INTO `nms_admin_log` VALUES ('2420', 'file=category&mid=24&action=letters', 'recycle', '171.11.7.19', '1593931667');
INSERT INTO `nms_admin_log` VALUES ('2421', 'file=category&mid=24&action=cache', 'recycle', '171.11.7.19', '1593931668');
INSERT INTO `nms_admin_log` VALUES ('2422', 'rand=47&file=category&mid=24', 'recycle', '171.11.7.19', '1593931668');
INSERT INTO `nms_admin_log` VALUES ('2423', 'moduleid=25', 'recycle', '171.11.7.19', '1593931670');
INSERT INTO `nms_admin_log` VALUES ('2424', 'moduleid=26', 'recycle', '171.11.7.19', '1593931671');
INSERT INTO `nms_admin_log` VALUES ('2425', 'file=mymenu', 'recycle', '171.11.7.19', '1593931673');
INSERT INTO `nms_admin_log` VALUES ('2426', 'moduleid=27', 'recycle', '171.11.7.19', '1593931673');
INSERT INTO `nms_admin_log` VALUES ('2427', 'moduleid=28', 'recycle', '171.11.7.19', '1593931674');
INSERT INTO `nms_admin_log` VALUES ('2428', 'moduleid=29', 'recycle', '171.11.7.19', '1593931674');
INSERT INTO `nms_admin_log` VALUES ('2429', 'moduleid=26&file=setting', 'recycle', '171.11.7.19', '1593931675');
INSERT INTO `nms_admin_log` VALUES ('2430', 'file=patch', 'recycle', '171.11.7.19', '1593931676');
INSERT INTO `nms_admin_log` VALUES ('2431', 'file=doctor', 'recycle', '171.11.7.19', '1593931677');
INSERT INTO `nms_admin_log` VALUES ('2432', 'file=database', 'recycle', '171.11.7.19', '1593931678');
INSERT INTO `nms_admin_log` VALUES ('2433', 'file=upload', 'recycle', '171.11.7.19', '1593931679');
INSERT INTO `nms_admin_log` VALUES ('2434', 'file=keyword', 'recycle', '171.11.7.19', '1593931679');
INSERT INTO `nms_admin_log` VALUES ('2435', 'file=404', 'recycle', '171.11.7.19', '1593931680');
INSERT INTO `nms_admin_log` VALUES ('2436', 'moduleid=26', 'recycle', '171.11.7.19', '1593931681');
INSERT INTO `nms_admin_log` VALUES ('2437', 'moduleid=26&file=index&action=edit&itemid=60', 'recycle', '171.11.7.19', '1593931747');
INSERT INTO `nms_admin_log` VALUES ('2438', 'rand=16&moduleid=26', 'recycle', '171.11.7.19', '1593931752');
INSERT INTO `nms_admin_log` VALUES ('2439', 'mid=24&file=category', 'recycle', '61.163.87.166', '1593932065');
INSERT INTO `nms_admin_log` VALUES ('2440', 'moduleid=28', 'recycle', '61.163.87.166', '1593932084');
INSERT INTO `nms_admin_log` VALUES ('2441', 'moduleid=29', 'recycle', '61.163.87.166', '1593932085');
INSERT INTO `nms_admin_log` VALUES ('2442', 'moduleid=28', 'recycle', '171.11.7.19', '1593932088');
INSERT INTO `nms_admin_log` VALUES ('2443', 'moduleid=29', 'recycle', '171.11.7.19', '1593932089');
INSERT INTO `nms_admin_log` VALUES ('2444', 'moduleid=28', 'recycle', '171.11.7.19', '1593932090');
INSERT INTO `nms_admin_log` VALUES ('2445', 'moduleid=27', 'recycle', '171.11.7.19', '1593932090');
INSERT INTO `nms_admin_log` VALUES ('2446', 'moduleid=28', 'recycle', '171.11.7.19', '1593932091');
INSERT INTO `nms_admin_log` VALUES ('2447', 'moduleid=27', 'recycle', '171.11.7.19', '1593932092');
INSERT INTO `nms_admin_log` VALUES ('2448', 'moduleid=26', 'recycle', '171.11.7.19', '1593932092');
INSERT INTO `nms_admin_log` VALUES ('2449', 'moduleid=25', 'recycle', '171.11.7.19', '1593932093');
INSERT INTO `nms_admin_log` VALUES ('2450', 'moduleid=26', 'recycle', '171.11.7.19', '1593932095');
INSERT INTO `nms_admin_log` VALUES ('2451', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1593932118');
INSERT INTO `nms_admin_log` VALUES ('2452', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1593932121');
INSERT INTO `nms_admin_log` VALUES ('2453', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1593932246');
INSERT INTO `nms_admin_log` VALUES ('2454', 'moduleid=24&file=index&action=edit&itemid=2', 'fulltimelink', '61.163.87.166', '1593932251');
INSERT INTO `nms_admin_log` VALUES ('2455', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1593932254');
INSERT INTO `nms_admin_log` VALUES ('2456', 'moduleid=24', 'fulltimelink', '61.163.87.166', '1593932303');
INSERT INTO `nms_admin_log` VALUES ('2457', 'file=admin', 'fulltimelink', '61.163.87.166', '1593932351');
INSERT INTO `nms_admin_log` VALUES ('2458', 'file=admin&action=right&userid=5&widget=1', 'fulltimelink', '61.163.87.166', '1593932355');
INSERT INTO `nms_admin_log` VALUES ('2459', 'rand=65&file=admin&action=right&userid=5', 'fulltimelink', '61.163.87.166', '1593932391');
INSERT INTO `nms_admin_log` VALUES ('2460', 'moduleid=29', 'recycle', '171.11.7.19', '1593932397');
INSERT INTO `nms_admin_log` VALUES ('2461', 'moduleid=24', 'recycle', '61.163.87.166', '1593932441');
INSERT INTO `nms_admin_log` VALUES ('2462', 'moduleid=24&action=add', 'recycle', '61.163.87.166', '1593932443');
INSERT INTO `nms_admin_log` VALUES ('2463', 'moduleid=24', 'recycle', '61.163.87.166', '1593932444');
INSERT INTO `nms_admin_log` VALUES ('2464', 'moduleid=26&file=setting', 'recycle', '171.11.7.19', '1593932503');
INSERT INTO `nms_admin_log` VALUES ('2465', 'moduleid=24', 'recycle', '171.11.7.19', '1593932569');
INSERT INTO `nms_admin_log` VALUES ('2466', 'moduleid=24&file=index&action=edit&itemid=3', 'recycle', '171.11.7.19', '1593932572');
INSERT INTO `nms_admin_log` VALUES ('2467', 'rand=67&moduleid=24', 'recycle', '171.11.7.19', '1593932596');
INSERT INTO `nms_admin_log` VALUES ('2468', 'moduleid=24&file=index&action=edit&itemid=2', 'recycle', '171.11.7.19', '1593932598');
INSERT INTO `nms_admin_log` VALUES ('2469', 'rand=19&moduleid=24', 'recycle', '171.11.7.19', '1593932632');
INSERT INTO `nms_admin_log` VALUES ('2470', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593937364');
INSERT INTO `nms_admin_log` VALUES ('2471', 'moduleid=25', 'recycle', '123.160.174.126', '1593937689');
INSERT INTO `nms_admin_log` VALUES ('2472', 'moduleid=25&action=check', 'recycle', '123.160.174.126', '1593937690');
INSERT INTO `nms_admin_log` VALUES ('2473', 'moduleid=25&file=index&action=edit&itemid=22', 'recycle', '123.160.174.126', '1593937697');
INSERT INTO `nms_admin_log` VALUES ('2474', 'rand=27&moduleid=25&action=check', 'recycle', '123.160.174.126', '1593937708');
INSERT INTO `nms_admin_log` VALUES ('2475', 'moduleid=25&action=check', 'recycle', '123.160.174.126', '1593937732');
INSERT INTO `nms_admin_log` VALUES ('2476', 'moduleid=25&file=index&action=edit&itemid=20', 'recycle', '123.160.174.126', '1593937735');
INSERT INTO `nms_admin_log` VALUES ('2477', 'rand=12&moduleid=25&action=check', 'recycle', '123.160.174.126', '1593937739');
INSERT INTO `nms_admin_log` VALUES ('2478', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938007');
INSERT INTO `nms_admin_log` VALUES ('2479', 'moduleid=24', 'recycle', '123.160.174.126', '1593938008');
INSERT INTO `nms_admin_log` VALUES ('2480', 'moduleid=25', 'recycle', '123.160.174.126', '1593938010');
INSERT INTO `nms_admin_log` VALUES ('2481', 'moduleid=26', 'recycle', '123.160.174.126', '1593938013');
INSERT INTO `nms_admin_log` VALUES ('2482', 'moduleid=29', 'recycle', '123.160.174.126', '1593938107');
INSERT INTO `nms_admin_log` VALUES ('2483', 'moduleid=26&file=setting', 'recycle', '123.160.174.126', '1593938109');
INSERT INTO `nms_admin_log` VALUES ('2484', 'file=patch', 'recycle', '123.160.174.126', '1593938113');
INSERT INTO `nms_admin_log` VALUES ('2485', 'moduleid=29', 'recycle', '123.160.174.126', '1593938114');
INSERT INTO `nms_admin_log` VALUES ('2486', 'moduleid=28', 'recycle', '123.160.174.126', '1593938115');
INSERT INTO `nms_admin_log` VALUES ('2487', 'moduleid=27', 'recycle', '123.160.174.126', '1593938116');
INSERT INTO `nms_admin_log` VALUES ('2488', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938124');
INSERT INTO `nms_admin_log` VALUES ('2489', 'moduleid=24', 'recycle', '123.160.174.126', '1593938125');
INSERT INTO `nms_admin_log` VALUES ('2490', 'file=mymenu', 'recycle', '123.160.174.126', '1593938126');
INSERT INTO `nms_admin_log` VALUES ('2491', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938130');
INSERT INTO `nms_admin_log` VALUES ('2492', 'moduleid=24', 'recycle', '123.160.174.126', '1593938132');
INSERT INTO `nms_admin_log` VALUES ('2493', 'moduleid=25', 'recycle', '123.160.174.126', '1593938133');
INSERT INTO `nms_admin_log` VALUES ('2494', 'moduleid=26', 'recycle', '123.160.174.126', '1593938134');
INSERT INTO `nms_admin_log` VALUES ('2495', 'moduleid=28', 'recycle', '123.160.174.126', '1593938135');
INSERT INTO `nms_admin_log` VALUES ('2496', 'file=patch', 'recycle', '123.160.174.126', '1593938136');
INSERT INTO `nms_admin_log` VALUES ('2497', 'file=doctor', 'recycle', '123.160.174.126', '1593938137');
INSERT INTO `nms_admin_log` VALUES ('2498', 'file=database', 'recycle', '123.160.174.126', '1593938138');
INSERT INTO `nms_admin_log` VALUES ('2499', 'file=upload', 'recycle', '123.160.174.126', '1593938139');
INSERT INTO `nms_admin_log` VALUES ('2500', 'file=keyword', 'recycle', '123.160.174.126', '1593938140');
INSERT INTO `nms_admin_log` VALUES ('2501', 'file=mymenu', 'recycle', '123.160.174.126', '1593938147');
INSERT INTO `nms_admin_log` VALUES ('2502', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938159');
INSERT INTO `nms_admin_log` VALUES ('2503', 'moduleid=24', 'recycle', '123.160.174.126', '1593938161');
INSERT INTO `nms_admin_log` VALUES ('2504', 'moduleid=25', 'recycle', '123.160.174.126', '1593938162');
INSERT INTO `nms_admin_log` VALUES ('2505', 'moduleid=26', 'recycle', '123.160.174.126', '1593938165');
INSERT INTO `nms_admin_log` VALUES ('2506', 'moduleid=27', 'recycle', '123.160.174.126', '1593938167');
INSERT INTO `nms_admin_log` VALUES ('2507', 'moduleid=25', 'recycle', '123.160.174.126', '1593938168');
INSERT INTO `nms_admin_log` VALUES ('2508', 'moduleid=24', 'recycle', '123.160.174.126', '1593938169');
INSERT INTO `nms_admin_log` VALUES ('2509', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938170');
INSERT INTO `nms_admin_log` VALUES ('2510', 'moduleid=25', 'recycle', '123.160.174.126', '1593938173');
INSERT INTO `nms_admin_log` VALUES ('2511', 'moduleid=26', 'recycle', '123.160.174.126', '1593938174');
INSERT INTO `nms_admin_log` VALUES ('2512', 'moduleid=27', 'recycle', '123.160.174.126', '1593938175');
INSERT INTO `nms_admin_log` VALUES ('2513', 'moduleid=28', 'recycle', '123.160.174.126', '1593938175');
INSERT INTO `nms_admin_log` VALUES ('2514', 'file=mymenu', 'recycle', '123.160.174.126', '1593938177');
INSERT INTO `nms_admin_log` VALUES ('2515', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938179');
INSERT INTO `nms_admin_log` VALUES ('2516', 'moduleid=24', 'recycle', '123.160.174.126', '1593938180');
INSERT INTO `nms_admin_log` VALUES ('2517', 'moduleid=25', 'recycle', '123.160.174.126', '1593938182');
INSERT INTO `nms_admin_log` VALUES ('2518', 'moduleid=26', 'recycle', '123.160.174.126', '1593938183');
INSERT INTO `nms_admin_log` VALUES ('2519', 'moduleid=27', 'recycle', '123.160.174.126', '1593938184');
INSERT INTO `nms_admin_log` VALUES ('2520', 'moduleid=28', 'recycle', '123.160.174.126', '1593938185');
INSERT INTO `nms_admin_log` VALUES ('2521', 'moduleid=25', 'recycle', '123.160.174.126', '1593938186');
INSERT INTO `nms_admin_log` VALUES ('2522', 'moduleid=24', 'recycle', '123.160.174.126', '1593938276');
INSERT INTO `nms_admin_log` VALUES ('2523', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938277');
INSERT INTO `nms_admin_log` VALUES ('2524', 'file=mymenu', 'recycle', '123.160.174.126', '1593938278');
INSERT INTO `nms_admin_log` VALUES ('2525', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938280');
INSERT INTO `nms_admin_log` VALUES ('2526', 'moduleid=24', 'recycle', '123.160.174.126', '1593938281');
INSERT INTO `nms_admin_log` VALUES ('2527', 'moduleid=25', 'recycle', '123.160.174.126', '1593938281');
INSERT INTO `nms_admin_log` VALUES ('2528', 'moduleid=24', 'recycle', '123.160.174.126', '1593938333');
INSERT INTO `nms_admin_log` VALUES ('2529', 'mid=24&file=category', 'recycle', '123.160.174.126', '1593938334');
INSERT INTO `nms_admin_log` VALUES ('2530', 'file=mymenu', 'recycle', '123.160.174.126', '1593938336');
INSERT INTO `nms_admin_log` VALUES ('2531', 'file=admin', 'fulltimelink', '61.163.87.166', '1593997822');
INSERT INTO `nms_admin_log` VALUES ('2532', 'mid=24&file=category', 'recycle', '123.149.69.123', '1593998215');
INSERT INTO `nms_admin_log` VALUES ('2533', 'file=mymenu', 'recycle', '123.149.69.123', '1593998217');
INSERT INTO `nms_admin_log` VALUES ('2534', 'moduleid=24', 'recycle', '123.149.69.123', '1593998232');
INSERT INTO `nms_admin_log` VALUES ('2535', 'mid=24&file=category', 'recycle', '123.149.69.123', '1593998233');
INSERT INTO `nms_admin_log` VALUES ('2536', 'moduleid=24', 'recycle', '123.149.69.123', '1593998235');
INSERT INTO `nms_admin_log` VALUES ('2537', 'moduleid=25', 'recycle', '123.149.69.123', '1593998239');
INSERT INTO `nms_admin_log` VALUES ('2538', 'moduleid=26', 'recycle', '123.149.69.123', '1593998248');
INSERT INTO `nms_admin_log` VALUES ('2539', 'moduleid=27', 'recycle', '123.149.69.123', '1593998253');
INSERT INTO `nms_admin_log` VALUES ('2540', 'moduleid=28', 'recycle', '123.149.69.123', '1593998256');
INSERT INTO `nms_admin_log` VALUES ('2541', 'moduleid=29', 'recycle', '123.149.69.123', '1593998258');
INSERT INTO `nms_admin_log` VALUES ('2542', 'moduleid=26&file=setting', 'recycle', '123.149.69.123', '1593998260');
INSERT INTO `nms_admin_log` VALUES ('2543', 'file=patch', 'recycle', '123.149.69.123', '1593998291');
INSERT INTO `nms_admin_log` VALUES ('2544', 'file=doctor', 'recycle', '123.149.69.123', '1593998298');
INSERT INTO `nms_admin_log` VALUES ('2545', 'file=database', 'recycle', '123.149.69.123', '1593998303');
INSERT INTO `nms_admin_log` VALUES ('2546', 'file=mymenu', 'recycle', '123.149.69.123', '1593998309');
INSERT INTO `nms_admin_log` VALUES ('2547', 'mid=24&file=category', 'recycle', '123.149.69.123', '1593998310');
INSERT INTO `nms_admin_log` VALUES ('2548', 'moduleid=24', 'recycle', '123.149.69.123', '1593998311');
INSERT INTO `nms_admin_log` VALUES ('2549', 'moduleid=25', 'recycle', '123.149.69.123', '1593998331');
INSERT INTO `nms_admin_log` VALUES ('2550', 'file=mymenu', 'recycle', '123.149.69.123', '1593998340');
INSERT INTO `nms_admin_log` VALUES ('2551', 'mid=24&file=category', 'recycle', '123.149.69.123', '1593998341');
INSERT INTO `nms_admin_log` VALUES ('2552', 'moduleid=24', 'recycle', '123.149.69.123', '1593998343');
INSERT INTO `nms_admin_log` VALUES ('2553', 'moduleid=26', 'recycle', '123.149.69.123', '1593998344');
INSERT INTO `nms_admin_log` VALUES ('2554', 'moduleid=25', 'recycle', '123.149.69.123', '1593998345');
INSERT INTO `nms_admin_log` VALUES ('2555', 'moduleid=26', 'recycle', '123.149.69.123', '1593998346');
INSERT INTO `nms_admin_log` VALUES ('2556', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594001573');
INSERT INTO `nms_admin_log` VALUES ('2557', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1594001600');
INSERT INTO `nms_admin_log` VALUES ('2558', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594001608');
INSERT INTO `nms_admin_log` VALUES ('2559', 'moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1594002791');
INSERT INTO `nms_admin_log` VALUES ('2560', 'moduleid=26&sum=49', 'fulltimelink', '61.163.87.166', '1594002793');
INSERT INTO `nms_admin_log` VALUES ('2561', 'moduleid=26&sum=49&page=3', 'fulltimelink', '61.163.87.166', '1594002794');
INSERT INTO `nms_admin_log` VALUES ('2562', 'moduleid=26&sum=49', 'fulltimelink', '61.163.87.166', '1594002796');
INSERT INTO `nms_admin_log` VALUES ('2563', 'moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1594002806');
INSERT INTO `nms_admin_log` VALUES ('2564', 'moduleid=26&sum=49', 'fulltimelink', '61.163.87.166', '1594002808');
INSERT INTO `nms_admin_log` VALUES ('2565', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1594002821');
INSERT INTO `nms_admin_log` VALUES ('2566', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1594005739');
INSERT INTO `nms_admin_log` VALUES ('2567', 'rand=96&moduleid=26&file=setting&tab=4', 'fulltimelink', '61.163.87.166', '1594005743');
INSERT INTO `nms_admin_log` VALUES ('2568', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1594005747');
INSERT INTO `nms_admin_log` VALUES ('2569', 'file=module', 'fulltimelink', '61.163.87.166', '1594006382');
INSERT INTO `nms_admin_log` VALUES ('2570', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1594006395');
INSERT INTO `nms_admin_log` VALUES ('2571', 'file=module', 'fulltimelink', '61.163.87.166', '1594006399');
INSERT INTO `nms_admin_log` VALUES ('2572', 'file=category&mid=24', 'fulltimelink', '61.163.87.166', '1594007171');
INSERT INTO `nms_admin_log` VALUES ('2573', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594008738');
INSERT INTO `nms_admin_log` VALUES ('2574', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=6&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594011979');
INSERT INTO `nms_admin_log` VALUES ('2575', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1594011983');
INSERT INTO `nms_admin_log` VALUES ('2576', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.+%E8%B5%B5&exports=0', 'fulltimelink', '61.163.87.166', '1594011989');
INSERT INTO `nms_admin_log` VALUES ('2577', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1594012002');
INSERT INTO `nms_admin_log` VALUES ('2578', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.+%E8%B5%B5&exports=0', 'fulltimelink', '61.163.87.166', '1594012016');
INSERT INTO `nms_admin_log` VALUES ('2579', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594012219');
INSERT INTO `nms_admin_log` VALUES ('2580', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.Ding&exports=0', 'fulltimelink', '61.163.87.166', '1594012230');
INSERT INTO `nms_admin_log` VALUES ('2581', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594012235');
INSERT INTO `nms_admin_log` VALUES ('2582', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594012271');
INSERT INTO `nms_admin_log` VALUES ('2583', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E5%B0%8F%E5%A5%8E&exports=0', 'fulltimelink', '61.163.87.166', '1594012402');
INSERT INTO `nms_admin_log` VALUES ('2584', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1594012406');
INSERT INTO `nms_admin_log` VALUES ('2585', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.+%E8%B5%B5&exports=0', 'fulltimelink', '61.163.87.166', '1594012492');
INSERT INTO `nms_admin_log` VALUES ('2586', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594012600');
INSERT INTO `nms_admin_log` VALUES ('2587', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.+%E8%B5%B5&exports=0', 'fulltimelink', '61.163.87.166', '1594012605');
INSERT INTO `nms_admin_log` VALUES ('2588', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594012608');
INSERT INTO `nms_admin_log` VALUES ('2589', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E5%B0%8F%E5%A5%8E&exports=0', 'fulltimelink', '61.163.87.166', '1594012616');
INSERT INTO `nms_admin_log` VALUES ('2590', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594012660');
INSERT INTO `nms_admin_log` VALUES ('2591', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E5%B0%8F%E5%A5%8E&exports=0', 'fulltimelink', '61.163.87.166', '1594012665');
INSERT INTO `nms_admin_log` VALUES ('2592', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594012670');
INSERT INTO `nms_admin_log` VALUES ('2593', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.++%E8%B5%B5&exports=0', 'fulltimelink', '61.163.87.166', '1594018382');
INSERT INTO `nms_admin_log` VALUES ('2594', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1594018390');
INSERT INTO `nms_admin_log` VALUES ('2595', 'moduleid=26&action=&fields=1&orderkey=SUN&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594018411');
INSERT INTO `nms_admin_log` VALUES ('2596', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594018431');
INSERT INTO `nms_admin_log` VALUES ('2597', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.Ding&exports=0', 'fulltimelink', '61.163.87.166', '1594018453');
INSERT INTO `nms_admin_log` VALUES ('2598', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594018456');
INSERT INTO `nms_admin_log` VALUES ('2599', 'moduleid=26&action=&fields=0&orderkey=Mr.+%E8%B5%B5&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594018529');
INSERT INTO `nms_admin_log` VALUES ('2600', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594018530');
INSERT INTO `nms_admin_log` VALUES ('2601', 'moduleid=26&action=&fields=0&orderkey=Mr.++%E8%B5%B5&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594018544');
INSERT INTO `nms_admin_log` VALUES ('2602', 'moduleid=26&action=&fields=1&orderkey=Mr.++%E8%B5%B5&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594018549');
INSERT INTO `nms_admin_log` VALUES ('2603', 'moduleid=26&action=&fields=1&orderkey=Mr.+%E8%B5%B5&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594018557');
INSERT INTO `nms_admin_log` VALUES ('2604', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594018559');
INSERT INTO `nms_admin_log` VALUES ('2605', 'moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1594018566');
INSERT INTO `nms_admin_log` VALUES ('2606', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E8%87%AA%E7%94%B1%E9%A3%9E&exports=0', 'fulltimelink', '61.163.87.166', '1594018578');
INSERT INTO `nms_admin_log` VALUES ('2607', 'moduleid=26&file=index&action=', 'fulltimelink', '61.163.87.166', '1594018585');
INSERT INTO `nms_admin_log` VALUES ('2608', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594018701');
INSERT INTO `nms_admin_log` VALUES ('2609', 'moduleid=26&sum=49&page=2', 'fulltimelink', '61.163.87.166', '1594018709');
INSERT INTO `nms_admin_log` VALUES ('2610', 'moduleid=26&sum=49', 'fulltimelink', '61.163.87.166', '1594018713');
INSERT INTO `nms_admin_log` VALUES ('2611', 'moduleid=26&action=revieworder', 'fulltimelink', '61.163.87.166', '1594018732');
INSERT INTO `nms_admin_log` VALUES ('2612', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594018733');
INSERT INTO `nms_admin_log` VALUES ('2613', 'moduleid=26&action=revieworder', 'fulltimelink', '61.163.87.166', '1594018735');
INSERT INTO `nms_admin_log` VALUES ('2614', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594018736');
INSERT INTO `nms_admin_log` VALUES ('2615', 'moduleid=26&action=revieworder', 'fulltimelink', '61.163.87.166', '1594019993');
INSERT INTO `nms_admin_log` VALUES ('2616', 'moduleid=26&action=successorder', 'fulltimelink', '61.163.87.166', '1594019994');
INSERT INTO `nms_admin_log` VALUES ('2617', 'moduleid=26&action=alreadyorder', 'fulltimelink', '61.163.87.166', '1594019994');
INSERT INTO `nms_admin_log` VALUES ('2618', 'moduleid=26&action=waitorder', 'fulltimelink', '61.163.87.166', '1594019995');
INSERT INTO `nms_admin_log` VALUES ('2619', 'moduleid=26&action=cancelorder', 'fulltimelink', '61.163.87.166', '1594019996');
INSERT INTO `nms_admin_log` VALUES ('2620', 'moduleid=26&action=revieworder', 'fulltimelink', '61.163.87.166', '1594019997');
INSERT INTO `nms_admin_log` VALUES ('2621', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594019998');
INSERT INTO `nms_admin_log` VALUES ('2622', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=%E5%B0%8F%E5%A5%8E&exports=0', 'fulltimelink', '61.163.87.166', '1594020346');
INSERT INTO `nms_admin_log` VALUES ('2623', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594020349');
INSERT INTO `nms_admin_log` VALUES ('2624', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.Ding&exports=0', 'fulltimelink', '61.163.87.166', '1594020354');
INSERT INTO `nms_admin_log` VALUES ('2625', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594020356');
INSERT INTO `nms_admin_log` VALUES ('2626', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.+%E8%B5%B5&exports=0', 'fulltimelink', '61.163.87.166', '1594020362');
INSERT INTO `nms_admin_log` VALUES ('2627', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594020376');
INSERT INTO `nms_admin_log` VALUES ('2628', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.++%E8%B5%B5&exports=0', 'fulltimelink', '61.163.87.166', '1594020384');
INSERT INTO `nms_admin_log` VALUES ('2629', 'moduleid=26', 'fulltimelink', '61.163.87.166', '1594020389');
INSERT INTO `nms_admin_log` VALUES ('2630', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=Mr.Ding&exports=0', 'fulltimelink', '61.163.87.166', '1594020917');
INSERT INTO `nms_admin_log` VALUES ('2631', 'moduleid=26&action=&fields=0&orderkey=&psize=20&datetype=reservetime&fromdate=&todate=&catid=0&orderstatus=&recycletype=0&itemid=0&merchantname=&exports=0', 'fulltimelink', '61.163.87.166', '1594020921');
INSERT INTO `nms_admin_log` VALUES ('2632', 'moduleid=26', 'recycle', '61.163.87.166', '1594022165');
INSERT INTO `nms_admin_log` VALUES ('2633', 'moduleid=26&sum=49&page=2', 'recycle', '61.163.87.166', '1594022188');
INSERT INTO `nms_admin_log` VALUES ('2634', 'moduleid=26&file=setting', 'recycle', '223.88.62.41', '1594780327');
INSERT INTO `nms_admin_log` VALUES ('2635', 'moduleid=26', 'recycle', '223.88.62.41', '1594780331');
INSERT INTO `nms_admin_log` VALUES ('2636', 'moduleid=26&sum=49&page=2', 'recycle', '223.88.62.41', '1594780358');
INSERT INTO `nms_admin_log` VALUES ('2637', 'moduleid=26&file=index&action=delete&itemid=63', 'recycle', '223.88.62.41', '1594780611');
INSERT INTO `nms_admin_log` VALUES ('2638', 'rand=72&moduleid=26&sum=49&page=2', 'recycle', '223.88.62.41', '1594780611');
INSERT INTO `nms_admin_log` VALUES ('3198', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81%E7%81%AF%E7%AE%A1%E7%81%AF%E6%B3%A1', 'fulltimelink', '61.163.87.166', '1596192130');
INSERT INTO `nms_admin_log` VALUES ('3199', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81%E7%81%AF%E7%AE%A1%E7%81%AF%E6%B3%A1%E5%A2%A8%E7%9B%92', 'fulltimelink', '61.163.87.166', '1596192134');
INSERT INTO `nms_admin_log` VALUES ('3200', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81%E7%81%AF%E7%AE%A1%E7%81%AF%E6%B3%A1%E5%A2%A8%E7%9B%92%E6%9D%80%E8%99%AB%E5%89%82guan', 'fulltimelink', '61.163.87.166', '1596192144');
INSERT INTO `nms_admin_log` VALUES ('3201', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81%E7%81%AF%E7%AE%A1%E7%81%AF%E6%B3%A1%E5%A2%A8%E7%9B%92%E6%9D%80%E8%99%AB%E5%89%82%E7%BD%90', 'fulltimelink', '61.163.87.166', '1596192151');
INSERT INTO `nms_admin_log` VALUES ('3202', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81%E7%81%AF%E7%AE%A1%E7%81%AF%E6%B3%A1%E5%A2%A8%E7%9B%92%E6%9D%80%E8%99%AB%E5%89%82%E7%BD%90%E7%81%AF%E5%85%B7', 'fulltimelink', '61.163.87.166', '1596192155');
INSERT INTO `nms_admin_log` VALUES ('3203', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81%E7%81%AF%E7%AE%A1%E7%81%AF%E6%B3%A1%E5%A2%A8%E7%9B%92%E6%9D%80%E8%99%AB%E5%89%82%E7%BD%90%E7%81%AF%E5%85%B7%E7%94%B5%E8%B7%AF%E6%9', 'fulltimelink', '61.163.87.166', '1596192159');
INSERT INTO `nms_admin_log` VALUES ('3204', 'file=category&mid=30&action=letter&catname=%E5%8C%96%E5%A6%86%E5%93%81%E7%94%B5%E6%B1%A0%E8%BF%87%E6%9C%9F%E8%8D%AF%E5%93%81%E7%81%AF%E7%AE%A1%E7%81%AF%E6%B3%A1%E5%A2%A8%E7%9B%92%E6%9D%80%E8%99%AB%E5%89%82%E7%BD%90%E7%81%AF%E5%85%B7%E7%94%B5%E8%B7%AF%E6%9', 'fulltimelink', '61.163.87.166', '1596192166');
INSERT INTO `nms_admin_log` VALUES ('3205', 'rand=87&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596192166');
INSERT INTO `nms_admin_log` VALUES ('3206', 'mid=30&file=category&action=add&parentid=23', 'fulltimelink', '61.163.87.166', '1596192174');
INSERT INTO `nms_admin_log` VALUES ('3207', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596192181');
INSERT INTO `nms_admin_log` VALUES ('3208', 'file=category&mid=30&action=letter&catname=%E5%85%B6%E4%BB%96%E5%9E%83%E5%9C%BE', 'fulltimelink', '61.163.87.166', '1596192189');
INSERT INTO `nms_admin_log` VALUES ('3209', 'rand=56&file=category&mid=30&parentid=23', 'fulltimelink', '61.163.87.166', '1596192189');
INSERT INTO `nms_admin_log` VALUES ('3210', 'mid=30&file=category&action=add&parentid=95', 'fulltimelink', '61.163.87.166', '1596192192');
INSERT INTO `nms_admin_log` VALUES ('3211', 'file=category&mid=30&action=letter&catname=', 'fulltimelink', '61.163.87.166', '1596192193');
INSERT INTO `nms_admin_log` VALUES ('3212', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF', 'fulltimelink', '61.163.87.166', '1596192203');
INSERT INTO `nms_admin_log` VALUES ('3213', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF%E7%83%9F%E5%A4%B4', 'fulltimelink', '61.163.87.166', '1596192207');
INSERT INTO `nms_admin_log` VALUES ('3214', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF%E7%83%9F%E5%A4%B4%E5%BA%9F%E5%8D%AB%E7%94%9F%E7%BA%B8', 'fulltimelink', '61.163.87.166', '1596192213');
INSERT INTO `nms_admin_log` VALUES ('3215', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF%E7%83%9F%E5%A4%B4%E5%BA%9F%E5%8D%AB%E7%94%9F%E7%BA%B8%E7%A0%B4%E6%97%A7%E9%99%B6%E7%93%B7%E5%93%81', 'fulltimelink', '61.163.87.166', '1596192221');
INSERT INTO `nms_admin_log` VALUES ('3216', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF%E7%83%9F%E5%A4%B4%E5%BA%9F%E5%8D%AB%E7%94%9F%E7%BA%B8%E7%A0%B4%E6%97%A7%E9%99%B6%E7%93%B7%E5%93%81%E8%84%8F%E8%A1%A3%E7%89%A9', 'fulltimelink', '61.163.87.166', '1596192230');
INSERT INTO `nms_admin_log` VALUES ('3217', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF%E7%83%9F%E5%A4%B4%E5%BA%9F%E5%8D%AB%E7%94%9F%E7%BA%B8%E7%A0%B4%E6%97%A7%E9%99%B6%E7%93%B7%E5%93%81%E8%84%8F%E8%A1%A3%E7%89%A9%E6%B1%A1%E6%9F%93%E7%9A%84%E9%A4%90%E7%9B%92', 'fulltimelink', '61.163.87.166', '1596192238');
INSERT INTO `nms_admin_log` VALUES ('3218', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF%E7%83%9F%E5%A4%B4%E5%BA%9F%E5%8D%AB%E7%94%9F%E7%BA%B8%E7%A0%B4%E6%97%A7%E9%99%B6%E7%93%B7%E5%93%81%E8%84%8F%E8%A1%A3%E7%89%A9%E6%B1%A1%E6%9F%93%E7%9A%84%E9%A4%90%E7%9B%92%E5%B', 'fulltimelink', '61.163.87.166', '1596192243');
INSERT INTO `nms_admin_log` VALUES ('3219', 'file=category&mid=30&action=letter&catname=%E5%AE%A0%E7%89%A9%E7%B2%AA%E4%BE%BF%E7%83%9F%E5%A4%B4%E5%BA%9F%E5%8D%AB%E7%94%9F%E7%BA%B8%E7%A0%B4%E6%97%A7%E9%99%B6%E7%93%B7%E5%93%81%E8%84%8F%E8%A1%A3%E7%89%A9%E6%B1%A1%E6%9F%93%E7%9A%84%E9%A4%90%E7%9B%92%E5%B', 'fulltimelink', '61.163.87.166', '1596192248');
INSERT INTO `nms_admin_log` VALUES ('3220', 'rand=17&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596192248');
INSERT INTO `nms_admin_log` VALUES ('3221', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596192254');
INSERT INTO `nms_admin_log` VALUES ('3349', 'rand=85&file=category&mid=30&parentid=33', 'fulltimelink', '61.163.87.166', '1596335799');
INSERT INTO `nms_admin_log` VALUES ('3350', 'file=category&action=edit&mid=30&catid=78', 'fulltimelink', '61.163.87.166', '1596335802');
INSERT INTO `nms_admin_log` VALUES ('3351', 'rand=91&file=category&mid=30&parentid=33', 'fulltimelink', '61.163.87.166', '1596335814');
INSERT INTO `nms_admin_log` VALUES ('3352', 'mid=30&file=category&parentid=79', 'fulltimelink', '61.163.87.166', '1596335823');
INSERT INTO `nms_admin_log` VALUES ('3353', 'file=category&action=edit&mid=30&catid=80', 'fulltimelink', '61.163.87.166', '1596335876');
INSERT INTO `nms_admin_log` VALUES ('3354', 'rand=38&file=category&mid=30&parentid=79', 'fulltimelink', '61.163.87.166', '1596335895');
INSERT INTO `nms_admin_log` VALUES ('3355', 'file=category&action=edit&mid=30&catid=81', 'fulltimelink', '61.163.87.166', '1596335898');
INSERT INTO `nms_admin_log` VALUES ('3356', 'rand=82&file=category&mid=30&parentid=79', 'fulltimelink', '61.163.87.166', '1596335908');
INSERT INTO `nms_admin_log` VALUES ('3357', 'file=category&action=edit&mid=30&catid=82', 'fulltimelink', '61.163.87.166', '1596335912');
INSERT INTO `nms_admin_log` VALUES ('3358', 'rand=70&file=category&mid=30&parentid=79', 'fulltimelink', '61.163.87.166', '1596335929');
INSERT INTO `nms_admin_log` VALUES ('3359', 'file=category&action=edit&mid=30&catid=83', 'fulltimelink', '61.163.87.166', '1596335932');
INSERT INTO `nms_admin_log` VALUES ('3360', 'rand=47&file=category&mid=30&parentid=79', 'fulltimelink', '61.163.87.166', '1596335943');
INSERT INTO `nms_admin_log` VALUES ('3361', 'file=category&action=edit&mid=30&catid=84', 'fulltimelink', '61.163.87.166', '1596335947');
INSERT INTO `nms_admin_log` VALUES ('3362', 'rand=38&file=category&mid=30&parentid=79', 'fulltimelink', '61.163.87.166', '1596335956');
INSERT INTO `nms_admin_log` VALUES ('3363', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596335959');
INSERT INTO `nms_admin_log` VALUES ('3364', 'mid=30&file=category&parentid=85', 'fulltimelink', '61.163.87.166', '1596335971');
INSERT INTO `nms_admin_log` VALUES ('3365', 'file=category&action=edit&mid=30&catid=86', 'fulltimelink', '61.163.87.166', '1596336069');
INSERT INTO `nms_admin_log` VALUES ('3366', 'rand=11&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336090');
INSERT INTO `nms_admin_log` VALUES ('3367', 'file=category&action=edit&mid=30&catid=87', 'fulltimelink', '61.163.87.166', '1596336093');
INSERT INTO `nms_admin_log` VALUES ('3368', 'rand=79&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336107');
INSERT INTO `nms_admin_log` VALUES ('3369', 'file=category&action=edit&mid=30&catid=88', 'fulltimelink', '61.163.87.166', '1596336110');
INSERT INTO `nms_admin_log` VALUES ('3370', 'rand=32&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336123');
INSERT INTO `nms_admin_log` VALUES ('3371', 'file=category&action=edit&mid=30&catid=89', 'fulltimelink', '61.163.87.166', '1596336127');
INSERT INTO `nms_admin_log` VALUES ('3372', 'rand=73&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336140');
INSERT INTO `nms_admin_log` VALUES ('3373', 'file=category&action=edit&mid=30&catid=90', 'fulltimelink', '61.163.87.166', '1596336144');
INSERT INTO `nms_admin_log` VALUES ('3374', 'rand=95&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336157');
INSERT INTO `nms_admin_log` VALUES ('3375', 'file=category&action=edit&mid=30&catid=91', 'fulltimelink', '61.163.87.166', '1596336160');
INSERT INTO `nms_admin_log` VALUES ('3376', 'rand=13&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336170');
INSERT INTO `nms_admin_log` VALUES ('3377', 'file=category&action=edit&mid=30&catid=92', 'fulltimelink', '61.163.87.166', '1596336173');
INSERT INTO `nms_admin_log` VALUES ('3378', 'rand=16&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336185');
INSERT INTO `nms_admin_log` VALUES ('3379', 'file=category&action=edit&mid=30&catid=93', 'fulltimelink', '61.163.87.166', '1596336188');
INSERT INTO `nms_admin_log` VALUES ('3380', 'rand=61&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336200');
INSERT INTO `nms_admin_log` VALUES ('3381', 'file=category&action=edit&mid=30&catid=94', 'fulltimelink', '61.163.87.166', '1596336203');
INSERT INTO `nms_admin_log` VALUES ('3382', 'rand=69&file=category&mid=30&parentid=85', 'fulltimelink', '61.163.87.166', '1596336216');
INSERT INTO `nms_admin_log` VALUES ('3383', 'mid=30&file=category&parentid=95', 'fulltimelink', '61.163.87.166', '1596336225');
INSERT INTO `nms_admin_log` VALUES ('3384', 'file=category&action=edit&mid=30&catid=96', 'fulltimelink', '61.163.87.166', '1596336290');
INSERT INTO `nms_admin_log` VALUES ('3385', 'rand=16&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336303');
INSERT INTO `nms_admin_log` VALUES ('3386', 'file=category&action=edit&mid=30&catid=97', 'fulltimelink', '61.163.87.166', '1596336306');
INSERT INTO `nms_admin_log` VALUES ('3387', 'rand=53&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336316');
INSERT INTO `nms_admin_log` VALUES ('3388', 'file=category&action=edit&mid=30&catid=98', 'fulltimelink', '61.163.87.166', '1596336319');
INSERT INTO `nms_admin_log` VALUES ('3389', 'rand=57&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336332');
INSERT INTO `nms_admin_log` VALUES ('3390', 'file=category&action=edit&mid=30&catid=99', 'fulltimelink', '61.163.87.166', '1596336337');
INSERT INTO `nms_admin_log` VALUES ('3391', 'rand=97&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336353');
INSERT INTO `nms_admin_log` VALUES ('3392', 'file=category&action=edit&mid=30&catid=100', 'fulltimelink', '61.163.87.166', '1596336356');
INSERT INTO `nms_admin_log` VALUES ('3393', 'rand=69&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336368');
INSERT INTO `nms_admin_log` VALUES ('3394', 'file=category&action=edit&mid=30&catid=101', 'fulltimelink', '61.163.87.166', '1596336372');
INSERT INTO `nms_admin_log` VALUES ('3395', 'rand=92&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336383');
INSERT INTO `nms_admin_log` VALUES ('3396', 'file=category&action=edit&mid=30&catid=102', 'fulltimelink', '61.163.87.166', '1596336386');
INSERT INTO `nms_admin_log` VALUES ('3397', 'rand=85&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336398');
INSERT INTO `nms_admin_log` VALUES ('3398', 'file=category&action=edit&mid=30&catid=103', 'fulltimelink', '61.163.87.166', '1596336400');
INSERT INTO `nms_admin_log` VALUES ('3399', 'rand=28&file=category&mid=30&parentid=95', 'fulltimelink', '61.163.87.166', '1596336410');
INSERT INTO `nms_admin_log` VALUES ('3400', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596336414');
INSERT INTO `nms_admin_log` VALUES ('3401', 'moduleid=26', 'recycle', '61.163.87.166', '1596349695');
INSERT INTO `nms_admin_log` VALUES ('3402', 'moduleid=26&file=index&action=delete&itemid=113', 'recycle', '61.163.87.166', '1596349700');
INSERT INTO `nms_admin_log` VALUES ('3403', 'rand=31&moduleid=26', 'recycle', '61.163.87.166', '1596349701');
INSERT INTO `nms_admin_log` VALUES ('3404', 'moduleid=26&file=index&action=delete&itemid=112', 'recycle', '61.163.87.166', '1596349923');
INSERT INTO `nms_admin_log` VALUES ('3405', 'rand=13&moduleid=26', 'recycle', '61.163.87.166', '1596349923');
INSERT INTO `nms_admin_log` VALUES ('3406', 'moduleid=26&file=index&action=delete&itemid=114', 'recycle', '61.163.87.166', '1596349955');
INSERT INTO `nms_admin_log` VALUES ('3407', 'rand=45&moduleid=26', 'recycle', '61.163.87.166', '1596349955');
INSERT INTO `nms_admin_log` VALUES ('3408', 'moduleid=26', 'recycle', '61.163.87.166', '1596350343');
INSERT INTO `nms_admin_log` VALUES ('3409', 'moduleid=26&file=index&action=delete&itemid=115', 'recycle', '61.163.87.166', '1596350346');
INSERT INTO `nms_admin_log` VALUES ('3410', 'rand=96&moduleid=26', 'recycle', '61.163.87.166', '1596350346');
INSERT INTO `nms_admin_log` VALUES ('3411', 'moduleid=26', 'recycle', '61.163.87.166', '1596350526');
INSERT INTO `nms_admin_log` VALUES ('3412', 'moduleid=26&file=index&action=delete&itemid=116', 'recycle', '61.163.87.166', '1596350529');
INSERT INTO `nms_admin_log` VALUES ('3413', 'rand=97&moduleid=26', 'recycle', '61.163.87.166', '1596350529');
INSERT INTO `nms_admin_log` VALUES ('3414', 'moduleid=26&file=setting', 'fulltimelink', '61.163.87.166', '1596352474');
INSERT INTO `nms_admin_log` VALUES ('3415', 'moduleid=30', 'fulltimelink', '61.163.87.166', '1596353093');
INSERT INTO `nms_admin_log` VALUES ('3416', 'moduleid=30&action=check', 'fulltimelink', '61.163.87.166', '1596353095');
INSERT INTO `nms_admin_log` VALUES ('3417', 'file=category&mid=30', 'fulltimelink', '61.163.87.166', '1596353095');
INSERT INTO `nms_admin_log` VALUES ('3418', 'mid=30&file=category&parentid=24', 'fulltimelink', '61.163.87.166', '1596353110');
INSERT INTO `nms_admin_log` VALUES ('3419', 'file=category&action=edit&mid=30&catid=37', 'fulltimelink', '61.163.87.166', '1596353114');
INSERT INTO `nms_admin_log` VALUES ('3420', 'rand=89&file=category&mid=30&parentid=24', 'fulltimelink', '61.163.87.166', '1596353227');

-- ----------------------------
-- Table structure for nms_admin_online
-- ----------------------------
DROP TABLE IF EXISTS `nms_admin_online`;
CREATE TABLE `nms_admin_online` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `moduleid` int(10) unsigned NOT NULL DEFAULT '0',
  `qstring` varchar(255) NOT NULL DEFAULT '',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `sid` (`sid`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='在线管理员';

-- ----------------------------
-- Records of nms_admin_online
-- ----------------------------
INSERT INTO `nms_admin_online` VALUES ('jtgqqev3pbpo8dfdea2924d4r2', 'fulltimelink', '61.163.87.166', '1', 'file=category&mid=30&parentid=24', '1596353227');

-- ----------------------------
-- Table structure for nms_alert
-- ----------------------------
DROP TABLE IF EXISTS `nms_alert`;
CREATE TABLE `nms_alert` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `word` varchar(100) NOT NULL DEFAULT '',
  `rate` smallint(4) unsigned NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='贸易提醒';

-- ----------------------------
-- Records of nms_alert
-- ----------------------------

-- ----------------------------
-- Table structure for nms_announce
-- ----------------------------
DROP TABLE IF EXISTS `nms_announce`;
CREATE TABLE `nms_announce` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `template` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公告';

-- ----------------------------
-- Records of nms_announce
-- ----------------------------

-- ----------------------------
-- Table structure for nms_area
-- ----------------------------
DROP TABLE IF EXISTS `nms_area`;
CREATE TABLE `nms_area` (
  `areaid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `areaname` varchar(50) NOT NULL DEFAULT '',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0',
  `arrparentid` varchar(255) NOT NULL DEFAULT '',
  `child` tinyint(1) NOT NULL DEFAULT '0',
  `arrchildid` text NOT NULL,
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`areaid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='地区';

-- ----------------------------
-- Records of nms_area
-- ----------------------------
INSERT INTO `nms_area` VALUES ('1', '默认地区', '0', '0', '0', '1', '1');

-- ----------------------------
-- Table structure for nms_article_24
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_24`;
CREATE TABLE `nms_article_24` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `subtitle` mediumtext NOT NULL,
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `copyfrom` varchar(30) NOT NULL DEFAULT '',
  `fromurl` varchar(255) NOT NULL DEFAULT '',
  `voteid` varchar(100) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  `picurl` text NOT NULL,
  `picwidth` text NOT NULL,
  `picheight` text NOT NULL,
  `pictitle` text NOT NULL,
  `picdesc` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `openid` varchar(255) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='回收分类';

-- ----------------------------
-- Records of nms_article_24
-- ----------------------------
INSERT INTO `nms_article_24` VALUES ('2', '6', '0', '0', '北环回收站每公斤5元', '', '0', '', '北环回收站每公斤5元', '', '北环回收站每公斤5元,纸类', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1592462480', 'recycle', '1593932632', '', '0', '3', '0', 'show.php?itemid=2', '', '', 'wap_user_1_18860353040');
INSERT INTO `nms_article_24` VALUES ('3', '6', '0', '0', '南四环精品收购，量大从优', '', '0', '', '南四环精品收购，量大从优', '', '南四环精品收购，量大从优,纸类', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1592463100', 'recycle', '1593932596', '', '0', '3', '0', 'show.php?itemid=3', '', '', 'wap_user_1_18860353040');

-- ----------------------------
-- Table structure for nms_article_25
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_25`;
CREATE TABLE `nms_article_25` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `subtitle` mediumtext NOT NULL,
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `copyfrom` varchar(30) NOT NULL DEFAULT '',
  `fromurl` varchar(255) NOT NULL DEFAULT '',
  `voteid` varchar(100) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  `picurl` text NOT NULL,
  `picwidth` text NOT NULL,
  `picheight` text NOT NULL,
  `pictitle` text NOT NULL,
  `picdesc` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `openid` varchar(50) NOT NULL,
  `telphone` varchar(11) NOT NULL,
  `defaultlng` varchar(255) NOT NULL,
  `defaultlat` varchar(255) NOT NULL,
  `star` float NOT NULL,
  `realname` varchar(255) NOT NULL,
  `idcard` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `idfront` varchar(255) NOT NULL,
  `idback` varchar(255) NOT NULL,
  `license` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `unid` varchar(255) NOT NULL,
  `accept` varchar(255) NOT NULL DEFAULT '1' COMMENT '默认接单',
  `grade` varchar(10) NOT NULL DEFAULT '3' COMMENT '默认普通商家',
  `punid` varchar(255) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of nms_article_25
-- ----------------------------
INSERT INTO `nms_article_25` VALUES ('1', '12', '0', '0', '1111111', '', '0', '', '123123123123123123123123123123123123123123', '', '1111111,商家', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1591759538', 'fulltimelink', '1591759577', '', '', '1', '0', 'show.php?itemid=1', '', '', 'wap_user_1_18860353040', '15225179917', '', '', '0', '翟继磊', '411424199010042855', '15225179917', '', 'http://www.baidu.com', 'http://www.baidu.com', '', 'workstation', '', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('2', '13', '0', '0', 'ceshi', '', '0', '', 'ceshissssssssss', '', 'ceshi,用户', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1591770345', 'fulltimelink', '1593658610', '', '0', '3', '0', 'show.php?itemid=2', '', '', '11224488', '', '', '', '0', '52Hz', '', '', '', '', '', '', '', '1122448899', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('3', '13', '0', '0', '', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1591779497', '', '0', '', '0', '3', '0', '', '', '', '1111', '', '113.62593', '34.74625', '0', '111', '', '', '', '', '', '', '', '1111', '', '3', '');
INSERT INTO `nms_article_25` VALUES ('5', '13', '0', '0', '', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1591842263', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25EppANTvb-LnXXHnk1c9Fv41', '', '', '', '0', 'SUN', '', '', '', '', '', '', '', 'oeVlDww6v3OnQqi8MaOM3UkqWhc01', '', '3', '');
INSERT INTO `nms_article_25` VALUES ('6', '13', '0', '0', '小奎', '', '0', '', '用户默认内容请勿改动', '', '小奎,用户', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1592010821', 'recycle', '1593854134', '', '0', '3', '0', 'show.php?itemid=6', '', '', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '', '', '', '0', '小奎', '', '', '', '', '', '', '', 'oeVlDwyOKvP43gP28-Iklyg3lgA4', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('8', '13', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1592617898', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '', '', '', '0', 'SUN', '', '', '', '', '', '', '', 'oeVlDww6v3OnQqi8MaOM3UkqWhc0', '', '', '');
INSERT INTO `nms_article_25` VALUES ('13', '13', '0', '0', '52Hz', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/gReakaPaF7gibh22tsQperxvMkW509PibdpuDjia5OJicOPEMNry8zAh0gS73A0sVmkOLWH4W4TD0qJsBhnicXjP0JQ/132', '', '', '', '', '', '', '', '1593848450', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5QorpGBWuW-DyxPnjFxVSl0', '', '', '', '0', '52Hz', '', '', '', '', '', '', '', 'oeVlDww02nQSbQul0Adh6jbEMpyw', '', '', '');
INSERT INTO `nms_article_25` VALUES ('11', '12', '0', '0', '小奎', '', '0', '', '用户默认内容请勿改动', '', '小奎,商家', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1592705461', 'recycle', '1595146369', '', '0', '3', '0', 'show.php?itemid=11', '', '', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '15937108635', '', '', '5', '小奎', '412725198802137534', '15937108635', '', 'https://cms.hnlvyi.cn/file/upload/202007/19/161229545.png', 'https://cms.hnlvyi.cn/file/upload/202007/19/161236575.png', 'https://cms.hnlvyi.cn/file/upload/202007/19/161246825.png', 'workstation', 'oeVlDwyOKvP43gP28-Iklyg3lgA4', '1', '', 'oeVlDwyOKvP43gP28-Iklyg3lgA4');
INSERT INTO `nms_article_25` VALUES ('12', '13', '0', '0', 'Mr.Ding', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1592725029', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25FQhsHOdMMbiu3kkwRoqiiM', '', '', '', '0', 'Mr.Ding', '', '', '', '', '', '', '', 'oeVlDw3nRGxax3beeaeT1sL3RO6k', '', '', '');
INSERT INTO `nms_article_25` VALUES ('14', '12', '0', '0', 'SUN', '', '0', '', '用户默认内容请勿改动', '', 'SUN,商家', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIbqibCPVqSIbED7hriclpficpQgzqwNugOA9xd33SyrzZJW2P7SjMcj1iaDJLgrh6E5KaDHvGjKkWKsA/132', '', '', '', '', '', '', '', '1593849878', 'fulltimelink', '1593854750', '', '0', '3', '0', 'show.php?itemid=14', '', '', 'o6NpL5BEkFnO6pInMqNAUTs-TdZI', '', '113.62493', '34.74725', '3', '测试测试', '412725198901115770', '15225179917', '河南省郑州市中原区中原中路233号', 'https://upload.recycle.fulltime.vip/c3xppz2eq7drhhilhi.png', 'https://upload.recycle.fulltime.vip/c3xpq0imc8f97dvhjg.png', '', 'merchant', 'oeVlDww6v3OnQqi8MaOM3UkqWhc0', '1', '', 'oeVlDwyOKvP43gP28-Iklyg3lgA4');
INSERT INTO `nms_article_25` VALUES ('15', '12', '0', '0', '52Hz', '', '0', '', '用户默认内容请勿改动', '', '52Hz,商家', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/nz7ibbjxZz3BHGeKFTezbYCycTjTHoKWGmU1LPjT7a7tMBibZvnibMQhraNpmkokGkjAnAVkd6thhAmk5NZtmxqXw/132', '', '', '', '', '', '', '', '1593849916', 'fulltimelink', '1593851650', '', '0', '3', '0', 'show.php?itemid=15', '', '', 'o6NpL5IxxekaXyWwt2YO61giyDPE', '', '113.67823', '34.767033', '0', '测试测试', '411424199010042855', '15225179917', '河南省郑州市金水区纬一路2号院', 'https://upload.recycle.fulltime.vip/c3xp2n8jhn9hznfomu.jpg', 'https://upload.recycle.fulltime.vip/c3xp2ornpsfmtur4nc.jpg', '', 'merchant', 'oeVlDww02nQSbQul0Adh6jbEMpyw', '1', '', 'oeVlDwyOKvP43gP28-Iklyg3lgA4');
INSERT INTO `nms_article_25` VALUES ('16', '12', '0', '0', 'in time', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DhhBZ7jStKXA2PfZNeBAHpyfvh5KcB52qZT9Ep4bR0ZCpIibwbIV83Ld4Jfz7CYsprHwwjQfSEFm6WibyauiaDyKw/132', '', '', '', '', '', '', '', '1593850636', '', '0', '', '0', '2', '0', '', '', '', 'o6NpL5OGd09gYPWMLwB9fGHJsUSg', '', '', '', '0', 'in time', '', '', '', '', '', '', '', 'oeVlDw6VNJzzfY0Jtxl27PofDCIw', '', '', '');
INSERT INTO `nms_article_25` VALUES ('34', '12', '0', '0', '自由飞', '', '0', '', '用户默认内容请勿改动', '', '自由飞,商家', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83ermcQr5IQh0AopTMqtb16a7gNlDBTyO7tVeU1XEu9S3sy7ydeRV3Tjqibol4AOQ5UpHWyHqEWGSv0Q/132', '', '', '', '', '', '', '', '1595147968', 'fulltimelink', '1595148092', '', '0', '3', '0', 'show.php?itemid=34', '', '', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '', '113.67822', '34.76712', '0', '飞飞', '410482199107263331', '15903600834', '河南省郑州市金水区纬一路2号院', 'https://upload.recycle.fulltime.vip/c4ags5xe6w5jibog7q.jpg', 'https://upload.recycle.fulltime.vip/c4ags7l9j0grawchgv.jpg', '', 'merchant', 'oeVlDw-PLUEcKe1b9Aazq0G5ZEFA', '1', '3', 'oeVlDwyOKvP43gP28-Iklyg3lgA4');
INSERT INTO `nms_article_25` VALUES ('53', '12', '0', '0', 'Mr .Ding', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpfaXSzq1MRicZibcVxJFkzXUILPXHv2TZJUicCveQsrdS5X3YuVy16n1NlrEUrkkApzGZibkdKZwouA/132', '', '', '', '', '', '', '', '1596188248', '', '0', '', '0', '2', '0', '', '', '', 'o6NpL5PrW3eblP2VX7sWEArwsfbs', '', '', '', '0', 'Mr .Ding', '', '', '', '', '', '', '', 'oeVlDw5lDGZKAKREb_wiQ2VeDikc', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('18', '13', '0', '0', 'Mr.  赵', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593935390', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25EIFv-LKIgjkr-g6sBt68Ww', '', '', '', '0', 'Mr.  赵', '', '', '', '', '', '', '', 'oeVlDw40v48-fWycgYmTWErq0vP0', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('19', '13', '0', '0', 'Mr.  赵', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Dcw13OSpWuW8dQZXVfzkWTrjDKylrxYmhItw7P5f79T5HCgYicGFcIzoC1gGQ6uZkzDv83r65szWict6c39qAJ4A/132', '', '', '', '', '', '', '', '1593936302', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5QfGGRPxuex2-nIdrFdsMOI', '', '', '', '0', 'Mr.  赵', '', '', '', '', '', '', '', 'oeVlDw40v48-fWycgYmTWErq0vP0', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('20', '12', '0', '0', 'Mr.  赵', '', '0', '', '用户默认内容请勿改动', '', 'Mr.  赵,商家', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/6Ok11JsaKqLfuD5YgoNzzEoNiaicQdvztsw3JQ5MPuHI5KQuyYGkJ2YEra48joiaN8l9K5s1pPgxfiaO9VJJSicMcicQ/132', '', '', '', '', '', '', '', '1593936496', 'recycle', '1593937739', '', '0', '3', '0', 'show.php?itemid=20', '', '', 'o6NpL5J8a0quIErp2B76IJeJnf1c', '', '113.70515', '34.70554', '0', '丢手绢', '412725198911260330', '15127843783', '河南省郑州市管城回族区中州大道', 'https://upload.recycle.fulltime.vip/c3yjqygtaxz6ehzrnj.jpg', 'https://upload.recycle.fulltime.vip/c3yjr0t52l42argqrd.jpg', '', 'merchant', 'oeVlDw40v48-fWycgYmTWErq0vP0', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('21', '13', '0', '0', 'Mr.Ding', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/V9aueT1cHJ7XGibB4HiasSTwZaOUNvFE3ibcVEYiaBw9Ua3awkFP2W62vwBfoJ2l0Ycv5oicC3U6QibBVTiazQk7YJxcQ/132', '', '', '', '', '', '', '', '1593936659', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5a79ntGaBHFtibxtfyfjIyk', '', '', '', '0', 'Mr.Ding', '', '', '', '', '', '', '', 'oeVlDw3nRGxax3beeaeT1sL3RO6k', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('22', '12', '0', '0', 'Mr.Ding', '', '0', '', '用户默认内容请勿改动', '', 'Mr.Ding,商家', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/vGQrdN7b9aeFCzmLfAxjjCusTEDAA7VwT7fdlt5shwH7ibFlRocxvfdX3Ga8EibOkkYGWGT8e3mJfgJmsibDicdX3w/132', '', '', '', '', '', '', '', '1593936985', 'recycle', '1593937708', '', '0', '3', '0', 'show.php?itemid=22', '', '', 'o6NpL5CCNZf-xJmTDA6TgBu7aGOQ', '', '113.70507', '34.705555', '0', '丁超然', '412725198910170333', '17719891017', '河南省郑州市管城回族区中州大道', 'https://upload.recycle.fulltime.vip/c3yjquwwg3m315apmf.jpg', 'https://upload.recycle.fulltime.vip/c3yjqxqtmqklqevkkp.jpg', '', 'merchant', 'oeVlDw3nRGxax3beeaeT1sL3RO6k', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('23', '13', '0', '0', '铁拳ROCKY', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83ergX4Q6fwmGmJib6q21U2ib8jUkPQ903VR4NQ6ISmnCjIIaz6B9TBcQiaR1wr6efj7Qk42P7d44mwicqg/132', '', '', '', '', '', '', '', '1594890425', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5c-NES6RYsTyCoAVJR7IbBs', '', '', '', '0', '铁拳ROCKY', '', '', '', '', '', '', '', 'oeVlDw78kQA0dodOR_btwgFzWjjQ', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('24', '13', '0', '0', '铁拳ROCKY', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1594890568', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25BeH1avY8wPNWIowU0mA0pI', '', '', '', '0', '铁拳ROCKY', '', '', '', '', '', '', '', 'oeVlDw78kQA0dodOR_btwgFzWjjQ', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('25', '13', '0', '0', '52Hz', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595128687', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25Az1HuUeAisQuR9lKpeFpWg', '', '', '', '0', '52Hz', '', '', '', '', '', '', '', 'oeVlDww02nQSbQul0Adh6jbEMpyw', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('26', '13', '0', '0', 'in time', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/k4fsWLdqvWjRpOVnJvniaI8xZe4slFQxgIbNT4oXH4ewZAkYxM3FG7RWLVcZcFxyKbxqAhHaSnD6b9EQ08nGd2w/132', '', '', '', '', '', '', '', '1595132827', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5W0BFGtBudAeZGv49Ra78tM', '', '', '', '0', 'in time', '', '', '', '', '', '', '', 'oeVlDw6VNJzzfY0Jtxl27PofDCIw', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('27', '13', '0', '0', '陈惠如', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595138856', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25CLs43xqQOKTiDzVR5d-59U', '', '', '', '0', '陈惠如', '', '', '', '', '', '', '', 'oeVlDw7V2JN2oRrqyA1nrhuHaA-U', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('28', '13', '0', '0', '赖哲荣', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595140494', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25ILrLewAqlfbg2iUbpf6x9Y', '', '', '', '0', '赖哲荣', '', '', '', '', '', '', '', 'oeVlDw2EcXrGpfYpsVxnjntKASKI', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('30', '13', '0', '0', '福星高照', '', '0', '', '用户默认内容请勿改动123', '', '福星高照,用户', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/INc61dATFS8jYsK3T868qMXPnt50R3pstrmMnDM8o8I9RjAfXJsp9zLCdlmt2SKADCL2qnTIRTCEyttYBmMLBQ/132', '', '', '', '', '', '', '', '1595146727', 'fulltimelink', '1595147263', '', '0', '3', '0', 'show.php?itemid=30', '', '', 'oRRhA5SuBGtbTw5nSik5Qe7Ebhl8', '', '', '', '0', '福星高照', '', '', '', '', '', '', '', 'oeVlDw1FA-nP0SFJviQ_Er4XEX1A', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('35', '12', '0', '0', '低调', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/siacArmwCKLHusicFUmZ7XB3ESX18POfxVKWcbNh0HmwAbOibzictTArZibvy3SnFtroeDiaGiamCb9fibhia5bzUpELHtQ/132', '', '', '', '', '', '', '', '1595148143', '', '0', '', '0', '2', '0', '', '', '', 'o6NpL5FJDXH05iXs7ozle_o62_2k', '', '', '', '0', '低调', '', '', '', '', '', '', '', 'oeVlDw22GO8Dr7rsQJ2PMMBA29zM', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('36', '12', '0', '0', '吴慈山', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmhead/5gRq3ia05J69p4zc1Cthd1iajysUvymMSYxveib9ybGJ20/132', '', '', '', '', '', '', '', '1595207237', '', '0', '', '0', '2', '0', '', '', '', 'o6NpL5KpdFt6Heze8MEGdYwZv_2M', '', '', '', '0', '吴慈山', '', '', '', '', '', '', '', 'oeVlDw_nVz9HSHd78wwMi7V0jJuw', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('37', '12', '0', '0', '小可爱', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJu6UPy60jo9uHLIoEKFq8dicMZKlRZQj7HamC4Q2mquZgXep0NoUankHjV4JxAJdZLdwHoolhvdjQ/132', '', '', '', '', '', '', '', '1595207292', '', '0', '', '0', '2', '0', '', '', '', 'o6NpL5LLp8llTqAiRetiP-FD6iBQ', '', '', '', '0', '小可爱', '', '', '', '', '', '', '', 'oeVlDwx3U7c5dQEOvaaX_zf8RwC8', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('38', '13', '0', '0', 'S、', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKoA9icgeAxSEyibHrtKk9alQ7iahLlIXvpFrnbbDSql5fQGO5DVFpWyLicydTlkBaI0Wia84WV26hflFA/132', '', '', '', '', '', '', '', '1595231766', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5XSm2AW199gciLlM70Dp9Vo', '', '', '', '0', 'S、', '', '', '', '', '', '', '', 'oeVlDw7y630Ex6B-C_EIVqmf4tHE', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('39', '13', '0', '0', 'PIU· ', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83ereczJ5oJzz9eoN0CBBbALsfVtDriclgPd8JIdicIbUQpIO1X2dtmadQmAIacFqYU2864ibaHx43yT6Q/132', '', '', '', '', '', '', '', '1595231958', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5c3HWhj6aoxvDy6MjC_6ods', '', '', '', '0', 'PIU· ', '', '', '', '', '', '', '', 'oeVlDw5UwthNg5aH-_CsqDD4ukTU', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('40', '13', '0', '0', 'Mr .Ding', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ1ibx8k4eia1wsnVEnOKsbglqGBXyorz8LFaFT9INUnEaDrwZO1PzMLdicQegaqYiaa90R5pw6LQxlFg/132', '', '', '', '', '', '', '', '1595241240', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5RiwqJMJhLOZZijImYPLzu0', '', '', '', '0', 'Mr .Ding', '', '', '', '', '', '', '', 'oeVlDw5lDGZKAKREb_wiQ2VeDikc', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('41', '12', '0', '0', '祖冠珠', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmhead/NVscISQ1icjE2ic4gKXfx5HajElCnAMJQPRr3W3UQEHZM/132', '', '', '', '', '', '', '', '1595306206', '', '0', '', '0', '2', '0', '', '', '', 'o6NpL5A7j16JmLVzEuE91YwbCoFY', '', '', '', '0', '祖冠珠', '', '', '', '', '', '', '', 'oeVlDw_rsx6uM_PTlxt7PQR7IuAU', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('42', '13', '0', '0', '蜻一点设计业务部', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/qZpNEGdyhj3sQ3YgHahHuXLqTC76vYrX56MDjI0FXiaoseKyiada5SrGib3yEem4Dic4pDW52OuyxY1ic7lkaqQ5T7g/132', '', '', '', '', '', '', '', '1595312405', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5chFplg7cKI_6KQ2J5G4XqU', '', '', '', '0', '蜻一点设计业务部', '', '', '', '', '', '', '', 'oeVlDw-qjwcIgrCLw-MO5WZhBD1g', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('43', '13', '0', '0', '终。', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595336798', '', '0', '', '0', '3', '0', '', '', '', 'oHqq25NVqCTtyYSlIz7tFw9wpMLQ', '', '', '', '0', '终。', '', '', '', '', '', '', '', 'oeVlDwyLch6B7qcCJxnZyiLNSohM', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('44', '13', '0', '0', '终。', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/kpQw1lHLxnzTsqQeKOPmKYArQDr1MricJvvqRgJdCymxcTKRiazWavdsLN4rL6qb4sqOAwucHnVu90UphO9ueCBA/132', '', '', '', '', '', '', '', '1595336855', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5aFJJcpRNfEoaSZvXDjPeIk', '', '', '', '0', '终。', '', '', '', '', '', '', '', 'oeVlDwyLch6B7qcCJxnZyiLNSohM', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('45', '12', '0', '0', '终。', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/MLFkN5riaDqoaLboId5k5xmCNvIoPMzibB8Y5Oria3ibDvLVvmvX0icJI7lDapsMNicxl300eDrOQg5oXAvZUWfNfTRw/132', '', '', '', '', '', '', '', '1595336865', '', '1595337019', '', '0', '2', '0', '', '', '', 'o6NpL5IOJNl7sRNqjLcSWQXtg9OA', '', '118.66323', '39.500713', '0', '苏鹏杰', '130434199709034431', '13234435206', '河北省唐山市滦南县', 'https://upload.recycle.fulltime.vip/c4cbqbw1vunuyqnjye.jpg', 'https://upload.recycle.fulltime.vip/c4cbqpnoxdabxwgep8.jpg', '', 'merchant', 'oeVlDwyLch6B7qcCJxnZyiLNSohM', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('46', '13', '0', '0', '豆芽', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/bfaKLIricIhM3zTqKIpCgTEjlsF1LwIssmeOy6ickdgJic2icMJQGnpAqJibb9v2pXaicvOQZ5mDXO7MrfrSwjB0VdzQ/132', '', '', '', '', '', '', '', '1595341553', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5ak5zw4pNuplu1-Q2UX0k_c', '', '', '', '0', '豆芽', '', '', '', '', '', '', '', 'oeVlDw4NLTSqjXUQUQh1PLcxF9lU', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('47', '13', '0', '0', '小白', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo8cWvv0hsl4SlxqSJ2MFQzPE4mmSTQPYmgR1HWic0kZdq5Bib0GrmaQQGMEBdmfZ5orZakHWV7fJpw/132', '', '', '', '', '', '', '', '1595380634', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5ZaURzhVA18CYky4vU45t5E', '', '', '', '0', '小白', '', '', '', '', '', '', '', 'oeVlDw-MoQyiiqiNNjVBP7Ka5hVM', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('48', '13', '0', '0', 'A注册公司 郑州金阔财务陈老师', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKDtm9ANlVnQ8BPLic0ayhbGzCOUj6Sn7dqTSpZ2DdLWpbyEyTMOaexSQSmU8kpkGH7QwqcbFK9icnA/132', '', '', '', '', '', '', '', '1595497816', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5TH4PZ7vEoW1nyKwLmBl2kw', '', '', '', '0', 'A注册公司 郑州金阔财务陈老师', '', '', '', '', '', '', '', 'oeVlDwzG3vyqGbCYsLmBBWOHFb50', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('49', '13', '0', '0', '超_越梦想', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/pftx8PrTibZqbvEDpRKHSJ3iczmmc2SOgic1F79SCsMZndpn6W0ibNmibmSeu50W6I1E8TPe4YAWTWTvvGSCaXJH99A/132', '', '', '', '', '', '', '', '1595897119', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5XuGIYzQrSEaNXcnFbDDBlI', '', '', '', '0', '超_越梦想', '', '', '', '', '', '', '', 'oeVlDw7Cnk5ebzC5oVZ10EPWvi0o', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('51', '13', '0', '0', '自由飞', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eq63TMP8ibq4NAg6x53JBhWckjvIb5rU20z56UHTqicHCvcmEACicvFJAwyLgYMpLbia6eVuicw7Psn7Pw/132', '', '', '', '', '', '', '', '1595905623', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5Y2VAUGs3iK0CejL_pIXwLs', '', '', '', '0', '自由飞', '', '', '', '', '', '', '', 'oeVlDw-PLUEcKe1b9Aazq0G5ZEFA', '1', '3', 'oeVlDww6v3OnQqi8MaOM3UkqWhc0');
INSERT INTO `nms_article_25` VALUES ('52', '13', '0', '0', '柠檬柚', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLaaW0S15CAF7moiaX7pYJ5V3D7ibN0y7zBn8ZuGDoFhAyRnk4Jx2WG6ic0D7KlUnUT8Gcw36ckUXj5w/132', '', '', '', '', '', '', '', '1596099827', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5VY-5GAd4mgk1ADxjGfxwtQ', '', '', '', '0', '柠檬柚', '', '', '', '', '', '', '', 'oeVlDw3OXfIlVbhHY1kzlz0m6nuM', '1', '3', '');
INSERT INTO `nms_article_25` VALUES ('54', '13', '0', '0', 'Kent', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqk2LpYfWO8ylZQLEIWSVxYKfQGddeFPRWRh2MLqSU9vMmZUTJbUurF9N5O5AZhGY1P7MATApQ64g/132', '', '', '', '', '', '', '', '1596264679', '', '0', '', '0', '3', '0', '', '', '', 'oRRhA5aWCfpODkmDmlO5PHFkmB2g', '', '', '', '0', 'Kent', '', '', '', '', '', '', '', 'oeVlDw4v14Rjs-WCWiInlqZndPRA', '1', '3', '');

-- ----------------------------
-- Table structure for nms_article_26
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_26`;
CREATE TABLE `nms_article_26` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `subtitle` mediumtext NOT NULL,
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `copyfrom` varchar(30) NOT NULL DEFAULT '',
  `fromurl` varchar(255) NOT NULL DEFAULT '',
  `voteid` varchar(100) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  `picurl` text NOT NULL,
  `picwidth` text NOT NULL,
  `picheight` text NOT NULL,
  `pictitle` text NOT NULL,
  `picdesc` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `ordernum` varchar(50) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `usertelphone` varchar(20) NOT NULL,
  `useraddress` text NOT NULL,
  `userlng` varchar(255) NOT NULL,
  `userlat` varchar(255) NOT NULL,
  `reservetime` int(10) NOT NULL,
  `userrecycletype` varchar(255) NOT NULL,
  `merchantid` varchar(255) NOT NULL,
  `userwritea` text NOT NULL,
  `successtime` varchar(255) NOT NULL,
  `taketime` varchar(255) NOT NULL,
  `rounds` int(10) NOT NULL,
  `flag` int(10) NOT NULL,
  `caceltype` int(10) NOT NULL DEFAULT '0',
  `locktime` varchar(10) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of nms_article_26
-- ----------------------------
INSERT INTO `nms_article_26` VALUES ('35', '14', '0', '0', '小奎', '', '0', '', 'null', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593661271', '', '1593677633', '115.60.21.25', '0', '0', '1', 'show.php?itemid=35', '', '', '256-9E1A60B9-B9A9-4E31-ADFA-02E388EDFDAA', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市金水区金水东路', '113.75318915221015', '34.764036182551806', '0', '7,8', '', '美声中心', '', '', '3', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('2', '14', '0', '0', '小奎', '', '11.6', '', '速度', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3i79h0d0vkqcdtcon.png', '', '', '', '', '', '1592277498', '', '0', '115.60.23.39', '0', '3', '0', '', '', '', '256-18EC54C4-7C09-70D9-5DCF-672B90699322', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市中原区中原中路233号', '113.62493', '34.74725', '0', '8,7', 'oRRhA5QFiKpd1vcOZxqcOuPTU999', '大萨达撒多', '1592475203', '', '0', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('39', '14', '0', '0', '小奎', '', '0', '', 'null', '', '小奎,抢单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593678396', 'recycle', '1593679665', '223.104.105.43', '0', '0', '1', 'show.php?itemid=39', '', '', '256-D9B19D3A-7A67-764C-6A45-95D6308E83CB', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市金水区金水东路', '113.75355260293337', '34.76406690620066', '1593679643', '7,8', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '1122', '1593679643', '1593678399', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('36', '14', '0', '0', '小奎', '', '0', '', 'null', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593677840', '', '1593677885', '223.104.105.43', '0', '0', '1', 'show.php?itemid=36', '', '', '256-4F15DA69-D40B-ADDD-91B4-1C94686F400C', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市金水区金水东路', '113.75327574960615', '34.763911929194', '0', '7,8', '', '任命路', '', '', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('37', '14', '0', '0', '小奎', '', '0', '', 'null', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593677978', '', '1593677996', '223.104.105.43', '0', '0', '1', 'show.php?itemid=37', '', '', '256-E43E0241-0A9F-B036-2B7D-E5A5BE9D3E80', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '444', '河南省郑州市金水区金水东路', '113.75324050206713', '34.7641034886827', '0', '7,8', '', '在于他们在', '', '', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('38', '14', '0', '0', '小奎', '', '0', '', 'null', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593678087', '', '1593678117', '223.104.105.43', '0', '0', '1', 'show.php?itemid=38', '', '', '256-027FD1E8-0DE3-4FEB-B7AF-62716EF3737F', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '111', '河南省郑州市金水区金水东路', '113.75320775407555', '34.76399048262902', '0', '7,8', '', '115', '', '', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('111', '14', '0', '0', 'SUN', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596180136', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=111', '', '', '2020073115221600008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18866666666', '河南省郑州市金水区红旗路', '113.69186401367188', '34.77640914916992', '1596178800', '7,10,11', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '测测测', '1596180163', '1596180151', '1', '0', '0', '1596180278');
INSERT INTO `nms_article_26` VALUES ('110', '14', '0', '0', '自由飞', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596173975', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=110', '', '', '2020073113393500051', 'oRRhA5Y2VAUGs3iK0CejL_pIXwLs', '15900000003', '河南省郑州市金水区纬一路2号院', '113.67817', '34.767017', '1596175200', '7', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '666', '1596177795', '1596173998', '1', '0', '0', '1596177909');
INSERT INTO `nms_article_26` VALUES ('109', '14', '0', '0', '自由飞', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596173880', '', '1596173939', '61.163.87.166', '0', '0', '1', 'show.php?itemid=109', '', '', '2020073113380000051', 'oRRhA5Y2VAUGs3iK0CejL_pIXwLs', '15900000000', '河南省郑州市金水区纬一路2号院', '113.67822', '34.76712', '1596175200', '6', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '5555', '', '1596173914', '1', '0', '1', '');
INSERT INTO `nms_article_26` VALUES ('108', '14', '0', '0', '自由飞', '', '0', '', '', '', '自由飞,抢单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596172649', 'fulltimelink', '1596173835', '61.163.87.166', '0', '0', '1', 'show.php?itemid=108', '', '', '2020073113172900051', 'oRRhA5Y2VAUGs3iK0CejL_pIXwLs', '15900000000', '河南省郑州市金水区纬一路2号院', '113.67816', '34.76702', '1596168000', '7,8', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '444', '1596173832', '1596172667', '1', '0', '0', '2020-07-31');
INSERT INTO `nms_article_26` VALUES ('107', '14', '0', '0', '自由飞', '', '0', '', '', '', '自由飞,抢单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596172470', 'fulltimelink', '1596172616', '61.163.87.166', '0', '0', '1', 'show.php?itemid=107', '', '', '2020073113143000051', 'oRRhA5Y2VAUGs3iK0CejL_pIXwLs', '15900000000', '河南省郑州市金水区纬一路2号院', '113.67822', '34.76712', '1596175200', '6,8,7', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '333', '1596172611', '1596172568', '2', '0', '0', '2020-07-31');
INSERT INTO `nms_article_26` VALUES ('106', '14', '0', '0', '自由飞', '', '0', '', '', '', '自由飞,抢单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596172262', 'fulltimelink', '1596172426', '61.163.87.166', '0', '0', '1', 'show.php?itemid=106', '', '', '2020073113110200051', 'oRRhA5Y2VAUGs3iK0CejL_pIXwLs', '15900000000', '河南省郑州市金水区纬一路2号院', '113.67822', '34.76712', '1596168000', '6,8', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '222', '1596172421', '1596172303', '1', '0', '0', '2020-07-31');
INSERT INTO `nms_article_26` VALUES ('105', '15', '0', '0', '自由飞', '', '0', '', '', '', '自由飞,派单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596167908', 'fulltimelink', '1596172150', '61.163.87.166', '0', '0', '1', 'show.php?itemid=105', '', '', '2020073111582800051', 'oRRhA5Y2VAUGs3iK0CejL_pIXwLs', '15900000000', '河南省郑州市金水区纬一路2号院', '113.678154', '34.76701', '1596168000', '6,7,8', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '1111', '1596172147', '1596168091', '3', '0', '0', '2020-07-31');
INSERT INTO `nms_article_26` VALUES ('103', '14', '0', '0', 'SUN', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596162960', '', '0', '223.104.107.242', '0', '3', '1', 'show.php?itemid=103', '', '', '2020073110360000008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860666666', '河南省郑州市金水区纬一路2号院', '113.67815103708327', '34.76707923345361', '1596160800', '6,10', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '我们', '1596164616', '1596162984', '1', '0', '0', '1596164730');
INSERT INTO `nms_article_26` VALUES ('57', '14', '0', '0', 'SUN', '', '0', '', '测测', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593871945', '', '1593872059', '39.144.22.15', '0', '0', '1', 'show.php?itemid=57', '', '', '2020070422122500008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区魏庄西街', '113.69737035250772', '34.70587687240107', '0', '7,10,9', '', '我', '', '', '2', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('49', '14', '0', '0', '小奎', '', '0', '', 'null', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593853970', '', '1593854192', '115.60.19.9', '0', '0', '1', 'show.php?itemid=49', '', '', '0000620200704171250', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市中原区中原中路233号', '113.62493', '34.74725', '0', '8', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '123123', '', '1593854151', '3', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('52', '14', '0', '0', '小奎', '', '0', '', 'null', '', '小奎,抢单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593855101', 'recycle', '1593857256', '115.60.19.9', '0', '3', '1', 'show.php?itemid=52', '', '', '0000620200704173141', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市中原区中原中路233号', '113.62493', '34.74725', '1593857250', '7,8', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '12121', '1593857250', '1593856043', '3', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('53', '14', '0', '0', 'SUN', '', '0', '', '第一个', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3xqujmoi0xzptewdw.jpg', '', '', '', '', '', '1593856196', '', '1593856354', '61.163.87.166', '0', '0', '1', 'show.php?itemid=53', '', '', '0000820200704174956', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市金水区纬一路2号院', '113.67816767087395', '34.767100751927636', '0', '7,10', '', '我们', '', '', '3', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('120', '14', '0', '0', '小奎', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596352499', '', '0', '61.163.87.166', '0', '1', '1', 'show.php?itemid=120', '', '', '2020080215145900006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省焦作市温县获轵线', '113.0970068928528', '34.95336536320088', '1596351600', '7,8', '', 'ewerewrewr', '', '', '3', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('104', '15', '0', '0', 'SUN', '', '0', '', '', '', 'SUN,派单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596164693', 'fulltimelink', '1596172155', '223.104.107.242', '0', '0', '1', 'show.php?itemid=104', '', '', '2020073111045300008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '15855555555', '河南省郑州市金水区纬一路2号院', '113.67814118793775', '34.767057401902946', '1596160800', '7,10,9', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '我们', '1596172153', '1596164876', '3', '0', '0', '2020-07-31');
INSERT INTO `nms_article_26` VALUES ('26', '15', '0', '0', '52Hz', '', '0', '', 'null', '', '52Hz,派单', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3lqk8qbk4voocxgtb.jpg', '', '', '', '', '', '1592636622', 'recycle', '1596015612', '223.88.94.207', '0', '0', '0', 'show.php?itemid=26', '', '', '2510-6E8838A2-91CD-8ABE-3B49-A2E515FD885D', 'oRRhA5QorpGBWuW-DyxPnjFxVSl0', '15225179917', '河南省郑州市管城回族区玉凤路', '113.7099', '34.750095', '1593599326', '7', 'oRRhA5QFiKpd1vcOZxqcOuPTU999', '测试测试', '1593599326', '1592636806', '3', '0', '2', '');
INSERT INTO `nms_article_26` VALUES ('93', '15', '0', '0', '小奎', '', '0', '', '向第三方', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595475489', '', '1596336135', '115.60.20.231', '0', '0', '1', 'show.php?itemid=93', '', '', '2020072311380900006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市中原区中原西路233号', '113.62493', '34.74725', '1595473200', '7,8', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '时代峰峻第三方第三方', '', '1595475671', '3', '0', '2', '');
INSERT INTO `nms_article_26` VALUES ('61', '14', '0', '0', '小奎', '', '0.01', '', 'null', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593914367', '', '0', '223.88.62.117', '0', '3', '1', 'show.php?itemid=61', '', '', '2020070509592700006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市中原区中原中路220号裕达国贸写字楼', '113.62289', '34.74693', '0', '7,8', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '12121', '1593926352', '1593914481', '2', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('88', '14', '0', '0', 'SUN', '', '0.01', '', '东西很多', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c4aez21qmzfjlpnrn9.jpg|https://upload.recycle.fulltime.vip/c4aez61l42qlt2epvg.jpg', '', '', '', '', '', '1595142940', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=88', '', '', '2020071915154000008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市金水区纬一路2号院', '113.67821910487454', '34.767049747218735', '1595145600', '7,10,6', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '广播电视台', '1595142985', '1595142947', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('84', '14', '0', '0', '小奎', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1594830368', '', '1594830383', '117.136.106.58', '0', '0', '1', 'show.php?itemid=84', '', '', '2020071600260800006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '14712211242121', '河南省郑州市金水区金水东路', '113.75322', '34.76571', '1594828800', '8,7', '', 'dd', '', '', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('85', '14', '0', '0', '小奎', '', '0', '', 'xsd', '', '小奎,抢单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595050543', 'fulltimelink', '1595143309', '223.88.62.22', '0', '0', '1', 'show.php?itemid=85', '', '', '2020071813354300006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市金水区金水东路', '113.75322', '34.76571', '1595034000', '8', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', 'xx', '1595139792', '1595050554', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('64', '14', '0', '0', 'SUN', '', '0.01', '', '我们的', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yhnysxc9a9oxtvbz.jpg', '', '', '', '', '', '1593931797', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=64', '', '', '2020070514495700008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路', '113.69727343232047', '34.7035564054611', '1593928800', '7,6', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '二期', '1593931888', '1593931804', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('102', '14', '0', '0', 'SUN', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596161612', '', '0', '223.104.107.242', '0', '3', '1', 'show.php?itemid=102', '', '', '2020073110133200008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18888888888', '河南省郑州市金水区纬一路2号院', '113.67815510667286', '34.76708486992071', '1596160800', '7,10,8', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '我们', '1596162706', '1596161634', '1', '0', '0', '1596162821');
INSERT INTO `nms_article_26` VALUES ('66', '14', '0', '0', 'SUN', '', '0.01', '', '我们', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yhqouk072dgfudzv.jpg', '', '', '', '', '', '1593932017', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=66', '', '', '2020070514533700008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '我们的生活', '河南省郑州市管城回族区紫东路', '113.69215290984496', '34.70416400404588', '1593928800', '7,10', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '测试', '1593932257', '1593932147', '3', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('67', '14', '0', '0', 'SUN', '', '0.01', '', '你好', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yhulw08utfag0ulz.jpg', '', '', '', '', '', '1593932319', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=67', '', '', '2020070514583900008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路72-47号', '113.69735609293313', '34.70189567273519', '1593928800', '7,10', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '擦擦画画', '1593932444', '1593932333', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('68', '14', '0', '0', 'SUN', '', '0.01', '', '我们', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yhxqn5erkrfidpys.jpg', '', '', '', '', '', '1593932563', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=68', '', '', '2020070515024300008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路72-18号', '113.69660405539555', '34.70349303024438', '1593928800', '7,10,11', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '我要', '1593932664', '1593932582', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('69', '14', '0', '0', 'SUN', '', '0.01', '', '123444', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yhzxi7mvjn9ovt4d.jpg', '', '', '', '', '', '1593932735', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=69', '', '', '2020070515053500008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路72-19号', '113.69683100145417', '34.7030635044218', '1593928800', '7,10,9', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '好', '1593932776', '1593932740', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('70', '14', '0', '0', 'SUN', '', '0.01', '', '我们的', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yi1ucz6q0rgbtoqt.jpg', '', '', '', '', '', '1593932884', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=70', '', '', '2020070515080400008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路', '113.696948406683', '34.7037313712949', '1593928800', '7,10', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '你的', '1593933017', '1593932938', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('71', '14', '0', '0', 'SUN', '', '0.01', '', '123', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yi5so586cw52pzyn.jpg', '', '', '', '', '', '1593933194', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=71', '', '', '2020070515131400008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路72-30号', '113.69717470067933', '34.702744397386205', '1593932400', '7,9', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '我们', '1593933226', '1593933198', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('72', '14', '0', '0', 'SUN', '', '0.01', '', '我们的', '', '', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yi9q00apw20vtbrt.jpg', '', '', '', '', '', '1593933502', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=72', '', '', '2020070515182200008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路72-35号', '113.69746518669788', '34.70258201321206', '1593932400', '7,10', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '我们的', '1593933542', '1593933518', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('73', '14', '0', '0', 'SUN', '', '0.01', '', '我', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593933573', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=73', '', '', '2020070515193300008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353010', '河南省郑州市管城回族区紫辰路', '113.6972158601573', '34.703316123363955', '1593932400', '6,11', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '我们的', '1593933699', '1593933625', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('74', '14', '0', '0', 'SUN', '', '0', '', '你的', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593933725', '', '1593933808', '171.11.7.19', '0', '0', '1', 'show.php?itemid=74', '', '', '2020070515220500008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路72-9号', '113.69648955442008', '34.70343735551923', '1593932400', '11,8', '', '你在', '', '', '2', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('75', '14', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593933838', '', '1593933918', '171.11.7.19', '0', '0', '1', 'show.php?itemid=75', '', '', '2020070515235800008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区紫辰路72-35号', '113.69746328358468', '34.70262213136623', '1593932400', '9,7,11', '', '111', '', '', '2', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('76', '14', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593933941', '', '1593933960', '171.11.7.19', '0', '0', '1', 'show.php?itemid=76', '', '', '2020070515254100008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18888888888', '河南省郑州市管城回族区紫辰路72-30号', '113.69717307763524', '34.702876439562225', '1593932400', '7,10,9', '', '你', '', '', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('77', '14', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593934007', '', '1593934058', '171.11.7.19', '0', '0', '1', 'show.php?itemid=77', '', '', '2020070515264700008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '15555555555', '河南省郑州市管城回族区紫辰路72-30号', '113.69678472032486', '34.70319068922263', '1593932400', '7,10,6', '', '我们', '', '', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('78', '14', '0', '0', 'SUN', '', '0.01', '', '111', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593934092', '', '0', '171.11.7.19', '0', '3', '1', 'show.php?itemid=78', '', '', '2020070515281200008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18888888888', '河南省郑州市管城回族区南三环', '113.69912086857796', '34.70116803347978', '1593932400', '7,10,8', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '你们', '1593934173', '1593934099', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('79', '14', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593934401', '', '1593934424', '223.104.105.97', '0', '0', '1', 'show.php?itemid=79', '', '', '2020070515332100008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18868363636', '河南省郑州市管城回族区紫辰路', '113.6974062761606', '34.70384439457212', '1593932400', '7,10', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '我们', '', '1593934415', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('80', '14', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593936811', '', '1593936878', '123.160.174.126', '0', '0', '1', 'show.php?itemid=80', '', '', '2020070516133100008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区中州大道', '113.70510260799898', '34.705511433270495', '1593936000', '7,10', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '我们', '', '1593936863', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('81', '14', '0', '0', 'Mr.  赵', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593936974', '', '0', '106.33.72.2', '0', '3', '1', 'show.php?itemid=81', '', '', '2020070516161400019', 'oRRhA5QfGGRPxuex2-nIdrFdsMOI', '1349378168654', '河南省郑州市管城回族区中州大道', '113.70512', '34.70557', '1594108800', '6', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '啊雷姐', '1593937069', '1593937008', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('82', '14', '0', '0', 'SUN', '', '0', '', '123', '', 'SUN,抢单', '', '', '', '', '', '0', '0', '', '', 'https://upload.recycle.fulltime.vip/c3yjs3uj1d3zppm6vz.jpg', '', '', '', '', '', '1593937765', 'fulltimelink', '1595141545', '123.160.174.126', '0', '0', '1', 'show.php?itemid=82', '', '', '2020070516292500008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市管城回族区中州大道', '113.70506499722859', '34.70545899361982', '1593936000', '7,6,8', 'o6NpL5CCNZf-xJmTDA6TgBu7aGOQ', '我们', '1595141539', '1593937772', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('83', '14', '0', '0', 'Mr.  赵', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1593937895', '', '1593938118', '106.33.72.2', '0', '0', '1', 'show.php?itemid=83', '', '', '2020070516313500019', 'oRRhA5QfGGRPxuex2-nIdrFdsMOI', '13487348466', '河南省郑州市管城回族区中州大道', '113.70512', '34.705544', '1593936000', '6', 'o6NpL5J8a0quIErp2B76IJeJnf1c', '耦合器', '', '1593937919', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('91', '14', '0', '0', '小奎', '', '0', '', '', '', '小奎,抢单', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595145124', 'fulltimelink', '1595471770', '223.104.107.35', '0', '0', '1', 'show.php?itemid=91', '', '', '2020071915520400006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市金水区纬一路2号院', '113.67816376719857', '34.76708061480262', '1595142000', '7,8', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', 'hlomou', '1595471762', '1595145130', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('101', '14', '0', '0', 'SUN', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596160152', '', '0', '223.104.107.242', '0', '3', '1', 'show.php?itemid=101', '', '', '2020073109491200008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18888888888', '河南省郑州市金水区纬一路2号院', '113.67816765087396', '34.76709785056988', '1596157200', '7,10', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '我们', '1596160192', '1596160168', '1', '0', '0', '1596160304');
INSERT INTO `nms_article_26` VALUES ('100', '14', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596159988', '', '1596160080', '61.163.87.166', '0', '0', '1', 'show.php?itemid=100', '', '', '2020073109462800008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18866666666', '河南省郑州市金水区纬一路2号院', '113.67814240016673', '34.767094858118575', '1596157200', '7,10,6', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '我们', '', '1596160030', '1', '0', '1', '');
INSERT INTO `nms_article_26` VALUES ('96', '14', '0', '0', 'SUN', '', '0', '', '我们的', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595476606', '', '1595476671', '223.104.111.146', '0', '0', '1', 'show.php?itemid=96', '', '', '2020072311564600008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市金水区经五路1-16号', '113.67794036865233', '34.76667785644533', '1595473200', '7,10,11', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '擦擦', '', '1595476615', '1', '0', '1', '');
INSERT INTO `nms_article_26` VALUES ('97', '14', '0', '0', 'SUN', '', '0.01', '', '1111', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595561039', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=97', '', '', '2020072411235900008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18860353040', '河南省郑州市金水区纬一路2号院', '113.67820865557593', '34.76707900626801', '1595559600', '7,10,9', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '1111', '1595561193', '1595561043', '1', '0', '0', '');
INSERT INTO `nms_article_26` VALUES ('98', '14', '0', '0', 'SUN', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595561388', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=98', '', '', '2020072411294800008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18888888888', '河南省郑州市金水区纬一路2号', '113.67816162109371', '34.76710510253908', '1595559600', '7,10,6', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '我们', '1595561959', '1595561393', '1', '0', '0', '1595562073');
INSERT INTO `nms_article_26` VALUES ('99', '14', '0', '0', 'SUN', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1595562010', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=99', '', '', '2020072411401000008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18888888888', '河南省郑州市金水区纬一路2号院', '113.67817301280036', '34.76708120481137', '1595559600', '8,10', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '1111', '1595562034', '1595562013', '1', '0', '0', '1595562144');
INSERT INTO `nms_article_26` VALUES ('119', '14', '0', '0', '小奎', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596351957', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=119', '', '', '2020080215055700006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '1593737373', '河南省郑州市中原区中原西路233号', '113.62493', '34.74725', '1596351600', '7,8', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '32424', '1596352275', '1596352030', '2', '0', '0', '1596352390');
INSERT INTO `nms_article_26` VALUES ('118', '14', '0', '0', '小奎', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596351171', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=118', '', '', '2020080214525100006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108765', '河南省郑州市中原区中原西路233号', '113.62493', '34.74725', '1596348000', '8,7', 'o6NpL5ENbeAvIy4XQM0YuXR-FKpA', '123123', '1596351286', '1596351180', '1', '0', '0', '1596351388');
INSERT INTO `nms_article_26` VALUES ('117', '14', '0', '0', '小奎', '', '0.01', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596350564', '', '0', '61.163.87.166', '0', '3', '1', 'show.php?itemid=117', '', '', '2020080214424400006', 'oRRhA5UT5xEQvd79lum-NOdPHY8U', '15937108635', '河南省郑州市中原区中原西路233号', '113.62493', '34.74725', '1596348000', '8,7', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', 'fff', '1596350754', '1596350576', '1', '0', '0', '1596350868');
INSERT INTO `nms_article_26` VALUES ('121', '14', '0', '0', 'SUN', '', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '', '1596353272', '', '0', '61.163.87.166', '0', '2', '1', 'show.php?itemid=121', '', '', '2020080215275200008', 'oRRhA5QFiKpd1vcOZxqcOuPTU3yc', '18888888888', '河南省郑州市金水区纬一路2号院', '113.67816100465403', '34.76709114070651', '1596351600', '7,10,11', 'o6NpL5Fgirr0OMD7vtXtsUKrXGvg', '我们', '', '1596353279', '1', '0', '0', '');

-- ----------------------------
-- Table structure for nms_article_27
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_27`;
CREATE TABLE `nms_article_27` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `subtitle` mediumtext NOT NULL,
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `copyfrom` varchar(30) NOT NULL DEFAULT '',
  `fromurl` varchar(255) NOT NULL DEFAULT '',
  `voteid` varchar(100) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  `picurl` text NOT NULL,
  `picwidth` text NOT NULL,
  `picheight` text NOT NULL,
  `pictitle` text NOT NULL,
  `picdesc` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='首页';

-- ----------------------------
-- Records of nms_article_27
-- ----------------------------
INSERT INTO `nms_article_27` VALUES ('1', '16', '0', '0', '测试文章标题', '', '0', '', '测试文章内容等其他信息', '', '测试文章标题,用户端焦点图', '', '', '', '', '', '0', '0', 'https://cms.hnlvyi.cn/file/upload/202007/29/154815791.png', '', '', '', '', '', '', 'fulltimelink', '1590571085', 'fulltimelink', '1596008898', '61.163.87.166', '', '3', '0', 'show.php?itemid=1', '', '');
INSERT INTO `nms_article_27` VALUES ('2', '16', '0', '0', '测试小程序路由', '', '0', '', '', '', '测试小程序路由,用户端焦点图', '', '', '', '', '', '0', '0', 'https://cms.hnlvyi.cn/file/upload/202007/29/154756671.png', '', '', '', '', '', '', 'fulltimelink', '1590571188', 'fulltimelink', '1596008887', '61.163.87.166', '', '3', '1', 'pages/index/index', '', '');

-- ----------------------------
-- Table structure for nms_article_28
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_28`;
CREATE TABLE `nms_article_28` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `subtitle` mediumtext NOT NULL,
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `copyfrom` varchar(30) NOT NULL DEFAULT '',
  `fromurl` varchar(255) NOT NULL DEFAULT '',
  `voteid` varchar(100) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  `picurl` text NOT NULL,
  `picwidth` text NOT NULL,
  `picheight` text NOT NULL,
  `pictitle` text NOT NULL,
  `picdesc` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='使用指南';

-- ----------------------------
-- Records of nms_article_28
-- ----------------------------
INSERT INTO `nms_article_28` VALUES ('1', '18', '0', '0', '如何使用个人端', '', '0', '', '如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用', '', '如何使用个人端,使用指南', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', 'fulltimelink', '1590651184', 'fulltimelink', '1590651205', '61.163.87.166', '', '3', '0', 'show.php?itemid=1', '', '');
INSERT INTO `nms_article_28` VALUES ('2', '18', '0', '0', '如何使用商户端', '', '0', '', '如何使用商户端如何使用商户端如何使用商户端如何使用商户端如何使用商户端如何使用商户端如何使用商户端如何使用商户端', '', '如何使用商户端,使用指南', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', 'fulltimelink', '1590651205', 'fulltimelink', '1590651227', '61.163.87.166', '', '3', '0', 'show.php?itemid=2', '', '');

-- ----------------------------
-- Table structure for nms_article_29
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_29`;
CREATE TABLE `nms_article_29` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `subtitle` mediumtext NOT NULL,
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `copyfrom` varchar(30) NOT NULL DEFAULT '',
  `fromurl` varchar(255) NOT NULL DEFAULT '',
  `voteid` varchar(100) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  `picurl` text NOT NULL,
  `picwidth` text NOT NULL,
  `picheight` text NOT NULL,
  `pictitle` text NOT NULL,
  `picdesc` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='回收价格';

-- ----------------------------
-- Records of nms_article_29
-- ----------------------------
INSERT INTO `nms_article_29` VALUES ('1', '19', '0', '0', '回收价格哦', '', '0', '', '回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦', '', '回收价格哦,回收价格', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', 'fulltimelink', '1590651320', 'fulltimelink', '1590651346', '61.163.87.166', '', '3', '0', 'show.php?itemid=1', '', '');

-- ----------------------------
-- Table structure for nms_article_30
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_30`;
CREATE TABLE `nms_article_30` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `subtitle` mediumtext NOT NULL,
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `copyfrom` varchar(30) NOT NULL DEFAULT '',
  `fromurl` varchar(255) NOT NULL DEFAULT '',
  `voteid` varchar(100) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  `picurl` text NOT NULL,
  `picwidth` text NOT NULL,
  `picheight` text NOT NULL,
  `pictitle` text NOT NULL,
  `picdesc` text NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='回收类型';

-- ----------------------------
-- Records of nms_article_30
-- ----------------------------

-- ----------------------------
-- Table structure for nms_article_data_24
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_data_24`;
CREATE TABLE `nms_article_data_24` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='回收分类内容';

-- ----------------------------
-- Records of nms_article_data_24
-- ----------------------------
INSERT INTO `nms_article_data_24` VALUES ('3', '<p>南四环精品收购，量大从优南四环精品收购，量大从优南四环精品收购，量大从优</p>');
INSERT INTO `nms_article_data_24` VALUES ('2', '<p>北环回收站每公斤5元北环回收站每公斤5元</p>');

-- ----------------------------
-- Table structure for nms_article_data_25
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_data_25`;
CREATE TABLE `nms_article_data_25` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户内容';

-- ----------------------------
-- Records of nms_article_data_25
-- ----------------------------
INSERT INTO `nms_article_data_25` VALUES ('1', '<p>123123123123123123123123123123123123123123</p>');
INSERT INTO `nms_article_data_25` VALUES ('2', '<p>ceshissssssssss</p>');
INSERT INTO `nms_article_data_25` VALUES ('11', '<p>用户默认内容请勿改动</p>');
INSERT INTO `nms_article_data_25` VALUES ('15', '<p>用户默认内容请勿改动</p>');
INSERT INTO `nms_article_data_25` VALUES ('6', '<p>用户默认内容请勿改动</p>');
INSERT INTO `nms_article_data_25` VALUES ('14', '<p>用户默认内容请勿改动</p>');
INSERT INTO `nms_article_data_25` VALUES ('34', '<p>用户默认内容请勿改动</p>');
INSERT INTO `nms_article_data_25` VALUES ('22', '<p>用户默认内容请勿改动</p>');
INSERT INTO `nms_article_data_25` VALUES ('20', '<p>用户默认内容请勿改动</p>');
INSERT INTO `nms_article_data_25` VALUES ('30', '<p>用户默认内容请勿改动123</p>');

-- ----------------------------
-- Table structure for nms_article_data_26
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_data_26`;
CREATE TABLE `nms_article_data_26` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `repicurl` text NOT NULL,
  `reviewtime` int(10) NOT NULL,
  `replycontent` longtext NOT NULL,
  `replytime` int(10) NOT NULL,
  `plaincontent` longtext NOT NULL,
  `plaintime` int(10) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单内容';

-- ----------------------------
-- Records of nms_article_data_26
-- ----------------------------
INSERT INTO `nms_article_data_26` VALUES ('35', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('2', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('36', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('37', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('118', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('111', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('110', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('109', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('108', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('107', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('106', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('105', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('104', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('38', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('39', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('49', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('52', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('119', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('120', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('26', '<p>1111111111111111<br/></p>', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('53', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('117', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('57', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('93', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('103', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('101', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('61', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('88', 'all陌陌陌陌', '5', '', '1595143092', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('84', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('64', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('102', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('66', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('67', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('68', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('69', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('70', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('71', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('72', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('73', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('74', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('75', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('76', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('77', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('78', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('79', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('80', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('81', '帅哥好', '5', '', '1593937104', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('82', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('83', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('85', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('100', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('91', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('96', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('97', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('98', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('99', '', '0', '', '0', '', '0', '', '0');
INSERT INTO `nms_article_data_26` VALUES ('121', '', '0', '', '0', '', '0', '', '0');

-- ----------------------------
-- Table structure for nms_article_data_27
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_data_27`;
CREATE TABLE `nms_article_data_27` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='首页内容';

-- ----------------------------
-- Records of nms_article_data_27
-- ----------------------------
INSERT INTO `nms_article_data_27` VALUES ('1', '<p>测试文章内容等其他信息</p><p><img src=\"https://cms.hnlvyi.cn/file/upload/202005/27/171940691.png\" title=\"\" alt=\"\"/></p>');
INSERT INTO `nms_article_data_27` VALUES ('2', '');

-- ----------------------------
-- Table structure for nms_article_data_28
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_data_28`;
CREATE TABLE `nms_article_data_28` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='使用指南内容';

-- ----------------------------
-- Records of nms_article_data_28
-- ----------------------------
INSERT INTO `nms_article_data_28` VALUES ('1', '<p>如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端</p><p>如何使用个人端如何使用个人端</p><p>如何使用个人端如何使用个人端</p><p>如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端</p><p>如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端如何使用个人端</p><p><br/></p><p><br/></p><p>如何使用个人端如何使用个人端</p><p><br/></p><p>如何使用个人端如何使用个人端如何使用个人端</p><p><br/></p><p><br/></p><p>如何使用个人端如何使用个人端如何使用个人端如何使用个人端</p>');
INSERT INTO `nms_article_data_28` VALUES ('2', '<p>如何使用商户端如何使用商户端如何使用商户端</p><p>如何使用商户端如何使用商户端</p><p><br/></p><p><br/></p><p><br/></p><p>如何使用商户端如何使用商户端如何使用商户端</p>');

-- ----------------------------
-- Table structure for nms_article_data_29
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_data_29`;
CREATE TABLE `nms_article_data_29` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='回收价格内容';

-- ----------------------------
-- Records of nms_article_data_29
-- ----------------------------
INSERT INTO `nms_article_data_29` VALUES ('1', '<p>回收价格哦</p><p>回收价格哦回收价格哦回收价格哦</p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p>回收价格哦回收价格哦</p><p><br/></p><p>回收价格哦回收价格哦回收价格哦</p><p><br/></p><p><br/></p><p>回收价格哦回收价格哦回收价格哦</p><p><br/></p><p><br/></p><p>回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦回收价格哦</p><p><br/></p><p><br/></p><p><br/></p><p>回收价格哦回收价格哦回收价格哦</p>');

-- ----------------------------
-- Table structure for nms_article_data_30
-- ----------------------------
DROP TABLE IF EXISTS `nms_article_data_30`;
CREATE TABLE `nms_article_data_30` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='回收类型内容';

-- ----------------------------
-- Records of nms_article_data_30
-- ----------------------------

-- ----------------------------
-- Table structure for nms_ask
-- ----------------------------
DROP TABLE IF EXISTS `nms_ask`;
CREATE TABLE `nms_ask` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(10) unsigned NOT NULL DEFAULT '0',
  `qid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL,
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `reply` mediumtext NOT NULL,
  `star` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='客服中心';

-- ----------------------------
-- Records of nms_ask
-- ----------------------------

-- ----------------------------
-- Table structure for nms_banip
-- ----------------------------
DROP TABLE IF EXISTS `nms_banip`;
CREATE TABLE `nms_banip` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='IP禁止';

-- ----------------------------
-- Records of nms_banip
-- ----------------------------

-- ----------------------------
-- Table structure for nms_banword
-- ----------------------------
DROP TABLE IF EXISTS `nms_banword`;
CREATE TABLE `nms_banword` (
  `bid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `replacefrom` varchar(255) NOT NULL DEFAULT '',
  `replaceto` varchar(255) NOT NULL DEFAULT '',
  `deny` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='词语过滤';

-- ----------------------------
-- Records of nms_banword
-- ----------------------------

-- ----------------------------
-- Table structure for nms_cache
-- ----------------------------
DROP TABLE IF EXISTS `nms_cache`;
CREATE TABLE `nms_cache` (
  `cacheid` varchar(32) NOT NULL DEFAULT '',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `cacheid` (`cacheid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件缓存';

-- ----------------------------
-- Records of nms_cache
-- ----------------------------

-- ----------------------------
-- Table structure for nms_cart
-- ----------------------------
DROP TABLE IF EXISTS `nms_cart`;
CREATE TABLE `nms_cart` (
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='购物车';

-- ----------------------------
-- Records of nms_cart
-- ----------------------------

-- ----------------------------
-- Table structure for nms_category
-- ----------------------------
DROP TABLE IF EXISTS `nms_category`;
CREATE TABLE `nms_category` (
  `catid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `catname` varchar(50) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `catdir` varchar(255) NOT NULL DEFAULT '',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `letter` varchar(4) NOT NULL DEFAULT '',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `item` bigint(20) unsigned NOT NULL DEFAULT '0',
  `property` smallint(6) unsigned NOT NULL DEFAULT '0',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0',
  `arrparentid` varchar(255) NOT NULL DEFAULT '',
  `child` tinyint(1) NOT NULL DEFAULT '0',
  `arrchildid` text NOT NULL,
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `template` varchar(30) NOT NULL DEFAULT '',
  `show_template` varchar(30) NOT NULL DEFAULT '',
  `seo_title` varchar(255) NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  `group_list` varchar(255) NOT NULL DEFAULT '',
  `group_show` varchar(255) NOT NULL DEFAULT '',
  `group_add` varchar(255) NOT NULL DEFAULT '',
  `default_water` tinyint(1) NOT NULL DEFAULT '0',
  `wxpic` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COMMENT='栏目分类';

-- ----------------------------
-- Records of nms_category
-- ----------------------------
INSERT INTO `nms_category` VALUES ('7', '24', '金属类', '', 'jinshulei', 'list.php?catid=7', 'j', '1', '0', '0', '0', '0', '0', '7', '7', '', '', '0.5-0.7元/kg', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('6', '24', '纸类', '', 'zhilei', 'list.php?catid=6', 'z', '1', '2', '0', '0', '0', '0', '6', '6', '', '', '0.5-0.7元/kg', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('3', '4', '公司默认分类', '', '1', 'list.php?catid=3', '', '1', '0', '0', '0', '0', '0', '', '1', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('8', '24', '塑料类', '', 'suliaolei', 'list.php?catid=8', 's', '1', '0', '0', '0', '0', '0', '8', '8', '', '', '0.5-0.7元/kg', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('9', '24', '纺织类', '', 'fangzhilei', 'list.php?catid=9', 'f', '1', '0', '0', '0', '0', '0', '9', '9', '', '', '0.5-0.7元/kg', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('10', '24', '家电类', '', 'jiadianlei', 'list.php?catid=10', 'j', '1', '0', '0', '0', '0', '0', '10', '10', '', '', '0.5-0.7元/kg', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('11', '24', '其他', '', 'qita', 'list.php?catid=11', 'q', '1', '0', '0', '0', '0', '0', '11', '11', '', '', '0.5-0.7元/kg', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('12', '25', '商家', '', 'shangjia', 'list.php?catid=12', 's', '1', '0', '0', '0', '0', '0', '12', '12', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('13', '25', '用户', '', 'yonghu', 'list.php?catid=13', 'y', '1', '0', '0', '0', '0', '0', '13', '13', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('14', '26', '抢单', '', 'qiangdan', 'list.php?catid=14', 'q', '1', '18', '0', '0', '0', '0', '14', '14', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('15', '26', '派单', '', 'paidan', 'list.php?catid=15', 'p', '1', '0', '0', '0', '0', '0', '15', '15', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('16', '27', '用户端焦点图', '', '16', 'list.php?catid=16', '', '1', '0', '0', '0', '0', '0', '16', '16', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('17', '27', '商户端焦点图', '', '17', 'list.php?catid=17', '', '1', '0', '0', '0', '0', '0', '17', '17', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('18', '28', '使用指南', '', 'shiyongzhinan', 'list.php?catid=18', 's', '1', '0', '0', '0', '0', '0', '18', '18', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('19', '29', '回收价格', '', 'huishoujiage', 'list.php?catid=19', 'h', '1', '0', '0', '0', '0', '0', '19', '19', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('20', '30', '可回收物', '', 'kehuishouwu', 'list.php?catid=20', 'k', '1', '0', '0', '0', '0', '1', '20,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78', '20', '', '', '', '', '小件装入环保袋，大件请打包', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095131591.png');
INSERT INTO `nms_category` VALUES ('21', '30', '厨余垃圾', '', 'chuyulaji', 'list.php?catid=21', 'c', '1', '0', '0', '0', '0', '1', '21,79,80,81,82,83,84', '21', '', '', '', '', '请投递至厨余垃圾桶', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095220641.png');
INSERT INTO `nms_category` VALUES ('22', '30', '有害垃圾', '', 'youhailaji', 'list.php?catid=22', 'y', '1', '0', '0', '0', '0', '1', '22,85,86,87,88,89,90,91,92,93,94', '22', '', '', '', '', '请投递致有害垃圾桶内', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095247671.png');
INSERT INTO `nms_category` VALUES ('23', '30', '其他垃圾', '', 'qitalaji', 'list.php?catid=23', 'q', '1', '0', '0', '0', '0', '1', '23,95,96,97,98,99,100,101,102,103', '23', '', '', '', '', '请投递至其他垃圾桶', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095302361.png');
INSERT INTO `nms_category` VALUES ('24', '30', '废塑料类', '', '24', 'list.php?catid=24', '', '1', '0', '0', '20', '0,20', '1', '24,34,35,36,37,38,39,40,41', '24', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('25', '30', '废旧纸类', '', '25', 'list.php?catid=25', '', '1', '0', '0', '20', '0,20', '1', '25,42,43,44,45', '25', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('26', '30', '旧衣物纺织品类（干净）', '', '26', 'list.php?catid=26', '', '1', '0', '0', '20', '0,20', '1', '26,46,47,48,49', '26', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('27', '30', '废旧包装类', '', '27', 'list.php?catid=27', '', '1', '0', '0', '20', '0,20', '1', '27,50,51,52,53,54', '27', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('28', '30', '废旧金属类', '', '28', 'list.php?catid=28', '', '1', '0', '0', '20', '0,20', '1', '28,55,56,57,58', '28', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('29', '30', '废玻璃类', '', '29', 'list.php?catid=29', '', '1', '0', '0', '20', '0,20', '1', '29,59,60,61,62', '29', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('30', '30', '电子产品', '', '30', 'list.php?catid=30', '', '1', '0', '0', '20', '0,20', '1', '30,63,64,65,66', '30', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('31', '30', '废旧小家电', '', '31', 'list.php?catid=31', '', '1', '0', '0', '20', '0,20', '1', '31,67,68,69,70', '31', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('32', '30', '废旧大家电', '', '32', 'list.php?catid=32', '', '1', '0', '0', '20', '0,20', '1', '32,71,72,73,74', '32', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('33', '30', '废车', '', '33', 'list.php?catid=33', '', '1', '0', '0', '20', '0,20', '1', '33,75,76,77,78', '33', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('34', '30', '塑料袋', '', '34', 'list.php?catid=34', '', '1', '0', '0', '24', '0,20,24', '0', '34', '34', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100248551.png');
INSERT INTO `nms_category` VALUES ('35', '30', '饮料瓶', '', '35', 'list.php?catid=35', '', '1', '0', '0', '24', '0,20,24', '0', '35', '35', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100307261.png');
INSERT INTO `nms_category` VALUES ('36', '30', '塑料玩具', '', '36', 'list.php?catid=36', '', '1', '0', '0', '24', '0,20,24', '0', '36', '36', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100322581.png');
INSERT INTO `nms_category` VALUES ('37', '30', '手机壳', '', '37', 'list.php?catid=37', '', '1', '0', '0', '24', '0,20,24', '0', '37', '37', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/152701411.png');
INSERT INTO `nms_category` VALUES ('38', '30', '塑料凳', '', '38', 'list.php?catid=38', '', '1', '0', '0', '24', '0,20,24', '0', '38', '38', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100351251.png');
INSERT INTO `nms_category` VALUES ('39', '30', '塑料脸盆', '', '39', 'list.php?catid=39', '', '1', '0', '0', '24', '0,20,24', '0', '39', '39', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100405931.png');
INSERT INTO `nms_category` VALUES ('40', '30', '塑料杯', '', '40', 'list.php?catid=40', '', '1', '0', '0', '24', '0,20,24', '0', '40', '40', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100422221.png');
INSERT INTO `nms_category` VALUES ('41', '30', '中性笔壳', '', '41', 'list.php?catid=41', '', '1', '0', '0', '24', '0,20,24', '0', '41', '41', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100438331.png');
INSERT INTO `nms_category` VALUES ('42', '30', '报纸', '', '42', 'list.php?catid=42', '', '1', '0', '0', '25', '0,20,25', '0', '42', '42', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102015931.png');
INSERT INTO `nms_category` VALUES ('43', '30', '宣传单', '', '43', 'list.php?catid=43', '', '1', '0', '0', '25', '0,20,25', '0', '43', '43', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102031691.png');
INSERT INTO `nms_category` VALUES ('44', '30', '杂志', '', '44', 'list.php?catid=44', '', '1', '0', '0', '25', '0,20,25', '0', '44', '44', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102047901.png');
INSERT INTO `nms_category` VALUES ('45', '30', '书籍', '', '45', 'list.php?catid=45', '', '1', '0', '0', '25', '0,20,25', '0', '45', '45', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102102151.png');
INSERT INTO `nms_category` VALUES ('46', '30', '各类衣物', '', '46', 'list.php?catid=46', '', '1', '0', '0', '26', '0,20,26', '0', '46', '46', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102144911.png');
INSERT INTO `nms_category` VALUES ('47', '30', '棉被', '', '47', 'list.php?catid=47', '', '1', '0', '0', '26', '0,20,26', '0', '47', '47', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102202981.png');
INSERT INTO `nms_category` VALUES ('48', '30', '皮包', '', '48', 'list.php?catid=48', '', '1', '0', '0', '26', '0,20,26', '0', '48', '48', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102222461.png');
INSERT INTO `nms_category` VALUES ('49', '30', '各类鞋', '', '49', 'list.php?catid=49', '', '1', '0', '0', '26', '0,20,26', '0', '49', '49', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102236501.png');
INSERT INTO `nms_category` VALUES ('50', '30', '纸箱', '', '50', 'list.php?catid=50', '', '1', '0', '0', '27', '0,20,27', '0', '50', '50', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102334671.png');
INSERT INTO `nms_category` VALUES ('51', '30', '快递包装', '', '51', 'list.php?catid=51', '', '1', '0', '0', '27', '0,20,27', '0', '51', '51', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102401831.png');
INSERT INTO `nms_category` VALUES ('52', '30', '泡沫箱', '', '52', 'list.php?catid=52', '', '1', '0', '0', '27', '0,20,27', '0', '52', '52', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102452271.png');
INSERT INTO `nms_category` VALUES ('53', '30', '礼盒', '', '53', 'list.php?catid=53', '', '1', '0', '0', '27', '0,20,27', '0', '53', '53', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102509941.png');
INSERT INTO `nms_category` VALUES ('54', '30', '利乐包装', '', '54', 'list.php?catid=54', '', '1', '0', '0', '27', '0,20,27', '0', '54', '54', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102539821.png');
INSERT INTO `nms_category` VALUES ('55', '30', '金属易拉罐', '', '55', 'list.php?catid=55', '', '1', '0', '0', '28', '0,20,28', '0', '55', '55', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102719961.png');
INSERT INTO `nms_category` VALUES ('56', '30', '金属锅', '', '56', 'list.php?catid=56', '', '1', '0', '0', '28', '0,20,28', '0', '56', '56', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102735601.png');
INSERT INTO `nms_category` VALUES ('57', '30', '螺丝', '', '57', 'list.php?catid=57', '', '1', '0', '0', '28', '0,20,28', '0', '57', '57', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102748671.png');
INSERT INTO `nms_category` VALUES ('58', '30', '奶粉罐', '', '58', 'list.php?catid=58', '', '1', '0', '0', '28', '0,20,28', '0', '58', '58', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102801961.png');
INSERT INTO `nms_category` VALUES ('59', '30', '酒瓶', '', '59', 'list.php?catid=59', '', '1', '0', '0', '29', '0,20,29', '0', '59', '59', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103015581.png');
INSERT INTO `nms_category` VALUES ('60', '30', '醋瓶', '', '60', 'list.php?catid=60', '', '1', '0', '0', '29', '0,20,29', '0', '60', '60', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103036601.png');
INSERT INTO `nms_category` VALUES ('61', '30', '烟灰缸', '', '61', 'list.php?catid=61', '', '1', '0', '0', '29', '0,20,29', '0', '61', '61', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103051841.png');
INSERT INTO `nms_category` VALUES ('62', '30', '玻璃花瓶', '', '62', 'list.php?catid=62', '', '1', '0', '0', '29', '0,20,29', '0', '62', '62', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103104511.png');
INSERT INTO `nms_category` VALUES ('63', '30', '笔记本电脑', '', '63', 'list.php?catid=63', '', '1', '0', '0', '30', '0,20,30', '0', '63', '63', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103153201.png');
INSERT INTO `nms_category` VALUES ('64', '30', '平板电脑', '', '64', 'list.php?catid=64', '', '1', '0', '0', '30', '0,20,30', '0', '64', '64', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103216381.png');
INSERT INTO `nms_category` VALUES ('65', '30', '手机', '', '65', 'list.php?catid=65', '', '1', '0', '0', '30', '0,20,30', '0', '65', '65', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103229811.png');
INSERT INTO `nms_category` VALUES ('66', '30', '显示屏', '', '66', 'list.php?catid=66', '', '1', '0', '0', '30', '0,20,30', '0', '66', '66', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103241801.png');
INSERT INTO `nms_category` VALUES ('67', '30', '电风扇', '', '67', 'list.php?catid=67', '', '1', '0', '0', '31', '0,20,31', '0', '67', '67', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103304241.png');
INSERT INTO `nms_category` VALUES ('68', '30', '插线板', '', '68', 'list.php?catid=68', '', '1', '0', '0', '31', '0,20,31', '0', '68', '68', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103318521.png');
INSERT INTO `nms_category` VALUES ('69', '30', '电磁炉', '', '69', 'list.php?catid=69', '', '1', '0', '0', '31', '0,20,31', '0', '69', '69', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103335571.png');
INSERT INTO `nms_category` VALUES ('70', '30', '收音机', '', '70', 'list.php?catid=70', '', '1', '0', '0', '31', '0,20,31', '0', '70', '70', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103346581.png');
INSERT INTO `nms_category` VALUES ('71', '30', '冰箱', '', '71', 'list.php?catid=71', '', '1', '0', '0', '32', '0,20,32', '0', '71', '71', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103452191.png');
INSERT INTO `nms_category` VALUES ('72', '30', '空调', '', '72', 'list.php?catid=72', '', '1', '0', '0', '32', '0,20,32', '0', '72', '72', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103507811.png');
INSERT INTO `nms_category` VALUES ('73', '30', '洗衣机', '', '73', 'list.php?catid=73', '', '1', '0', '0', '32', '0,20,32', '0', '73', '73', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103522171.png');
INSERT INTO `nms_category` VALUES ('74', '30', '电视机', '', '74', 'list.php?catid=74', '', '1', '0', '0', '32', '0,20,32', '0', '74', '74', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103539821.png');
INSERT INTO `nms_category` VALUES ('75', '30', '电动车', '', '75', 'list.php?catid=75', '', '1', '0', '0', '33', '0,20,33', '0', '75', '75', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103607421.png');
INSERT INTO `nms_category` VALUES ('76', '30', '儿童车', '', '76', 'list.php?catid=76', '', '1', '0', '0', '33', '0,20,33', '0', '76', '76', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103626481.png');
INSERT INTO `nms_category` VALUES ('77', '30', '自行车', '', '77', 'list.php?catid=77', '', '1', '0', '0', '33', '0,20,33', '0', '77', '77', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103638461.png');
INSERT INTO `nms_category` VALUES ('78', '30', '摩托车', '', '78', 'list.php?catid=78', '', '1', '0', '0', '33', '0,20,33', '0', '78', '78', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103652561.png');
INSERT INTO `nms_category` VALUES ('79', '30', '厨余垃圾', '', '79', 'list.php?catid=79', 'c', '1', '0', '0', '21', '0,21', '1', '79,80,81,82,83,84', '79', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('80', '30', '骨骼内脏', '', '80', 'list.php?catid=80', '', '1', '0', '0', '79', '0,21,79', '0', '80', '80', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103813371.png');
INSERT INTO `nms_category` VALUES ('81', '30', '瓜果果皮', '', '81', 'list.php?catid=81', '', '1', '0', '0', '79', '0,21,79', '0', '81', '81', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103827351.png');
INSERT INTO `nms_category` VALUES ('82', '30', '菜梗菜叶', '', '82', 'list.php?catid=82', '', '1', '0', '0', '79', '0,21,79', '0', '82', '82', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103846791.png');
INSERT INTO `nms_category` VALUES ('83', '30', '剩饭剩菜', '', '83', 'list.php?catid=83', '', '1', '0', '0', '79', '0,21,79', '0', '83', '83', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103901401.png');
INSERT INTO `nms_category` VALUES ('84', '30', '树枝落叶', '', '84', 'list.php?catid=84', '', '1', '0', '0', '79', '0,21,79', '0', '84', '84', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103915851.png');
INSERT INTO `nms_category` VALUES ('85', '30', '有害垃圾（无偿回收）', '', '85', 'list.php?catid=85', 'y', '1', '0', '0', '22', '0,22', '1', '85,86,87,88,89,90,91,92,93,94', '85', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('86', '30', '化妆品', '', '86', 'list.php?catid=86', '', '1', '0', '0', '85', '0,22,85', '0', '86', '86', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104127291.png');
INSERT INTO `nms_category` VALUES ('87', '30', '电池', '', '87', 'list.php?catid=87', '', '1', '0', '0', '85', '0,22,85', '0', '87', '87', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104145711.png');
INSERT INTO `nms_category` VALUES ('88', '30', '过期药品', '', '88', 'list.php?catid=88', '', '1', '0', '0', '85', '0,22,85', '0', '88', '88', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104202791.png');
INSERT INTO `nms_category` VALUES ('89', '30', '灯管灯泡', '', '89', 'list.php?catid=89', '', '1', '0', '0', '85', '0,22,85', '0', '89', '89', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104218111.png');
INSERT INTO `nms_category` VALUES ('90', '30', '墨盒', '', '90', 'list.php?catid=90', '', '1', '0', '0', '85', '0,22,85', '0', '90', '90', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104235301.png');
INSERT INTO `nms_category` VALUES ('91', '30', '杀虫剂罐', '', '91', 'list.php?catid=91', '', '1', '0', '0', '85', '0,22,85', '0', '91', '91', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104248441.png');
INSERT INTO `nms_category` VALUES ('92', '30', '灯具', '', '92', 'list.php?catid=92', '', '1', '0', '0', '85', '0,22,85', '0', '92', '92', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104303451.png');
INSERT INTO `nms_category` VALUES ('93', '30', '电路板', '', '93', 'list.php?catid=93', '', '1', '0', '0', '85', '0,22,85', '0', '93', '93', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104318441.png');
INSERT INTO `nms_category` VALUES ('94', '30', '温度计', '', '94', 'list.php?catid=94', '', '1', '0', '0', '85', '0,22,85', '0', '94', '94', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104334751.png');
INSERT INTO `nms_category` VALUES ('95', '30', '其他垃圾', '', '95', 'list.php?catid=95', 'q', '1', '0', '0', '23', '0,23', '1', '95,96,97,98,99,100,101,102,103', '95', '', '', '', '', '', '', '', '', '0', '');
INSERT INTO `nms_category` VALUES ('96', '30', '宠物粪便', '', '96', 'list.php?catid=96', '', '1', '0', '0', '95', '0,23,95', '0', '96', '96', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104501991.png');
INSERT INTO `nms_category` VALUES ('97', '30', '烟头', '', '97', 'list.php?catid=97', '', '1', '0', '0', '95', '0,23,95', '0', '97', '97', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104515231.png');
INSERT INTO `nms_category` VALUES ('98', '30', '废卫生纸', '', '98', 'list.php?catid=98', '', '1', '0', '0', '95', '0,23,95', '0', '98', '98', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104531401.png');
INSERT INTO `nms_category` VALUES ('99', '30', '破旧陶瓷品', '', '99', 'list.php?catid=99', '', '1', '0', '0', '95', '0,23,95', '0', '99', '99', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104551751.png');
INSERT INTO `nms_category` VALUES ('100', '30', '脏衣物', '', '100', 'list.php?catid=100', '', '1', '0', '0', '95', '0,23,95', '0', '100', '100', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104607751.png');
INSERT INTO `nms_category` VALUES ('101', '30', '污染的餐盒', '', '101', 'list.php?catid=101', '', '1', '0', '0', '95', '0,23,95', '0', '101', '101', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104622421.png');
INSERT INTO `nms_category` VALUES ('102', '30', '尿垫', '', '102', 'list.php?catid=102', '', '1', '0', '0', '95', '0,23,95', '0', '102', '102', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104636181.png');
INSERT INTO `nms_category` VALUES ('103', '30', '砂锅', '', '103', 'list.php?catid=103', '', '1', '0', '0', '95', '0,23,95', '0', '103', '103', '', '', '', '', '', '', '', '', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104649231.png');

-- ----------------------------
-- Table structure for nms_category_option
-- ----------------------------
DROP TABLE IF EXISTS `nms_category_option`;
CREATE TABLE `nms_category_option` (
  `oid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `search` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `extend` text NOT NULL,
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`oid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='分类属性';

-- ----------------------------
-- Records of nms_category_option
-- ----------------------------

-- ----------------------------
-- Table structure for nms_category_value
-- ----------------------------
DROP TABLE IF EXISTS `nms_category_value`;
CREATE TABLE `nms_category_value` (
  `oid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `moduleid` smallint(6) NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  KEY `moduleid` (`moduleid`,`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='分类属性值';

-- ----------------------------
-- Records of nms_category_value
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat`;
CREATE TABLE `nms_chat` (
  `chatid` varchar(32) NOT NULL,
  `fromuser` varchar(30) NOT NULL,
  `fgettime` int(10) unsigned NOT NULL DEFAULT '0',
  `freadtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fnew` int(10) unsigned NOT NULL DEFAULT '0',
  `touser` varchar(30) NOT NULL,
  `tgettime` int(10) unsigned NOT NULL DEFAULT '0',
  `treadtime` int(10) unsigned NOT NULL DEFAULT '0',
  `tnew` int(10) unsigned NOT NULL DEFAULT '0',
  `lastmsg` varchar(255) NOT NULL,
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  `forward` varchar(255) NOT NULL,
  UNIQUE KEY `chatid` (`chatid`),
  KEY `fromuser` (`fromuser`),
  KEY `touser` (`touser`),
  KEY `lasttime` (`lasttime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='在线聊天';

-- ----------------------------
-- Records of nms_chat
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_0
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_0`;
CREATE TABLE `nms_chat_data_0` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_0';

-- ----------------------------
-- Records of nms_chat_data_0
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_1
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_1`;
CREATE TABLE `nms_chat_data_1` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_1';

-- ----------------------------
-- Records of nms_chat_data_1
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_2
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_2`;
CREATE TABLE `nms_chat_data_2` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_2';

-- ----------------------------
-- Records of nms_chat_data_2
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_3
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_3`;
CREATE TABLE `nms_chat_data_3` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_3';

-- ----------------------------
-- Records of nms_chat_data_3
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_4
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_4`;
CREATE TABLE `nms_chat_data_4` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_4';

-- ----------------------------
-- Records of nms_chat_data_4
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_5
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_5`;
CREATE TABLE `nms_chat_data_5` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_5';

-- ----------------------------
-- Records of nms_chat_data_5
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_6
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_6`;
CREATE TABLE `nms_chat_data_6` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_6';

-- ----------------------------
-- Records of nms_chat_data_6
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_7
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_7`;
CREATE TABLE `nms_chat_data_7` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_7';

-- ----------------------------
-- Records of nms_chat_data_7
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_8
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_8`;
CREATE TABLE `nms_chat_data_8` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_8';

-- ----------------------------
-- Records of nms_chat_data_8
-- ----------------------------

-- ----------------------------
-- Table structure for nms_chat_data_9
-- ----------------------------
DROP TABLE IF EXISTS `nms_chat_data_9`;
CREATE TABLE `nms_chat_data_9` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chatid` varchar(32) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `chatid` (`chatid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='聊天记录_9';

-- ----------------------------
-- Records of nms_chat_data_9
-- ----------------------------

-- ----------------------------
-- Table structure for nms_city
-- ----------------------------
DROP TABLE IF EXISTS `nms_city`;
CREATE TABLE `nms_city` (
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `iparea` mediumtext NOT NULL,
  `domain` varchar(255) NOT NULL DEFAULT '',
  `letter` varchar(4) NOT NULL DEFAULT '',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `template` varchar(50) NOT NULL DEFAULT '',
  `seo_title` varchar(255) NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `areaid` (`areaid`),
  KEY `domain` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='城市分站';

-- ----------------------------
-- Records of nms_city
-- ----------------------------

-- ----------------------------
-- Table structure for nms_comment
-- ----------------------------
DROP TABLE IF EXISTS `nms_comment`;
CREATE TABLE `nms_comment` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_mid` smallint(6) NOT NULL DEFAULT '0',
  `item_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `item_title` varchar(255) NOT NULL DEFAULT '',
  `item_username` varchar(30) NOT NULL DEFAULT '',
  `star` tinyint(1) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `qid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `quotation` mediumtext NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `passport` varchar(30) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `reply` mediumtext NOT NULL,
  `editor` varchar(30) NOT NULL DEFAULT '',
  `replyer` varchar(30) NOT NULL DEFAULT '',
  `replytime` int(10) unsigned NOT NULL DEFAULT '0',
  `agree` int(10) unsigned NOT NULL DEFAULT '0',
  `against` int(10) unsigned NOT NULL DEFAULT '0',
  `quote` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `item_mid` (`item_mid`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论';

-- ----------------------------
-- Records of nms_comment
-- ----------------------------

-- ----------------------------
-- Table structure for nms_comment_ban
-- ----------------------------
DROP TABLE IF EXISTS `nms_comment_ban`;
CREATE TABLE `nms_comment_ban` (
  `bid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` smallint(6) NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论禁止';

-- ----------------------------
-- Records of nms_comment_ban
-- ----------------------------

-- ----------------------------
-- Table structure for nms_comment_stat
-- ----------------------------
DROP TABLE IF EXISTS `nms_comment_stat`;
CREATE TABLE `nms_comment_stat` (
  `sid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` smallint(6) NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment` int(10) unsigned NOT NULL DEFAULT '0',
  `star1` int(10) unsigned NOT NULL DEFAULT '0',
  `star2` int(10) unsigned NOT NULL DEFAULT '0',
  `star3` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论统计';

-- ----------------------------
-- Records of nms_comment_stat
-- ----------------------------

-- ----------------------------
-- Table structure for nms_company
-- ----------------------------
DROP TABLE IF EXISTS `nms_company`;
CREATE TABLE `nms_company` (
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `groupid` smallint(4) unsigned NOT NULL DEFAULT '0',
  `company` varchar(100) NOT NULL DEFAULT '',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `validated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `validator` varchar(100) NOT NULL DEFAULT '',
  `validtime` int(10) unsigned NOT NULL DEFAULT '0',
  `vip` smallint(2) unsigned NOT NULL DEFAULT '0',
  `vipt` smallint(2) unsigned NOT NULL DEFAULT '0',
  `vipr` smallint(2) NOT NULL DEFAULT '0',
  `type` varchar(100) NOT NULL DEFAULT '',
  `catid` varchar(100) NOT NULL DEFAULT '',
  `catids` varchar(100) NOT NULL DEFAULT '',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `mode` varchar(100) NOT NULL DEFAULT '',
  `capital` float unsigned NOT NULL DEFAULT '0',
  `regunit` varchar(15) NOT NULL DEFAULT '',
  `size` varchar(100) NOT NULL DEFAULT '',
  `regyear` varchar(4) NOT NULL DEFAULT '',
  `regcity` varchar(30) NOT NULL DEFAULT '',
  `sell` varchar(255) NOT NULL DEFAULT '',
  `buy` varchar(255) NOT NULL DEFAULT '',
  `business` varchar(255) NOT NULL DEFAULT '',
  `telephone` varchar(50) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `mail` varchar(50) NOT NULL DEFAULT '',
  `gzh` varchar(50) NOT NULL DEFAULT '',
  `gzhqr` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `postcode` varchar(20) NOT NULL DEFAULT '',
  `homepage` varchar(255) NOT NULL DEFAULT '',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `styletime` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '',
  `skin` varchar(30) NOT NULL DEFAULT '',
  `domain` varchar(100) NOT NULL DEFAULT '',
  `icp` varchar(100) NOT NULL DEFAULT '',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`userid`),
  KEY `domain` (`domain`),
  KEY `vip` (`vip`),
  KEY `areaid` (`areaid`),
  KEY `groupid` (`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司';

-- ----------------------------
-- Records of nms_company
-- ----------------------------
INSERT INTO `nms_company` VALUES ('1', 'fulltimelink', '1', 'NMS新闻内容管理系统', '0', '0', '', '0', '0', '0', '0', '企业单位', '', '', '1', '', '0', '人民币', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', '0', '0', 'NMS新闻内容管理系统默认地区,,,,', '', '', '', '', 'https://nms.general.dianzhenkeji.com/index.php?homepage=fulltimelink');
INSERT INTO `nms_company` VALUES ('2', 'fengshuo', '1', '冯硕', '0', '0', '', '0', '0', '0', '0', '', '', '', '1', '', '0', '人民币', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', '0', '0', '冯硕', '', '', '', '', 'https://nms.general.dianzhenkeji.com/com/fengshuo/');
INSERT INTO `nms_company` VALUES ('3', 'zhaopin1', '1', '招聘1', '0', '0', '', '0', '0', '0', '0', '', '', '', '1', '', '0', '人民币', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', '0', '0', '招聘1', '', '', '', '', 'https://nms.general.dianzhenkeji.com/com/zhaopin1/');
INSERT INTO `nms_company` VALUES ('4', 'zhaopin2', '1', '招聘2', '0', '0', '', '0', '0', '0', '0', '', '', '', '1', '', '0', '人民币', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', '0', '0', '招聘2', '', '', '', '', 'https://nms.general.dianzhenkeji.com/com/zhaopin2/');
INSERT INTO `nms_company` VALUES ('5', 'recycle', '1', 'recycle', '0', '0', '', '0', '0', '0', '0', '', '', '', '1', '', '0', '人民币', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', '0', '0', 'recycle', '', '', '', '', 'https://cms.hnlvyi.cn/com/recycle/');

-- ----------------------------
-- Table structure for nms_company_data
-- ----------------------------
DROP TABLE IF EXISTS `nms_company_data`;
CREATE TABLE `nms_company_data` (
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司内容';

-- ----------------------------
-- Records of nms_company_data
-- ----------------------------
INSERT INTO `nms_company_data` VALUES ('1', '');
INSERT INTO `nms_company_data` VALUES ('2', '');
INSERT INTO `nms_company_data` VALUES ('3', '');
INSERT INTO `nms_company_data` VALUES ('4', '');
INSERT INTO `nms_company_data` VALUES ('5', '');

-- ----------------------------
-- Table structure for nms_company_setting
-- ----------------------------
DROP TABLE IF EXISTS `nms_company_setting`;
CREATE TABLE `nms_company_setting` (
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `item_key` varchar(100) NOT NULL DEFAULT '',
  `item_value` text NOT NULL,
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司设置';

-- ----------------------------
-- Records of nms_company_setting
-- ----------------------------

-- ----------------------------
-- Table structure for nms_cron
-- ----------------------------
DROP TABLE IF EXISTS `nms_cron`;
CREATE TABLE `nms_cron` (
  `itemid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL,
  `schedule` varchar(255) NOT NULL,
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  `nexttime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `note` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `nexttime` (`nexttime`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='计划任务';

-- ----------------------------
-- Records of nms_cron
-- ----------------------------
INSERT INTO `nms_cron` VALUES ('1', '更新在线状态', '1', 'online', '10', '1596350340', '1596350940', '0', '');
INSERT INTO `nms_cron` VALUES ('2', '内容分表创建', '1', 'split', '0,0', '1596331450', '1596384000', '0', '');
INSERT INTO `nms_cron` VALUES ('3', '清理过期文件缓存', '0', 'cache', '30', '1596349690', '1596351490', '0', '');
INSERT INTO `nms_cron` VALUES ('20', '清理过期禁止IP', '0', 'banip', '0,10', '1596331450', '1596384600', '0', '');
INSERT INTO `nms_cron` VALUES ('21', '清理系统临时文件', '0', 'temp', '0,20', '1596331450', '1596385200', '0', '');
INSERT INTO `nms_cron` VALUES ('40', '清理3天前未付款充值记录', '0', 'charge', '1,0', '1596331450', '1596387600', '0', '');
INSERT INTO `nms_cron` VALUES ('41', '清理30天前404日志', '0', '404', '1,10', '1596331450', '1596388200', '0', '');
INSERT INTO `nms_cron` VALUES ('42', '清理30天前登录日志', '0', 'loginlog', '1,20', '1596331450', '1596388800', '0', '');
INSERT INTO `nms_cron` VALUES ('43', '清理30天前管理日志', '0', 'adminlog', '1,30', '1596331450', '1596389400', '0', '');
INSERT INTO `nms_cron` VALUES ('44', '清理30天前站内交谈', '0', 'chat', '1,40', '1596331450', '1596390000', '0', '');
INSERT INTO `nms_cron` VALUES ('60', '清理90天前已读信件', '0', 'message', '2,0', '0', '0', '1', '');
INSERT INTO `nms_cron` VALUES ('61', '清理90天前资金流水', '0', 'money', '2,10', '0', '0', '1', '');
INSERT INTO `nms_cron` VALUES ('62', '清理90天前积分流水', '0', 'credit', '2,20', '0', '0', '1', '');
INSERT INTO `nms_cron` VALUES ('63', '清理90天前短信流水', '0', 'sms', '2,30', '0', '0', '1', '');
INSERT INTO `nms_cron` VALUES ('64', '清理90天前短信记录', '0', 'smssend', '2,40', '0', '0', '1', '');
INSERT INTO `nms_cron` VALUES ('65', '清理90天前邮件记录', '0', 'maillog', '2,50', '0', '0', '1', '');

-- ----------------------------
-- Table structure for nms_favorite
-- ----------------------------
DROP TABLE IF EXISTS `nms_favorite`;
CREATE TABLE `nms_favorite` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `tid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `typeid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商机收藏';

-- ----------------------------
-- Records of nms_favorite
-- ----------------------------

-- ----------------------------
-- Table structure for nms_fetch
-- ----------------------------
DROP TABLE IF EXISTS `nms_fetch`;
CREATE TABLE `nms_fetch` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sitename` varchar(100) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `encode` varchar(30) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='单页采编';

-- ----------------------------
-- Records of nms_fetch
-- ----------------------------

-- ----------------------------
-- Table structure for nms_fields
-- ----------------------------
DROP TABLE IF EXISTS `nms_fields`;
CREATE TABLE `nms_fields` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tb` varchar(30) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `length` smallint(4) unsigned NOT NULL DEFAULT '0',
  `html` varchar(30) NOT NULL DEFAULT '',
  `default_value` text NOT NULL,
  `option_value` text NOT NULL,
  `width` smallint(4) unsigned NOT NULL DEFAULT '0',
  `height` smallint(4) unsigned NOT NULL DEFAULT '0',
  `input_limit` varchar(255) NOT NULL DEFAULT '',
  `addition` varchar(255) NOT NULL DEFAULT '',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `tablename` (`tb`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='自定义字段';

-- ----------------------------
-- Records of nms_fields
-- ----------------------------
INSERT INTO `nms_fields` VALUES ('8', 'article_25', 'defaultlng', '默认经度', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('7', 'article_25', 'telphone', '手机号', '', 'varchar', '11', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('6', 'article_25', 'openid', 'OPENID', '', 'varchar', '50', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('9', 'article_25', 'defaultlat', '默认纬度', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('10', 'article_26', 'ordernum', '订单号', '', 'varchar', '50', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('11', 'article_26', 'userid', '用户openid', '', 'varchar', '255', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '1');
INSERT INTO `nms_fields` VALUES ('12', 'article_26', 'usertelphone', '用户手机号', '', 'varchar', '20', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '2');
INSERT INTO `nms_fields` VALUES ('13', 'article_26', 'useraddress', '用户地址', '', 'text', '0', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '3');
INSERT INTO `nms_fields` VALUES ('14', 'article_26', 'userlng', '用户经度', '', 'varchar', '255', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '5');
INSERT INTO `nms_fields` VALUES ('15', 'article_26', 'userlat', '用户纬度', '', 'varchar', '255', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '6');
INSERT INTO `nms_fields` VALUES ('16', 'article_26', 'reservetime', '预约时间', '', 'int', '10', 'time', '', '', '120', '90', 'is_time', '', '1', '1', '7');
INSERT INTO `nms_fields` VALUES ('17', 'article_26', 'userrecycletype', '用户回收类型', '', 'varchar', '255', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '8');
INSERT INTO `nms_fields` VALUES ('21', 'article_26', 'userwritea', '用户填写地址', '', 'text', '0', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '1', '4');
INSERT INTO `nms_fields` VALUES ('19', 'article_26', 'merchantid', '商家openid', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '1', '9');
INSERT INTO `nms_fields` VALUES ('22', 'article_25', 'star', '评分星级', '', 'float', '0', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('23', 'article_26', 'successtime', '完成时间', '', 'varchar', '255', 'time', '', '', '120', '90', 'is_time', '', '1', '1', '11');
INSERT INTO `nms_fields` VALUES ('24', 'article_25', 'realname', '真实姓名', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('25', 'article_25', 'idcard', '身份证号', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('26', 'article_25', 'mobile', '手机号', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('27', 'article_25', 'address', '回收站地址', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('28', 'article_25', 'idfront', '身份证正面', '', 'varchar', '255', 'file', '', '', '120', '90', '', '', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('29', 'article_25', 'idback', '身份证反面', '', 'varchar', '255', 'file', '', '', '120', '90', '', '', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('30', 'article_25', 'license', '营业执照', '', 'varchar', '255', 'file', '', '', '120', '90', '', '', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('31', 'article_25', 'type', '商户类型', '', 'varchar', '255', 'radio', '', 'merchant|普通商户*\r\nworkstation|回收站*\r\n', '120', '90', '', '', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('32', 'article_25', 'unid', 'unio&#110;id', '', 'varchar', '255', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('33', 'article_26', 'taketime', '商家接单时间', '', 'varchar', '255', 'time', '', '', '120', '90', 'is_time', '', '1', '1', '10');
INSERT INTO `nms_fields` VALUES ('34', 'article_26', 'rounds', '轮次', '', 'int', '10', 'text', '0', '', '120', '90', '1', 'size=\"30\"', '1', '1', '12');
INSERT INTO `nms_fields` VALUES ('35', 'article_24', 'openid', 'openid', '', 'varchar', '255', 'text', '', '', '120', '90', '1', 'size=\"30\"', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('36', 'article_25', 'accept', '是否接受派单', '', 'varchar', '255', 'select', '1', '1|允许*\r\n2|拒绝*\r\n', '120', '90', '1', '', '1', '0', '0');
INSERT INTO `nms_fields` VALUES ('37', 'article_25', 'grade', '商家等级', '', 'varchar', '10', 'select', '3', '1|超级商家*\r\n2|vip商家*\r\n3|普通商家*\r\n', '120', '90', '', '', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('38', 'article_25', 'punid', '父unid', '', 'varchar', '255', 'text', '', '', '120', '90', '', 'size=\"30\"', '1', '1', '0');
INSERT INTO `nms_fields` VALUES ('39', 'article_26', 'flag', '是否完成加币扫描', '', 'int', '10', 'text', '0', '', '120', '90', '', 'size=\"30\"', '1', '1', '13');
INSERT INTO `nms_fields` VALUES ('40', 'article_26', 'caceltype', '用户取消订单类型', '', 'int', '10', 'radio', '0', '0|商家未接单取消*\r\n1|商家接单一段时间内取消*\r\n2|超过用户预约最晚时间一段时间后取消*', '120', '90', '', '', '1', '1', '14');
INSERT INTO `nms_fields` VALUES ('41', 'article_26', 'locktime', '锁单时间', '', 'varchar', '10', 'time', '0', '', '120', '90', '', '', '1', '1', '15');

-- ----------------------------
-- Table structure for nms_finance_award
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_award`;
CREATE TABLE `nms_finance_award` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `fee` float unsigned NOT NULL DEFAULT '0',
  `paytime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `mid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `tid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='打赏记录';

-- ----------------------------
-- Records of nms_finance_award
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_card
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_card`;
CREATE TABLE `nms_finance_card` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `number` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(30) NOT NULL DEFAULT '',
  `amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  UNIQUE KEY `number` (`number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='充值卡';

-- ----------------------------
-- Records of nms_finance_card
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_cash
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_cash`;
CREATE TABLE `nms_finance_cash` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `bank` varchar(50) NOT NULL DEFAULT '',
  `banktype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `branch` varchar(100) NOT NULL,
  `account` varchar(30) NOT NULL DEFAULT '',
  `truename` varchar(30) NOT NULL DEFAULT '',
  `amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='申请提现';

-- ----------------------------
-- Records of nms_finance_cash
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_charge
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_charge`;
CREATE TABLE `nms_finance_charge` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `bank` varchar(20) NOT NULL DEFAULT '',
  `amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0',
  `receivetime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `reason` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='在线充值';

-- ----------------------------
-- Records of nms_finance_charge
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_coupon
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_coupon`;
CREATE TABLE `nms_finance_coupon` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `seller` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `pid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `oid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL,
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='优惠券';

-- ----------------------------
-- Records of nms_finance_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_credit
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_credit`;
CREATE TABLE `nms_finance_credit` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `amount` int(10) NOT NULL DEFAULT '0',
  `balance` int(10) NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `reason` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='积分流水';

-- ----------------------------
-- Records of nms_finance_credit
-- ----------------------------
INSERT INTO `nms_finance_credit` VALUES ('1', 'fulltimelink', '1', '1', '1588121139', '登录奖励', '10.1.3.30', 'system');
INSERT INTO `nms_finance_credit` VALUES ('2', 'fulltimelink', '1', '2', '1588209510', '登录奖励', '10.1.3.30', 'system');
INSERT INTO `nms_finance_credit` VALUES ('3', 'zhaopin1', '2', '2', '1588238695', '大象招聘发布', 'ID:2', 'system');
INSERT INTO `nms_finance_credit` VALUES ('4', 'zhaopin1', '-5', '-3', '1588239182', '大象招聘删除', 'ID:2', 'system');
INSERT INTO `nms_finance_credit` VALUES ('5', 'zhaopin1', '1', '-2', '1588679227', '登录奖励', '10.1.3.30', 'system');
INSERT INTO `nms_finance_credit` VALUES ('6', 'fengshuo', '1', '1', '1588680207', '登录奖励', '10.1.3.75', 'system');
INSERT INTO `nms_finance_credit` VALUES ('7', 'fengshuo', '2', '3', '1588680450', '大象招聘发布', 'ID:3', 'system');
INSERT INTO `nms_finance_credit` VALUES ('8', 'fengshuo', '-5', '-2', '1588680480', '大象招聘删除', 'ID:3', 'system');
INSERT INTO `nms_finance_credit` VALUES ('9', 'zhaopin1', '1', '-1', '1588723779', '登录奖励', '10.1.3.75', 'system');
INSERT INTO `nms_finance_credit` VALUES ('10', 'zhaopin2', '1', '1', '1588723935', '登录奖励', '10.1.3.75', 'system');
INSERT INTO `nms_finance_credit` VALUES ('11', 'zhaopin2', '2', '3', '1588724610', '大象招聘发布', 'ID:4', 'system');
INSERT INTO `nms_finance_credit` VALUES ('12', 'fulltimelink', '1', '3', '1588728787', '登录奖励', '10.1.3.31', 'system');
INSERT INTO `nms_finance_credit` VALUES ('13', 'fulltimelink', '1', '4', '1588823143', '登录奖励', '10.1.3.75', 'system');
INSERT INTO `nms_finance_credit` VALUES ('14', 'zhaopin2', '1', '4', '1588835744', '登录奖励', '10.1.3.31', 'system');
INSERT INTO `nms_finance_credit` VALUES ('15', 'zhaopin2', '-5', '-1', '1588835756', '大象招聘删除', 'ID:4', 'system');
INSERT INTO `nms_finance_credit` VALUES ('16', 'fulltimelink', '2', '6', '1588836312', '大象招聘发布', 'ID:5', 'system');
INSERT INTO `nms_finance_credit` VALUES ('17', 'fulltimelink', '2', '8', '1588840911', '大象招聘发布', 'ID:7', 'system');
INSERT INTO `nms_finance_credit` VALUES ('18', 'zhaopin1', '1', '0', '1588841966', '登录奖励', '10.1.3.30', 'system');
INSERT INTO `nms_finance_credit` VALUES ('19', 'fulltimelink', '1', '9', '1588934351', '登录奖励', '10.1.3.75', 'system');
INSERT INTO `nms_finance_credit` VALUES ('20', 'fulltimelink', '1', '10', '1588993975', '登录奖励', '10.1.3.31', 'system');
INSERT INTO `nms_finance_credit` VALUES ('21', 'zhaopin2', '1', '0', '1589021838', '登录奖励', '10.1.3.31', 'system');
INSERT INTO `nms_finance_credit` VALUES ('22', 'zhaopin2', '2', '2', '1589023249', '大象招聘发布', 'ID:8', 'system');
INSERT INTO `nms_finance_credit` VALUES ('23', 'zhaopin2', '2', '4', '1589023374', '大象招聘发布', 'ID:9', 'system');
INSERT INTO `nms_finance_credit` VALUES ('24', 'zhaopin2', '2', '6', '1589023417', '大象招聘发布', 'ID:10', 'system');
INSERT INTO `nms_finance_credit` VALUES ('25', 'zhaopin2', '2', '8', '1589023487', '大象招聘发布', 'ID:11', 'system');
INSERT INTO `nms_finance_credit` VALUES ('26', 'zhaopin1', '1', '1', '1589023528', '登录奖励', '10.1.3.31', 'system');
INSERT INTO `nms_finance_credit` VALUES ('27', 'fulltimelink', '-5', '5', '1589023544', '大象招聘删除', 'ID:7', 'system');
INSERT INTO `nms_finance_credit` VALUES ('28', 'zhaopin2', '2', '10', '1589023546', '大象招聘发布', 'ID:12', 'system');
INSERT INTO `nms_finance_credit` VALUES ('29', 'fulltimelink', '-5', '0', '1589023548', '大象招聘删除', 'ID:5', 'system');
INSERT INTO `nms_finance_credit` VALUES ('30', 'zhaopin2', '2', '12', '1589023586', '大象招聘发布', 'ID:13', 'system');
INSERT INTO `nms_finance_credit` VALUES ('31', 'zhaopin2', '2', '14', '1589023653', '大象招聘发布', 'ID:14', 'system');
INSERT INTO `nms_finance_credit` VALUES ('32', 'zhaopin2', '2', '16', '1589023694', '大象招聘发布', 'ID:15', 'system');
INSERT INTO `nms_finance_credit` VALUES ('33', 'zhaopin2', '2', '18', '1589023728', '大象招聘发布', 'ID:16', 'system');
INSERT INTO `nms_finance_credit` VALUES ('34', 'zhaopin2', '2', '20', '1589023764', '大象招聘发布', 'ID:17', 'system');
INSERT INTO `nms_finance_credit` VALUES ('35', 'zhaopin1', '2', '3', '1589023765', '大象招聘发布', 'ID:18', 'system');
INSERT INTO `nms_finance_credit` VALUES ('36', 'zhaopin2', '2', '22', '1589023815', '大象招聘发布', 'ID:19', 'system');
INSERT INTO `nms_finance_credit` VALUES ('37', 'zhaopin2', '2', '24', '1589023851', '大象招聘发布', 'ID:20', 'system');
INSERT INTO `nms_finance_credit` VALUES ('38', 'zhaopin1', '2', '5', '1589023857', '大象招聘发布', 'ID:21', 'system');
INSERT INTO `nms_finance_credit` VALUES ('39', 'zhaopin2', '2', '26', '1589023888', '大象招聘发布', 'ID:22', 'system');
INSERT INTO `nms_finance_credit` VALUES ('40', 'zhaopin1', '2', '7', '1589023915', '大象招聘发布', 'ID:23', 'system');
INSERT INTO `nms_finance_credit` VALUES ('41', 'zhaopin2', '2', '28', '1589023920', '大象招聘发布', 'ID:24', 'system');
INSERT INTO `nms_finance_credit` VALUES ('42', 'zhaopin2', '2', '30', '1589023954', '大象招聘发布', 'ID:25', 'system');
INSERT INTO `nms_finance_credit` VALUES ('43', 'zhaopin1', '2', '9', '1589023965', '大象招聘发布', 'ID:26', 'system');
INSERT INTO `nms_finance_credit` VALUES ('44', 'zhaopin1', '2', '11', '1589024016', '大象招聘发布', 'ID:27', 'system');
INSERT INTO `nms_finance_credit` VALUES ('45', 'zhaopin1', '2', '13', '1589024068', '大象招聘发布', 'ID:28', 'system');
INSERT INTO `nms_finance_credit` VALUES ('46', 'zhaopin1', '2', '15', '1589024098', '大象招聘发布', 'ID:29', 'system');
INSERT INTO `nms_finance_credit` VALUES ('47', 'zhaopin1', '2', '17', '1589024132', '大象招聘发布', 'ID:30', 'system');
INSERT INTO `nms_finance_credit` VALUES ('48', 'zhaopin1', '2', '19', '1589024168', '大象招聘发布', 'ID:31', 'system');
INSERT INTO `nms_finance_credit` VALUES ('49', 'zhaopin1', '2', '21', '1589024208', '大象招聘发布', 'ID:32', 'system');
INSERT INTO `nms_finance_credit` VALUES ('50', 'zhaopin1', '2', '23', '1589024246', '大象招聘发布', 'ID:33', 'system');
INSERT INTO `nms_finance_credit` VALUES ('51', 'zhaopin1', '2', '25', '1589024278', '大象招聘发布', 'ID:34', 'system');
INSERT INTO `nms_finance_credit` VALUES ('52', 'zhaopin1', '2', '27', '1589024322', '大象招聘发布', 'ID:35', 'system');
INSERT INTO `nms_finance_credit` VALUES ('53', 'zhaopin1', '2', '29', '1589024354', '大象招聘发布', 'ID:36', 'system');
INSERT INTO `nms_finance_credit` VALUES ('54', 'zhaopin1', '2', '31', '1589024383', '大象招聘发布', 'ID:37', 'system');
INSERT INTO `nms_finance_credit` VALUES ('55', 'zhaopin1', '2', '33', '1589024413', '大象招聘发布', 'ID:38', 'system');
INSERT INTO `nms_finance_credit` VALUES ('56', 'zhaopin1', '2', '35', '1589024438', '大象招聘发布', 'ID:39', 'system');
INSERT INTO `nms_finance_credit` VALUES ('57', 'zhaopin1', '-5', '30', '1589024455', '大象招聘删除', 'ID:39', 'system');
INSERT INTO `nms_finance_credit` VALUES ('58', 'fengshuo', '1', '-1', '1589024841', '登录奖励', '10.1.3.30', 'system');
INSERT INTO `nms_finance_credit` VALUES ('59', 'fulltimelink', '1', '1', '1589514035', '登录奖励', '10.1.3.30', 'system');
INSERT INTO `nms_finance_credit` VALUES ('60', 'fulltimelink', '1', '2', '1589856846', '登录奖励', '10.1.3.31', 'system');
INSERT INTO `nms_finance_credit` VALUES ('61', 'fulltimelink', '1', '3', '1589936128', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('62', 'fulltimelink', '1', '4', '1590551125', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('63', 'fulltimelink', '2', '6', '1590571187', '焦点图发布', 'ID:1', 'system');
INSERT INTO `nms_finance_credit` VALUES ('64', 'fulltimelink', '2', '8', '1590571570', '焦点图发布', 'ID:2', 'system');
INSERT INTO `nms_finance_credit` VALUES ('65', 'fulltimelink', '2', '10', '1590651205', '使用指南发布', 'ID:1', 'system');
INSERT INTO `nms_finance_credit` VALUES ('66', 'fulltimelink', '2', '12', '1590651227', '使用指南发布', 'ID:2', 'system');
INSERT INTO `nms_finance_credit` VALUES ('67', 'fulltimelink', '2', '14', '1590651346', '回收价格发布', 'ID:1', 'system');
INSERT INTO `nms_finance_credit` VALUES ('68', 'fulltimelink', '1', '15', '1591232738', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('69', 'fulltimelink', '1', '16', '1591669677', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('70', 'fulltimelink', '1', '17', '1592031503', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('71', 'fulltimelink', '1', '18', '1592278480', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('72', 'fulltimelink', '1', '19', '1592706126', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('73', 'fulltimelink', '1', '20', '1592788522', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('74', 'fulltimelink', '1', '21', '1593487582', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('75', 'recycle', '1', '1', '1593574066', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('76', 'fulltimelink', '1', '22', '1593591149', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('77', 'fulltimelink', '1', '23', '1593833747', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('78', 'recycle', '1', '2', '1593920716', '登录奖励', '171.11.7.19', 'system');
INSERT INTO `nms_finance_credit` VALUES ('79', 'recycle', '1', '3', '1593997904', '登录奖励', '123.149.69.123', 'system');
INSERT INTO `nms_finance_credit` VALUES ('80', 'fulltimelink', '1', '24', '1594001569', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('81', 'recycle', '1', '4', '1594780327', '登录奖励', '223.88.62.41', 'system');
INSERT INTO `nms_finance_credit` VALUES ('82', 'fulltimelink', '1', '25', '1595126349', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('83', 'fulltimelink', '1', '26', '1595470921', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('84', 'fulltimelink', '1', '27', '1595897469', '登录奖励', '61.163.87.166', 'system');
INSERT INTO `nms_finance_credit` VALUES ('85', 'recycle', '1', '5', '1596349689', '登录奖励', '61.163.87.166', 'system');

-- ----------------------------
-- Table structure for nms_finance_deposit
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_deposit`;
CREATE TABLE `nms_finance_deposit` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL,
  `reason` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='保证金';

-- ----------------------------
-- Records of nms_finance_deposit
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_pay
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_pay`;
CREATE TABLE `nms_finance_pay` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `fee` float unsigned NOT NULL DEFAULT '0',
  `currency` varchar(20) NOT NULL DEFAULT '',
  `paytime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `mid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `tid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='支付记录';

-- ----------------------------
-- Records of nms_finance_pay
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_promo
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_promo`;
CREATE TABLE `nms_finance_promo` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `amount` int(10) unsigned NOT NULL DEFAULT '0',
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `open` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editor` varchar(30) NOT NULL,
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='优惠促销';

-- ----------------------------
-- Records of nms_finance_promo
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_record
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_record`;
CREATE TABLE `nms_finance_record` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `bank` varchar(30) NOT NULL DEFAULT '',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `reason` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='财务流水';

-- ----------------------------
-- Records of nms_finance_record
-- ----------------------------

-- ----------------------------
-- Table structure for nms_finance_sms
-- ----------------------------
DROP TABLE IF EXISTS `nms_finance_sms`;
CREATE TABLE `nms_finance_sms` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `amount` int(10) NOT NULL DEFAULT '0',
  `balance` int(10) NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `reason` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='短信增减';

-- ----------------------------
-- Records of nms_finance_sms
-- ----------------------------

-- ----------------------------
-- Table structure for nms_form
-- ----------------------------
DROP TABLE IF EXISTS `nms_form`;
CREATE TABLE `nms_form` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `groupid` varchar(255) NOT NULL,
  `verify` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `question` int(10) unsigned NOT NULL DEFAULT '0',
  `answer` int(10) unsigned NOT NULL DEFAULT '0',
  `maxanswer` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='表单';

-- ----------------------------
-- Records of nms_form
-- ----------------------------

-- ----------------------------
-- Table structure for nms_form_answer
-- ----------------------------
DROP TABLE IF EXISTS `nms_form_answer`;
CREATE TABLE `nms_form_answer` (
  `aid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `rid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `qid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `other` varchar(255) NOT NULL,
  `item` varchar(100) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='表单回复';

-- ----------------------------
-- Records of nms_form_answer
-- ----------------------------

-- ----------------------------
-- Table structure for nms_form_question
-- ----------------------------
DROP TABLE IF EXISTS `nms_form_question`;
CREATE TABLE `nms_form_question` (
  `qid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fid` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` mediumtext NOT NULL,
  `required` varchar(30) NOT NULL,
  `extend` mediumtext NOT NULL,
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`qid`),
  KEY `fid` (`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='表单选项';

-- ----------------------------
-- Records of nms_form_question
-- ----------------------------

-- ----------------------------
-- Table structure for nms_form_record
-- ----------------------------
DROP TABLE IF EXISTS `nms_form_record`;
CREATE TABLE `nms_form_record` (
  `rid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `item` varchar(100) NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='表单回复记录';

-- ----------------------------
-- Records of nms_form_record
-- ----------------------------

-- ----------------------------
-- Table structure for nms_friend
-- ----------------------------
DROP TABLE IF EXISTS `nms_friend`;
CREATE TABLE `nms_friend` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `typeid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `truename` varchar(20) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `company` varchar(100) NOT NULL DEFAULT '',
  `career` varchar(20) NOT NULL DEFAULT '',
  `telephone` varchar(20) NOT NULL DEFAULT '',
  `mobile` varchar(20) NOT NULL DEFAULT '',
  `homepage` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `qq` varchar(20) NOT NULL DEFAULT '',
  `wx` varchar(50) NOT NULL DEFAULT '',
  `ali` varchar(30) NOT NULL DEFAULT '',
  `skype` varchar(30) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='我的商友';

-- ----------------------------
-- Records of nms_friend
-- ----------------------------

-- ----------------------------
-- Table structure for nms_gift
-- ----------------------------
DROP TABLE IF EXISTS `nms_gift`;
CREATE TABLE `nms_gift` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `credit` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` int(10) unsigned NOT NULL DEFAULT '0',
  `groupid` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `orders` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `maxorder` int(10) unsigned NOT NULL DEFAULT '1',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='积分换礼';

-- ----------------------------
-- Records of nms_gift
-- ----------------------------

-- ----------------------------
-- Table structure for nms_gift_order
-- ----------------------------
DROP TABLE IF EXISTS `nms_gift_order`;
CREATE TABLE `nms_gift_order` (
  `oid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `credit` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`oid`),
  KEY `itemid` (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='积分换礼订单';

-- ----------------------------
-- Records of nms_gift_order
-- ----------------------------

-- ----------------------------
-- Table structure for nms_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `nms_guestbook`;
CREATE TABLE `nms_guestbook` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `reply` text NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `truename` varchar(30) NOT NULL DEFAULT '',
  `telephone` varchar(50) NOT NULL DEFAULT '',
  `picurl` text,
  `email` varchar(50) NOT NULL DEFAULT '',
  `qq` varchar(20) NOT NULL DEFAULT '',
  `wx` varchar(50) NOT NULL DEFAULT '',
  `ali` varchar(30) NOT NULL DEFAULT '',
  `skype` varchar(30) NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='留言本';

-- ----------------------------
-- Records of nms_guestbook
-- ----------------------------
INSERT INTO `nms_guestbook` VALUES ('1', '0', '', '11111111111111111', '', '0', '', '', '', '', '', '', '', '', 'wap_user_1_18860353040', '', '0', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('2', '0', '', '11111111111111111', '', '0', '', '', 'https://upload.recycle.fulltime.vip/c374h6hcid8tl4a3vw.png', '', '', '', '', '', 'wap_user_1_18860353040', '', '0', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('3', '0', '', '123123123', '', '0', '', '', '', '', '', '', '', '', 'wap_user_1_18860353040', '', '1591612581', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('4', '0', '', '你好红魔攻你哦泼墨POP名哦公婆哦婆婆哦婆婆破', '', '0', '', '', 'https://upload.recycle.fulltime.vip/c3d3vfdejxve51ydbg.jpg', '', '', '', '', '', 'wap_user_1_18860353040', '', '1591760160', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('5', '0', '', '他说我的？在家干吗去。你？，一个。？我的！弟弟，。？！，你要我一不#。不？', '', '0', '', '', 'https://upload.recycle.fulltime.vip/c3d6venr6zeei9oddh.jpg', '', '', '', '', '', 'wap_user_1_18860353040', '', '1591768601', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('6', '0', '', '他说我的？在家干吗去。你？，一个。？我的！弟弟，。？！，你要我一不#。不？', '', '0', '', '', 'https://upload.recycle.fulltime.vip/c3d6venr6zeei9oddh.jpg', '', '', '', '', '', 'wap_user_1_18860353040', '', '1591768605', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('7', '0', '', '他说我的？在家干吗去。你？，一个。？我的！弟弟，。？！，你要我一不#。不？', '', '0', '', '', 'https://upload.recycle.fulltime.vip/c3d6venr6zeei9oddh.jpg', '', '', '', '', '', 'wap_user_1_18860353040', '', '1591768606', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('8', '0', '', '他说我的？在家干吗去。你？，一个。？我的！弟弟，。？！，你要我一不#。不？', '', '0', '', '', 'https://upload.recycle.fulltime.vip/c3d6venr6zeei9oddh.jpg', '', '', '', '', '', 'wap_user_1_18860353040', '', '1591768607', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('9', '0', '', 'OMGOMGOMG敏娜某某某莫娜摸头摸娜', '', '0', '', '', 'https://upload.recycle.fulltime.vip/c3d78xd6m84vgafwth.jpg', '', '', '', '', '', 'wap_user_1_18860353040', '', '1591769652', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('10', '0', '', 'dsfdsfsdafdsafdsfsdf', '', '0', '', '', '', '', '', '', '', '', '', '', '1592550413', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('11', '0', '', '第三方第三方第三方第三方第三方d\'s', '', '0', '', '', '', '', '', '', '', '', '', '', '1592550908', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('12', '0', '', 'dsfdsfdsfdsfdsfdsfdsfsd', '', '0', '', '', '', '', '', '', '', '', '', '', '1592616957', '', '0', '0');
INSERT INTO `nms_guestbook` VALUES ('13', '0', '', '无关联痛快淋漓具体旅途兔兔可口可乐了', '', '0', '', '', '', '', '', '', '', '', '', '', '1592636727', '', '0', '0');

-- ----------------------------
-- Table structure for nms_honor
-- ----------------------------
DROP TABLE IF EXISTS `nms_honor`;
CREATE TABLE `nms_honor` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `authority` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='荣誉资质';

-- ----------------------------
-- Records of nms_honor
-- ----------------------------

-- ----------------------------
-- Table structure for nms_keylink
-- ----------------------------
DROP TABLE IF EXISTS `nms_keylink`;
CREATE TABLE `nms_keylink` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `item` varchar(20) NOT NULL DEFAULT '',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='关联链接';

-- ----------------------------
-- Records of nms_keylink
-- ----------------------------

-- ----------------------------
-- Table structure for nms_keyword
-- ----------------------------
DROP TABLE IF EXISTS `nms_keyword`;
CREATE TABLE `nms_keyword` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` smallint(6) NOT NULL DEFAULT '0',
  `word` varchar(255) NOT NULL DEFAULT '',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `letter` varchar(255) NOT NULL DEFAULT '',
  `items` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `total_search` int(10) unsigned NOT NULL DEFAULT '0',
  `month_search` int(10) unsigned NOT NULL DEFAULT '0',
  `week_search` int(10) unsigned NOT NULL DEFAULT '0',
  `today_search` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`itemid`),
  KEY `moduleid` (`moduleid`),
  KEY `word` (`word`),
  KEY `letter` (`letter`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='关键词';

-- ----------------------------
-- Records of nms_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for nms_link
-- ----------------------------
DROP TABLE IF EXISTS `nms_link`;
CREATE TABLE `nms_link` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`),
  KEY `listorder` (`listorder`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='友情链接';

-- ----------------------------
-- Records of nms_link
-- ----------------------------
INSERT INTO `nms_link` VALUES ('1', '0', '0', 'DESTOON B2B', '', 'http://static.destoon.com/logo.gif', 'DESTOON B2B网站管理系统', '', '1588062475', 'fulltimelink', '1588062475', '0', '1', '3', 'http://www.destoon.com/');

-- ----------------------------
-- Table structure for nms_login
-- ----------------------------
DROP TABLE IF EXISTS `nms_login`;
CREATE TABLE `nms_login` (
  `logid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `passsalt` varchar(8) NOT NULL,
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `loginip` varchar(50) NOT NULL DEFAULT '',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0',
  `message` varchar(255) NOT NULL DEFAULT '',
  `agent` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`logid`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COMMENT='登录日志';

-- ----------------------------
-- Records of nms_login
-- ----------------------------
INSERT INTO `nms_login` VALUES ('89', 'fulltimelink', '80e0597b8c555e2e5f0d19bb0dc5f549', '', '1', '61.163.87.166', '1593833731', '密码错误,请重试', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');
INSERT INTO `nms_login` VALUES ('90', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1593833747', '成功', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');
INSERT INTO `nms_login` VALUES ('91', 'recycle', 'd14c39341dd18602baaa843bf7ba2d33', '9KpWeObH', '1', '171.11.7.19', '1593920716', '成功', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `nms_login` VALUES ('92', 'recycle', 'd14c39341dd18602baaa843bf7ba2d33', '9KpWeObH', '1', '123.160.174.126', '1593937360', '成功', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36');
INSERT INTO `nms_login` VALUES ('93', 'recycle', 'd14c39341dd18602baaa843bf7ba2d33', '9KpWeObH', '1', '123.149.69.123', '1593997904', '成功', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36 QBCore/4.0.1301.400 QQBrowser/9.0.2524.400 Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2875.116 Safar');
INSERT INTO `nms_login` VALUES ('94', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1594001569', '成功', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36');
INSERT INTO `nms_login` VALUES ('97', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1595126349', '成功', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');
INSERT INTO `nms_login` VALUES ('95', 'recycle', 'd14c39341dd18602baaa843bf7ba2d33', '9KpWeObH', '1', '61.163.87.166', '1594022162', '成功', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36');
INSERT INTO `nms_login` VALUES ('96', 'recycle', 'd14c39341dd18602baaa843bf7ba2d33', '9KpWeObH', '1', '223.88.62.41', '1594780327', '成功', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36');
INSERT INTO `nms_login` VALUES ('99', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1595470921', '成功', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');
INSERT INTO `nms_login` VALUES ('98', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1595146105', '成功', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36');
INSERT INTO `nms_login` VALUES ('100', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1595897469', '成功', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36');
INSERT INTO `nms_login` VALUES ('102', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1595930914', '成功', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');
INSERT INTO `nms_login` VALUES ('101', 'fulltimelink', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', '1', '61.163.87.166', '1595898156', '成功', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');
INSERT INTO `nms_login` VALUES ('103', 'recycle', 'd14c39341dd18602baaa843bf7ba2d33', '9KpWeObH', '1', '61.163.87.166', '1596349689', '成功', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36');

-- ----------------------------
-- Table structure for nms_mail
-- ----------------------------
DROP TABLE IF EXISTS `nms_mail`;
CREATE TABLE `nms_mail` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='邮件订阅';

-- ----------------------------
-- Records of nms_mail
-- ----------------------------

-- ----------------------------
-- Table structure for nms_mail_list
-- ----------------------------
DROP TABLE IF EXISTS `nms_mail_list`;
CREATE TABLE `nms_mail_list` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `typeids` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订阅列表';

-- ----------------------------
-- Records of nms_mail_list
-- ----------------------------

-- ----------------------------
-- Table structure for nms_mail_log
-- ----------------------------
DROP TABLE IF EXISTS `nms_mail_log`;
CREATE TABLE `nms_mail_log` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='邮件记录';

-- ----------------------------
-- Records of nms_mail_log
-- ----------------------------

-- ----------------------------
-- Table structure for nms_member
-- ----------------------------
DROP TABLE IF EXISTS `nms_member`;
CREATE TABLE `nms_member` (
  `userid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `passport` varchar(30) NOT NULL DEFAULT '',
  `company` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `passsalt` varchar(8) NOT NULL,
  `payword` varchar(32) NOT NULL DEFAULT '',
  `paysalt` varchar(8) NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '',
  `message` smallint(6) unsigned NOT NULL DEFAULT '0',
  `chat` smallint(6) unsigned NOT NULL DEFAULT '0',
  `sound` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `online` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `avatar` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `truename` varchar(20) NOT NULL DEFAULT '',
  `mobile` varchar(50) NOT NULL DEFAULT '',
  `qq` varchar(20) NOT NULL DEFAULT '',
  `wx` varchar(50) NOT NULL DEFAULT '',
  `wxqr` varchar(255) NOT NULL DEFAULT '',
  `ali` varchar(30) NOT NULL DEFAULT '',
  `skype` varchar(30) NOT NULL DEFAULT '',
  `department` varchar(30) NOT NULL DEFAULT '',
  `career` varchar(30) NOT NULL DEFAULT '',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `role` varchar(255) NOT NULL DEFAULT '',
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `groupid` smallint(4) unsigned NOT NULL DEFAULT '4',
  `regid` smallint(4) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `sms` int(10) NOT NULL DEFAULT '0',
  `credit` int(10) NOT NULL DEFAULT '0',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `deposit` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `regip` varchar(50) NOT NULL DEFAULT '',
  `regtime` int(10) unsigned NOT NULL DEFAULT '0',
  `loginip` varchar(50) NOT NULL DEFAULT '',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0',
  `logintimes` int(10) unsigned NOT NULL DEFAULT '1',
  `send` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `vemail` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vmobile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vtruename` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vbank` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vcompany` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vtrade` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `trade` varchar(50) NOT NULL DEFAULT '',
  `support` varchar(50) NOT NULL DEFAULT '',
  `inviter` varchar(30) NOT NULL DEFAULT '',
  `note` text NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `passport` (`passport`),
  KEY `groupid` (`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='会员';

-- ----------------------------
-- Records of nms_member
-- ----------------------------
INSERT INTO `nms_member` VALUES ('1', 'fulltimelink', 'fulltimelink', 'NMS新闻内容管理系统', '388ca66702eb3ca022a4e4dae6b773a6', 'xBnirXVH', 'f5933f619d4241c3c3a5d7b7ca6921f7', 'E4jVIhzW', '120608668@qq.com', '0', '0', '0', '0', '1', '1', '姓名', '', '', '', '', '', '', '', '', '1', '', '0', '1', '6', '1', '0', '27', '0.00', '0.00', '1588064295', '10.1.3.30', '1588062475', '61.163.87.166', '1595930914', '56', '1', '1', '1', '1', '0', '0', '0', '', '', '', '');
INSERT INTO `nms_member` VALUES ('2', 'fengshuo', '冯硕', '冯硕', 'c135b0d8422fdf91c79f0b46b3037f9c', 'cIzjHFM6', '5a6537df53d1e014a9928fc5df5de429', 'Mpvu6wEt', 'fengshuo@hndt.com', '1', '0', '1', '1', '0', '1', '冯硕', '', '', '', '', '', '', '', '', '2', '大象', '0', '1', '5', '1', '0', '-1', '0.00', '0.00', '1588128242', '10.1.3.75', '1588128242', '10.1.3.75', '1589028140', '8', '1', '0', '0', '0', '0', '0', '0', '', '', 'fengshuo', '');
INSERT INTO `nms_member` VALUES ('3', 'zhaopin1', '招聘1', '招聘1', 'df1a23522d34c2758b82acde64ce53b3', 'c2gHfoZq', '52c731787e8abb9924ef6a45070cb139', 'y7hSkmWW', 'zhaopin1@hndt.com', '0', '0', '1', '1', '0', '1', '招聘1', '', '', '', '', '', '', '', '', '2', '大象', '0', '1', '5', '1', '0', '30', '0.00', '0.00', '1588234103', '10.1.3.31', '1588234103', '10.1.3.31', '1589023528', '6', '1', '0', '0', '0', '0', '0', '0', '', '', 'zhaopin1', '');
INSERT INTO `nms_member` VALUES ('4', 'zhaopin2', '招聘2', '招聘2', 'efc612ad9843047b3fc41466b348007d', '6RDxC6Ip', '381177034162356591e9b3f05647663c', 'CvcMxPsJ', 'zhaopin2@hndt.com', '1', '0', '1', '1', '0', '1', '招聘2', '', '', '', '', '', '', '', '', '2', '大象', '0', '1', '5', '1', '0', '30', '0.00', '0.00', '1588234133', '10.1.3.31', '1588234133', '10.1.3.31', '1589022044', '6', '1', '0', '0', '0', '0', '0', '0', '', '', 'zhaopin2', '');
INSERT INTO `nms_member` VALUES ('5', 'recycle', 'recycle', 'recycle', 'd14c39341dd18602baaa843bf7ba2d33', '9KpWeObH', 'cf6f5f48c7a5f83a011c867855fd9b5b', 'J29boiHH', '1@qq.com', '0', '0', '1', '1', '0', '1', 'recycle', '', '', '', '', '', '', '', '', '2', '普通管理员', '0', '1', '5', '1', '0', '5', '0.00', '0.00', '1592706196', '61.163.87.166', '1592706196', '61.163.87.166', '1596349689', '13', '1', '0', '0', '0', '0', '0', '0', '', '', 'recycle', '');

-- ----------------------------
-- Table structure for nms_member_check
-- ----------------------------
DROP TABLE IF EXISTS `nms_member_check`;
CREATE TABLE `nms_member_check` (
  `userid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='会员资料审核';

-- ----------------------------
-- Records of nms_member_check
-- ----------------------------

-- ----------------------------
-- Table structure for nms_member_group
-- ----------------------------
DROP TABLE IF EXISTS `nms_member_group`;
CREATE TABLE `nms_member_group` (
  `groupid` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(50) NOT NULL DEFAULT '',
  `vip` smallint(2) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='会员组';

-- ----------------------------
-- Records of nms_member_group
-- ----------------------------
INSERT INTO `nms_member_group` VALUES ('1', '管理员', '0', '1');
INSERT INTO `nms_member_group` VALUES ('2', '禁止访问', '0', '2');
INSERT INTO `nms_member_group` VALUES ('3', '游客', '0', '3');
INSERT INTO `nms_member_group` VALUES ('4', '待审核会员', '0', '4');
INSERT INTO `nms_member_group` VALUES ('5', '个人会员', '0', '5');
INSERT INTO `nms_member_group` VALUES ('6', '企业会员', '0', '6');
INSERT INTO `nms_member_group` VALUES ('7', 'VIP会员', '1', '7');

-- ----------------------------
-- Table structure for nms_member_misc
-- ----------------------------
DROP TABLE IF EXISTS `nms_member_misc`;
CREATE TABLE `nms_member_misc` (
  `userid` bigint(20) unsigned NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `bank` varchar(30) NOT NULL DEFAULT '',
  `banktype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `branch` varchar(100) NOT NULL,
  `account` varchar(30) NOT NULL DEFAULT '',
  `reply` text NOT NULL,
  `black` text NOT NULL,
  `send` tinyint(1) unsigned NOT NULL DEFAULT '1',
  UNIQUE KEY `userid` (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='会员杂项';

-- ----------------------------
-- Records of nms_member_misc
-- ----------------------------
INSERT INTO `nms_member_misc` VALUES ('1', 'fulltimelink', '', '0', '', '', '', '', '1');
INSERT INTO `nms_member_misc` VALUES ('2', 'fengshuo', '', '0', '', '', '', '', '1');
INSERT INTO `nms_member_misc` VALUES ('3', 'zhaopin1', '', '0', '', '', '', '', '1');
INSERT INTO `nms_member_misc` VALUES ('4', 'zhaopin2', '', '0', '', '', '', '', '1');
INSERT INTO `nms_member_misc` VALUES ('5', 'recycle', '', '0', '', '', '', '', '1');

-- ----------------------------
-- Table structure for nms_message
-- ----------------------------
DROP TABLE IF EXISTS `nms_message`;
CREATE TABLE `nms_message` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `typeid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `fromuser` varchar(30) NOT NULL DEFAULT '',
  `touser` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `isread` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `feedback` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `groupids` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `touser` (`touser`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='站内信件';

-- ----------------------------
-- Records of nms_message
-- ----------------------------
INSERT INTO `nms_message` VALUES ('1', '欢迎加入NMS新闻内容管理系统', '', '4', '<table cellpadding=\"0\" cellspacing=\"0\" width=\"700\" align=\"center\">\n<tr>\n<td><a href=\"https://nms.general.dianzhenkeji.com/\" target=\"_blank\"><img src=\"https://nms.general.dianzhenkeji.com/skin/default/image/logo.gif\" style=\"margin:10px 0;border:none;\" alt=\"NMS新闻内容管理系统 LOGO\" title=\"NMS新闻内容管理系统\"/></a></td>\n</tr>\n<tr>\n<td style=\"border-top:solid 1px #DDDDDD;border-bottom:solid 1px #DDDDDD;padding:10px 0;line-height:200%;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:14px;color:#333333;\">\n尊敬的会员：<br/>\n恭喜您成功注册成为NMS新闻内容管理系统会员！<br/>\n以下为您的会员帐号信息：<br/>\n<strong>户名：</strong>fengshuo<br/>\n<strong>密码：</strong>b3QDdVm642/D1Q==<br/>\n请您妥善保存，切勿告诉他人。<br/>\n如果您在使用过程中遇到任何问题，欢迎随时与我们取得联系。<br/>\n</td>\n</tr>\n<tr>\n<td style=\"line-height:22px;padding:10px 0;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:12px;color:#666666;\">\n请注意：此邮件系 <a href=\"https://nms.general.dianzhenkeji.com/\" target=\"_blank\" style=\"color:#005590;\">NMS新闻内容管理系统</a> 自动发送，请勿直接回复。如果此邮件不是您请求的，请忽略并删除\n</td>\n</tr>\n</table>', '', 'fengshuo', '1588128242', '10.1.3.75', '0', '0', '0', '3', '');
INSERT INTO `nms_message` VALUES ('2', '欢迎加入NMS新闻内容管理系统', '', '4', '<table cellpadding=\"0\" cellspacing=\"0\" width=\"700\" align=\"center\">\n<tr>\n<td><a href=\"https://nms.general.dianzhenkeji.com/\" target=\"_blank\"><img src=\"https://nms.general.dianzhenkeji.com/file/upload/202004/30/104101601.png\" style=\"margin:10px 0;border:none;\" alt=\"NMS新闻内容管理系统 LOGO\" title=\"NMS新闻内容管理系统\"/></a></td>\n</tr>\n<tr>\n<td style=\"border-top:solid 1px #DDDDDD;border-bottom:solid 1px #DDDDDD;padding:10px 0;line-height:200%;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:14px;color:#333333;\">\n尊敬的会员：<br/>\n恭喜您成功注册成为NMS新闻内容管理系统会员！<br/>\n以下为您的会员帐号信息：<br/>\n<strong>户名：</strong>zhaopin1<br/>\n<strong>密码：</strong>$zhaopin111224488<br/>\n请您妥善保存，切勿告诉他人。<br/>\n如果您在使用过程中遇到任何问题，欢迎随时与我们取得联系。<br/>\n</td>\n</tr>\n<tr>\n<td style=\"line-height:22px;padding:10px 0;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:12px;color:#666666;\">\n请注意：此邮件系 <a href=\"https://nms.general.dianzhenkeji.com/\" target=\"_blank\" style=\"color:#005590;\">NMS新闻内容管理系统</a> 自动发送，请勿直接回复。如果此邮件不是您请求的，请忽略并删除\n</td>\n</tr>\n</table>', '', 'zhaopin1', '1588234103', '10.1.3.31', '1', '0', '0', '3', '');
INSERT INTO `nms_message` VALUES ('3', '欢迎加入NMS新闻内容管理系统', '', '4', '<table cellpadding=\"0\" cellspacing=\"0\" width=\"700\" align=\"center\">\n<tr>\n<td><a href=\"https://nms.general.dianzhenkeji.com/\" target=\"_blank\"><img src=\"https://nms.general.dianzhenkeji.com/file/upload/202004/30/104101601.png\" style=\"margin:10px 0;border:none;\" alt=\"NMS新闻内容管理系统 LOGO\" title=\"NMS新闻内容管理系统\"/></a></td>\n</tr>\n<tr>\n<td style=\"border-top:solid 1px #DDDDDD;border-bottom:solid 1px #DDDDDD;padding:10px 0;line-height:200%;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:14px;color:#333333;\">\n尊敬的会员：<br/>\n恭喜您成功注册成为NMS新闻内容管理系统会员！<br/>\n以下为您的会员帐号信息：<br/>\n<strong>户名：</strong>zhaopin2<br/>\n<strong>密码：</strong>$zhaopin211224488<br/>\n请您妥善保存，切勿告诉他人。<br/>\n如果您在使用过程中遇到任何问题，欢迎随时与我们取得联系。<br/>\n</td>\n</tr>\n<tr>\n<td style=\"line-height:22px;padding:10px 0;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:12px;color:#666666;\">\n请注意：此邮件系 <a href=\"https://nms.general.dianzhenkeji.com/\" target=\"_blank\" style=\"color:#005590;\">NMS新闻内容管理系统</a> 自动发送，请勿直接回复。如果此邮件不是您请求的，请忽略并删除\n</td>\n</tr>\n</table>', '', 'zhaopin2', '1588234133', '10.1.3.31', '0', '0', '0', '3', '');
INSERT INTO `nms_message` VALUES ('4', '欢迎加入NMS新闻内容管理系统', '', '4', '<table cellpadding=\"0\" cellspacing=\"0\" width=\"700\" align=\"center\">\n<tr>\n<td><a href=\"https://cms.hnlvyi.cn/\" target=\"_blank\"><img src=\"https://nms.general.dianzhenkeji.com/file/upload/202004/30/104101601.png\" style=\"margin:10px 0;border:none;\" alt=\"NMS新闻内容管理系统 LOGO\" title=\"NMS新闻内容管理系统\"/></a></td>\n</tr>\n<tr>\n<td style=\"border-top:solid 1px #DDDDDD;border-bottom:solid 1px #DDDDDD;padding:10px 0;line-height:200%;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:14px;color:#333333;\">\n尊敬的会员：<br/>\n恭喜您成功注册成为NMS新闻内容管理系统会员！<br/>\n以下为您的会员帐号信息：<br/>\n<strong>户名：</strong>recycle<br/>\n<strong>密码：</strong>recycle123456<br/>\n请您妥善保存，切勿告诉他人。<br/>\n如果您在使用过程中遇到任何问题，欢迎随时与我们取得联系。<br/>\n</td>\n</tr>\n<tr>\n<td style=\"line-height:22px;padding:10px 0;font-family:\'Microsoft YaHei\',Verdana,Arial;font-size:12px;color:#666666;\">\n请注意：此邮件系 <a href=\"https://cms.hnlvyi.cn/\" target=\"_blank\" style=\"color:#005590;\">NMS新闻内容管理系统</a> 自动发送，请勿直接回复。如果此邮件不是您请求的，请忽略并删除\n</td>\n</tr>\n</table>', '', 'recycle', '1592706196', '61.163.87.166', '1', '0', '0', '3', '');

-- ----------------------------
-- Table structure for nms_module
-- ----------------------------
DROP TABLE IF EXISTS `nms_module`;
CREATE TABLE `nms_module` (
  `moduleid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(20) NOT NULL DEFAULT '',
  `moduledir` varchar(20) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isblank` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `logo` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `installtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='模型';

-- ----------------------------
-- Records of nms_module
-- ----------------------------
INSERT INTO `nms_module` VALUES ('1', 'destoon', '核心', '', '', '', 'https://cms.hnlvyi.cn/', '', '1', '0', '0', '0', '0', '0', '1588062475');
INSERT INTO `nms_module` VALUES ('2', 'member', '会员', 'member', '', '', 'https://cms.hnlvyi.cn/member/', '', '2', '0', '0', '0', '0', '0', '1588062475');
INSERT INTO `nms_module` VALUES ('3', 'extend', '扩展', 'extend', '', '', 'https://cms.hnlvyi.cn/extend/', '', '0', '0', '0', '0', '0', '0', '1588062475');
INSERT INTO `nms_module` VALUES ('4', 'company', '公司', 'company', '', '', 'https://cms.hnlvyi.cn/company/', '', '7', '0', '0', '0', '0', '0', '1588062475');
INSERT INTO `nms_module` VALUES ('24', 'article', '求购', 'recyclecate', '', '', 'https://cms.hnlvyi.cn/recyclecate/', '', '24', '0', '1', '0', '0', '0', '1590553907');
INSERT INTO `nms_module` VALUES ('30', 'article', '回收类型', 'recycletype', '', '', 'https://cms.hnlvyi.cn/recycletype/', '', '30', '0', '1', '0', '0', '0', '1596190911');
INSERT INTO `nms_module` VALUES ('25', 'article', '用户', 'recycleuser', '', '', 'https://cms.hnlvyi.cn/recycleuser/', '', '25', '0', '1', '0', '0', '0', '1590553946');
INSERT INTO `nms_module` VALUES ('26', 'article', '订单', 'recycleorder', '', '', 'https://cms.hnlvyi.cn/recycleorder/', '', '26', '0', '1', '0', '0', '0', '1590553992');
INSERT INTO `nms_module` VALUES ('27', 'article', '焦点图', 'root', '', '', 'https://cms.hnlvyi.cn/root/', '', '27', '0', '1', '0', '0', '0', '1590568835');
INSERT INTO `nms_module` VALUES ('28', 'article', '使用指南', 'help', '', '', 'https://cms.hnlvyi.cn/help/', '', '28', '0', '1', '0', '0', '0', '1590568968');
INSERT INTO `nms_module` VALUES ('29', 'article', '回收价格', 'price', '', '', 'https://cms.hnlvyi.cn/price/', '', '29', '0', '1', '0', '0', '0', '1590568989');

-- ----------------------------
-- Table structure for nms_news
-- ----------------------------
DROP TABLE IF EXISTS `nms_news`;
CREATE TABLE `nms_news` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司新闻';

-- ----------------------------
-- Records of nms_news
-- ----------------------------

-- ----------------------------
-- Table structure for nms_news_data
-- ----------------------------
DROP TABLE IF EXISTS `nms_news_data`;
CREATE TABLE `nms_news_data` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司新闻内容';

-- ----------------------------
-- Records of nms_news_data
-- ----------------------------

-- ----------------------------
-- Table structure for nms_oauth
-- ----------------------------
DROP TABLE IF EXISTS `nms_oauth`;
CREATE TABLE `nms_oauth` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `site` varchar(30) NOT NULL DEFAULT '',
  `openid` varchar(255) NOT NULL DEFAULT '',
  `nickname` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `logintimes` int(10) unsigned NOT NULL DEFAULT '0',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`),
  KEY `site` (`site`,`openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='一键登录';

-- ----------------------------
-- Records of nms_oauth
-- ----------------------------

-- ----------------------------
-- Table structure for nms_online
-- ----------------------------
DROP TABLE IF EXISTS `nms_online`;
CREATE TABLE `nms_online` (
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `moduleid` int(10) unsigned NOT NULL DEFAULT '0',
  `online` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `lasttime` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `userid` (`userid`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='在线会员';

-- ----------------------------
-- Records of nms_online
-- ----------------------------
INSERT INTO `nms_online` VALUES ('1', 'fulltimelink', '61.163.87.166', '30', '0', '1596353221');

-- ----------------------------
-- Table structure for nms_order
-- ----------------------------
DROP TABLE IF EXISTS `nms_order`;
CREATE TABLE `nms_order` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mid` smallint(6) unsigned NOT NULL DEFAULT '16',
  `mallid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `cid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `buyer` varchar(30) NOT NULL DEFAULT '',
  `seller` varchar(30) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `fee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fee_name` varchar(30) NOT NULL DEFAULT '',
  `buyer_name` varchar(30) NOT NULL DEFAULT '',
  `buyer_address` varchar(255) NOT NULL DEFAULT '',
  `buyer_postcode` varchar(10) NOT NULL DEFAULT '',
  `buyer_mobile` varchar(30) NOT NULL DEFAULT '',
  `buyer_star` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `seller_star` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `send_type` varchar(50) NOT NULL DEFAULT '',
  `send_no` varchar(50) NOT NULL DEFAULT '',
  `send_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `send_time` varchar(20) NOT NULL DEFAULT '',
  `send_days` int(10) unsigned NOT NULL DEFAULT '0',
  `cod` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `trade_no` varchar(50) NOT NULL DEFAULT '',
  `add_time` smallint(6) NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `buyer_reason` mediumtext NOT NULL,
  `refund_reason` mediumtext NOT NULL,
  `note` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `buyer` (`buyer`),
  KEY `seller` (`seller`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of nms_order
-- ----------------------------

-- ----------------------------
-- Table structure for nms_page
-- ----------------------------
DROP TABLE IF EXISTS `nms_page`;
CREATE TABLE `nms_page` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司单页';

-- ----------------------------
-- Records of nms_page
-- ----------------------------

-- ----------------------------
-- Table structure for nms_page_data
-- ----------------------------
DROP TABLE IF EXISTS `nms_page_data`;
CREATE TABLE `nms_page_data` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公司单页内容';

-- ----------------------------
-- Records of nms_page_data
-- ----------------------------

-- ----------------------------
-- Table structure for nms_poll
-- ----------------------------
DROP TABLE IF EXISTS `nms_poll`;
CREATE TABLE `nms_poll` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `groupid` varchar(255) NOT NULL,
  `verify` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `thumb_width` smallint(6) unsigned NOT NULL DEFAULT '0',
  `thumb_height` smallint(6) unsigned NOT NULL DEFAULT '0',
  `poll_max` smallint(6) unsigned NOT NULL DEFAULT '0',
  `poll_page` smallint(6) unsigned NOT NULL DEFAULT '0',
  `poll_cols` smallint(6) unsigned NOT NULL DEFAULT '0',
  `poll_order` smallint(6) unsigned NOT NULL DEFAULT '0',
  `polls` int(10) unsigned NOT NULL DEFAULT '0',
  `items` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `template_poll` varchar(30) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='票选';

-- ----------------------------
-- Records of nms_poll
-- ----------------------------

-- ----------------------------
-- Table structure for nms_poll_item
-- ----------------------------
DROP TABLE IF EXISTS `nms_poll_item`;
CREATE TABLE `nms_poll_item` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pollid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `polls` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `pollid` (`pollid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='票选选项';

-- ----------------------------
-- Records of nms_poll_item
-- ----------------------------

-- ----------------------------
-- Table structure for nms_poll_record
-- ----------------------------
DROP TABLE IF EXISTS `nms_poll_record`;
CREATE TABLE `nms_poll_record` (
  `rid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pollid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `polltime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='票选记录';

-- ----------------------------
-- Records of nms_poll_record
-- ----------------------------

-- ----------------------------
-- Table structure for nms_question
-- ----------------------------
DROP TABLE IF EXISTS `nms_question`;
CREATE TABLE `nms_question` (
  `qid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL DEFAULT '',
  `answer` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`qid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='验证问题';

-- ----------------------------
-- Records of nms_question
-- ----------------------------
INSERT INTO `nms_question` VALUES ('1', '5+6=?', '11');
INSERT INTO `nms_question` VALUES ('2', '7+8=?', '15');
INSERT INTO `nms_question` VALUES ('3', '11*11=?', '121');
INSERT INTO `nms_question` VALUES ('4', '12-5=?', '7');
INSERT INTO `nms_question` VALUES ('5', '21-9=?', '12');

-- ----------------------------
-- Table structure for nms_session
-- ----------------------------
DROP TABLE IF EXISTS `nms_session`;
CREATE TABLE `nms_session` (
  `sessionid` varchar(32) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `lastvisit` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `sessionid` (`sessionid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='SESSION';

-- ----------------------------
-- Records of nms_session
-- ----------------------------

-- ----------------------------
-- Table structure for nms_setting
-- ----------------------------
DROP TABLE IF EXISTS `nms_setting`;
CREATE TABLE `nms_setting` (
  `item` varchar(30) NOT NULL DEFAULT '',
  `item_key` varchar(100) NOT NULL DEFAULT '',
  `item_value` text NOT NULL,
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='网站设置';

-- ----------------------------
-- Records of nms_setting
-- ----------------------------
INSERT INTO `nms_setting` VALUES ('1', 'water_mark', 'watermark.png');
INSERT INTO `nms_setting` VALUES ('1', 'max_image', '800');
INSERT INTO `nms_setting` VALUES ('1', 'thumb_title', '0');
INSERT INTO `nms_setting` VALUES ('1', 'thumb_album', '0');
INSERT INTO `nms_setting` VALUES ('1', 'middle_h', '300');
INSERT INTO `nms_setting` VALUES ('1', 'middle_w', '400');
INSERT INTO `nms_setting` VALUES ('1', 'water_middle', '0');
INSERT INTO `nms_setting` VALUES ('1', 'water_com', '1');
INSERT INTO `nms_setting` VALUES ('1', 'gif_ani', '1');
INSERT INTO `nms_setting` VALUES ('1', 'bmp_jpg', '1');
INSERT INTO `nms_setting` VALUES ('1', 'water_pos', '9');
INSERT INTO `nms_setting` VALUES ('1', 'water_min_wh', '180');
INSERT INTO `nms_setting` VALUES ('1', 'water_margin', '10');
INSERT INTO `nms_setting` VALUES ('1', 'water_type', '0');
INSERT INTO `nms_setting` VALUES ('1', 'file_my', 'xxxnmsmy.php');
INSERT INTO `nms_setting` VALUES ('1', 'file_login', 'xxxnmslogin.php');
INSERT INTO `nms_setting` VALUES ('1', 'file_register', 'xxxnmsregister.php');
INSERT INTO `nms_setting` VALUES ('1', 'defend_proxy', '0');
INSERT INTO `nms_setting` VALUES ('1', 'defend_reload', '0');
INSERT INTO `nms_setting` VALUES ('1', 'defend_cc', '0');
INSERT INTO `nms_setting` VALUES ('1', 'safe_domain', '');
INSERT INTO `nms_setting` VALUES ('1', 'check_referer', '1');
INSERT INTO `nms_setting` VALUES ('1', 'uploaddir', 'Ym/d');
INSERT INTO `nms_setting` VALUES ('1', 'uploadsize', '204800');
INSERT INTO `nms_setting` VALUES ('1', 'uploadtype', 'jpg|jpeg|png|gif|bmp|mp4|flv|rar|zip|pdf|doc|docx|xls|xlsx|ppt|ppts');
INSERT INTO `nms_setting` VALUES ('1', 'uploadlog', '1');
INSERT INTO `nms_setting` VALUES ('1', 'anticopy', '0');
INSERT INTO `nms_setting` VALUES ('1', 'ip_login', '0');
INSERT INTO `nms_setting` VALUES ('1', 'login_log', '2');
INSERT INTO `nms_setting` VALUES ('1', 'admin_log', '2');
INSERT INTO `nms_setting` VALUES ('1', 'admin_online', '1');
INSERT INTO `nms_setting` VALUES ('1', 'md5_pass', '1');
INSERT INTO `nms_setting` VALUES ('1', 'captcha_admin', '0');
INSERT INTO `nms_setting` VALUES ('1', 'captcha_cn', '0');
INSERT INTO `nms_setting` VALUES ('1', 'captcha_chars', '');
INSERT INTO `nms_setting` VALUES ('1', 'check_hour', '');
INSERT INTO `nms_setting` VALUES ('1', 'admin_hour', '');
INSERT INTO `nms_setting` VALUES ('1', 'admin_ip', '');
INSERT INTO `nms_setting` VALUES ('1', 'admin_area', '');
INSERT INTO `nms_setting` VALUES ('1', 'remote_url', '');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_path', '');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_save', '0');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_pasv', '1');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_ssl', '0');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_pass', '');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_user', '');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_port', '21');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_host', '');
INSERT INTO `nms_setting` VALUES ('1', 'ftp_remote', '0');
INSERT INTO `nms_setting` VALUES ('1', 'max_len', '50000');
INSERT INTO `nms_setting` VALUES ('1', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('1', 'schcate_limit', '10');
INSERT INTO `nms_setting` VALUES ('1', 'pushtime', '10');
INSERT INTO `nms_setting` VALUES ('1', 'online', '1200');
INSERT INTO `nms_setting` VALUES ('1', 'search_limit', '1');
INSERT INTO `nms_setting` VALUES ('1', 'max_kw', '32');
INSERT INTO `nms_setting` VALUES ('1', 'min_kw', '3');
INSERT INTO `nms_setting` VALUES ('1', 'search_check_kw', '0');
INSERT INTO `nms_setting` VALUES ('1', 'search_kw', '1');
INSERT INTO `nms_setting` VALUES ('1', 'save_draft', '2');
INSERT INTO `nms_setting` VALUES ('1', 'search_tips', '1');
INSERT INTO `nms_setting` VALUES ('1', 'anti_spam', '1');
INSERT INTO `nms_setting` VALUES ('1', 'log_credit', '1');
INSERT INTO `nms_setting` VALUES ('1', 'pages_mode', '0');
INSERT INTO `nms_setting` VALUES ('1', 'lazy', '0');
INSERT INTO `nms_setting` VALUES ('1', 'gzip_enable', '0');
INSERT INTO `nms_setting` VALUES ('1', 'cache_hits', '0');
INSERT INTO `nms_setting` VALUES ('1', 'cache_search', '0');
INSERT INTO `nms_setting` VALUES ('1', 'task_item', '86400');
INSERT INTO `nms_setting` VALUES ('1', 'task_list', '1800');
INSERT INTO `nms_setting` VALUES ('1', 'task_index', '600');
INSERT INTO `nms_setting` VALUES ('1', 'log_404', '1');
INSERT INTO `nms_setting` VALUES ('1', 'pcharset', '0');
INSERT INTO `nms_setting` VALUES ('2', 'oauth', '0');
INSERT INTO `nms_setting` VALUES ('2', 'edit_check', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_bbspre', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_bbs', '1');
INSERT INTO `nms_setting` VALUES ('2', 'uc_key', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_appid', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_charset', 'utf8');
INSERT INTO `nms_setting` VALUES ('2', 'uc_dbpre', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_dbname', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_dbpwd', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_dbuser', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_dbhost', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_mysql', '1');
INSERT INTO `nms_setting` VALUES ('2', 'uc_ip', '');
INSERT INTO `nms_setting` VALUES ('2', 'uc_api', '');
INSERT INTO `nms_setting` VALUES ('2', 'passport_key', '');
INSERT INTO `nms_setting` VALUES ('2', 'passport_url', '');
INSERT INTO `nms_setting` VALUES ('2', 'passport_charset', 'gbk');
INSERT INTO `nms_setting` VALUES ('2', 'passport', '0');
INSERT INTO `nms_setting` VALUES ('2', 'ex_name', '');
INSERT INTO `nms_setting` VALUES ('2', 'ex_rate', '');
INSERT INTO `nms_setting` VALUES ('2', 'ex_fdnm', '');
INSERT INTO `nms_setting` VALUES ('2', 'ex_prex', '');
INSERT INTO `nms_setting` VALUES ('2', 'ex_data', '');
INSERT INTO `nms_setting` VALUES ('2', 'ex_pass', '');
INSERT INTO `nms_setting` VALUES ('2', 'ex_user', 'root');
INSERT INTO `nms_setting` VALUES ('2', 'ex_host', 'localhost');
INSERT INTO `nms_setting` VALUES ('2', 'ex_type', 'PW');
INSERT INTO `nms_setting` VALUES ('2', 'credit_exchange', '0');
INSERT INTO `nms_setting` VALUES ('2', 'credit_price', '5|10|45|85');
INSERT INTO `nms_setting` VALUES ('2', 'credit_buy', '30|100|500|1000');
INSERT INTO `nms_setting` VALUES ('2', 'credit_del_page', '5');
INSERT INTO `nms_setting` VALUES ('2', 'credit_add_page', '2');
INSERT INTO `nms_setting` VALUES ('2', 'credit_del_news', '5');
INSERT INTO `nms_setting` VALUES ('2', 'credit_add_news', '2');
INSERT INTO `nms_setting` VALUES ('2', 'credit_less', '1');
INSERT INTO `nms_setting` VALUES ('2', 'credit_edit', '10');
INSERT INTO `nms_setting` VALUES ('2', 'credit_login', '1');
INSERT INTO `nms_setting` VALUES ('2', 'credit_user', '20');
INSERT INTO `nms_setting` VALUES ('2', 'credit_ip', '2');
INSERT INTO `nms_setting` VALUES ('2', 'credit_maxip', '50');
INSERT INTO `nms_setting` VALUES ('2', 'send_types', '平邮|EMS|顺丰快递|申通快递|圆通快递|中通快递|国通快递|宅急送|韵达快递|天天快递|如风达|百世汇通|全峰快递|快捷快递|其它');
INSERT INTO `nms_setting` VALUES ('2', 'credit_del_credit', '5');
INSERT INTO `nms_setting` VALUES ('2', 'credit_add_credit', '2');
INSERT INTO `nms_setting` VALUES ('2', 'credit_charge', '1');
INSERT INTO `nms_setting` VALUES ('2', 'cash_fee_min', '1');
INSERT INTO `nms_setting` VALUES ('2', 'cash_fee_max', '50');
INSERT INTO `nms_setting` VALUES ('2', 'deposit', '1000');
INSERT INTO `nms_setting` VALUES ('2', 'trade_day', '10');
INSERT INTO `nms_setting` VALUES ('2', 'pay_banks', '站内|支付宝|微信支付|财付通|现金|招商银行|工商银行|农业银行|建设银行|交通银行|中国银行|邮政储蓄|邮政汇款');
INSERT INTO `nms_setting` VALUES ('2', 'cash_fee', '1');
INSERT INTO `nms_setting` VALUES ('2', 'cash_max', '10000');
INSERT INTO `nms_setting` VALUES ('2', 'cash_min', '50');
INSERT INTO `nms_setting` VALUES ('2', 'cash_enable', '0');
INSERT INTO `nms_setting` VALUES ('2', 'cash_banks', '支付宝|微信|财付通|招商银行|工商银行|农业银行|建设银行|交通银行|中国银行|邮政储蓄|邮政汇款');
INSERT INTO `nms_setting` VALUES ('2', 'cash_times', '3');
INSERT INTO `nms_setting` VALUES ('2', 'pay_url', '');
INSERT INTO `nms_setting` VALUES ('2', 'awards', '1|2|5|10|20|50|100');
INSERT INTO `nms_setting` VALUES ('2', 'mincharge', '0');
INSERT INTO `nms_setting` VALUES ('2', 'pay_card', '0');
INSERT INTO `nms_setting` VALUES ('2', 'pay_online', '0');
INSERT INTO `nms_setting` VALUES ('2', 'link_check', '2');
INSERT INTO `nms_setting` VALUES ('2', 'credit_clear', '0');
INSERT INTO `nms_setting` VALUES ('2', 'credit_save', '0');
INSERT INTO `nms_setting` VALUES ('2', 'credit_check', '2');
INSERT INTO `nms_setting` VALUES ('2', 'page_clear', '0');
INSERT INTO `nms_setting` VALUES ('2', 'page_save', '0');
INSERT INTO `nms_setting` VALUES ('2', 'page_check', '2');
INSERT INTO `nms_setting` VALUES ('2', 'news_clear', '0');
INSERT INTO `nms_setting` VALUES ('2', 'news_save', '0');
INSERT INTO `nms_setting` VALUES ('2', 'news_thumb_height', '180');
INSERT INTO `nms_setting` VALUES ('2', 'news_thumb_width', '240');
INSERT INTO `nms_setting` VALUES ('2', 'news_check', '2');
INSERT INTO `nms_setting` VALUES ('2', 'introduce_clear', '0');
INSERT INTO `nms_setting` VALUES ('2', 'introduce_save', '0');
INSERT INTO `nms_setting` VALUES ('2', 'introduce_length', '0');
INSERT INTO `nms_setting` VALUES ('2', 'thumb_height', '180');
INSERT INTO `nms_setting` VALUES ('2', 'com_mode', '制造商|贸易商|服务商|其他机构');
INSERT INTO `nms_setting` VALUES ('2', 'money_unit', '人民币|港元|台币|美元|欧元|英镑');
INSERT INTO `nms_setting` VALUES ('2', 'thumb_width', '180');
INSERT INTO `nms_setting` VALUES ('2', 'cate_max', '6');
INSERT INTO `nms_setting` VALUES ('2', 'mode_max', '2');
INSERT INTO `nms_setting` VALUES ('1', 'search_rewrite', '0');
INSERT INTO `nms_setting` VALUES ('1', 'com_https', '0');
INSERT INTO `nms_setting` VALUES ('1', 'com_www', '0');
INSERT INTO `nms_setting` VALUES ('1', 'rewrite', '1');
INSERT INTO `nms_setting` VALUES ('1', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('1', 'file_ext', 'html');
INSERT INTO `nms_setting` VALUES ('1', 'index', 'index');
INSERT INTO `nms_setting` VALUES ('1', 'seo_description', 'NMS新闻内容管理系统');
INSERT INTO `nms_setting` VALUES ('3', 'sitemaps_priority', '0.8');
INSERT INTO `nms_setting` VALUES ('3', 'sitemaps_changefreq', 'monthly');
INSERT INTO `nms_setting` VALUES ('3', 'sitemaps', '0');
INSERT INTO `nms_setting` VALUES ('3', 'feed_pagesize', '50');
INSERT INTO `nms_setting` VALUES ('3', 'feed_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'feed_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'archiver_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'archiver_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'form_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'form_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'poll_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'poll_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'vote_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'vote_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'gift_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'gift_time', '86400');
INSERT INTO `nms_setting` VALUES ('3', 'gift_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'guestbook_captcha', '1');
INSERT INTO `nms_setting` VALUES ('3', 'guestbook_type', '意见反馈');
INSERT INTO `nms_setting` VALUES ('3', 'guestbook_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'guestbook_enable', '1');
INSERT INTO `nms_setting` VALUES ('3', 'comment_am', '网友');
INSERT INTO `nms_setting` VALUES ('3', 'credit_del_comment', '5');
INSERT INTO `nms_setting` VALUES ('3', 'credit_add_comment', '2');
INSERT INTO `nms_setting` VALUES ('3', 'comment_limit', '30');
INSERT INTO `nms_setting` VALUES ('3', 'comment_pagesize', '10');
INSERT INTO `nms_setting` VALUES ('3', 'comment_time', '30');
INSERT INTO `nms_setting` VALUES ('3', 'comment_max', '500');
INSERT INTO `nms_setting` VALUES ('3', 'comment_min', '5');
INSERT INTO `nms_setting` VALUES ('3', 'comment_vote', '1');
INSERT INTO `nms_setting` VALUES ('3', 'comment_admin_del', '1');
INSERT INTO `nms_setting` VALUES ('3', 'comment_user_del', '4');
INSERT INTO `nms_setting` VALUES ('3', 'comment_captcha_add', '1');
INSERT INTO `nms_setting` VALUES ('3', 'comment_check', '2');
INSERT INTO `nms_setting` VALUES ('3', 'comment_vote_group', '5,6,7');
INSERT INTO `nms_setting` VALUES ('3', 'comment_group', '5,6,7');
INSERT INTO `nms_setting` VALUES ('3', 'comment_show', '0');
INSERT INTO `nms_setting` VALUES ('3', 'comment_api_key', '');
INSERT INTO `nms_setting` VALUES ('3', 'comment_api_id', '');
INSERT INTO `nms_setting` VALUES ('3', 'comment_api', '');
INSERT INTO `nms_setting` VALUES ('3', 'comment_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'link_request', '');
INSERT INTO `nms_setting` VALUES ('3', 'link_reg', '1');
INSERT INTO `nms_setting` VALUES ('3', 'link_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'link_enable', '0');
INSERT INTO `nms_setting` VALUES ('4', 'group_message', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'group_buy', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('4', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('4', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('4', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'group_price', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'seo_description_show', '{内容标题}{内容简介}{分类名称}{分类SEO描述}{模块名称}{网站名称}{网站SEO描述}');
INSERT INTO `nms_setting` VALUES ('4', 'group_inquiry', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'seo_keywords_show', '{内容标题}{分类名称}{分类SEO关键词}{模块名称}{网站SEO关键词}');
INSERT INTO `nms_setting` VALUES ('4', 'seo_title_show', '{内容标题}{分类名称}{分类SEO标题}{模块名称}{网站名称}{网站SEO标题}{分隔符}');
INSERT INTO `nms_setting` VALUES ('4', 'seo_description_list', '{网站SEO描述}{网站名称}{模块名称}{分类SEO描述}{分类名称}');
INSERT INTO `nms_setting` VALUES ('4', 'seo_keywords_list', '{分类名称}{分类SEO关键词}{模块名称}{网站名称}{网站SEO关键词}');
INSERT INTO `nms_setting` VALUES ('4', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('4', 'seo_keywords_index', '{模块名称}{网站名称}{网站SEO标题}');
INSERT INTO `nms_setting` VALUES ('4', 'seo_description_index', '{模块名称}{网站名称}{网站SEO标题}');
INSERT INTO `nms_setting` VALUES ('4', 'php_list_urlid', '5');
INSERT INTO `nms_setting` VALUES ('4', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('4', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('4', 'htm_list_prefix', 'company_list_');
INSERT INTO `nms_setting` VALUES ('4', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('4', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('4', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('4', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('4', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('4', 'page_inew', '10');
INSERT INTO `nms_setting` VALUES ('4', 'group_contact', '5,6,7');
INSERT INTO `nms_setting` VALUES ('4', 'page_inews', '10');
INSERT INTO `nms_setting` VALUES ('4', 'page_ivip', '10');
INSERT INTO `nms_setting` VALUES ('4', 'page_irec', '10');
INSERT INTO `nms_setting` VALUES ('4', 'page_subcat', '6');
INSERT INTO `nms_setting` VALUES ('4', 'level', '推荐公司');
INSERT INTO `nms_setting` VALUES ('4', 'kf', 'qq,53kf,tq,qiao');
INSERT INTO `nms_setting` VALUES ('4', 'stats', 'baidu,qq,cnzz,51la');
INSERT INTO `nms_setting` VALUES ('4', 'map', 'baidu');
INSERT INTO `nms_setting` VALUES ('4', 'vip_honor', '1');
INSERT INTO `nms_setting` VALUES ('4', 'vip_maxyear', '5');
INSERT INTO `nms_setting` VALUES ('4', 'vip_year', '1');
INSERT INTO `nms_setting` VALUES ('4', 'vip_cominfo', '1');
INSERT INTO `nms_setting` VALUES ('4', 'vip_maxgroupvip', '3');
INSERT INTO `nms_setting` VALUES ('4', 'delvip', '1');
INSERT INTO `nms_setting` VALUES ('4', 'openall', '0');
INSERT INTO `nms_setting` VALUES ('4', 'split', '0');
INSERT INTO `nms_setting` VALUES ('4', 'comment', '1');
INSERT INTO `nms_setting` VALUES ('4', 'homeurl', '0');
INSERT INTO `nms_setting` VALUES ('4', 'fields', 'userid,username,company,linkurl,thumb,catid,areaid,vip,groupid,validated,business,mode');
INSERT INTO `nms_setting` VALUES ('4', 'order', 'vip desc,userid desc');
INSERT INTO `nms_setting` VALUES ('4', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('4', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('4', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('4', 'title_index', '{$seo_modulename}{$seo_delimiter}{$seo_page}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('4', 'title_list', '{$seo_cattitle}{$seo_page}{$seo_modulename}{$seo_delimiter}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('4', 'title_show', '{$seo_showtitle}{$seo_catname}{$seo_cattitle}{$seo_modulename}{$seo_sitename}{$seo_sitetitle}{$seo_delimiter}');
INSERT INTO `nms_setting` VALUES ('4', 'title_search', '');
INSERT INTO `nms_setting` VALUES ('4', 'keywords_index', '{$seo_modulename}{$seo_sitename}{$seo_sitetitle}');
INSERT INTO `nms_setting` VALUES ('4', 'keywords_list', '{$seo_catname}{$seo_catkeywords}{$seo_modulename}{$seo_sitename}{$seo_sitekeywords}');
INSERT INTO `nms_setting` VALUES ('4', 'keywords_show', '{$seo_showtitle}{$seo_catname}{$seo_catkeywords}{$seo_modulename}{$seo_sitekeywords}');
INSERT INTO `nms_setting` VALUES ('4', 'keywords_search', '');
INSERT INTO `nms_setting` VALUES ('4', 'description_index', '{$seo_modulename}{$seo_sitename}{$seo_sitetitle}');
INSERT INTO `nms_setting` VALUES ('4', 'description_list', '{$seo_sitedescription}{$seo_sitename}{$seo_modulename}{$seo_catdescription}{$seo_catname}');
INSERT INTO `nms_setting` VALUES ('4', 'description_show', '{$seo_showtitle}{$seo_showintroduce}{$seo_catname}{$seo_catdescription}{$seo_modulename}{$seo_sitename}{$seo_sitedescription}');
INSERT INTO `nms_setting` VALUES ('4', 'description_search', '');
INSERT INTO `nms_setting` VALUES ('4', 'module', 'company');
INSERT INTO `nms_setting` VALUES ('4', 'mobile', 'http://demo.destoon.com/v7.0/mobile/company/');
INSERT INTO `nms_setting` VALUES ('1', 'seo_keywords', 'NMS新闻内容管理系统');
INSERT INTO `nms_setting` VALUES ('1', 'seo_title', 'NMS新闻内容管理系统');
INSERT INTO `nms_setting` VALUES ('1', 'seo_delimiter', '_');
INSERT INTO `nms_setting` VALUES ('1', 'im_skype', '0');
INSERT INTO `nms_setting` VALUES ('1', 'im_ali', '0');
INSERT INTO `nms_setting` VALUES ('1', 'im_wx', '0');
INSERT INTO `nms_setting` VALUES ('1', 'im_qq', '0');
INSERT INTO `nms_setting` VALUES ('1', 'im_web', '0');
INSERT INTO `nms_setting` VALUES ('1', 'admin_left', '218');
INSERT INTO `nms_setting` VALUES ('1', 'max_cart', '50');
INSERT INTO `nms_setting` VALUES ('1', 'quick_pay', '200');
INSERT INTO `nms_setting` VALUES ('1', 'credit_unit', '点');
INSERT INTO `nms_setting` VALUES ('1', 'credit_name', '积分');
INSERT INTO `nms_setting` VALUES ('1', 'money_sign', '￥');
INSERT INTO `nms_setting` VALUES ('1', 'money_unit', '元');
INSERT INTO `nms_setting` VALUES ('1', 'money_name', '资金');
INSERT INTO `nms_setting` VALUES ('1', 'city_ip', '0');
INSERT INTO `nms_setting` VALUES ('1', 'city', '0');
INSERT INTO `nms_setting` VALUES ('24', 'page_child', '6');
INSERT INTO `nms_setting` VALUES ('24', 'show_lcat', '1');
INSERT INTO `nms_setting` VALUES ('24', 'page_lrecimg', '4');
INSERT INTO `nms_setting` VALUES ('24', 'page_lrec', '10');
INSERT INTO `nms_setting` VALUES ('24', 'page_lhits', '10');
INSERT INTO `nms_setting` VALUES ('24', 'page_srelate', '10');
INSERT INTO `nms_setting` VALUES ('24', 'page_srecimg', '4');
INSERT INTO `nms_setting` VALUES ('24', 'page_srec', '10');
INSERT INTO `nms_setting` VALUES ('24', 'page_shits', '10');
INSERT INTO `nms_setting` VALUES ('24', 'max_width', '800');
INSERT INTO `nms_setting` VALUES ('24', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('24', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('24', 'show_np', '1');
INSERT INTO `nms_setting` VALUES ('24', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('24', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('24', 'htm_list_prefix', '');
INSERT INTO `nms_setting` VALUES ('24', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('24', 'php_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('24', 'show_html', '0');
INSERT INTO `nms_setting` VALUES ('24', 'htm_item_prefix', '');
INSERT INTO `nms_setting` VALUES ('24', 'htm_item_urlid', '1');
INSERT INTO `nms_setting` VALUES ('24', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('24', 'php_item_urlid', '0');
INSERT INTO `nms_setting` VALUES ('24', 'seo_keywords_index', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_description_index', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_keywords_list', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('24', 'seo_description_list', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_keywords_show', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_title_show', '{内容标题}{分隔符}{分类名称}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('24', 'seo_description_show', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('24', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('24', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('24', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('24', 'group_show', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('24', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('24', 'group_color', '7');
INSERT INTO `nms_setting` VALUES ('24', 'check_add', '2');
INSERT INTO `nms_setting` VALUES ('24', 'captcha_add', '2');
INSERT INTO `nms_setting` VALUES ('24', 'question_add', '2');
INSERT INTO `nms_setting` VALUES ('24', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('24', 'fee_currency', 'money');
INSERT INTO `nms_setting` VALUES ('24', 'fee_add', '0');
INSERT INTO `nms_setting` VALUES ('24', 'fee_view', '0');
INSERT INTO `nms_setting` VALUES ('24', 'fee_period', '0');
INSERT INTO `nms_setting` VALUES ('24', 'fee_back', '0');
INSERT INTO `nms_setting` VALUES ('24', 'fee_award', '100');
INSERT INTO `nms_setting` VALUES ('24', 'pre_view', '200');
INSERT INTO `nms_setting` VALUES ('24', 'credit_add', '2');
INSERT INTO `nms_setting` VALUES ('24', 'credit_del', '5');
INSERT INTO `nms_setting` VALUES ('24', 'credit_color', '100');
INSERT INTO `nms_setting` VALUES ('24', 'limit_1', '0');
INSERT INTO `nms_setting` VALUES ('24', 'free_limit_1', '-1');
INSERT INTO `nms_setting` VALUES ('24', 'free_limit_2', '0');
INSERT INTO `nms_setting` VALUES ('24', 'limit_2', '-1');
INSERT INTO `nms_setting` VALUES ('24', 'limit_3', '-1');
INSERT INTO `nms_setting` VALUES ('24', 'free_limit_3', '0');
INSERT INTO `nms_setting` VALUES ('24', 'limit_4', '-1');
INSERT INTO `nms_setting` VALUES ('24', 'free_limit_4', '0');
INSERT INTO `nms_setting` VALUES ('24', 'limit_5', '3');
INSERT INTO `nms_setting` VALUES ('24', 'free_limit_5', '0');
INSERT INTO `nms_setting` VALUES ('24', 'limit_6', '30');
INSERT INTO `nms_setting` VALUES ('24', 'free_limit_6', '0');
INSERT INTO `nms_setting` VALUES ('24', 'limit_7', '100');
INSERT INTO `nms_setting` VALUES ('24', 'free_limit_7', '-1');
INSERT INTO `nms_setting` VALUES ('2', 'editor', 'Default');
INSERT INTO `nms_setting` VALUES ('2', 'com_type', '企业单位|事业单位或社会团体|个体经营|其他');
INSERT INTO `nms_setting` VALUES ('2', 'com_size', '1-49人|50-99人|100-499人|500-999人|1000-3000人|3000-5000人|5000-10000人|10000人以上');
INSERT INTO `nms_setting` VALUES ('2', 'vfax', '');
INSERT INTO `nms_setting` VALUES ('2', 'vcompany', '1');
INSERT INTO `nms_setting` VALUES ('2', 'vbank', '1');
INSERT INTO `nms_setting` VALUES ('2', 'vtruename', '1');
INSERT INTO `nms_setting` VALUES ('2', 'vmobile', '1');
INSERT INTO `nms_setting` VALUES ('2', 'vemail', '1');
INSERT INTO `nms_setting` VALUES ('2', 'vmember', '0');
INSERT INTO `nms_setting` VALUES ('2', 'chat_img', '0');
INSERT INTO `nms_setting` VALUES ('2', 'chat_url', '0');
INSERT INTO `nms_setting` VALUES ('2', 'chat_file', '0');
INSERT INTO `nms_setting` VALUES ('2', 'chat_mintime', '3');
INSERT INTO `nms_setting` VALUES ('2', 'chat_poll', '1');
INSERT INTO `nms_setting` VALUES ('2', 'chat_timeout', '600');
INSERT INTO `nms_setting` VALUES ('2', 'chat_maxlen', '300');
INSERT INTO `nms_setting` VALUES ('2', 'alert_check', '2');
INSERT INTO `nms_setting` VALUES ('2', 'alertid', '5|6|22');
INSERT INTO `nms_setting` VALUES ('2', 'auth_days', '3');
INSERT INTO `nms_setting` VALUES ('2', 'captcha_home', '2');
INSERT INTO `nms_setting` VALUES ('2', 'captcha_edit', '2');
INSERT INTO `nms_setting` VALUES ('2', 'captcha_sendmessage', '2');
INSERT INTO `nms_setting` VALUES ('2', 'maxtouser', '5');
INSERT INTO `nms_setting` VALUES ('2', 'login_scan', '0');
INSERT INTO `nms_setting` VALUES ('2', 'login_sms', '0');
INSERT INTO `nms_setting` VALUES ('2', 'login_time', '864000');
INSERT INTO `nms_setting` VALUES ('2', 'captcha_login', '0');
INSERT INTO `nms_setting` VALUES ('2', 'lock_hour', '1');
INSERT INTO `nms_setting` VALUES ('2', 'login_times', '5000');
INSERT INTO `nms_setting` VALUES ('2', 'usernote', '');
INSERT INTO `nms_setting` VALUES ('2', 'iptimeout', '24');
INSERT INTO `nms_setting` VALUES ('2', 'banagent', '');
INSERT INTO `nms_setting` VALUES ('2', 'defend_proxy', '0');
INSERT INTO `nms_setting` VALUES ('2', 'sms_register', '0');
INSERT INTO `nms_setting` VALUES ('2', 'credit_register', '0');
INSERT INTO `nms_setting` VALUES ('2', 'money_register', '0');
INSERT INTO `nms_setting` VALUES ('2', 'question_register', '1');
INSERT INTO `nms_setting` VALUES ('2', 'captcha_register', '1');
INSERT INTO `nms_setting` VALUES ('2', 'welcome_sms', '1');
INSERT INTO `nms_setting` VALUES ('2', 'welcome_email', '1');
INSERT INTO `nms_setting` VALUES ('2', 'welcome_message', '1');
INSERT INTO `nms_setting` VALUES ('2', 'checkuser', '0');
INSERT INTO `nms_setting` VALUES ('2', 'banemail', '');
INSERT INTO `nms_setting` VALUES ('2', 'banmodec', '0');
INSERT INTO `nms_setting` VALUES ('2', 'bancompany', '');
INSERT INTO `nms_setting` VALUES ('2', 'mixpassword', '1,2');
INSERT INTO `nms_setting` VALUES ('2', 'maxpassword', '20');
INSERT INTO `nms_setting` VALUES ('2', 'minpassword', '6');
INSERT INTO `nms_setting` VALUES ('2', 'banmodeu', '0');
INSERT INTO `nms_setting` VALUES ('2', 'banusername', 'admin|system|master|web|sell|buy|company|quote|job|article|info|page|bbs');
INSERT INTO `nms_setting` VALUES ('2', 'maxusername', '20');
INSERT INTO `nms_setting` VALUES ('2', 'minusername', '4');
INSERT INTO `nms_setting` VALUES ('2', 'enable_register', '0');
INSERT INTO `nms_setting` VALUES ('oauth-taobao', 'key', '');
INSERT INTO `nms_setting` VALUES ('pay-chinabank', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('oauth-netease', 'key', '');
INSERT INTO `nms_setting` VALUES ('1', 'close_reason', '网站维护中，请稍候访问...');
INSERT INTO `nms_setting` VALUES ('1', 'close', '0');
INSERT INTO `nms_setting` VALUES ('1', 'icpno', '');
INSERT INTO `nms_setting` VALUES ('1', 'telephone', '');
INSERT INTO `nms_setting` VALUES ('1', 'copyright', '(c)2020 NMS CMS All Rights Reserved');
INSERT INTO `nms_setting` VALUES ('1', 'sitename', '蕙收管理系统');
INSERT INTO `nms_setting` VALUES ('1', 'logo', 'https://nms.general.dianzhenkeji.com/file/upload/202004/30/104101601.png');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'email', '');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'order', '1');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'name', '支付宝');
INSERT INTO `nms_setting` VALUES ('pay-alipay', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-aliwap', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('pay-aliwap', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-aliwap', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-aliwap', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-aliwap', 'order', '2');
INSERT INTO `nms_setting` VALUES ('pay-aliwap', 'name', '支付宝');
INSERT INTO `nms_setting` VALUES ('pay-aliwap', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'percent', '2');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'appid', '');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'order', '3');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'name', '微信支付');
INSERT INTO `nms_setting` VALUES ('pay-weixin', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-tenpay', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('pay-tenpay', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-tenpay', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-tenpay', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-tenpay', 'order', '4');
INSERT INTO `nms_setting` VALUES ('pay-tenpay', 'name', '财付通');
INSERT INTO `nms_setting` VALUES ('pay-tenpay', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'cert', '');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'order', '5');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'name', '中国银联');
INSERT INTO `nms_setting` VALUES ('pay-upay', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-chinabank', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-chinabank', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-chinabank', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-chinabank', 'order', '6');
INSERT INTO `nms_setting` VALUES ('pay-chinabank', 'name', '网银在线');
INSERT INTO `nms_setting` VALUES ('pay-chinabank', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-yeepay', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('pay-yeepay', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-yeepay', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-yeepay', 'order', '7');
INSERT INTO `nms_setting` VALUES ('pay-yeepay', 'name', '易宝支付');
INSERT INTO `nms_setting` VALUES ('pay-yeepay', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-kq99bill', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('pay-kq99bill', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-kq99bill', 'cert', '');
INSERT INTO `nms_setting` VALUES ('pay-kq99bill', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-kq99bill', 'order', '8');
INSERT INTO `nms_setting` VALUES ('pay-kq99bill', 'name', '快钱支付');
INSERT INTO `nms_setting` VALUES ('pay-kq99bill', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-chinapay', 'percent', '1');
INSERT INTO `nms_setting` VALUES ('pay-chinapay', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-chinapay', 'order', '9');
INSERT INTO `nms_setting` VALUES ('pay-chinapay', 'name', '银联在线');
INSERT INTO `nms_setting` VALUES ('pay-chinapay', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'percent', '0');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'currency', 'USD');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'keycode', '');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'notify', '');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'partnerid', '');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'order', '10');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'name', '贝宝');
INSERT INTO `nms_setting` VALUES ('pay-paypal', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('oauth-netease', 'id', '');
INSERT INTO `nms_setting` VALUES ('oauth-qq', 'key', '');
INSERT INTO `nms_setting` VALUES ('oauth-qq', 'id', '');
INSERT INTO `nms_setting` VALUES ('oauth-qq', 'order', '1');
INSERT INTO `nms_setting` VALUES ('oauth-qq', 'name', 'QQ登录');
INSERT INTO `nms_setting` VALUES ('oauth-qq', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('oauth-sina', 'sync', '0');
INSERT INTO `nms_setting` VALUES ('oauth-sina', 'key', '');
INSERT INTO `nms_setting` VALUES ('oauth-sina', 'id', '');
INSERT INTO `nms_setting` VALUES ('oauth-sina', 'order', '2');
INSERT INTO `nms_setting` VALUES ('oauth-sina', 'name', '新浪微博');
INSERT INTO `nms_setting` VALUES ('oauth-sina', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('oauth-baidu', 'key', '');
INSERT INTO `nms_setting` VALUES ('oauth-baidu', 'id', '');
INSERT INTO `nms_setting` VALUES ('oauth-baidu', 'order', '3');
INSERT INTO `nms_setting` VALUES ('oauth-baidu', 'name', '百度');
INSERT INTO `nms_setting` VALUES ('oauth-baidu', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('oauth-netease', 'order', '4');
INSERT INTO `nms_setting` VALUES ('oauth-netease', 'name', '网易通行证');
INSERT INTO `nms_setting` VALUES ('oauth-netease', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('oauth-wechat', 'key', '');
INSERT INTO `nms_setting` VALUES ('oauth-wechat', 'id', '');
INSERT INTO `nms_setting` VALUES ('oauth-wechat', 'order', '5');
INSERT INTO `nms_setting` VALUES ('oauth-wechat', 'name', '微信');
INSERT INTO `nms_setting` VALUES ('oauth-wechat', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('oauth-taobao', 'id', '');
INSERT INTO `nms_setting` VALUES ('oauth-taobao', 'order', '6');
INSERT INTO `nms_setting` VALUES ('oauth-taobao', 'name', '淘宝');
INSERT INTO `nms_setting` VALUES ('oauth-taobao', 'enable', '0');
INSERT INTO `nms_setting` VALUES ('weixin', 'bind', '点击可绑定会员帐号、查看会员信息、收发站内信件、管理我的订单等服务内容');
INSERT INTO `nms_setting` VALUES ('weixin', 'welcome', '感谢您的关注，请点击菜单查看相应的服务');
INSERT INTO `nms_setting` VALUES ('weixin', 'auto', '');
INSERT INTO `nms_setting` VALUES ('weixin', 'weixin', '');
INSERT INTO `nms_setting` VALUES ('weixin', 'aeskey', '');
INSERT INTO `nms_setting` VALUES ('weixin', 'apptoken', '');
INSERT INTO `nms_setting` VALUES ('weixin', 'appsecret', '');
INSERT INTO `nms_setting` VALUES ('weixin', 'appid', '');
INSERT INTO `nms_setting` VALUES ('weixin', 'credit', '10');
INSERT INTO `nms_setting` VALUES ('weixin-menu', 'menu', 'a:3:{i:0;a:6:{i:0;a:2:{s:4:\"name\";s:6:\"最新\";s:3:\"key\";s:0:\"\";}i:1;a:2:{s:4:\"name\";s:6:\"资讯\";s:3:\"key\";s:7:\"V_mid21\";}i:2;a:2:{s:4:\"name\";s:6:\"供应\";s:3:\"key\";s:6:\"V_mid5\";}i:3;a:2:{s:4:\"name\";s:6:\"求购\";s:3:\"key\";s:6:\"V_mid6\";}i:4;a:2:{s:4:\"name\";s:6:\"商城\";s:3:\"key\";s:7:\"V_mid16\";}i:5;a:2:{s:4:\"name\";s:6:\"招商\";s:3:\"key\";s:7:\"V_mid22\";}}i:1;a:6:{i:0;a:2:{s:4:\"name\";s:6:\"会员\";s:3:\"key\";s:8:\"V_member\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:2;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:3;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:4;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:5;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}}i:2;a:6:{i:0;a:2:{s:4:\"name\";s:6:\"更多\";s:3:\"key\";s:44:\"https://nms.general.dianzhenkeji.com/mobile/\";}i:1;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:2;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:3;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:4;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}i:5;a:2:{s:4:\"name\";s:0:\"\";s:3:\"key\";s:0:\"\";}}}');
INSERT INTO `nms_setting` VALUES ('group-1', 'listorder', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'reg', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'type', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'edit_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'refresh_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'day_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'hour_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'add_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'copy', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'delete', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'vweixin', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'vdeposit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'vcompany', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'vtruename', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'vmobile', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'resume', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'vemail', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'moduleids', '16,5,6,17,7,8,21,22,13,9,10,12,14,15,18');
INSERT INTO `nms_setting` VALUES ('group-1', 'link_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'honor_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'page_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'news_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'kf', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'stats', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'map', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'style', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'main_d', '1,5');
INSERT INTO `nms_setting` VALUES ('group-1', 'main_c', '1,5');
INSERT INTO `nms_setting` VALUES ('group-1', 'home_main', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'side_d', '0,3,6');
INSERT INTO `nms_setting` VALUES ('group-1', 'side_c', '0,3,6');
INSERT INTO `nms_setting` VALUES ('group-1', 'home_side', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'menu_d', '0,6,7,11');
INSERT INTO `nms_setting` VALUES ('group-1', 'menu_c', '0,6,7,11');
INSERT INTO `nms_setting` VALUES ('group-1', 'home_menu', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'home', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'styleid', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'homepage', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'type_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'price_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'inquiry_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'message_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'promo_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'express_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'address_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'alert_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'favorite_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'friend_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'inbox_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'chat', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'ad', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'spread', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'sms', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'sendmail', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'trade_order', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'group_order', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'mail', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'ask', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'cash', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'question', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'captcha', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'check', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'uploadpt', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'uploadcredit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'uploadday', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'uploadlimit', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'uploadsize', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'uploadtype', '');
INSERT INTO `nms_setting` VALUES ('group-1', 'upload', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('group-1', 'grade', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'biz', '1');
INSERT INTO `nms_setting` VALUES ('group-1', 'commission', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'discount', '100');
INSERT INTO `nms_setting` VALUES ('group-1', 'fee', '0');
INSERT INTO `nms_setting` VALUES ('group-1', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'listorder', '2');
INSERT INTO `nms_setting` VALUES ('group-2', 'reg', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'type', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'vmobile', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'edit_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'refresh_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'day_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'hour_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'add_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'copy', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'delete', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'vweixin', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'vdeposit', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'vcompany', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'vtruename', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'vemail', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'resume', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'moduleids', '6');
INSERT INTO `nms_setting` VALUES ('group-2', 'link_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'honor_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'page_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'news_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'kf', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'stats', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'map', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'style', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'main_d', '5');
INSERT INTO `nms_setting` VALUES ('group-2', 'main_c', '5');
INSERT INTO `nms_setting` VALUES ('group-2', 'home_main', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'side_d', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'side_c', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'home_side', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'menu_d', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'menu_c', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'home_menu', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'home', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'styleid', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'homepage', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'type_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'price_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'inquiry_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'message_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'promo_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'express_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'address_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'alert_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'favorite_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'friend_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'inbox_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-2', 'chat', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'ad', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'group_order', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'spread', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'trade_order', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'sendmail', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'sms', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'mail', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'ask', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'cash', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'question', '1');
INSERT INTO `nms_setting` VALUES ('group-2', 'captcha', '1');
INSERT INTO `nms_setting` VALUES ('group-2', 'check', '1');
INSERT INTO `nms_setting` VALUES ('group-2', 'uploadpt', '1');
INSERT INTO `nms_setting` VALUES ('group-2', 'uploadcredit', '10');
INSERT INTO `nms_setting` VALUES ('group-2', 'uploadday', '10');
INSERT INTO `nms_setting` VALUES ('group-2', 'uploadlimit', '2');
INSERT INTO `nms_setting` VALUES ('group-2', 'uploadsize', '200');
INSERT INTO `nms_setting` VALUES ('group-2', 'uploadtype', '');
INSERT INTO `nms_setting` VALUES ('group-2', 'upload', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'editor', 'Basic');
INSERT INTO `nms_setting` VALUES ('group-2', 'grade', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'biz', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'commission', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'discount', '100');
INSERT INTO `nms_setting` VALUES ('group-2', 'fee', '0');
INSERT INTO `nms_setting` VALUES ('group-2', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'listorder', '3');
INSERT INTO `nms_setting` VALUES ('group-3', 'reg', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'type', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'refresh_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'day_limit', '3');
INSERT INTO `nms_setting` VALUES ('group-3', 'edit_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'hour_limit', '1');
INSERT INTO `nms_setting` VALUES ('group-3', 'add_limit', '30');
INSERT INTO `nms_setting` VALUES ('group-3', 'copy', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'vweixin', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'delete', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'vdeposit', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'vcompany', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'vtruename', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'vmobile', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'vemail', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'resume', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'moduleids', '5,6,8,22,9');
INSERT INTO `nms_setting` VALUES ('group-3', 'link_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'honor_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'page_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'news_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'kf', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'stats', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'map', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'style', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'main_d', '5');
INSERT INTO `nms_setting` VALUES ('group-3', 'main_c', '5');
INSERT INTO `nms_setting` VALUES ('group-3', 'home_main', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'side_d', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'side_c', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'home_side', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'menu_d', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'menu_c', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'home_menu', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'home', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'styleid', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'homepage', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'type_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'price_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-3', 'inquiry_limit', '30');
INSERT INTO `nms_setting` VALUES ('group-3', 'message_limit', '30');
INSERT INTO `nms_setting` VALUES ('group-3', 'promo_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'express_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'address_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'alert_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'favorite_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'friend_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'inbox_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-3', 'chat', '1');
INSERT INTO `nms_setting` VALUES ('group-3', 'ad', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'spread', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'group_order', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'trade_order', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'sendmail', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'sms', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'mail', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'ask', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'cash', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'question', '1');
INSERT INTO `nms_setting` VALUES ('group-3', 'captcha', '1');
INSERT INTO `nms_setting` VALUES ('group-3', 'check', '1');
INSERT INTO `nms_setting` VALUES ('group-3', 'uploadpt', '1');
INSERT INTO `nms_setting` VALUES ('group-3', 'uploadcredit', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'uploadday', '10');
INSERT INTO `nms_setting` VALUES ('group-3', 'uploadlimit', '5');
INSERT INTO `nms_setting` VALUES ('group-3', 'uploadsize', '500');
INSERT INTO `nms_setting` VALUES ('group-3', 'uploadtype', '');
INSERT INTO `nms_setting` VALUES ('group-3', 'upload', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'editor', 'Basic');
INSERT INTO `nms_setting` VALUES ('group-3', 'grade', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'biz', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'commission', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'discount', '100');
INSERT INTO `nms_setting` VALUES ('group-3', 'fee', '0');
INSERT INTO `nms_setting` VALUES ('group-3', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'listorder', '4');
INSERT INTO `nms_setting` VALUES ('group-4', 'reg', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'type', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'edit_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'refresh_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'day_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'hour_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'add_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'copy', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'delete', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'vweixin', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'vdeposit', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'vcompany', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'vtruename', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'vmobile', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'vemail', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'resume', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'moduleids', '6');
INSERT INTO `nms_setting` VALUES ('group-4', 'link_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'honor_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'page_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'news_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'kf', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'stats', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'map', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'style', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'main_c', '5');
INSERT INTO `nms_setting` VALUES ('group-4', 'main_d', '5');
INSERT INTO `nms_setting` VALUES ('group-4', 'home_main', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'side_d', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'menu_c', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'menu_d', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'side_c', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'home_side', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'home_menu', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'home', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'styleid', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'homepage', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'type_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'price_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'inquiry_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'message_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'promo_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'express_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'address_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'alert_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'favorite_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'friend_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'inbox_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-4', 'trade_order', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'group_order', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'spread', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'ad', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'chat', '1');
INSERT INTO `nms_setting` VALUES ('group-4', 'sendmail', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'sms', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'mail', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'ask', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'cash', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'question', '1');
INSERT INTO `nms_setting` VALUES ('group-4', 'captcha', '1');
INSERT INTO `nms_setting` VALUES ('group-4', 'check', '1');
INSERT INTO `nms_setting` VALUES ('group-4', 'uploadpt', '1');
INSERT INTO `nms_setting` VALUES ('group-4', 'uploadcredit', '5');
INSERT INTO `nms_setting` VALUES ('group-4', 'uploadday', '10');
INSERT INTO `nms_setting` VALUES ('group-4', 'uploadlimit', '5');
INSERT INTO `nms_setting` VALUES ('group-4', 'uploadsize', '500');
INSERT INTO `nms_setting` VALUES ('group-4', 'uploadtype', '');
INSERT INTO `nms_setting` VALUES ('group-4', 'upload', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'editor', 'Basic');
INSERT INTO `nms_setting` VALUES ('group-4', 'grade', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'biz', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'commission', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'discount', '100');
INSERT INTO `nms_setting` VALUES ('group-4', 'fee', '0');
INSERT INTO `nms_setting` VALUES ('group-4', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'listorder', '5');
INSERT INTO `nms_setting` VALUES ('group-5', 'reg', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'type', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'edit_limit', '3');
INSERT INTO `nms_setting` VALUES ('group-5', 'day_limit', '3');
INSERT INTO `nms_setting` VALUES ('group-5', 'refresh_limit', '43200');
INSERT INTO `nms_setting` VALUES ('group-5', 'hour_limit', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'add_limit', '60');
INSERT INTO `nms_setting` VALUES ('group-5', 'copy', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'delete', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'vweixin', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'vdeposit', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'vcompany', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'vtruename', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'vmobile', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'vemail', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'resume', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'moduleids', '5,6,10,12,18');
INSERT INTO `nms_setting` VALUES ('group-5', 'link_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-5', 'honor_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-5', 'page_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-5', 'news_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-5', 'kf', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'stats', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'map', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'style', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'main_d', '5');
INSERT INTO `nms_setting` VALUES ('group-5', 'main_c', '5');
INSERT INTO `nms_setting` VALUES ('group-5', 'home_main', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'side_d', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'side_c', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'home_side', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'menu_d', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'menu_c', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'home_menu', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'home', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'styleid', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'homepage', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'type_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-5', 'price_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-5', 'inquiry_limit', '3');
INSERT INTO `nms_setting` VALUES ('group-5', 'message_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-5', 'promo_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-5', 'express_limit', '-1');
INSERT INTO `nms_setting` VALUES ('group-5', 'address_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-5', 'alert_limit', '3');
INSERT INTO `nms_setting` VALUES ('group-5', 'favorite_limit', '20');
INSERT INTO `nms_setting` VALUES ('group-5', 'friend_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-5', 'inbox_limit', '20');
INSERT INTO `nms_setting` VALUES ('group-5', 'chat', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'ad', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'spread', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'group_order', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'trade_order', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'sendmail', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'sms', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'mail', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'ask', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'cash', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'question', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'captcha', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'check', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'uploadpt', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'uploadcredit', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'uploadday', '20');
INSERT INTO `nms_setting` VALUES ('group-5', 'uploadlimit', '5');
INSERT INTO `nms_setting` VALUES ('group-5', 'uploadsize', '');
INSERT INTO `nms_setting` VALUES ('group-5', 'uploadtype', '');
INSERT INTO `nms_setting` VALUES ('group-5', 'upload', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'editor', 'Simple');
INSERT INTO `nms_setting` VALUES ('group-5', 'grade', '1');
INSERT INTO `nms_setting` VALUES ('group-5', 'biz', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'commission', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'discount', '100');
INSERT INTO `nms_setting` VALUES ('group-5', 'fee', '0');
INSERT INTO `nms_setting` VALUES ('group-5', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'listorder', '6');
INSERT INTO `nms_setting` VALUES ('group-6', 'reg', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'type', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'day_limit', '5');
INSERT INTO `nms_setting` VALUES ('group-6', 'refresh_limit', '21600');
INSERT INTO `nms_setting` VALUES ('group-6', 'edit_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'hour_limit', '2');
INSERT INTO `nms_setting` VALUES ('group-6', 'add_limit', '60');
INSERT INTO `nms_setting` VALUES ('group-6', 'copy', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'delete', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'vweixin', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'vtruename', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'vcompany', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'vdeposit', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'vmobile', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'vemail', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'resume', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'moduleids', '16,5,6,17,7,8,22,13,9,10,12');
INSERT INTO `nms_setting` VALUES ('group-6', 'link_limit', '20');
INSERT INTO `nms_setting` VALUES ('group-6', 'honor_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-6', 'page_limit', '5');
INSERT INTO `nms_setting` VALUES ('group-6', 'news_limit', '20');
INSERT INTO `nms_setting` VALUES ('group-6', 'kf', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'map', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'stats', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'style', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'main_d', '0,1,2');
INSERT INTO `nms_setting` VALUES ('group-6', 'main_c', '0,1,2,3,4,5,6');
INSERT INTO `nms_setting` VALUES ('group-6', 'home_main', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'side_c', '0,1,2,3,4,5,6');
INSERT INTO `nms_setting` VALUES ('group-6', 'side_d', '0,2,4,6');
INSERT INTO `nms_setting` VALUES ('group-6', 'home_menu', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'menu_c', '0,1,2,3,4,5,6,7,8,9,10,11');
INSERT INTO `nms_setting` VALUES ('group-6', 'menu_d', '0,1,2,3,4,6,7');
INSERT INTO `nms_setting` VALUES ('group-6', 'home_side', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'home', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'styleid', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'type_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-6', 'homepage', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'price_limit', '3');
INSERT INTO `nms_setting` VALUES ('group-6', 'inquiry_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-6', 'message_limit', '20');
INSERT INTO `nms_setting` VALUES ('group-6', 'promo_limit', '3');
INSERT INTO `nms_setting` VALUES ('group-6', 'express_limit', '5');
INSERT INTO `nms_setting` VALUES ('group-6', 'address_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-6', 'alert_limit', '5');
INSERT INTO `nms_setting` VALUES ('group-6', 'favorite_limit', '50');
INSERT INTO `nms_setting` VALUES ('group-6', 'friend_limit', '50');
INSERT INTO `nms_setting` VALUES ('group-6', 'inbox_limit', '50');
INSERT INTO `nms_setting` VALUES ('group-6', 'group_order', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'spread', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'ad', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'chat', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'trade_order', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'sendmail', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'sms', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'mail', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'ask', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'cash', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'question', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'captcha', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'check', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'uploadday', '50');
INSERT INTO `nms_setting` VALUES ('group-6', 'uploadcredit', '2');
INSERT INTO `nms_setting` VALUES ('group-6', 'uploadpt', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'uploadlimit', '5');
INSERT INTO `nms_setting` VALUES ('group-6', 'uploadsize', '');
INSERT INTO `nms_setting` VALUES ('group-6', 'uploadtype', '');
INSERT INTO `nms_setting` VALUES ('group-6', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('group-6', 'upload', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'grade', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'biz', '1');
INSERT INTO `nms_setting` VALUES ('group-6', 'commission', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'discount', '100');
INSERT INTO `nms_setting` VALUES ('group-6', 'fee', '0');
INSERT INTO `nms_setting` VALUES ('group-6', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'listorder', '7');
INSERT INTO `nms_setting` VALUES ('group-7', 'reg', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'type', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'edit_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'refresh_limit', '3600');
INSERT INTO `nms_setting` VALUES ('group-7', 'day_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-7', 'hour_limit', '5');
INSERT INTO `nms_setting` VALUES ('group-7', 'add_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'copy', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'delete', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'vweixin', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'vdeposit', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'vcompany', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'vtruename', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'vmobile', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'resume', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'vemail', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'moduleids', '16,5,6,17,7,8,21,22,13,9,10,12,14,15,18');
INSERT INTO `nms_setting` VALUES ('group-7', 'link_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'kf', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'news_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'page_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-7', 'honor_limit', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'stats', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'map', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'style', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'main_d', '0,1,2,7');
INSERT INTO `nms_setting` VALUES ('group-7', 'main_c', '0,1,2,4,5,6,7');
INSERT INTO `nms_setting` VALUES ('group-7', 'home_main', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'side_c', '0,1,2,3,4,5,6');
INSERT INTO `nms_setting` VALUES ('group-7', 'side_d', '0,1,2,4,6');
INSERT INTO `nms_setting` VALUES ('group-7', 'home_side', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'menu_d', '0,1,2,3,4,5,6,7,8,9,10,11,12,13');
INSERT INTO `nms_setting` VALUES ('group-7', 'home', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'home_menu', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'menu_c', '0,1,2,3,4,5,6,7,8,9,10,11,12,13');
INSERT INTO `nms_setting` VALUES ('group-7', 'styleid', '2');
INSERT INTO `nms_setting` VALUES ('group-7', 'homepage', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'type_limit', '20');
INSERT INTO `nms_setting` VALUES ('group-7', 'price_limit', '20');
INSERT INTO `nms_setting` VALUES ('group-7', 'inquiry_limit', '50');
INSERT INTO `nms_setting` VALUES ('group-7', 'message_limit', '100');
INSERT INTO `nms_setting` VALUES ('group-7', 'promo_limit', '5');
INSERT INTO `nms_setting` VALUES ('group-7', 'express_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-7', 'address_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-7', 'alert_limit', '10');
INSERT INTO `nms_setting` VALUES ('group-7', 'favorite_limit', '100');
INSERT INTO `nms_setting` VALUES ('group-7', 'friend_limit', '200');
INSERT INTO `nms_setting` VALUES ('group-7', 'inbox_limit', '500');
INSERT INTO `nms_setting` VALUES ('group-7', 'chat', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'ad', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'spread', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'group_order', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'trade_order', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'sendmail', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'sms', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'mail', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'ask', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'cash', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'question', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'captcha', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'check', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'uploadpt', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'uploadcredit', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'uploadday', '100');
INSERT INTO `nms_setting` VALUES ('group-7', 'uploadlimit', '10');
INSERT INTO `nms_setting` VALUES ('group-7', 'uploadsize', '');
INSERT INTO `nms_setting` VALUES ('group-7', 'uploadtype', '');
INSERT INTO `nms_setting` VALUES ('group-7', 'upload', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('group-7', 'grade', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'biz', '1');
INSERT INTO `nms_setting` VALUES ('group-7', 'commission', '0');
INSERT INTO `nms_setting` VALUES ('group-7', 'discount', '');
INSERT INTO `nms_setting` VALUES ('group-7', 'fee', '2000');
INSERT INTO `nms_setting` VALUES ('group-7', 'fee_mode', '1');
INSERT INTO `nms_setting` VALUES ('destoon', 'backtime', '1588062475');
INSERT INTO `nms_setting` VALUES ('24', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('24', 'page_ihits', '10');
INSERT INTO `nms_setting` VALUES ('24', 'page_irecimg', '6');
INSERT INTO `nms_setting` VALUES ('24', 'show_icat', '1');
INSERT INTO `nms_setting` VALUES ('24', 'page_icat', '6');
INSERT INTO `nms_setting` VALUES ('24', 'page_islide', '3');
INSERT INTO `nms_setting` VALUES ('24', 'swfu', '2');
INSERT INTO `nms_setting` VALUES ('24', 'fulltext', '1');
INSERT INTO `nms_setting` VALUES ('24', 'level', '推荐文章|幻灯图片|推荐图文|头条相关|头条推荐');
INSERT INTO `nms_setting` VALUES ('24', 'split', '0');
INSERT INTO `nms_setting` VALUES ('24', 'keylink', '1');
INSERT INTO `nms_setting` VALUES ('24', 'clear_link', '0');
INSERT INTO `nms_setting` VALUES ('24', 'save_remotepic', '0');
INSERT INTO `nms_setting` VALUES ('24', 'cat_property', '0');
INSERT INTO `nms_setting` VALUES ('24', 'order', 'addtime desc');
INSERT INTO `nms_setting` VALUES ('24', 'fields', 'itemid,title,thumb,linkurl,style,catid,introduce,addtime,edittime,username,islink,hits');
INSERT INTO `nms_setting` VALUES ('24', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('24', 'introduce_length', '120');
INSERT INTO `nms_setting` VALUES ('24', 'thumb_height', '180');
INSERT INTO `nms_setting` VALUES ('24', 'thumb_width', '240');
INSERT INTO `nms_setting` VALUES ('24', 'template_my', '');
INSERT INTO `nms_setting` VALUES ('24', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('24', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('24', 'template_show', '');
INSERT INTO `nms_setting` VALUES ('24', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('24', 'title_index', '{$seo_modulename}{$seo_delimiter}{$seo_page}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('24', 'title_list', '{$seo_cattitle}{$seo_page}{$seo_modulename}{$seo_delimiter}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('24', 'title_show', '{$seo_showtitle}{$seo_delimiter}{$seo_catname}{$seo_modulename}{$seo_delimiter}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('24', 'title_search', '');
INSERT INTO `nms_setting` VALUES ('24', 'keywords_index', '');
INSERT INTO `nms_setting` VALUES ('24', 'keywords_list', '');
INSERT INTO `nms_setting` VALUES ('24', 'keywords_show', '');
INSERT INTO `nms_setting` VALUES ('24', 'keywords_search', '');
INSERT INTO `nms_setting` VALUES ('24', 'description_index', '');
INSERT INTO `nms_setting` VALUES ('24', 'description_list', '');
INSERT INTO `nms_setting` VALUES ('24', 'description_show', '');
INSERT INTO `nms_setting` VALUES ('24', 'description_search', '');
INSERT INTO `nms_setting` VALUES ('24', 'moduleid', '21');
INSERT INTO `nms_setting` VALUES ('24', 'name', '资讯');
INSERT INTO `nms_setting` VALUES ('24', 'moduledir', 'news');
INSERT INTO `nms_setting` VALUES ('24', 'module', 'article');
INSERT INTO `nms_setting` VALUES ('24', 'ismenu', '1');
INSERT INTO `nms_setting` VALUES ('24', 'domain', '');
INSERT INTO `nms_setting` VALUES ('24', 'linkurl', 'http://demo.destoon.com/v7.0/news/');
INSERT INTO `nms_setting` VALUES ('24', 'mobile', 'http://demo.destoon.com/v7.0/mobile/news/');
INSERT INTO `nms_setting` VALUES ('25', 'credit_color', '100');
INSERT INTO `nms_setting` VALUES ('25', 'credit_del', '5');
INSERT INTO `nms_setting` VALUES ('25', 'credit_add', '2');
INSERT INTO `nms_setting` VALUES ('25', 'pre_view', '200');
INSERT INTO `nms_setting` VALUES ('25', 'fee_award', '100');
INSERT INTO `nms_setting` VALUES ('25', 'fee_back', '0');
INSERT INTO `nms_setting` VALUES ('25', 'fee_period', '0');
INSERT INTO `nms_setting` VALUES ('25', 'fee_view', '0');
INSERT INTO `nms_setting` VALUES ('25', 'fee_add', '0');
INSERT INTO `nms_setting` VALUES ('25', 'fee_currency', 'money');
INSERT INTO `nms_setting` VALUES ('25', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('25', 'question_add', '2');
INSERT INTO `nms_setting` VALUES ('25', 'captcha_add', '2');
INSERT INTO `nms_setting` VALUES ('25', 'check_add', '2');
INSERT INTO `nms_setting` VALUES ('25', 'group_color', '7');
INSERT INTO `nms_setting` VALUES ('25', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('25', 'group_show', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('25', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('25', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('25', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_description_show', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_keywords_show', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_title_show', '{内容标题}{分隔符}{分类名称}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('25', 'seo_description_list', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_keywords_list', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('25', 'seo_description_index', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_keywords_index', '');
INSERT INTO `nms_setting` VALUES ('25', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('25', 'php_item_urlid', '0');
INSERT INTO `nms_setting` VALUES ('25', 'htm_item_urlid', '1');
INSERT INTO `nms_setting` VALUES ('25', 'htm_item_prefix', '');
INSERT INTO `nms_setting` VALUES ('25', 'show_html', '0');
INSERT INTO `nms_setting` VALUES ('25', 'php_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('25', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('25', 'htm_list_prefix', '');
INSERT INTO `nms_setting` VALUES ('25', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('25', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('25', 'show_np', '1');
INSERT INTO `nms_setting` VALUES ('25', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('25', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('25', 'max_width', '800');
INSERT INTO `nms_setting` VALUES ('25', 'page_shits', '10');
INSERT INTO `nms_setting` VALUES ('25', 'page_srec', '10');
INSERT INTO `nms_setting` VALUES ('25', 'page_srecimg', '4');
INSERT INTO `nms_setting` VALUES ('25', 'page_srelate', '10');
INSERT INTO `nms_setting` VALUES ('25', 'page_lhits', '10');
INSERT INTO `nms_setting` VALUES ('25', 'page_lrec', '10');
INSERT INTO `nms_setting` VALUES ('25', 'page_lrecimg', '4');
INSERT INTO `nms_setting` VALUES ('25', 'show_lcat', '1');
INSERT INTO `nms_setting` VALUES ('25', 'page_child', '6');
INSERT INTO `nms_setting` VALUES ('25', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('25', 'page_ihits', '10');
INSERT INTO `nms_setting` VALUES ('25', 'page_irecimg', '6');
INSERT INTO `nms_setting` VALUES ('25', 'page_islide', '3');
INSERT INTO `nms_setting` VALUES ('25', 'page_icat', '6');
INSERT INTO `nms_setting` VALUES ('25', 'show_icat', '1');
INSERT INTO `nms_setting` VALUES ('25', 'swfu', '2');
INSERT INTO `nms_setting` VALUES ('25', 'level', '推荐文章|幻灯图片|推荐图文|头条相关|头条推荐');
INSERT INTO `nms_setting` VALUES ('25', 'fulltext', '1');
INSERT INTO `nms_setting` VALUES ('25', 'split', '0');
INSERT INTO `nms_setting` VALUES ('25', 'clear_link', '0');
INSERT INTO `nms_setting` VALUES ('25', 'keylink', '1');
INSERT INTO `nms_setting` VALUES ('25', 'cat_property', '0');
INSERT INTO `nms_setting` VALUES ('25', 'save_remotepic', '0');
INSERT INTO `nms_setting` VALUES ('25', 'fields', 'itemid,title,thumb,linkurl,style,catid,introduce,addtime,edittime,username,islink,hits');
INSERT INTO `nms_setting` VALUES ('25', 'order', 'addtime desc');
INSERT INTO `nms_setting` VALUES ('25', 'editor', 'Default');
INSERT INTO `nms_setting` VALUES ('25', 'introduce_length', '120');
INSERT INTO `nms_setting` VALUES ('25', 'thumb_height', '180');
INSERT INTO `nms_setting` VALUES ('25', 'thumb_width', '240');
INSERT INTO `nms_setting` VALUES ('25', 'default_water', '0');
INSERT INTO `nms_setting` VALUES ('25', 'wxpic', '');
INSERT INTO `nms_setting` VALUES ('25', 'template_my', '');
INSERT INTO `nms_setting` VALUES ('25', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('25', 'template_show', '');
INSERT INTO `nms_setting` VALUES ('25', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('25', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('27', 'fee_back', '0');
INSERT INTO `nms_setting` VALUES ('26', 'free_limit_7', '-1');
INSERT INTO `nms_setting` VALUES ('26', 'limit_7', '100');
INSERT INTO `nms_setting` VALUES ('26', 'free_limit_6', '0');
INSERT INTO `nms_setting` VALUES ('26', 'limit_6', '30');
INSERT INTO `nms_setting` VALUES ('26', 'free_limit_5', '0');
INSERT INTO `nms_setting` VALUES ('26', 'limit_5', '3');
INSERT INTO `nms_setting` VALUES ('26', 'free_limit_4', '0');
INSERT INTO `nms_setting` VALUES ('26', 'limit_4', '-1');
INSERT INTO `nms_setting` VALUES ('26', 'free_limit_3', '0');
INSERT INTO `nms_setting` VALUES ('26', 'limit_3', '-1');
INSERT INTO `nms_setting` VALUES ('26', 'free_limit_2', '0');
INSERT INTO `nms_setting` VALUES ('26', 'limit_2', '-1');
INSERT INTO `nms_setting` VALUES ('26', 'free_limit_1', '-1');
INSERT INTO `nms_setting` VALUES ('26', 'limit_1', '0');
INSERT INTO `nms_setting` VALUES ('26', 'credit_color', '100');
INSERT INTO `nms_setting` VALUES ('26', 'credit_del', '5');
INSERT INTO `nms_setting` VALUES ('26', 'credit_add', '2');
INSERT INTO `nms_setting` VALUES ('26', 'pre_view', '200');
INSERT INTO `nms_setting` VALUES ('26', 'fee_award', '100');
INSERT INTO `nms_setting` VALUES ('26', 'fee_back', '0');
INSERT INTO `nms_setting` VALUES ('26', 'fee_period', '0');
INSERT INTO `nms_setting` VALUES ('26', 'fee_view', '0');
INSERT INTO `nms_setting` VALUES ('26', 'fee_add', '0');
INSERT INTO `nms_setting` VALUES ('26', 'fee_currency', 'money');
INSERT INTO `nms_setting` VALUES ('26', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('26', 'question_add', '2');
INSERT INTO `nms_setting` VALUES ('26', 'captcha_add', '2');
INSERT INTO `nms_setting` VALUES ('26', 'check_add', '2');
INSERT INTO `nms_setting` VALUES ('26', 'group_color', '7');
INSERT INTO `nms_setting` VALUES ('26', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('26', 'group_show', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('26', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('26', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('26', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_description_show', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_keywords_show', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_title_show', '{内容标题}{分隔符}{分类名称}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('26', 'seo_description_list', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_keywords_list', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('26', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('26', 'seo_description_index', '');
INSERT INTO `nms_setting` VALUES ('26', 'seo_keywords_index', '');
INSERT INTO `nms_setting` VALUES ('26', 'php_item_urlid', '0');
INSERT INTO `nms_setting` VALUES ('26', 'htm_item_urlid', '1');
INSERT INTO `nms_setting` VALUES ('26', 'htm_item_prefix', '');
INSERT INTO `nms_setting` VALUES ('26', 'show_html', '0');
INSERT INTO `nms_setting` VALUES ('26', 'php_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('26', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('26', 'htm_list_prefix', '');
INSERT INTO `nms_setting` VALUES ('26', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('26', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('26', 'recycle_ulast_cancel_order', '30');
INSERT INTO `nms_setting` VALUES ('26', 'merchant_order_max', '5');
INSERT INTO `nms_setting` VALUES ('26', 'add_puser_money', '1');
INSERT INTO `nms_setting` VALUES ('26', 'recycle_num', '3');
INSERT INTO `nms_setting` VALUES ('26', 'recycle_time', '60');
INSERT INTO `nms_setting` VALUES ('26', 'recycle_distance', '10000');
INSERT INTO `nms_setting` VALUES ('26', 'recycle_user_cacel', '10');
INSERT INTO `nms_setting` VALUES ('26', 'show_np', '1');
INSERT INTO `nms_setting` VALUES ('26', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('26', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('26', 'max_width', '800');
INSERT INTO `nms_setting` VALUES ('26', 'page_shits', '10');
INSERT INTO `nms_setting` VALUES ('26', 'page_srec', '10');
INSERT INTO `nms_setting` VALUES ('26', 'page_srecimg', '4');
INSERT INTO `nms_setting` VALUES ('26', 'page_srelate', '10');
INSERT INTO `nms_setting` VALUES ('26', 'page_lhits', '10');
INSERT INTO `nms_setting` VALUES ('26', 'page_lrec', '10');
INSERT INTO `nms_setting` VALUES ('27', 'fee_period', '0');
INSERT INTO `nms_setting` VALUES ('27', 'fee_view', '0');
INSERT INTO `nms_setting` VALUES ('27', 'fee_add', '0');
INSERT INTO `nms_setting` VALUES ('27', 'fee_currency', 'money');
INSERT INTO `nms_setting` VALUES ('27', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('27', 'question_add', '2');
INSERT INTO `nms_setting` VALUES ('27', 'captcha_add', '2');
INSERT INTO `nms_setting` VALUES ('27', 'check_add', '2');
INSERT INTO `nms_setting` VALUES ('27', 'group_color', '7');
INSERT INTO `nms_setting` VALUES ('27', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('27', 'group_show', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('27', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('27', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('27', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_description_show', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_keywords_show', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_title_show', '{内容标题}{分隔符}{分类名称}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('27', 'seo_description_list', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_keywords_list', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('27', 'seo_description_index', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_keywords_index', '');
INSERT INTO `nms_setting` VALUES ('27', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('27', 'php_item_urlid', '0');
INSERT INTO `nms_setting` VALUES ('27', 'htm_item_urlid', '1');
INSERT INTO `nms_setting` VALUES ('27', 'htm_item_prefix', '');
INSERT INTO `nms_setting` VALUES ('27', 'php_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('27', 'show_html', '0');
INSERT INTO `nms_setting` VALUES ('27', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('27', 'htm_list_prefix', '');
INSERT INTO `nms_setting` VALUES ('27', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('27', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('27', 'recycle_ulast_cancel_order', '');
INSERT INTO `nms_setting` VALUES ('27', 'merchant_order_max', '');
INSERT INTO `nms_setting` VALUES ('27', 'add_puser_money', '0');
INSERT INTO `nms_setting` VALUES ('27', 'recycle_num', '');
INSERT INTO `nms_setting` VALUES ('27', 'recycle_time', '');
INSERT INTO `nms_setting` VALUES ('27', 'recycle_distance', '');
INSERT INTO `nms_setting` VALUES ('27', 'recycle_user_cacel', '');
INSERT INTO `nms_setting` VALUES ('27', 'show_np', '1');
INSERT INTO `nms_setting` VALUES ('27', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('27', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('27', 'max_width', '800');
INSERT INTO `nms_setting` VALUES ('27', 'page_shits', '10');
INSERT INTO `nms_setting` VALUES ('27', 'page_srec', '10');
INSERT INTO `nms_setting` VALUES ('27', 'page_srecimg', '4');
INSERT INTO `nms_setting` VALUES ('27', 'page_srelate', '10');
INSERT INTO `nms_setting` VALUES ('27', 'page_lhits', '10');
INSERT INTO `nms_setting` VALUES ('27', 'page_lrec', '10');
INSERT INTO `nms_setting` VALUES ('27', 'page_lrecimg', '4');
INSERT INTO `nms_setting` VALUES ('27', 'show_lcat', '1');
INSERT INTO `nms_setting` VALUES ('27', 'page_child', '6');
INSERT INTO `nms_setting` VALUES ('27', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('27', 'page_ihits', '10');
INSERT INTO `nms_setting` VALUES ('27', 'page_irecimg', '6');
INSERT INTO `nms_setting` VALUES ('27', 'page_islide', '3');
INSERT INTO `nms_setting` VALUES ('27', 'page_icat', '6');
INSERT INTO `nms_setting` VALUES ('27', 'show_icat', '1');
INSERT INTO `nms_setting` VALUES ('27', 'swfu', '2');
INSERT INTO `nms_setting` VALUES ('27', 'level', '推荐文章|幻灯图片|推荐图文|头条相关|头条推荐');
INSERT INTO `nms_setting` VALUES ('27', 'fulltext', '1');
INSERT INTO `nms_setting` VALUES ('27', 'split', '0');
INSERT INTO `nms_setting` VALUES ('27', 'clear_link', '0');
INSERT INTO `nms_setting` VALUES ('27', 'keylink', '1');
INSERT INTO `nms_setting` VALUES ('27', 'cat_property', '0');
INSERT INTO `nms_setting` VALUES ('27', 'save_remotepic', '0');
INSERT INTO `nms_setting` VALUES ('27', 'fields', 'itemid,title,thumb,linkurl,style,catid,introduce,addtime,edittime,username,islink,hits');
INSERT INTO `nms_setting` VALUES ('27', 'order', 'addtime desc');
INSERT INTO `nms_setting` VALUES ('27', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('27', 'introduce_length', '120');
INSERT INTO `nms_setting` VALUES ('27', 'thumb_height', '0');
INSERT INTO `nms_setting` VALUES ('27', 'thumb_width', '0');
INSERT INTO `nms_setting` VALUES ('27', 'default_water', '0');
INSERT INTO `nms_setting` VALUES ('27', 'wxpic', '');
INSERT INTO `nms_setting` VALUES ('27', 'template_my', '');
INSERT INTO `nms_setting` VALUES ('27', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('27', 'template_show', '');
INSERT INTO `nms_setting` VALUES ('27', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('27', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('28', 'free_limit_7', '-1');
INSERT INTO `nms_setting` VALUES ('28', 'limit_7', '100');
INSERT INTO `nms_setting` VALUES ('28', 'free_limit_6', '0');
INSERT INTO `nms_setting` VALUES ('28', 'limit_6', '30');
INSERT INTO `nms_setting` VALUES ('28', 'free_limit_5', '0');
INSERT INTO `nms_setting` VALUES ('28', 'limit_5', '3');
INSERT INTO `nms_setting` VALUES ('28', 'free_limit_4', '0');
INSERT INTO `nms_setting` VALUES ('28', 'limit_4', '-1');
INSERT INTO `nms_setting` VALUES ('28', 'free_limit_3', '0');
INSERT INTO `nms_setting` VALUES ('28', 'limit_3', '-1');
INSERT INTO `nms_setting` VALUES ('28', 'free_limit_2', '0');
INSERT INTO `nms_setting` VALUES ('28', 'limit_2', '-1');
INSERT INTO `nms_setting` VALUES ('28', 'free_limit_1', '-1');
INSERT INTO `nms_setting` VALUES ('28', 'limit_1', '0');
INSERT INTO `nms_setting` VALUES ('28', 'credit_color', '100');
INSERT INTO `nms_setting` VALUES ('28', 'credit_del', '5');
INSERT INTO `nms_setting` VALUES ('28', 'credit_add', '2');
INSERT INTO `nms_setting` VALUES ('28', 'pre_view', '200');
INSERT INTO `nms_setting` VALUES ('28', 'fee_award', '100');
INSERT INTO `nms_setting` VALUES ('28', 'fee_back', '0');
INSERT INTO `nms_setting` VALUES ('28', 'fee_period', '0');
INSERT INTO `nms_setting` VALUES ('28', 'fee_view', '0');
INSERT INTO `nms_setting` VALUES ('28', 'fee_add', '0');
INSERT INTO `nms_setting` VALUES ('28', 'fee_currency', 'money');
INSERT INTO `nms_setting` VALUES ('28', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('28', 'question_add', '2');
INSERT INTO `nms_setting` VALUES ('28', 'captcha_add', '2');
INSERT INTO `nms_setting` VALUES ('28', 'check_add', '2');
INSERT INTO `nms_setting` VALUES ('28', 'group_color', '7');
INSERT INTO `nms_setting` VALUES ('28', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('28', 'group_show', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('28', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('28', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('28', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_description_show', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_title_show', '{内容标题}{分隔符}{分类名称}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('28', 'seo_keywords_show', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_description_list', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_keywords_list', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('28', 'seo_description_index', '');
INSERT INTO `nms_setting` VALUES ('28', 'seo_keywords_index', '');
INSERT INTO `nms_setting` VALUES ('28', 'php_item_urlid', '0');
INSERT INTO `nms_setting` VALUES ('28', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('28', 'htm_item_urlid', '1');
INSERT INTO `nms_setting` VALUES ('28', 'htm_item_prefix', '');
INSERT INTO `nms_setting` VALUES ('28', 'show_html', '0');
INSERT INTO `nms_setting` VALUES ('28', 'php_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('28', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('28', 'htm_list_prefix', '');
INSERT INTO `nms_setting` VALUES ('28', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('28', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('28', 'show_np', '1');
INSERT INTO `nms_setting` VALUES ('28', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('28', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('28', 'max_width', '800');
INSERT INTO `nms_setting` VALUES ('28', 'page_shits', '10');
INSERT INTO `nms_setting` VALUES ('28', 'page_srec', '10');
INSERT INTO `nms_setting` VALUES ('28', 'page_srecimg', '4');
INSERT INTO `nms_setting` VALUES ('28', 'page_srelate', '10');
INSERT INTO `nms_setting` VALUES ('28', 'page_lhits', '10');
INSERT INTO `nms_setting` VALUES ('28', 'page_lrec', '10');
INSERT INTO `nms_setting` VALUES ('28', 'page_lrecimg', '4');
INSERT INTO `nms_setting` VALUES ('28', 'show_lcat', '1');
INSERT INTO `nms_setting` VALUES ('28', 'page_child', '6');
INSERT INTO `nms_setting` VALUES ('28', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('28', 'page_ihits', '10');
INSERT INTO `nms_setting` VALUES ('28', 'page_irecimg', '6');
INSERT INTO `nms_setting` VALUES ('28', 'show_icat', '1');
INSERT INTO `nms_setting` VALUES ('28', 'page_icat', '6');
INSERT INTO `nms_setting` VALUES ('28', 'page_islide', '3');
INSERT INTO `nms_setting` VALUES ('28', 'swfu', '2');
INSERT INTO `nms_setting` VALUES ('28', 'fulltext', '1');
INSERT INTO `nms_setting` VALUES ('28', 'level', '推荐文章|幻灯图片|推荐图文|头条相关|头条推荐');
INSERT INTO `nms_setting` VALUES ('28', 'split', '0');
INSERT INTO `nms_setting` VALUES ('28', 'keylink', '1');
INSERT INTO `nms_setting` VALUES ('28', 'clear_link', '0');
INSERT INTO `nms_setting` VALUES ('28', 'save_remotepic', '0');
INSERT INTO `nms_setting` VALUES ('28', 'cat_property', '0');
INSERT INTO `nms_setting` VALUES ('28', 'order', 'addtime desc');
INSERT INTO `nms_setting` VALUES ('28', 'fields', 'itemid,title,thumb,linkurl,style,catid,introduce,addtime,edittime,username,islink,hits');
INSERT INTO `nms_setting` VALUES ('28', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('28', 'introduce_length', '120');
INSERT INTO `nms_setting` VALUES ('28', 'thumb_height', '180');
INSERT INTO `nms_setting` VALUES ('28', 'thumb_width', '240');
INSERT INTO `nms_setting` VALUES ('28', 'template_my', '');
INSERT INTO `nms_setting` VALUES ('28', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('28', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('28', 'template_show', '');
INSERT INTO `nms_setting` VALUES ('28', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('28', 'title_index', '{$seo_modulename}{$seo_delimiter}{$seo_page}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('28', 'title_list', '{$seo_cattitle}{$seo_page}{$seo_modulename}{$seo_delimiter}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('28', 'title_show', '{$seo_showtitle}{$seo_delimiter}{$seo_catname}{$seo_modulename}{$seo_delimiter}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('28', 'title_search', '');
INSERT INTO `nms_setting` VALUES ('28', 'keywords_index', '');
INSERT INTO `nms_setting` VALUES ('28', 'keywords_list', '');
INSERT INTO `nms_setting` VALUES ('28', 'keywords_show', '');
INSERT INTO `nms_setting` VALUES ('28', 'keywords_search', '');
INSERT INTO `nms_setting` VALUES ('28', 'description_index', '');
INSERT INTO `nms_setting` VALUES ('28', 'description_list', '');
INSERT INTO `nms_setting` VALUES ('28', 'description_show', '');
INSERT INTO `nms_setting` VALUES ('28', 'description_search', '');
INSERT INTO `nms_setting` VALUES ('28', 'moduleid', '21');
INSERT INTO `nms_setting` VALUES ('28', 'name', '资讯');
INSERT INTO `nms_setting` VALUES ('28', 'moduledir', 'news');
INSERT INTO `nms_setting` VALUES ('28', 'module', 'article');
INSERT INTO `nms_setting` VALUES ('28', 'ismenu', '1');
INSERT INTO `nms_setting` VALUES ('28', 'domain', '');
INSERT INTO `nms_setting` VALUES ('28', 'linkurl', 'http://demo.destoon.com/v7.0/news/');
INSERT INTO `nms_setting` VALUES ('28', 'mobile', 'http://demo.destoon.com/v7.0/mobile/news/');
INSERT INTO `nms_setting` VALUES ('29', 'free_limit_7', '-1');
INSERT INTO `nms_setting` VALUES ('29', 'limit_7', '100');
INSERT INTO `nms_setting` VALUES ('29', 'free_limit_6', '0');
INSERT INTO `nms_setting` VALUES ('29', 'limit_6', '30');
INSERT INTO `nms_setting` VALUES ('29', 'free_limit_5', '0');
INSERT INTO `nms_setting` VALUES ('29', 'limit_5', '3');
INSERT INTO `nms_setting` VALUES ('29', 'free_limit_4', '0');
INSERT INTO `nms_setting` VALUES ('29', 'limit_4', '-1');
INSERT INTO `nms_setting` VALUES ('29', 'free_limit_3', '0');
INSERT INTO `nms_setting` VALUES ('29', 'limit_3', '-1');
INSERT INTO `nms_setting` VALUES ('29', 'free_limit_2', '0');
INSERT INTO `nms_setting` VALUES ('29', 'limit_2', '-1');
INSERT INTO `nms_setting` VALUES ('29', 'free_limit_1', '-1');
INSERT INTO `nms_setting` VALUES ('29', 'limit_1', '0');
INSERT INTO `nms_setting` VALUES ('29', 'credit_color', '100');
INSERT INTO `nms_setting` VALUES ('29', 'credit_del', '5');
INSERT INTO `nms_setting` VALUES ('29', 'credit_add', '2');
INSERT INTO `nms_setting` VALUES ('29', 'pre_view', '200');
INSERT INTO `nms_setting` VALUES ('29', 'fee_award', '100');
INSERT INTO `nms_setting` VALUES ('29', 'fee_back', '0');
INSERT INTO `nms_setting` VALUES ('29', 'fee_period', '0');
INSERT INTO `nms_setting` VALUES ('29', 'fee_view', '0');
INSERT INTO `nms_setting` VALUES ('29', 'fee_add', '0');
INSERT INTO `nms_setting` VALUES ('29', 'fee_currency', 'money');
INSERT INTO `nms_setting` VALUES ('29', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('29', 'question_add', '2');
INSERT INTO `nms_setting` VALUES ('29', 'captcha_add', '2');
INSERT INTO `nms_setting` VALUES ('29', 'check_add', '2');
INSERT INTO `nms_setting` VALUES ('29', 'group_color', '7');
INSERT INTO `nms_setting` VALUES ('29', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('29', 'group_show', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('29', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('29', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('29', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_description_show', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_title_show', '{内容标题}{分隔符}{分类名称}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('29', 'seo_keywords_show', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_description_list', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_keywords_list', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('29', 'seo_description_index', '');
INSERT INTO `nms_setting` VALUES ('29', 'seo_keywords_index', '');
INSERT INTO `nms_setting` VALUES ('29', 'php_item_urlid', '0');
INSERT INTO `nms_setting` VALUES ('29', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('29', 'htm_item_urlid', '1');
INSERT INTO `nms_setting` VALUES ('29', 'htm_item_prefix', '');
INSERT INTO `nms_setting` VALUES ('29', 'show_html', '0');
INSERT INTO `nms_setting` VALUES ('29', 'php_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('29', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('29', 'htm_list_prefix', '');
INSERT INTO `nms_setting` VALUES ('29', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('29', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('29', 'show_np', '1');
INSERT INTO `nms_setting` VALUES ('29', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('29', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('29', 'max_width', '800');
INSERT INTO `nms_setting` VALUES ('29', 'page_shits', '10');
INSERT INTO `nms_setting` VALUES ('29', 'page_srec', '10');
INSERT INTO `nms_setting` VALUES ('29', 'page_srecimg', '4');
INSERT INTO `nms_setting` VALUES ('29', 'page_srelate', '10');
INSERT INTO `nms_setting` VALUES ('29', 'page_lhits', '10');
INSERT INTO `nms_setting` VALUES ('29', 'page_lrec', '10');
INSERT INTO `nms_setting` VALUES ('29', 'page_lrecimg', '4');
INSERT INTO `nms_setting` VALUES ('29', 'show_lcat', '1');
INSERT INTO `nms_setting` VALUES ('29', 'page_child', '6');
INSERT INTO `nms_setting` VALUES ('29', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('29', 'page_ihits', '10');
INSERT INTO `nms_setting` VALUES ('29', 'page_irecimg', '6');
INSERT INTO `nms_setting` VALUES ('29', 'show_icat', '1');
INSERT INTO `nms_setting` VALUES ('29', 'page_icat', '6');
INSERT INTO `nms_setting` VALUES ('29', 'page_islide', '3');
INSERT INTO `nms_setting` VALUES ('29', 'swfu', '2');
INSERT INTO `nms_setting` VALUES ('29', 'fulltext', '1');
INSERT INTO `nms_setting` VALUES ('29', 'level', '推荐文章|幻灯图片|推荐图文|头条相关|头条推荐');
INSERT INTO `nms_setting` VALUES ('29', 'split', '0');
INSERT INTO `nms_setting` VALUES ('29', 'keylink', '1');
INSERT INTO `nms_setting` VALUES ('29', 'clear_link', '0');
INSERT INTO `nms_setting` VALUES ('29', 'save_remotepic', '0');
INSERT INTO `nms_setting` VALUES ('29', 'cat_property', '0');
INSERT INTO `nms_setting` VALUES ('29', 'order', 'addtime desc');
INSERT INTO `nms_setting` VALUES ('29', 'fields', 'itemid,title,thumb,linkurl,style,catid,introduce,addtime,edittime,username,islink,hits');
INSERT INTO `nms_setting` VALUES ('29', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('29', 'introduce_length', '120');
INSERT INTO `nms_setting` VALUES ('29', 'thumb_height', '180');
INSERT INTO `nms_setting` VALUES ('29', 'thumb_width', '240');
INSERT INTO `nms_setting` VALUES ('29', 'template_my', '');
INSERT INTO `nms_setting` VALUES ('29', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('29', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('29', 'template_show', '');
INSERT INTO `nms_setting` VALUES ('29', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('29', 'title_index', '{$seo_modulename}{$seo_delimiter}{$seo_page}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('29', 'title_list', '{$seo_cattitle}{$seo_page}{$seo_modulename}{$seo_delimiter}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('29', 'title_show', '{$seo_showtitle}{$seo_delimiter}{$seo_catname}{$seo_modulename}{$seo_delimiter}{$seo_sitename}');
INSERT INTO `nms_setting` VALUES ('29', 'title_search', '');
INSERT INTO `nms_setting` VALUES ('29', 'keywords_index', '');
INSERT INTO `nms_setting` VALUES ('29', 'keywords_list', '');
INSERT INTO `nms_setting` VALUES ('29', 'keywords_show', '');
INSERT INTO `nms_setting` VALUES ('29', 'keywords_search', '');
INSERT INTO `nms_setting` VALUES ('29', 'description_index', '');
INSERT INTO `nms_setting` VALUES ('29', 'description_list', '');
INSERT INTO `nms_setting` VALUES ('29', 'description_show', '');
INSERT INTO `nms_setting` VALUES ('29', 'description_search', '');
INSERT INTO `nms_setting` VALUES ('29', 'moduleid', '21');
INSERT INTO `nms_setting` VALUES ('29', 'name', '资讯');
INSERT INTO `nms_setting` VALUES ('29', 'moduledir', 'news');
INSERT INTO `nms_setting` VALUES ('29', 'module', 'article');
INSERT INTO `nms_setting` VALUES ('29', 'ismenu', '1');
INSERT INTO `nms_setting` VALUES ('29', 'domain', '');
INSERT INTO `nms_setting` VALUES ('29', 'linkurl', 'http://demo.destoon.com/v7.0/news/');
INSERT INTO `nms_setting` VALUES ('29', 'mobile', 'http://demo.destoon.com/v7.0/mobile/news/');
INSERT INTO `nms_setting` VALUES ('3', 'announce_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'announce_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'ad_currency', 'money');
INSERT INTO `nms_setting` VALUES ('3', 'ad_buy', '1');
INSERT INTO `nms_setting` VALUES ('3', 'ad_view', '1');
INSERT INTO `nms_setting` VALUES ('3', 'ad_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'ad_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'spread_currency', 'money');
INSERT INTO `nms_setting` VALUES ('3', 'spread_list', '1');
INSERT INTO `nms_setting` VALUES ('3', 'spread_check', '1');
INSERT INTO `nms_setting` VALUES ('3', 'spread_max', '10');
INSERT INTO `nms_setting` VALUES ('3', 'spread_month', '6');
INSERT INTO `nms_setting` VALUES ('3', 'spread_step', '100');
INSERT INTO `nms_setting` VALUES ('3', 'spread_price', '200');
INSERT INTO `nms_setting` VALUES ('3', 'spread_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_adr', '');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_ios', '');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_ajax', '0');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_goto', '0');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_pid', '14');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_sitename', 'nms');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_domain', '');
INSERT INTO `nms_setting` VALUES ('3', 'mobile_enable', '0');
INSERT INTO `nms_setting` VALUES ('3', 'show_url', '1');
INSERT INTO `nms_setting` VALUES ('3', 'list_url', '1');
INSERT INTO `nms_setting` VALUES ('3', 'weixin', '0');
INSERT INTO `nms_setting` VALUES ('3', 'oauth', '0');
INSERT INTO `nms_setting` VALUES ('3', 'sitemaps_module', '4');
INSERT INTO `nms_setting` VALUES ('3', 'sitemaps_update', '60');
INSERT INTO `nms_setting` VALUES ('3', 'sitemaps_items', '10000');
INSERT INTO `nms_setting` VALUES ('3', 'baidunews', '0');
INSERT INTO `nms_setting` VALUES ('3', 'baidunews_email', 'web@destoon.com');
INSERT INTO `nms_setting` VALUES ('3', 'baidunews_update', '60');
INSERT INTO `nms_setting` VALUES ('3', 'baidunews_items', '90');
INSERT INTO `nms_setting` VALUES ('25', 'limit_1', '0');
INSERT INTO `nms_setting` VALUES ('25', 'free_limit_1', '-1');
INSERT INTO `nms_setting` VALUES ('25', 'limit_2', '-1');
INSERT INTO `nms_setting` VALUES ('25', 'free_limit_2', '0');
INSERT INTO `nms_setting` VALUES ('25', 'limit_3', '-1');
INSERT INTO `nms_setting` VALUES ('25', 'free_limit_3', '0');
INSERT INTO `nms_setting` VALUES ('25', 'limit_4', '-1');
INSERT INTO `nms_setting` VALUES ('25', 'free_limit_4', '0');
INSERT INTO `nms_setting` VALUES ('25', 'limit_5', '3');
INSERT INTO `nms_setting` VALUES ('25', 'free_limit_5', '0');
INSERT INTO `nms_setting` VALUES ('25', 'limit_6', '30');
INSERT INTO `nms_setting` VALUES ('25', 'free_limit_6', '0');
INSERT INTO `nms_setting` VALUES ('25', 'limit_7', '100');
INSERT INTO `nms_setting` VALUES ('25', 'free_limit_7', '-1');
INSERT INTO `nms_setting` VALUES ('26', 'page_lrecimg', '4');
INSERT INTO `nms_setting` VALUES ('26', 'show_lcat', '1');
INSERT INTO `nms_setting` VALUES ('26', 'page_child', '6');
INSERT INTO `nms_setting` VALUES ('26', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('26', 'page_ihits', '10');
INSERT INTO `nms_setting` VALUES ('26', 'page_irecimg', '6');
INSERT INTO `nms_setting` VALUES ('26', 'show_icat', '1');
INSERT INTO `nms_setting` VALUES ('26', 'page_icat', '6');
INSERT INTO `nms_setting` VALUES ('26', 'page_islide', '3');
INSERT INTO `nms_setting` VALUES ('26', 'swfu', '2');
INSERT INTO `nms_setting` VALUES ('26', 'level', '推荐文章|幻灯图片|推荐图文|头条相关|头条推荐');
INSERT INTO `nms_setting` VALUES ('26', 'fulltext', '1');
INSERT INTO `nms_setting` VALUES ('26', 'split', '0');
INSERT INTO `nms_setting` VALUES ('26', 'keylink', '1');
INSERT INTO `nms_setting` VALUES ('26', 'clear_link', '0');
INSERT INTO `nms_setting` VALUES ('26', 'save_remotepic', '0');
INSERT INTO `nms_setting` VALUES ('26', 'fields', 'itemid,title,thumb,linkurl,style,catid,introduce,addtime,edittime,username,islink,hits');
INSERT INTO `nms_setting` VALUES ('1', 'water_transition', '60');
INSERT INTO `nms_setting` VALUES ('1', 'water_jpeg_quality', '90');
INSERT INTO `nms_setting` VALUES ('1', 'water_text', 'www.destoon.com');
INSERT INTO `nms_setting` VALUES ('1', 'water_font', 'simhei.ttf');
INSERT INTO `nms_setting` VALUES ('1', 'water_fontsize', '20');
INSERT INTO `nms_setting` VALUES ('1', 'water_fontcolor', '#000000');
INSERT INTO `nms_setting` VALUES ('1', 'mail_type', 'close');
INSERT INTO `nms_setting` VALUES ('1', 'mail_delimiter', '1');
INSERT INTO `nms_setting` VALUES ('1', 'smtp_host', '');
INSERT INTO `nms_setting` VALUES ('1', 'smtp_port', '25');
INSERT INTO `nms_setting` VALUES ('1', 'smtp_auth', '1');
INSERT INTO `nms_setting` VALUES ('1', 'smtp_user', '');
INSERT INTO `nms_setting` VALUES ('1', 'smtp_pass', '');
INSERT INTO `nms_setting` VALUES ('1', 'mail_sign', '');
INSERT INTO `nms_setting` VALUES ('1', 'mail_sender', '');
INSERT INTO `nms_setting` VALUES ('1', 'mail_name', '');
INSERT INTO `nms_setting` VALUES ('1', 'mail_log', '1');
INSERT INTO `nms_setting` VALUES ('1', 'message_email', '0');
INSERT INTO `nms_setting` VALUES ('1', 'message_group', '6,7');
INSERT INTO `nms_setting` VALUES ('1', 'message_time', '60');
INSERT INTO `nms_setting` VALUES ('1', 'message_type', '1,2,3');
INSERT INTO `nms_setting` VALUES ('1', 'message_weixin', '1');
INSERT INTO `nms_setting` VALUES ('1', 'page_mall', '10');
INSERT INTO `nms_setting` VALUES ('1', 'page_sell', '10');
INSERT INTO `nms_setting` VALUES ('1', 'page_info', '10');
INSERT INTO `nms_setting` VALUES ('1', 'page_group', '10');
INSERT INTO `nms_setting` VALUES ('1', 'page_newst', '1');
INSERT INTO `nms_setting` VALUES ('1', 'page_newsh', '1');
INSERT INTO `nms_setting` VALUES ('1', 'page_news', '6');
INSERT INTO `nms_setting` VALUES ('1', 'page_special', '1');
INSERT INTO `nms_setting` VALUES ('1', 'page_video', '3');
INSERT INTO `nms_setting` VALUES ('1', 'page_photo', '3');
INSERT INTO `nms_setting` VALUES ('1', 'page_brand', '16');
INSERT INTO `nms_setting` VALUES ('1', 'page_exhibit', '6');
INSERT INTO `nms_setting` VALUES ('1', 'page_job', '5');
INSERT INTO `nms_setting` VALUES ('1', 'page_know', '6');
INSERT INTO `nms_setting` VALUES ('1', 'page_down', '3');
INSERT INTO `nms_setting` VALUES ('1', 'page_club', '8');
INSERT INTO `nms_setting` VALUES ('1', 'page_logo', '18');
INSERT INTO `nms_setting` VALUES ('1', 'page_text', '18');
INSERT INTO `nms_setting` VALUES ('1', 'sms', '0');
INSERT INTO `nms_setting` VALUES ('1', 'sms_fee', '0.1');
INSERT INTO `nms_setting` VALUES ('1', 'sms_max', '5');
INSERT INTO `nms_setting` VALUES ('1', 'sms_len', '70');
INSERT INTO `nms_setting` VALUES ('1', 'sms_ok', '成功');
INSERT INTO `nms_setting` VALUES ('1', 'sms_sign', '');
INSERT INTO `nms_setting` VALUES ('1', 'cloud_express', '0');
INSERT INTO `nms_setting` VALUES ('1', 'trade_pw', '');
INSERT INTO `nms_setting` VALUES ('1', 'admin_week', '');
INSERT INTO `nms_setting` VALUES ('1', 'check_week', '');
INSERT INTO `nms_setting` VALUES ('26', 'cat_property', '0');
INSERT INTO `nms_setting` VALUES ('26', 'order', 'addtime desc');
INSERT INTO `nms_setting` VALUES ('26', 'editor', 'Default');
INSERT INTO `nms_setting` VALUES ('26', 'introduce_length', '120');
INSERT INTO `nms_setting` VALUES ('26', 'thumb_height', '180');
INSERT INTO `nms_setting` VALUES ('26', 'thumb_width', '240');
INSERT INTO `nms_setting` VALUES ('26', 'default_water', '0');
INSERT INTO `nms_setting` VALUES ('26', 'wxpic', '');
INSERT INTO `nms_setting` VALUES ('26', 'template_my', '');
INSERT INTO `nms_setting` VALUES ('26', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('26', 'template_show', '');
INSERT INTO `nms_setting` VALUES ('26', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('26', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('27', 'fee_award', '100');
INSERT INTO `nms_setting` VALUES ('27', 'pre_view', '200');
INSERT INTO `nms_setting` VALUES ('27', 'credit_add', '2');
INSERT INTO `nms_setting` VALUES ('27', 'credit_del', '5');
INSERT INTO `nms_setting` VALUES ('27', 'credit_color', '100');
INSERT INTO `nms_setting` VALUES ('27', 'limit_1', '0');
INSERT INTO `nms_setting` VALUES ('27', 'free_limit_1', '-1');
INSERT INTO `nms_setting` VALUES ('27', 'limit_2', '-1');
INSERT INTO `nms_setting` VALUES ('27', 'free_limit_2', '0');
INSERT INTO `nms_setting` VALUES ('27', 'limit_3', '-1');
INSERT INTO `nms_setting` VALUES ('27', 'free_limit_3', '0');
INSERT INTO `nms_setting` VALUES ('27', 'limit_4', '-1');
INSERT INTO `nms_setting` VALUES ('27', 'free_limit_4', '0');
INSERT INTO `nms_setting` VALUES ('27', 'limit_5', '3');
INSERT INTO `nms_setting` VALUES ('27', 'free_limit_5', '0');
INSERT INTO `nms_setting` VALUES ('27', 'limit_6', '30');
INSERT INTO `nms_setting` VALUES ('27', 'free_limit_6', '0');
INSERT INTO `nms_setting` VALUES ('27', 'limit_7', '100');
INSERT INTO `nms_setting` VALUES ('27', 'free_limit_7', '-1');
INSERT INTO `nms_setting` VALUES ('30', 'fee_period', '0');
INSERT INTO `nms_setting` VALUES ('30', 'fee_view', '0');
INSERT INTO `nms_setting` VALUES ('30', 'fee_add', '0');
INSERT INTO `nms_setting` VALUES ('30', 'fee_currency', 'money');
INSERT INTO `nms_setting` VALUES ('30', 'fee_mode', '0');
INSERT INTO `nms_setting` VALUES ('30', 'question_add', '2');
INSERT INTO `nms_setting` VALUES ('30', 'captcha_add', '2');
INSERT INTO `nms_setting` VALUES ('30', 'check_add', '2');
INSERT INTO `nms_setting` VALUES ('30', 'group_color', '7');
INSERT INTO `nms_setting` VALUES ('30', 'group_search', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('30', 'group_show', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('30', 'group_list', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('30', 'group_index', '3,5,6,7');
INSERT INTO `nms_setting` VALUES ('30', 'seo_description_search', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_keywords_search', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_title_search', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_description_show', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_keywords_show', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_title_show', '{内容标题}{分隔符}{分类名称}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('30', 'seo_description_list', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_keywords_list', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_title_list', '{分类SEO标题}{页码}{模块名称}{分隔符}{网站名称}');
INSERT INTO `nms_setting` VALUES ('30', 'seo_description_index', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_keywords_index', '');
INSERT INTO `nms_setting` VALUES ('30', 'seo_title_index', '{模块名称}{分隔符}{页码}{网站名称}');
INSERT INTO `nms_setting` VALUES ('30', 'php_item_urlid', '0');
INSERT INTO `nms_setting` VALUES ('30', 'htm_item_urlid', '1');
INSERT INTO `nms_setting` VALUES ('30', 'htm_item_prefix', '');
INSERT INTO `nms_setting` VALUES ('30', 'php_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('30', 'show_html', '0');
INSERT INTO `nms_setting` VALUES ('30', 'htm_list_urlid', '0');
INSERT INTO `nms_setting` VALUES ('30', 'htm_list_prefix', '');
INSERT INTO `nms_setting` VALUES ('30', 'list_html', '0');
INSERT INTO `nms_setting` VALUES ('30', 'index_html', '0');
INSERT INTO `nms_setting` VALUES ('30', 'recycle_ulast_cancel_order', '');
INSERT INTO `nms_setting` VALUES ('30', 'merchant_order_max', '');
INSERT INTO `nms_setting` VALUES ('30', 'add_puser_money', '0');
INSERT INTO `nms_setting` VALUES ('30', 'recycle_num', '');
INSERT INTO `nms_setting` VALUES ('30', 'recycle_time', '');
INSERT INTO `nms_setting` VALUES ('30', 'recycle_distance', '');
INSERT INTO `nms_setting` VALUES ('30', 'recycle_user_cacel', '');
INSERT INTO `nms_setting` VALUES ('30', 'show_np', '1');
INSERT INTO `nms_setting` VALUES ('30', 'page_comment', '0');
INSERT INTO `nms_setting` VALUES ('30', 'hits', '1');
INSERT INTO `nms_setting` VALUES ('30', 'max_width', '800');
INSERT INTO `nms_setting` VALUES ('30', 'page_shits', '10');
INSERT INTO `nms_setting` VALUES ('30', 'page_srec', '10');
INSERT INTO `nms_setting` VALUES ('30', 'page_srecimg', '4');
INSERT INTO `nms_setting` VALUES ('30', 'page_srelate', '10');
INSERT INTO `nms_setting` VALUES ('30', 'page_lhits', '10');
INSERT INTO `nms_setting` VALUES ('30', 'page_lrec', '10');
INSERT INTO `nms_setting` VALUES ('30', 'page_lrecimg', '4');
INSERT INTO `nms_setting` VALUES ('30', 'show_lcat', '1');
INSERT INTO `nms_setting` VALUES ('30', 'page_child', '6');
INSERT INTO `nms_setting` VALUES ('30', 'pagesize', '20');
INSERT INTO `nms_setting` VALUES ('30', 'page_ihits', '10');
INSERT INTO `nms_setting` VALUES ('30', 'page_irecimg', '6');
INSERT INTO `nms_setting` VALUES ('30', 'page_islide', '3');
INSERT INTO `nms_setting` VALUES ('30', 'page_icat', '6');
INSERT INTO `nms_setting` VALUES ('30', 'show_icat', '1');
INSERT INTO `nms_setting` VALUES ('30', 'swfu', '2');
INSERT INTO `nms_setting` VALUES ('30', 'level', '推荐文章|幻灯图片|推荐图文|头条相关|头条推荐');
INSERT INTO `nms_setting` VALUES ('30', 'fulltext', '1');
INSERT INTO `nms_setting` VALUES ('30', 'split', '0');
INSERT INTO `nms_setting` VALUES ('30', 'clear_link', '0');
INSERT INTO `nms_setting` VALUES ('30', 'keylink', '1');
INSERT INTO `nms_setting` VALUES ('30', 'cat_property', '0');
INSERT INTO `nms_setting` VALUES ('30', 'save_remotepic', '0');
INSERT INTO `nms_setting` VALUES ('30', 'fields', 'itemid,title,thumb,linkurl,style,catid,introduce,addtime,edittime,username,islink,hits');
INSERT INTO `nms_setting` VALUES ('30', 'order', 'addtime desc');
INSERT INTO `nms_setting` VALUES ('30', 'editor', 'Destoon');
INSERT INTO `nms_setting` VALUES ('30', 'introduce_length', '120');
INSERT INTO `nms_setting` VALUES ('30', 'thumb_height', '0');
INSERT INTO `nms_setting` VALUES ('30', 'thumb_width', '0');
INSERT INTO `nms_setting` VALUES ('30', 'default_water', '0');
INSERT INTO `nms_setting` VALUES ('30', 'wxpic', '');
INSERT INTO `nms_setting` VALUES ('30', 'template_my', '');
INSERT INTO `nms_setting` VALUES ('30', 'template_search', '');
INSERT INTO `nms_setting` VALUES ('30', 'template_show', '');
INSERT INTO `nms_setting` VALUES ('30', 'template_list', '');
INSERT INTO `nms_setting` VALUES ('30', 'template_index', '');
INSERT INTO `nms_setting` VALUES ('30', 'fee_back', '0');
INSERT INTO `nms_setting` VALUES ('30', 'fee_award', '100');
INSERT INTO `nms_setting` VALUES ('30', 'pre_view', '200');
INSERT INTO `nms_setting` VALUES ('30', 'credit_add', '2');
INSERT INTO `nms_setting` VALUES ('30', 'credit_del', '5');
INSERT INTO `nms_setting` VALUES ('30', 'credit_color', '100');
INSERT INTO `nms_setting` VALUES ('30', 'limit_1', '0');
INSERT INTO `nms_setting` VALUES ('30', 'free_limit_1', '-1');
INSERT INTO `nms_setting` VALUES ('30', 'limit_2', '-1');
INSERT INTO `nms_setting` VALUES ('30', 'free_limit_2', '0');
INSERT INTO `nms_setting` VALUES ('30', 'limit_3', '-1');
INSERT INTO `nms_setting` VALUES ('30', 'free_limit_3', '0');
INSERT INTO `nms_setting` VALUES ('30', 'limit_4', '-1');
INSERT INTO `nms_setting` VALUES ('30', 'free_limit_4', '0');
INSERT INTO `nms_setting` VALUES ('30', 'limit_5', '3');
INSERT INTO `nms_setting` VALUES ('30', 'free_limit_5', '0');
INSERT INTO `nms_setting` VALUES ('30', 'limit_6', '30');
INSERT INTO `nms_setting` VALUES ('30', 'free_limit_6', '0');
INSERT INTO `nms_setting` VALUES ('30', 'limit_7', '100');
INSERT INTO `nms_setting` VALUES ('30', 'free_limit_7', '-1');

-- ----------------------------
-- Table structure for nms_sms
-- ----------------------------
DROP TABLE IF EXISTS `nms_sms`;
CREATE TABLE `nms_sms` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(30) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `word` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL,
  `code` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='短信记录';

-- ----------------------------
-- Records of nms_sms
-- ----------------------------

-- ----------------------------
-- Table structure for nms_sphinx
-- ----------------------------
DROP TABLE IF EXISTS `nms_sphinx`;
CREATE TABLE `nms_sphinx` (
  `moduleid` int(10) unsigned NOT NULL DEFAULT '0',
  `maxid` bigint(20) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `moduleid` (`moduleid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Sphinx';

-- ----------------------------
-- Records of nms_sphinx
-- ----------------------------

-- ----------------------------
-- Table structure for nms_spread
-- ----------------------------
DROP TABLE IF EXISTS `nms_spread`;
CREATE TABLE `nms_spread` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `tid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `word` varchar(50) NOT NULL DEFAULT '',
  `price` float NOT NULL DEFAULT '0',
  `currency` varchar(30) NOT NULL DEFAULT '',
  `company` varchar(100) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='排名推广';

-- ----------------------------
-- Records of nms_spread
-- ----------------------------

-- ----------------------------
-- Table structure for nms_spread_price
-- ----------------------------
DROP TABLE IF EXISTS `nms_spread_price`;
CREATE TABLE `nms_spread_price` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `word` varchar(50) NOT NULL DEFAULT '',
  `price` float NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='排名起价';

-- ----------------------------
-- Records of nms_spread_price
-- ----------------------------

-- ----------------------------
-- Table structure for nms_style
-- ----------------------------
DROP TABLE IF EXISTS `nms_style`;
CREATE TABLE `nms_style` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `skin` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(30) NOT NULL DEFAULT '',
  `groupid` varchar(30) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `currency` varchar(20) NOT NULL DEFAULT '',
  `money` float NOT NULL DEFAULT '0',
  `credit` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='公司主页模板';

-- ----------------------------
-- Records of nms_style
-- ----------------------------
INSERT INTO `nms_style` VALUES ('1', '0', '默认模板', 'default', 'homepage', 'DESTOON.COM', ',6,7,', '0', 'money', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '0');
INSERT INTO `nms_style` VALUES ('2', '0', '深蓝模板', 'blue', 'homepage', 'DESTOON.COM', ',6,7,', '0', 'money', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '0');
INSERT INTO `nms_style` VALUES ('3', '0', '绿色模板', 'green', 'homepage', 'DESTOON.COM', ',6,7,', '0', 'money', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '0');
INSERT INTO `nms_style` VALUES ('4', '0', '紫色模板', 'purple', 'homepage', 'DESTOON.COM', ',6,7,', '0', 'money', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '0');
INSERT INTO `nms_style` VALUES ('5', '0', '橙色模板', 'orange', 'homepage', 'DESTOON.COM', ',6,7,', '0', 'money', '0', '0', '0', '1588062475', 'fulltimelink', '1588062475', '0');

-- ----------------------------
-- Table structure for nms_type
-- ----------------------------
DROP TABLE IF EXISTS `nms_type`;
CREATE TABLE `nms_type` (
  `typeid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) NOT NULL DEFAULT '0',
  `typename` varchar(255) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `item` varchar(20) NOT NULL DEFAULT '',
  `cache` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`typeid`),
  KEY `listorder` (`listorder`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='分类';

-- ----------------------------
-- Records of nms_type
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upgrade
-- ----------------------------
DROP TABLE IF EXISTS `nms_upgrade`;
CREATE TABLE `nms_upgrade` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `gid` smallint(4) unsigned NOT NULL DEFAULT '0',
  `groupid` smallint(4) unsigned NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `message` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `company` varchar(100) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `reason` text NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='会员升级';

-- ----------------------------
-- Records of nms_upgrade
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_0
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_0`;
CREATE TABLE `nms_upload_0` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录0';

-- ----------------------------
-- Records of nms_upload_0
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_1
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_1`;
CREATE TABLE `nms_upload_1` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COMMENT='上传记录1';

-- ----------------------------
-- Records of nms_upload_1
-- ----------------------------
INSERT INTO `nms_upload_1` VALUES ('2', 'cd7080bca04b0fc2917a2280fa5c6e91', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202004/29/092226981.jpg', '215009', 'jpg', 'editor', '800', '740', '1588123346', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('3', '1df77fe67257f6057706a00105e060d9', 'setting', '1', '1', 'https://nms.general.dianzhenkeji.com/file/upload/202004/30/104101601.png', '3762', 'png', 'thumb', '180', '180', '1588214461', 'fulltimelink', '10.1.3.31');
INSERT INTO `nms_upload_1` VALUES ('4', '006bde6a98590880d9549ef7ce011229', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/124132181.jpg', '334438', 'jpg', 'editor', '800', '450', '1589517692', 'fulltimelink', '10.1.3.31');
INSERT INTO `nms_upload_1` VALUES ('5', '5d3316c2f5fd5e1048078fceacd8e857', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/124132791.jpg', '702569', 'jpg', 'editor', '800', '450', '1589517692', 'fulltimelink', '10.1.3.31');
INSERT INTO `nms_upload_1` VALUES ('6', '7474479a6c6b960e0369b36b33f38337', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/153621461.jpg', '898191', 'jpg', 'editor', '800', '450', '1589528181', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('7', '23b0fcd44c051adff6b5cabda34592a6', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/153644141.jpg', '1004296', 'jpg', 'editor', '800', '450', '1589528204', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('8', 'd33d633c66f0e7ac7aae79a4dc202023', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/153745391.jpg', '898191', 'jpg', 'editor', '800', '450', '1589528265', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('9', '73f20157d200ad152a24a1c48bf18381', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/153856991.jpg', '898191', 'jpg', 'editor', '800', '450', '1589528336', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('10', '5e31d224042a2bf78cb671b8eddb3547', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/153948421.jpg', '898191', 'jpg', 'editor', '800', '450', '1589528388', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('11', '0462cd7405481e36384be5f85c12a6a1', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/154013651.jpg', '898191', 'jpg', 'editor', '800', '450', '1589528413', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('12', 'ec8b277fca6cd399835354425d8d21eb', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/154053261.jpg', '334438', 'jpg', 'editor', '800', '450', '1589528453', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('13', '7c89e7f30dcbffdfb99633f2dd34df76', '', '23', '0', 'https://nms.general.dianzhenkeji.com/file/upload/202005/15/154053531.jpg', '324465', 'jpg', 'editor', '800', '450', '1589528453', 'fulltimelink', '10.1.3.30');
INSERT INTO `nms_upload_1` VALUES ('31', '71b3ec78da61443d0ae0dd16db9af9d3', '', '27', '0', 'https://cms.hnlvyi.cn/file/upload/202007/29/154815791.png', '254821', 'png', 'thumb', '1035', '390', '1596008895', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('16', 'a6a717dd5e322f3135cd0da168a147ad', '', '27', '0', 'https://cms.hnlvyi.cn/file/upload/202005/27/171940691.png', '58113', 'png', 'editor', '427', '158', '1590571180', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('19', '3205e520138065c64552a3ac5e320630', '', '27', '0', 'https://cms.hnlvyi.cn/file/upload/202007/28/090553251.png', '43937', 'png', 'thumb', '240', '180', '1595898353', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('20', '63f2f1c5998746ccc886956cb9a6de39', '', '27', '0', 'https://cms.hnlvyi.cn/file/upload/202007/28/090628251.png', '43937', 'png', 'thumb', '240', '180', '1595898388', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('30', 'd99c60afd04f6384f8bed564cf781fe2', '', '27', '0', 'https://cms.hnlvyi.cn/file/upload/202007/29/154756671.png', '254821', 'png', 'thumb', '1035', '390', '1596008876', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('32', 'fa795703743af136a00b52ea34aa52bd', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095007371.png', '14683', 'png', 'thumb', '300', '300', '1596333007', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('33', 'd115e94f5bdb6c266d3f8d007f131283', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095131591.png', '12529', 'png', 'thumb', '176', '176', '1596333091', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('34', 'e6fc10c56bbfb8b3faa1f4c57f4e6827', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095220641.png', '12267', 'png', 'thumb', '176', '176', '1596333140', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('35', 'cdf049716d6f671b300e387d4d8ac82d', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095247671.png', '11608', 'png', 'thumb', '176', '176', '1596333167', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('36', '10e402d507ef8f7e30629f20f8fc5ce4', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/095302361.png', '11287', 'png', 'thumb', '176', '176', '1596333182', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('37', '6ac75d705526b33ca7efc6863c524078', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100248551.png', '5159', 'png', 'thumb', '136', '136', '1596333768', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('38', '5c32859d22a24c7f1c07a0432d6bcfe8', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100307261.png', '4346', 'png', 'thumb', '136', '136', '1596333787', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('39', '656351c5d0b9c381e1f1314d12426eea', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100322581.png', '9343', 'png', 'thumb', '136', '136', '1596333802', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('104', '6ce10a635ce4d100b758089e019fd6cf', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/152701411.png', '2787', 'png', 'thumb', '136', '136', '1596353221', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('41', '6fbe66b62957a1f4e231387d250ac651', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100351251.png', '3507', 'png', 'thumb', '136', '136', '1596333831', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('42', '8e33f3c2ffc71e932f2f2248a9d0fef1', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100405931.png', '9132', 'png', 'thumb', '136', '136', '1596333845', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('43', 'fec83bbb895fa166758f4de588d6d88f', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100422221.png', '4355', 'png', 'thumb', '136', '136', '1596333862', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('44', '7aae693772b1cdf216eb6d6f2ca30d45', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/100438331.png', '7456', 'png', 'thumb', '136', '136', '1596333878', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('45', '06b31492404662a24c3e64bbbd03e7b2', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102015931.png', '3967', 'png', 'thumb', '136', '136', '1596334815', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('46', '84e46b67efb7113379647e92bc638abc', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102031691.png', '3562', 'png', 'thumb', '136', '136', '1596334831', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('47', '634b16c643741480d9cc6908fd16962a', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102047901.png', '8331', 'png', 'thumb', '136', '136', '1596334847', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('48', 'bc72b2a7c8027f1cdc31f0d7f3ed16ac', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102102151.png', '3730', 'png', 'thumb', '136', '136', '1596334862', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('49', '7a01480bd56d3ce79fb0bd1a7861619f', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102144911.png', '7626', 'png', 'thumb', '136', '136', '1596334904', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('50', '41646e6948ef6f21c18ff4158bda84b2', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102202981.png', '6520', 'png', 'thumb', '136', '136', '1596334922', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('51', '42b19834be6d7e5e93c4f87120283d7b', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102222461.png', '6193', 'png', 'thumb', '136', '136', '1596334942', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('52', '03fbf5fa9fbcabb6fa1a8cab011a2631', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102236501.png', '5600', 'png', 'thumb', '136', '136', '1596334956', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('53', '873e62020428474e777418b8eb62a4ad', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102334671.png', '10968', 'png', 'thumb', '136', '136', '1596335014', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('54', 'b89c39742edad57f93dfb2b21f1cd5b0', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102401831.png', '7340', 'png', 'thumb', '136', '136', '1596335041', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('55', '223d546953d28042e439982daf812e23', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102452271.png', '4122', 'png', 'thumb', '136', '136', '1596335092', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('56', '6b256bd44cba593df3bed146bb5d3b19', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102509941.png', '7440', 'png', 'thumb', '136', '136', '1596335109', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('57', 'f6583ddefe7eb07a92d7753682496c11', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102539821.png', '5285', 'png', 'thumb', '136', '136', '1596335139', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('58', '061bf99446169fae3cd23c20ad0b2b8f', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102719961.png', '11054', 'png', 'thumb', '158', '156', '1596335239', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('59', '0b592d563d29051a449af19fd2bbffa1', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102735601.png', '6765', 'png', 'thumb', '136', '136', '1596335255', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('60', '464afd0868719553678895005682cefe', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102748671.png', '11171', 'png', 'thumb', '136', '136', '1596335268', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('61', '9a1a0baefd0b5c4eb00272392c64697b', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/102801961.png', '10449', 'png', 'thumb', '136', '136', '1596335281', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('62', '6d1b37c5836e6ce86f91238c680c0828', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103015581.png', '4498', 'png', 'thumb', '136', '136', '1596335415', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('63', '15f89de4f1f14b7dc2e903eff8575dfc', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103036601.png', '4901', 'png', 'thumb', '136', '136', '1596335436', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('64', 'bb502e936a8c9327926468d4d9af12d4', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103051841.png', '7933', 'png', 'thumb', '136', '136', '1596335451', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('65', '71da6c0f7390b9689bf3e46284a9db1f', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103104511.png', '9418', 'png', 'thumb', '136', '136', '1596335464', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('66', '44194651420a5ef9560cdb74a67b4916', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103153201.png', '4626', 'png', 'thumb', '136', '136', '1596335513', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('67', '49987de045a7da44b79e974e4e30283b', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103216381.png', '4695', 'png', 'thumb', '136', '136', '1596335536', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('68', 'd6952e65d4e2c77f50aaf1735c06b6eb', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103229811.png', '4121', 'png', 'thumb', '136', '136', '1596335549', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('69', '208960878bd37cc4c9bbcab622506a8c', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103241801.png', '4082', 'png', 'thumb', '136', '136', '1596335561', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('70', '195a16d647503649c0415eb3eb423324', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103304241.png', '15767', 'png', 'thumb', '136', '136', '1596335584', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('71', '22a57a97c091a477f299a6d51d828845', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103318521.png', '7941', 'png', 'thumb', '136', '136', '1596335598', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('72', '8f3c94fdea68f2d13a9c77b95c100a62', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103335571.png', '8821', 'png', 'thumb', '136', '136', '1596335615', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('73', '8b6fa499ffb6e2f51ba5b3f253df7530', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103346581.png', '7867', 'png', 'thumb', '136', '136', '1596335626', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('74', '35ea328b62facc633c92fc5098280b30', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103452191.png', '5924', 'png', 'thumb', '136', '136', '1596335692', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('75', 'c010c0d1fa889853f9558a884e1ca728', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103507811.png', '4355', 'png', 'thumb', '136', '136', '1596335707', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('76', 'bdebe3bf7868eb95ac0d95451e75f9b1', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103522171.png', '7323', 'png', 'thumb', '136', '136', '1596335722', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('77', 'e927533e74718ac94e19b2a7d1fbdd60', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103539821.png', '4218', 'png', 'thumb', '136', '136', '1596335739', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('78', 'f06d6ae912a02f86068e111d85c74f8d', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103607421.png', '9690', 'png', 'thumb', '156', '156', '1596335767', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('79', '9ecebd55f11e388c008dd8ab9337c97d', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103626481.png', '11054', 'png', 'thumb', '158', '156', '1596335786', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('80', '06f835745725a1e2ee5905a6d38b835f', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103638461.png', '13258', 'png', 'thumb', '158', '156', '1596335798', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('81', '673d89258651d751788b9a81ce947d13', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103652561.png', '13498', 'png', 'thumb', '158', '156', '1596335812', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('82', '3aa41e32201f18388f4371620353979b', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103813371.png', '10088', 'png', 'thumb', '144', '144', '1596335893', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('83', '6ba5405bd1b23942d8aa3d717d5c9232', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103827351.png', '15661', 'png', 'thumb', '144', '144', '1596335907', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('84', 'e64c58bcd4093a5fc073f5f69b583c03', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103846791.png', '16083', 'png', 'thumb', '144', '144', '1596335926', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('85', '4fe6f31369ce045683a6712862cf21ec', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103901401.png', '12122', 'png', 'thumb', '144', '144', '1596335941', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('86', 'dae7b6abc27cf92c49e2bf39a9bd7bf4', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/103915851.png', '10660', 'png', 'thumb', '144', '144', '1596335955', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('87', '34c38bee8f71dc474c1bb4d2ce521267', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104127291.png', '8000', 'png', 'thumb', '136', '136', '1596336087', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('88', 'dfbe24f60d1a84271771cde82f2938d2', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104145711.png', '6022', 'png', 'thumb', '136', '136', '1596336105', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('89', 'c0abbcb33e83478478d9cabd99e4c5d8', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104202791.png', '5700', 'png', 'thumb', '136', '136', '1596336122', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('90', '2b7680d5418ea455f37e2144138042fb', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104218111.png', '6901', 'png', 'thumb', '136', '136', '1596336138', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('91', '6fe5aed6da38a4c643037fb4fc0992c3', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104235301.png', '5537', 'png', 'thumb', '136', '136', '1596336155', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('92', 'f77aa6f58c45c88f3fbd1e8612ad6bed', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104248441.png', '9176', 'png', 'thumb', '136', '136', '1596336168', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('93', 'a55b959bc05e7c8ece117d9b7e25d106', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104303451.png', '6269', 'png', 'thumb', '136', '136', '1596336183', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('94', '314287fe89b44555382e00fcb9e17523', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104318441.png', '7871', 'png', 'thumb', '136', '136', '1596336198', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('95', '85a6d47d88e526f2da8b9a782844eae1', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104334751.png', '6022', 'png', 'thumb', '136', '136', '1596336214', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('96', '8be8447d9349dfa0193c318195297051', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104501991.png', '7639', 'png', 'thumb', '136', '136', '1596336301', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('97', '9e954cd232e75ffcd8e1398f27bf3270', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104515231.png', '7075', 'png', 'thumb', '136', '136', '1596336315', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('98', 'ace0235c0bcba9bbd75435996573758f', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104531401.png', '9541', 'png', 'thumb', '136', '136', '1596336331', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('99', '7318273617e0ff2ee672b370a8b52c10', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104551751.png', '15372', 'png', 'thumb', '300', '300', '1596336351', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('100', '11546dc81011b1f157f109eb8383712e', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104607751.png', '13069', 'png', 'thumb', '136', '136', '1596336367', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('101', 'a571b7df63dd29a79fe1935a5e9cd3e6', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104622421.png', '8868', 'png', 'thumb', '136', '136', '1596336382', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('102', '820c18e7e17447ea94a24ef42e01831b', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104636181.png', '7284', 'png', 'thumb', '136', '136', '1596336396', 'fulltimelink', '61.163.87.166');
INSERT INTO `nms_upload_1` VALUES ('103', 'da44df205ca8b2a30aabe3fa421b0d77', '', '30', '0', 'https://cms.hnlvyi.cn/file/upload/202008/02/104649231.png', '7555', 'png', 'thumb', '136', '136', '1596336409', 'fulltimelink', '61.163.87.166');

-- ----------------------------
-- Table structure for nms_upload_2
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_2`;
CREATE TABLE `nms_upload_2` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录2';

-- ----------------------------
-- Records of nms_upload_2
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_3
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_3`;
CREATE TABLE `nms_upload_3` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录3';

-- ----------------------------
-- Records of nms_upload_3
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_4
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_4`;
CREATE TABLE `nms_upload_4` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录4';

-- ----------------------------
-- Records of nms_upload_4
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_5
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_5`;
CREATE TABLE `nms_upload_5` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='上传记录5';

-- ----------------------------
-- Records of nms_upload_5
-- ----------------------------
INSERT INTO `nms_upload_5` VALUES ('1', '59290b71a59589863a40ed3e42acc4d7', '', '25', '0', 'https://cms.hnlvyi.cn/file/upload/202007/19/161229545.png', '1942', 'png', 'file', '44', '44', '1595146349', 'recycle', '61.163.87.166');
INSERT INTO `nms_upload_5` VALUES ('2', '37f77377c34ea36193c24fe449d4f38c', '', '25', '0', 'https://cms.hnlvyi.cn/file/upload/202007/19/161236575.png', '1071', 'png', 'file', '32', '36', '1595146356', 'recycle', '61.163.87.166');
INSERT INTO `nms_upload_5` VALUES ('3', '61cbf6f5106dd67f22e19975b13eac50', '', '25', '0', 'https://cms.hnlvyi.cn/file/upload/202007/19/161246825.png', '1842', 'png', 'file', '44', '44', '1595146366', 'recycle', '61.163.87.166');

-- ----------------------------
-- Table structure for nms_upload_6
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_6`;
CREATE TABLE `nms_upload_6` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录6';

-- ----------------------------
-- Records of nms_upload_6
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_7
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_7`;
CREATE TABLE `nms_upload_7` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录7';

-- ----------------------------
-- Records of nms_upload_7
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_8
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_8`;
CREATE TABLE `nms_upload_8` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录8';

-- ----------------------------
-- Records of nms_upload_8
-- ----------------------------

-- ----------------------------
-- Table structure for nms_upload_9
-- ----------------------------
DROP TABLE IF EXISTS `nms_upload_9`;
CREATE TABLE `nms_upload_9` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) NOT NULL DEFAULT '',
  `tb` varchar(30) NOT NULL,
  `moduleid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fileurl` varchar(255) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` varchar(10) NOT NULL DEFAULT '',
  `upfrom` varchar(10) NOT NULL DEFAULT '',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  KEY `item` (`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传记录9';

-- ----------------------------
-- Records of nms_upload_9
-- ----------------------------

-- ----------------------------
-- Table structure for nms_validate
-- ----------------------------
DROP TABLE IF EXISTS `nms_validate`;
CREATE TABLE `nms_validate` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(30) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `thumb1` varchar(255) NOT NULL DEFAULT '',
  `thumb2` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='资料认证';

-- ----------------------------
-- Records of nms_validate
-- ----------------------------

-- ----------------------------
-- Table structure for nms_vote
-- ----------------------------
DROP TABLE IF EXISTS `nms_vote`;
CREATE TABLE `nms_vote` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `groupid` varchar(255) NOT NULL,
  `verify` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `choose` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vote_min` smallint(2) unsigned NOT NULL DEFAULT '0',
  `vote_max` smallint(2) unsigned NOT NULL DEFAULT '0',
  `votes` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `linkto` varchar(255) NOT NULL DEFAULT '',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `template_vote` varchar(30) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '',
  `s1` varchar(255) NOT NULL DEFAULT '',
  `s2` varchar(255) NOT NULL DEFAULT '',
  `s3` varchar(255) NOT NULL DEFAULT '',
  `s4` varchar(255) NOT NULL DEFAULT '',
  `s5` varchar(255) NOT NULL DEFAULT '',
  `s6` varchar(255) NOT NULL DEFAULT '',
  `s7` varchar(255) NOT NULL DEFAULT '',
  `s8` varchar(255) NOT NULL DEFAULT '',
  `s9` varchar(255) NOT NULL DEFAULT '',
  `s10` varchar(255) NOT NULL DEFAULT '',
  `v1` int(10) unsigned NOT NULL DEFAULT '0',
  `v2` int(10) unsigned NOT NULL DEFAULT '0',
  `v3` int(10) unsigned NOT NULL DEFAULT '0',
  `v4` int(10) unsigned NOT NULL DEFAULT '0',
  `v5` int(10) unsigned NOT NULL DEFAULT '0',
  `v6` int(10) unsigned NOT NULL DEFAULT '0',
  `v7` int(10) unsigned NOT NULL DEFAULT '0',
  `v8` int(10) unsigned NOT NULL DEFAULT '0',
  `v9` int(10) unsigned NOT NULL DEFAULT '0',
  `v10` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='投票';

-- ----------------------------
-- Records of nms_vote
-- ----------------------------

-- ----------------------------
-- Table structure for nms_vote_record
-- ----------------------------
DROP TABLE IF EXISTS `nms_vote_record`;
CREATE TABLE `nms_vote_record` (
  `rid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `votetime` int(10) unsigned NOT NULL DEFAULT '0',
  `votes` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`rid`),
  KEY `itemid` (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='投票记录';

-- ----------------------------
-- Records of nms_vote_record
-- ----------------------------

-- ----------------------------
-- Table structure for nms_webpage
-- ----------------------------
DROP TABLE IF EXISTS `nms_webpage`;
CREATE TABLE `nms_webpage` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(30) NOT NULL DEFAULT '',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `seo_title` varchar(255) NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='单网页';

-- ----------------------------
-- Records of nms_webpage
-- ----------------------------
INSERT INTO `nms_webpage` VALUES ('1', '1', '0', '0', '关于我们', '', '关于我们', '', '', '', 'destoon', '1319006891', '5', '0', '0', 'about/index.html', '', '');
INSERT INTO `nms_webpage` VALUES ('2', '1', '0', '0', '联系方式', '', '联系方式', '', '', '', 'destoon', '1310696453', '4', '0', '0', 'about/contact.html', '', '');
INSERT INTO `nms_webpage` VALUES ('3', '1', '0', '0', '使用协议', '', '使用协议', '', '', '', 'destoon', '1310696460', '3', '0', '0', 'about/agreement.html', '', '');
INSERT INTO `nms_webpage` VALUES ('4', '1', '0', '0', '版权隐私', '', '版权隐私', '', '', '', 'destoon', '1310696468', '2', '0', '0', 'about/copyright.html', '', '');
INSERT INTO `nms_webpage` VALUES ('5', '1', '0', '0', '协议政策', '', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp; 废品回收，是由专业正规的废品回收站或公司进行收购。之后对废品的科学处理及分类达到再次循环利用的标准，废品回收这项服务对保护环境、节约能源和带动社会效益起着积极的作用。专业正规的废品公司不仅可以增加企业公司的效益更能对社会带来节约成本的效果，同时也能为环境作出贡献。&nbsp; &nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp; &nbsp;甲方: &nbsp;&nbsp;&nbsp; 乙方: &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;甲乙双方本着平等互利的原则，经友好协商，就乙方收购甲方可回收废品事宜，达成以下条款，以资双方遵照执行。 &nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;一、标的物 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">1、甲方同意将其单位管辖范围内的可回收废品出售给乙方，由乙方回收。 &nbsp;&nbsp;&nbsp; 2、可回收废品是指除正常商品外的经甲方确认为废品的一切可再生资源。乙方(□承担、□不承担)甲方单位管辖范围内的仅限于生活垃圾等百可回收废品清运工作。 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;二、合同价款及付款方式 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">1、乙方诚实经营，按照收购当时市场价收购废品。 &nbsp;&nbsp;&nbsp; 2、乙方每次回按商定付款方式废品所值价款。 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;三、合同期限 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">合同有效限自 年 月 日至 年 月 日。合同到期，乙方有优先签约条件。合同经双方授权代表签名并加盖公章成立，自签署日期起生效。 &nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;四、双方的权利和义务。 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">1、甲方应免费提供废品堆放场所。日常废品堆放应尽量集中，免费提供水电供应及甲方车辆人员进出之便。 &nbsp;&nbsp;&nbsp; 2、可回收废品由乙方派人捆扎、装运，费用及工资由乙方承担。 &nbsp;&nbsp;&nbsp; 3、乙方在甲方指定的场志及范围从事废品回收工作，不得在指定场所外走动、逗留或从事其他无关的活动。 &nbsp;&nbsp;&nbsp; 4、乙方人员遵守甲方单位管理制度，接受乙方的监督。 &nbsp;&nbsp;&nbsp; 5、在乙方收购过程中，乙方应尽量提供必要的协助工作。 &nbsp;&nbsp;&nbsp; 6、乙方应保证自身或转售的收购单位具有合法的收购资质和经营范围，且不会因收购行为或乙方之其他其他行为而导致任何司法或行政强制程序给乙方乱成任何损害。 &nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;五、其它事项 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">1、乙方工作人员进入甲方公司作业时，应衣着整齐，言行举止文明，行为规范，遵守本市场各项管理规定，服从甲方的管理。 &nbsp;&nbsp;&nbsp; 2、乙方不准在市场内有违法的行为，收取后及时离开。 &nbsp;&nbsp;&nbsp; 3、乙方有义务免费为甲方清理事前指定的垃圾，约定之外需要乙方清理的，按工作量大小，收取一定的费用，费用数额双方协商解决，如不能协商一致，乙方有权利拒绝。 &nbsp;&nbsp;&nbsp; 4、乙方作业人员进入市场前，甲方应严格确认身份，若因冒名顶替人员进入甲方公司造成乙方经济损失，乙方不负任何责任! &nbsp;&nbsp;&nbsp; 5、甲方人员、车辆出厂时，甲方相关负责人及保安人员应严格检查后方可放行。其间甲方公司若有丢失物品等事件乙方不负任何责任。但乙方有义务协助甲方和警务人员进行调查取证工作。 &nbsp;&nbsp;&nbsp; 6、凡因本合同引起的或与合同有关的任何争议，双方应首先友好协商解决，如在协商之后30日内不能解决争议的，则任何一方可向甲方所在地的人民法院提起诉讼。 &nbsp;&nbsp;&nbsp; 7.本合同一式两份。甲、乙双方各执一份，具有同等法律效力。 &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">甲方(公章):_________ 乙方(公章):_________ &nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">法定代表人(签字):_________ 法定代表人(签字):_________ &nbsp;&nbsp;&nbsp;</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft YaHei&quot;; font-size: 14px;\">&nbsp;_________年____月____日 _________年____月____日&nbsp; &nbsp;&nbsp;</span></p>', '', '', '', 'fulltimelink', '1595127390', '5', '1', '0', 'about/5.html', '', '');

-- ----------------------------
-- Table structure for nms_weixin_auto
-- ----------------------------
DROP TABLE IF EXISTS `nms_weixin_auto`;
CREATE TABLE `nms_weixin_auto` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  `reply` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信回复';

-- ----------------------------
-- Records of nms_weixin_auto
-- ----------------------------

-- ----------------------------
-- Table structure for nms_weixin_bind
-- ----------------------------
DROP TABLE IF EXISTS `nms_weixin_bind`;
CREATE TABLE `nms_weixin_bind` (
  `username` varchar(30) NOT NULL DEFAULT '',
  `sid` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信扫码绑定';

-- ----------------------------
-- Records of nms_weixin_bind
-- ----------------------------

-- ----------------------------
-- Table structure for nms_weixin_chat
-- ----------------------------
DROP TABLE IF EXISTS `nms_weixin_chat`;
CREATE TABLE `nms_weixin_chat` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `editor` varchar(30) NOT NULL,
  `openid` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL,
  `event` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `misc` mediumtext NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `openid` (`openid`),
  KEY `addtime` (`addtime`),
  KEY `event` (`event`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信消息';

-- ----------------------------
-- Records of nms_weixin_chat
-- ----------------------------

-- ----------------------------
-- Table structure for nms_weixin_user
-- ----------------------------
DROP TABLE IF EXISTS `nms_weixin_user`;
CREATE TABLE `nms_weixin_user` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `openid` varchar(255) NOT NULL DEFAULT '',
  `nickname` varchar(255) NOT NULL DEFAULT '',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `city` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  `headimgurl` varchar(255) NOT NULL,
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `visittime` int(10) unsigned NOT NULL DEFAULT '0',
  `credittime` int(10) unsigned NOT NULL DEFAULT '0',
  `subscribe` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `push` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`itemid`),
  UNIQUE KEY `openid` (`openid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信用户';

-- ----------------------------
-- Records of nms_weixin_user
-- ----------------------------

-- ----------------------------
-- Table structure for nms_xspreadmoney
-- ----------------------------
DROP TABLE IF EXISTS `nms_xspreadmoney`;
CREATE TABLE `nms_xspreadmoney` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ordernum` char(50) NOT NULL,
  `from` char(100) NOT NULL,
  `to` char(100) NOT NULL,
  `credit1` float(10,2) NOT NULL,
  `credit2` float(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=成功 2=失败',
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nms_xspreadmoney
-- ----------------------------
